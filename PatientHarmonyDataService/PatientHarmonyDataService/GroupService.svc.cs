﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PatientHarmony.Data;
using PatientHarmony.Data.Managers;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Context;

namespace PatientHarmonyDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class GroupService : IGroupService
    {
       
        private DataManager _ctx = DataManager.Instance;
       // private DataContext _ctxFunctions = new DataContext();
       

        public Group GetGroup(int value)
        {
            var group =  _ctx.Groups.Get(value);
            return group;
        }

        public List<Group> GetAllGroups()
        {
            var groups =  _ctx.Groups.GetAllGroups();
            return groups; 
        }

       
    }
}
