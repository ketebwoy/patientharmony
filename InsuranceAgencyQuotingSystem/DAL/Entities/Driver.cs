﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Driver
    {
        [Key]
        [ScaffoldColumn(false)]
        public int DriverId { get; set; }

        [Required]
        public bool CoApplicantFlag { get; set; }

        [Required]
        [StringLength(30)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(30)]
        public string LastName { get; set; }

        [Required]
        [StringLength(30)]
        public string MiddleInit { get; set; }

        [Required]
        [StringLength(30)]
        public string DateOfBirth { get; set; }

        [Required]
        [StringLength(30)]
        public string Gender { get; set; }

        [Required]
        public int AgeFirstLicensed { get; set; }

        [Required]
        [StringLength(30)]
        public string DriversLicenseNumber { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string Occupation { get; set; }

        [Required]
        [StringLength(30)]
        public string EducationLevel { get; set; }

        [Required]
        public bool CriminalRecord { get; set; }

        [Required]
        public bool CancelledNonRenew { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime CreatedDate { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime LastModifiedDate { get; set; }


    }

    public enum MaritalStatus
    {
        Single = 1,
        Married = 2,
        Divorced = 3, 
        Widowed = 4
    }

    public enum ApplicantRelationship
    {
        insured = 1, 
        spouse = 2,
        dependent = 3
    }

    public enum DriverStatus
    {
        active = 1,
        deactivated = 2

    }

    public enum LicenseStatus
    {
        active = 1,
        deactivated = 2
    }

    public enum Suspended
    {
        never = 1,
        less3years = 1
    }

    public enum Revoked
    {
        never = 1,
        less3years = 1
    }

    public enum DriversCourse
    {
        safeDriver = 1,
        none = 2
    }
}
