﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentScheduler;
using System.Threading;

using PatientHarmony.Data.Context;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Managers;
using System.Globalization;

namespace PatientHarmony.Taskmanager
{

    public class SMSEmailTaskRegistry : Registry
    {
        public SMSEmailTaskRegistry()
        {

            DefaultAllTasksAsNonReentrant();

            Schedule<CampaignTask>().ToRunNow().AndEvery(3).Minutes();
            Schedule<TriageFinishTask>().ToRunNow().AndEvery(1).Days().At(12, 00);
            Thread.Sleep(2000);
            Schedule<IncomingSMSTask>().ToRunNow().AndEvery(15).Seconds();

        }
    }

    public class CampaignTask : ITask
    {
        private static DataManager _ctxFunctions = PatientHarmony.Data.Managers.DataManager.Instance;

        public void Execute()
        {
            CampaignTaskExecute();
            Console.WriteLine("ITask Executed Campaign SMS on: " + DateTime.Now);

           ProcedureExecute();
            Console.WriteLine("ITask Executed Procedure SMS on: " + DateTime.Now);

            TriageExecute();
            Console.WriteLine("ITask Executed Triage SMS on: " + DateTime.Now);

            FollowUpExecute();
            Console.WriteLine("ITask Executed Follow Up SMS on: " + DateTime.Now);
        }

        public void CampaignTaskExecute()
        {
            var today = DateTime.Now;
            var newCampaignDefaultDate = Convert.ToDateTime("1753-01-01 00:00:00");

            using (var _ctx = new DataContext())
            {
                try
                {
                    var camapigns = _ctxFunctions.Campaigns.GetAllList().FindAll(p => p.DateSMSProcessed == newCampaignDefaultDate && p.SendDateTime.Date == today.Date);

                    foreach (var campaign in camapigns)
                    {
                        //get patients in campaign needed list for sendSms
                        var sendTime = campaign.SendDateTime - campaign.SendDateTime.Date;
                        System.Diagnostics.Debug.WriteLine("sendTime = {0}", sendTime.ToString());
                        var rightNow = today - today.Date;
                        System.Diagnostics.Debug.WriteLine("rightNow = {0}", rightNow.ToString());
                        var patientList = _ctxFunctions.ManyToManyRelationShips.GetAllContactsForCampaign(campaign.CampaignId);
                        var group = _ctxFunctions.Groups.Get(campaign.GroupID);


                        //Make sure sms sends at schedule time of day
                        if ((sendTime) >= (today.AddMinutes(-5) - today.Date) && (sendTime) <= (today.AddMinutes(5) - today.Date))
                        {
                            Console.WriteLine("Executing campaign " + campaign.CampaignTitle + " " + " for " + group.CompanyName);

                            campaign.VoiceEnabled = false; 
                            if (campaign.VoiceEnabled)
                            {
                                ///TODO: Make PhoneCall
                                _ctxFunctions.VoiceCalls.callContacts(patientList, group, null, campaign, null, null);
                                campaign.DateSMSProcessed = DateTime.Now;
                                _ctxFunctions.Campaigns.UpdateCampaign(campaign);
                                Console.WriteLine("campaign " + campaign.CampaignTitle + " processed at: " + campaign.DateSMSProcessed);
                                
                            }

                            else
                            {
                                var sms = new SMSOutgoing();
                                sms.Message = campaign.CampaignMessage;
                                sms.CampaignID = campaign.CampaignId;
                                sms.FromPhoneNumber = group.PhoneNumber;
                                _ctxFunctions.SMSmessages.sendSMS(sms, patientList);
                                System.Threading.Thread.Sleep(500);
                                campaign.DateSMSProcessed = DateTime.Now;
                                _ctxFunctions.Campaigns.UpdateCampaign(campaign);
                                Console.WriteLine("campaign " + campaign.CampaignTitle + " processed at: " + campaign.DateSMSProcessed);
                            }
                        }

                    }
                }

                catch (Exception ex)
                {
                    Console.WriteLine("An Error occured executing campaign scheduled task", ex);
                    //Console.WriteLine("An Error occured deleting campaign" + System.Environment.NewLine + ex);
                }

            }
        }

        public void ProcedureExecute()
        {
            var today = DateTime.Now;
            var newProcedureDefaultDate = Convert.ToDateTime("1753-01-01 00:00:00");

            using (var _ctx = new DataContext())
            {
                try
                {
                    var procedures = _ctxFunctions.Procedures.GetAllProcedureList().FindAll(p => p.StartDate.Date <= today && p.EndDate.Date >= today.Date && (p.DateSMSProcessed.Date == today.AddDays(-1).Date || p.DateSMSProcessed.Date == newProcedureDefaultDate.Date));

                    foreach (var procedure in procedures)
                    {
                        //get patients in procedure needed list for sendSms
                        var sendTime = procedure.SendDateTime - procedure.SendDateTime.Date;
                        System.Diagnostics.Debug.WriteLine("sendTime = {0}", sendTime.ToString());
                        var rightNow = today - today.Date;
                        System.Diagnostics.Debug.WriteLine("rightNow = {0}", rightNow.ToString());
                        var patientList = _ctxFunctions.ManyToManyRelationShips.GetAllContactsForCampaign(procedure.CampaignID);

                        var campaign = _ctxFunctions.Campaigns.GetAllList().First(p => p.CampaignId == procedure.CampaignID);
                        var group = _ctxFunctions.Groups.Get(campaign.GroupID);


                        //Make sure sms sends at schedule time of day
                        if ((sendTime) >= (today.AddMinutes(-5) - today.Date) && (sendTime) <= (today.AddMinutes(5) - today.Date))
                        {
                            Console.WriteLine("Executing Procedure " + procedure.ProcedureTitle + " " + " for " + group.CompanyName);

                            procedure.VoiceEnabled = false; 
                            if (procedure.VoiceEnabled)
                            {
                                _ctxFunctions.VoiceCalls.callContacts(patientList, group, null, null, procedure, null); 
                                procedure.DateSMSProcessed = DateTime.Now;
                                _ctxFunctions.Procedures.UpdateProcedure(procedure);
                            }
                            else
                            {
                               var sms = new SMSOutgoing();
                               sms.Message = procedure.ProcedureMessage;
                               sms.CampaignID = procedure.CampaignID;
                               sms.FromPhoneNumber = group.PhoneNumber;
                               _ctxFunctions.SMSmessages.sendSMS(sms, patientList);
                               System.Threading.Thread.Sleep(500);
                               procedure.DateSMSProcessed = DateTime.Now;
                               _ctxFunctions.Procedures.UpdateProcedure(procedure);
                            }
                      

                        }

                    }
                }

                catch (Exception ex)
                {
                    Console.WriteLine("An Error occured executing Procedure task", ex);
                    //Console.WriteLine("An Error occured executing Procedure task" + System.Environment.NewLine + ex);
                    return;
                }

            }
        }

        public void TriageExecute()
        {
            var today = DateTime.Now;
            var newTriageDefaultDate = Convert.ToDateTime("1753-01-01 00:00:00");

            try
            {
                using (var _ctx = new DataContext())
                {
                    List<Triage> Triages = _ctxFunctions.Triages.GetAllTriage().FindAll(p => p.StartDate.Date <= today.Date && p.EndDate.Date >= today.Date && p.EndMarker == false);


                    foreach (var Triage in Triages)
                    {
                        Contact patient = _ctxFunctions.Contacts.Get(Triage.ContactID);

                       //need list for sendSms
                        var patientList = new List<Contact>();
                        patientList.Add(patient);

                        var group = _ctxFunctions.Groups.Get(Triage.GroupID);
                        var sendTime = Triage.SendDateTime - Triage.SendDateTime.Date;

                        //Make sure Triage has not been processed today && frequency rules apply
                        if ((Triage.LastDateProcessed <= Triage.EndDate && Triage.LastDateProcessed.Date < today.Date || Triage.LastDateProcessed == newTriageDefaultDate) && AbleToSend(Triage))
                        {
                            var contact = _ctxFunctions.Contacts.Get(Triage.ContactID);
                            int fcount = contact.FirstName.Length + contact.LastName.Length; 
                            var pHash = System.Web.Security.Membership.GeneratePassword(fcount, 0);

                            var triageInfo = Triage.Title + " Triage Information " + Environment.NewLine + "Title: " + Triage.Title
                                + Environment.NewLine + "Favorable Response: " + Triage.GoodResponseMessage
                                + Environment.NewLine + "UnFavorable Response: " + Triage.BadResponseMessage
                                + Environment.NewLine + "To Patient: " + pHash;

                            Console.WriteLine(triageInfo);
                            //Make sure sms sends at Triage time of day
                            if ((sendTime) >= (today.AddMinutes(-5) - today.Date) && (sendTime) <= (today.AddMinutes(5) - today.Date) )
                            {
                                Console.WriteLine("Executing Triage " + Triage.Title + " " + " for " + group.CompanyName);

                                Triage.VoiceEnabled = false; 
                                if (Triage.VoiceEnabled)
                                {
                                    _ctxFunctions.VoiceCalls.callContacts(patientList, group, Triage, null, null, null);
                                    Triage.LastDateProcessed = DateTime.Now;
                                    _ctxFunctions.Triages.Update(Triage);
                                    Console.WriteLine("Triage " + Triage.Title + " processed at: " + Triage.LastDateProcessed);
                                }
                                else
                                {
                                    var sms = new SMSOutgoing();
                                    sms.Message = Triage.TriageMessage;
                                    sms.TriageID = Triage.TriageId;
                                    sms.FromPhoneNumber = group.PhoneNumber;
                                    _ctxFunctions.SMSmessages.sendSMS(sms, patientList);
                                    Console.WriteLine("Triage Message Sent");
                                    System.Threading.Thread.Sleep(500);
                                    Triage.LastDateProcessed = DateTime.Now;
                                    _ctxFunctions.Triages.Update(Triage);
                                    Console.WriteLine("Triage " + Triage.Title + " processed at: " + Triage.LastDateProcessed);
                                }
                            }

                        }
                    }


                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("An Error occured executing Triage task" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void FollowUpExecute()
        {
            var today = DateTime.Now;
            var newFollowUpDefaultDate = Convert.ToDateTime("1753-01-01 00:00:00");

            try
            {
                using (var _ctx = new DataContext())
                {
                    List<TriageFollowUp> temp = _ctxFunctions.TriageFollowUps.GetAllTriageFollowUpList();
                        var tfu = temp.FindAll(p => p.StartDate.Date <= today.Date && p.EndDate.Date >= today.Date && p.EndMarker == false && p.LastDateExecuted.Date < today.Date).OrderBy(p => p.CreatedDate); 

                    ///TODO: get followups previous to this entry
                    //var previous followup  = _ctx.getthem

                    foreach (var fu in tfu)
                    {
                        Contact patient = _ctxFunctions.Contacts.Get(fu.ContactID);

                        //need list for sendSms
                        var patientList = new List<Contact>();
                        patientList.Add(patient);

                        var triage = _ctxFunctions.Triages.GetTriage(fu.TriageID);
                        var group = _ctxFunctions.Groups.Get(triage.GroupID);
                        var sendTime = fu.SendDateTime - fu.SendDateTime.Date;
                        var ableToSend = allowSendBasedonDays(triage.SendDays); 

                        //Make sure Followup has not been processed today 
                        if ((fu.ProcessedDate.Date <= today.Date) || (fu.ProcessedDate == newFollowUpDefaultDate) && ableToSend)
                        {
                            fu.VoiceEnabled = false; 
                            if (fu.VoiceEnabled)
                            {
                                //Make sure sms sends at Triage time of day
                                if ((sendTime) >= (today.AddMinutes(-5) - today.Date) && (sendTime) <= (today.AddMinutes(5) - today.Date))
                                {
                                    var contact = patient;
                                    int fcount = contact.FirstName.Length + contact.LastName.Length;
                                    var pHash = System.Web.Security.Membership.GeneratePassword(fcount, 0);

                                    var followUpInfo = fu.Title + " Triage FOllowUp Information " + Environment.NewLine + "Title: " + fu.Title
                                        + Environment.NewLine + "Favorable Response: " + fu.GoodResponseMessage
                                        + Environment.NewLine + "UnFavorable Response: " + fu.BadResponseMessage
                                        + Environment.NewLine + "To Patient: " + pHash;

                                    Console.WriteLine(followUpInfo);

                                    _ctxFunctions.VoiceCalls.callContacts(patientList, group, null, null, null, fu);
                                    fu.LastDateExecuted = today;
                                    _ctxFunctions.TriageFollowUps.UpdateFollowUp(fu);
                                    Console.WriteLine("Triage FollowUp: " + fu.Title + " processed at: " + fu.ProcessedDate);
                                }
                            }

                            else
                            {
                                ///TODO MARK previous follow up as answered 
                                //Get all followups that have been run earlier but have not been answered
                                var unansweredFollowUps = tfu.Where(p => p.ContactID == patient.ContactId && p.ProcessedDate.Date < today.Date && p.ProcessedDate != newFollowUpDefaultDate).ToList<TriageFollowUp>();
                                var noAnswer = '$';

                                if (unansweredFollowUps.Count > 0)
                                {

                                    foreach (var uafu in unansweredFollowUps)
                                    {
                                        uafu.ProcessedDate = today;

                                        if (!String.IsNullOrEmpty(fu.ContactResponse))
                                        {
                                            uafu.ContactResponse = fu.ContactResponse + noAnswer;
                                        }
                                        else
                                        {
                                            uafu.ContactResponse = noAnswer.ToString();
                                        }

                                        _ctxFunctions.TriageFollowUps.UpdateFollowUp(uafu);

                                    }

                                    //send warning sms
                                    var sms = new SMSOutgoing();
                                    sms.Message = "Sorry, but we went ahead and marked the last question as No Response. In the future, please follow up at your earliest convenience.";
                                    sms.TriageID = triage.TriageId;
                                    sms.FromPhoneNumber = group.PhoneNumber;
                                    _ctxFunctions.SMSmessages.sendSMS(sms, patientList);
                                    System.Threading.Thread.Sleep(1000);

                                }

                                //Continue as normal 

                                var contact = patient;
                                int fcount = contact.FirstName.Length + contact.LastName.Length;
                                var pHash = System.Web.Security.Membership.GeneratePassword(fcount, 0);

                                var followUpInfo = fu.Title + " Triage FOllowUp Information " + Environment.NewLine + "Title: " + fu.Title
                                    + Environment.NewLine + "Favorable Response: " + fu.GoodResponseMessage
                                    + Environment.NewLine + "UnFavorable Response: " + fu.BadResponseMessage
                                    + Environment.NewLine + "To Patient: " + pHash;

                                Console.WriteLine(followUpInfo);

                                //Make sure sms sends at Triage time of day
                                if ((sendTime) >= (today.AddMinutes(-5) - today.Date) && (sendTime) <= (today.AddMinutes(5) - today.Date))
                                {
                                    Console.WriteLine("Executing Triage Follow Up " + fu.Title + " " + " for " + group.CompanyName);
                                    var sms = new SMSOutgoing();
                                    sms.Message = fu.TriageFollowUpMessage;
                                    sms.TriageID = fu.TriageID;
                                    sms.FromPhoneNumber = group.PhoneNumber;
                                    _ctxFunctions.SMSmessages.sendSMS(sms, patientList);
                                    Console.WriteLine("Triage Follow Up Message Sent");
                                    System.Threading.Thread.Sleep(500);
                                    fu.LastDateExecuted = today;
                                    _ctxFunctions.TriageFollowUps.UpdateFollowUp(fu);
                                    Console.WriteLine("Triage FollowUp: " + fu.Title + " processed at: " + fu.ProcessedDate);
                                }
                            }

                        }
                    }


                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("An Error occured executing Triage Follow Up task" + System.Environment.NewLine + ex);
                return;
            }
        }

        private static int WeekOfYearISO8601(DateTime date)
        {
            var day = (int)CultureInfo.CurrentCulture.Calendar.GetDayOfWeek(date);
            return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(date.AddDays(4 - (day == 0 ? 7 : day)), CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday);
        }

        private bool allowSendBasedonDays(string sendDays)
        {
            var daylist = new List<DayofWeek>();
            var today = DateTime.Today.DayOfWeek.ToString(); 
            var allow = false; 
            
            var sendArray = sendDays.ToCharArray();

            if (sendArray.Contains('0') && today.ToString().Equals(DayofWeek.Sunday.ToString()))
            {
                return true; 
            }

            if (sendArray.Contains('1') && today.ToString().Equals(DayofWeek.Monday.ToString()))
            {
                return true;
            }
            if (sendArray.Contains('2') && today.ToString().Equals(DayofWeek.Tuesday.ToString()))
            {
                return true;
            }
            if (sendArray.Contains('3') && today.ToString().Equals(DayofWeek.Wednesday.ToString()))
            {
                return true; 
            }
            if (sendArray.Contains('4') && today.ToString().Equals(DayofWeek.Thursday.ToString()))
            {
                return true;
            }
            if (sendArray.Contains('5') && today.ToString().Equals(DayofWeek.Friday.ToString()))
            {
                return true;  
            }

            if (sendArray.Contains('6') && today.ToString().Equals(DayofWeek.Saturday.ToString()))
            {
                return true;  
            }


            return allow;
        }

        private bool AbleToSend(Triage triage)
        {
            var today = DateTime.Now;
            var minSendDate = DateTime.Today.AddDays(-14);
            var currentWeek = WeekOfYearISO8601(DateTime.Today);
            var startweek = WeekOfYearISO8601(triage.StartDate);
            var minimumWeek = WeekOfYearISO8601(minSendDate);
            var lastDateProcessedWeek = WeekOfYearISO8601(triage.LastDateProcessed);
            

            var passed = false;

            if (allowSendBasedonDays(triage.SendDays))
            {

                switch (triage.Frequency)
                {
                    
                    case TriageFrequencyType.BiWeekly:
                  

                        var weeksAbleToSend = currentWeek - startweek; 

                        if ((weeksAbleToSend % 2 == 0) || triage.LastDateProcessed.Year == 1753 )
                        {
                            passed = true;
                        }
                        break;


                    case TriageFrequencyType.Weekly:
                        var weeklyAbleToSend = currentWeek - lastDateProcessedWeek; 

                        if ((weeklyAbleToSend <= 1) || triage.LastDateProcessed.Year == 1753)
                        {
                            passed = true;
                        }
                        break;

                    case TriageFrequencyType.Daily:
                        passed = true;
                        break;

                }

            }
            return passed;
        }
            
        }

    }

    public class TriageFinishTask : ITask
    {
        private static DataManager _ctxFunctions = DataManager.Instance;

        public void Execute()
        {
            TriageLastDayExecute();
            Console.WriteLine("ITask Executed Triage Finish: " + DateTime.Now);
        }

        public void TriageLastDayExecute()
        {
            var today = DateTime.Now;
            var yesterday = DateTime.Today.AddDays(-1);

            try
            {
                using (var _ctx = new DataContext())
                {
                    //grab Triages that finished yesterday
                    var Triages = _ctxFunctions.Triages.GetAllTriage().FindAll(p => p.EndDate.Date <= yesterday.Date && p.EndMarker == false);

                    foreach (var Triage in Triages)
                    {

                        var patient = _ctxFunctions.Contacts.GetAllList().FirstOrDefault(p => p.ContactId == Triage.ContactID);
                        var group = _ctxFunctions.Groups.Get(Triage.GroupID);

                        sendTriageFinishEmail(patient, group, Triage.HealthTracker);
                        Triage.EndMarker = true;
                        _ctxFunctions.Triages.Update(Triage);
                        Console.WriteLine("Triage " + Triage.Title + " Last Day execution processed at: " + Triage.LastDateProcessed);

                    }

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("An Error occured executing Last Day Triage task" + System.Environment.NewLine + ex);
                return;
            }
        }

        /*form and send email to doctor notifying him/her patient feels worse */
        private void sendTriageFinishEmail(Contact patient, Group group, string patientHealthRecord)
        {
            
            var newEmail = new Email();
            List<string> emailList = new List<string>();

   
            string emailTemplate = @"
                                                                               
                                        Patient ##PATIENTNAME## has reached the end of triage tracking. 

                                        Through out the triage they responded via SMS with a scale of 1 - 10 based on how they felt 1 being worst health 10 being best health. 
                                        
                                        Their responses in sorted by receiving date =: ##HEALTHRECORD##
                                       
                                        Their contact info is below: 

                                        ##PATIENTNAME##
                                        ##EMAILADDRESS##
                                        ##PHONENUMBER##

                                        Best regards, 
                                        ##ADMINNAME##";

            if (patientHealthRecord != null)
            {


                string emailBody = emailTemplate
                    .Replace("##ClientName##", group.CompanyName)
                    .Replace("##PATIENTNAME##", patient.FirstName + " " + patient.LastName)
                    .Replace("##EMAILADDRESS##", patient.EmailAddress)
                    .Replace("##PHONENUMBER##", patient.PhoneNumber)
                    .Replace("##HEALTHRECORD##", printHealthRecord(patientHealthRecord.ToCharArray()))
                    .Replace("##ADMINNAME##", "PatientHarmony Administrator");

                emailList.Add(group.Email);
                emailList.Add(patient.InsurerEmailAddress);
                newEmail.ToEmail = group.Email;
                newEmail.Subject = "Patient Health Status Notification";
                newEmail.Message = emailBody;
                newEmail.GroupID = group.GroupId;
            }

            else
            {
                string emailBody = emailTemplate
                   .Replace("##ClientName##", group.CompanyName)
                   .Replace("##PATIENTNAME##", patient.FirstName + " " + patient.LastName)
                   .Replace("##EMAILADDRESS##", patient.EmailAddress)
                   .Replace("##PHONENUMBER##", patient.PhoneNumber)
                   .Replace("##HEALTHRECORD##", "Patient " + patient.FirstName + " " + patient.LastName + " did not respond to Triage")
                   .Replace("##ADMINNAME##", "PatientHarmony Administrator");

                emailList.Add(group.Email);
                emailList.Add(patient.InsurerEmailAddress);
                newEmail.ToEmail = group.Email;
                newEmail.Subject = "Patient Health Status Notification";
                newEmail.Message = emailBody;
                newEmail.GroupID = group.GroupId;
            }

            _ctxFunctions.EmailMessages.SendEmail(newEmail, emailList);
        }

        private string printHealthRecord(char[] healthRecord)
        {
            var sb = new StringBuilder();

            foreach (var stat in healthRecord)
            {
                var recordData = string.Empty;
                if (stat.Equals('*'))
                {
                    recordData = "10";
                }
                else
                { recordData = stat.ToString(); }

                sb.Append("[" + recordData + "] ");

            }
            return sb.ToString();
        }
    }

    public class IncomingSMSTask : ITask
    {
        private static DataManager _ctxFunctions = DataManager.Instance;

        public void Execute()
        {
            var contact = _ctxFunctions.Contacts.GetAllList().First(p => p.FirstName == "Keith");
            
        //_ctxFunctions.VoiceCalls.callTest(contact, group);             


        #region working commented code

        Console.WriteLine("ITask Checking for Incoming SMS: " + DateTime.Now);

        var groups = _ctxFunctions.Groups.GetAllGroups();
        var defaultDate = Convert.ToDateTime("1753-01-01 00:00:00");
        var incomingtexts = _ctxFunctions.SMSmessages.GetAllincoming().Where(p => p.CreatedDate.Date == DateTime.Now.Date && p.processed == false);

        foreach (var sms in incomingtexts)
        {
            var patient = _ctxFunctions.Contacts.GetAllList().First(p => p.PhoneNumber.Equals(sms.FromPhoneNumber));
            var group = _ctxFunctions.Groups.GetAllGroups().First(p => p.PhoneNumber.Equals(sms.ToPhoneNumber));

            //need a list to send sms
            List<Contact> patients = new List<Contact>();
            patients.Add(patient);


            string triageResponseText = _ctxFunctions.SMSmessages.triageProcessing(sms, false);
            string followUpResponse = _ctxFunctions.SMSmessages.followUpProcessing(sms);

            var followUplist = _ctxFunctions.TriageFollowUps.GetAllTriageFollowUpList();

            var triageList = _ctxFunctions.Triages.GetAllTriage().Where(p => (p.ContactID == patient.ContactId) && (p.EndDate.Date >= DateTime.Now.Date) && (p.GroupID == group.GroupId) && (p.EndMarker == false)); ;

            var triage = triageList.FirstOrDefault(p => (p.LastReplyDate == defaultDate) || (p.LastReplyDate.Date < DateTime.Now.Date));

            var followUp = followUplist.FirstOrDefault(p => p.ContactID == patient.ContactId && p.EndMarker == false);


            if (_ctxFunctions.SMSmessages.smsMeantForTriage(sms))
            {
                //if autoreply is triage type then process triage
                #region TriageReply
                if (triage == null)
                {
                    System.Threading.Thread.Sleep(2000);

                    if (triageResponseText != null)
                    {

                        SMSOutgoing reply = new SMSOutgoing();
                        reply.ToPhoneNumber = sms.FromPhoneNumber;
                        reply.FromPhoneNumber = sms.ToPhoneNumber;
                        reply.Message = triageResponseText;
                        _ctxFunctions.SMSmessages.sendSMS(reply, patients);
                        System.Threading.Thread.Sleep(2000);
                    }
                }
                else //append triageID and Execute Triage Response System
                {
                    triageResponseText = _ctxFunctions.SMSmessages.triageProcessing(sms, true);
                    SMSOutgoing reply = new SMSOutgoing();
                    reply.TriageID = triage.TriageId;
                    reply.ToPhoneNumber = sms.FromPhoneNumber;
                    reply.FromPhoneNumber = sms.ToPhoneNumber;
                    reply.Message = triageResponseText;
                    _ctxFunctions.SMSmessages.sendSMS(reply, patients);
                    System.Threading.Thread.Sleep(2000);
                }
                #endregion
            }
            else
            {

                #region FollowUpReply

                if (followUp == null)
                {
                    System.Threading.Thread.Sleep(2000);

                    if (followUpResponse != null)
                    {

                        SMSOutgoing reply = new SMSOutgoing();
                        reply.ToPhoneNumber = sms.FromPhoneNumber;
                        reply.FromPhoneNumber = sms.ToPhoneNumber;
                        reply.Message = followUpResponse;
                        _ctxFunctions.SMSmessages.sendSMS(reply, patients);
                        System.Threading.Thread.Sleep(2000);
                    }
                }
                else //append triageID
                {
                    SMSOutgoing reply = new SMSOutgoing();
                    reply.TriageID = followUp.TriageID;
                    reply.ToPhoneNumber = sms.FromPhoneNumber;
                    reply.FromPhoneNumber = sms.ToPhoneNumber;
                    reply.Message = followUpResponse;
                    _ctxFunctions.SMSmessages.sendSMS(reply, patients);

                    //update followp as processed and input contact answer 
                    //Mark as finished 
                    if (followUpResponse.ToLower() == "please reply with 'yes' or 'no' ")
                    {

                    }
                    else
                    {
                        if (followUp.EndDate.Date == DateTime.Now.Date)
                        {
                            followUp.EndMarker = true;
                        }
                        else
                        {
                            followUp.EndMarker = false;
                        }

                        followUp.ProcessedDate = DateTime.Now;
                        followUp.ContactResponse = Convert.ToString(_ctxFunctions.SMSmessages.extractFollowUpRating(sms));
                    }

                    _ctxFunctions.TriageFollowUps.UpdateFollowUp(followUp);
                    System.Threading.Thread.Sleep(2000);
                }

                #endregion

            }


        }
        #endregion
    }



    }


   