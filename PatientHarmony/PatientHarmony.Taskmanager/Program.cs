﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentScheduler;


namespace PatientHarmony.Taskmanager
{
    public class Program
    {
        static void Main(string[] args)
        {
            
            
            Console.WriteLine("Starting PatientHarmony Task manager...");

            FluentScheduler.TaskManager.TaskStart += (Triage, e) => Console.WriteLine(Triage.Name + " Started: " + Triage.StartTime);
            FluentScheduler.TaskManager.TaskEnd += (Triage, e) => Console.WriteLine(Triage.Name + " Ended.\n\tStarted: " + Triage.StartTime + "\n\tDuration: " + Triage.Duration + "\n\tNext run: " + Triage.NextRunTime);

           
            FluentScheduler.TaskManager.Initialize(new SMSEmailTaskRegistry());
            Console.WriteLine("Done initializing...");

            Console.ReadLine(); 
        }
    }
}
