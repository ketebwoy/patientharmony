// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'angularjs-crypto', 'angularMoment', 'ui.calendar', 'chart.js', '500tech.simple-calendar', 'ui.router', 'services'])

.run(function ($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    $rootScope.base64Key = "16rdKQfqN3L4TY7YktgxBw==";
    $rootScope.devicePhoneNumber = '8080808080808080'; //'15618460337';
    $rootScope.deviceId = '8080808080808080'; //'3a20d2b9-6042-4c46-ace2-bc1d4ee555fb';
    
  });
})

.config(function ($compileProvider, $stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: "/search",
    views: {
      'menuContent': {
        templateUrl: "templates/search.html"
      }
    }
  })

  .state('app.browse', {
    url: "/browse",
    views: {
      'menuContent': {
        templateUrl: "templates/browse.html"
      }
    }
  })

  .state('app.playlists', {
      url: "/playlists",
      views: {
        'menuContent': {
          templateUrl: "templates/playlists.html",
          controller: 'PlaylistsCtrl'
        }
      }
  })

  .state('app.calendar', {
      url: "/calendar",
      views: {
          'menuContent': {
              templateUrl: "templates/calendar.html",
              controller: 'PlaylistsCtrl'
          }
      }
  })

  .state('app.progress', {
      url: "/progress",
      views: {
          'menuContent': {
              templateUrl: "templates/progress.html",
              controller: 'PlaylistsCtrl'
          }
      }
  })

  .state('app.settings', {
      url: "/settings",
      views: {
          'menuContent': {
              templateUrl: "templates/settings.html",
              controller: 'PlaylistsCtrl'
          }
      }
  })

    
  .state('app.triageResponse', {
      url: "/triageResponse",
      views: {
          'menuContent': {
              templateUrl: "templates/triageResponse.html",
              controller: "TriageCtrl"
          }
      }
  })


  .state('app.single', {
    url: "/playlists/:playlistId",
    views: {
      'menuContent': {
        templateUrl: "templates/playlist.html",
        controller: 'PlaylistCtrl'
      }
    }
  });

    
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/playlists');
});
