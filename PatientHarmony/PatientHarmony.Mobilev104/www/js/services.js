angular.module('services', ['ngResource'], function ($provide) {

    $provide.factory('EncData', function ($resource) {
        return $resource('test', {}, {
            get: { method: 'GET', isArray: false, crypt: true },
            post: {
                method: 'POST',
                fullcryptbody: true,
                decryptbody: true,
                transformRequest: function (data, headers) {
                    var test2 = data; 
                    return '"' + data + '"';
                },
                transformResponse: function (data, headers) {
                    var test3 = data; 
                    return data.slice(1, -1);
                }
            }
        });
    });
});