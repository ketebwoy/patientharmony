angular.module('starter.controllers', [])



.factory('dataFactory', ['$http', function ($http) {

    var urlBase = 'http://localhost/PatientHarmony.Web/Mobile/';
    var dataFactory = {};
    
        dataFactory.calEventService = function () {
            return $http.get(urlBase + 'GetCalendarEventsJson');
        };

        dataFactory.calService = function () {
            return $http.get(urlBase + 'GetCalendarJson');
        };
      
        dataFactory.triages = function () {
            return $http.get(urlBase + 'GetTriagesJson');
        };

        dataFactory.groups = function () {
            return $http.get(urlBase + 'GetGroupsJson');
        };

        dataFactory.contacts = function () {
            return $http.get(urlBase + 'GetContactsJson')
        };

        dataFactory.sendEncrypted = function (encryptedData, encryptedPassword) {
            var test = $http.post(urlBase + 'DecryptHttpTest', { cipher: encryptedData.toString(), passPhrase: encryptedPassword.toString() });
            return test; 
            
        };

          
        return dataFactory;
        }])

.factory('CacheService', function($cacheFactory) {
    return $cacheFactory('CacheService')

})

.factory('triageCache', function(CacheService) {
        return {
            getValue: function(key) {
                var valuedata = CacheService.get(key);

                if(valuedata) {
                    return valuedata;
                }

                return null;
            },
            setValue: function(key, value) {
                CacheService.put(key, value);
            },
            clearCalue: function(key) {
                CacheService.put(key, '');
            }
        };
})
        
.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function ($scope, $rootScope, $ionicPopup, $filter, $http, $state, dataFactory, triageCache, EncData) {

    getData();

    
    function getData() {
        dataFactory.calService().success(function (calendar) {
            $scope.calService = calendar;
        }).error(function (error) {
            $scope.status = 'Unable to load customer data: ' + error.message;
        });

        //dataFactory.calEventService().success(function (events) {
        //    $scope.calEventService = events;
        //}).error(function (error) {
        //    $scope.status = 'Unable to load customer data: ' + error.message;
        //});

        dataFactory.groups().success(function (groups) {
            $scope.groupData = groups;
            
        }).error(function (error) {
            $scope.status = 'Unable to load customer data: ' + error.message;
        });

        dataFactory.contacts().success(function (contacts) {
            $scope.contactData = contacts;
        }).error(function (error) {
            $scope.status = 'Unable to load customer data: ' + error.message;
        });
      
    };

        dataFactory.triages().success(function (triages) {
        $scope.triageData = triages;
        checkForTriage();
        }).error(function (error) {
            $scope.status = 'Unable to load customer data: ' + error.message;
        });
  
  $scope.calendarView = 'month';
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
   
  //var one = -1;
  //var two = -2;

  var today = new moment();
  var yesterday = new moment(new Date(new Date().setDate(new Date().getDate() - 1))).format('YYYY-MM-DD');
  var dby = new moment(new Date(new Date().setDate(new Date().getDate() - 2))).format('YYYY-MM-DD');


  $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
  $scope.series = ['Series A', 'Series B'];
  $scope.data = [
      [65, 59, 80, 81, 56, 55, 40],
      [28, 48, 40, 19, 86, 27, 90]
  ];

  $scope.triages = [
      { triageId: '1', triageMessage: 'On a scale of 1-10, with 10 being perfect. How does your leg feel?' },
      { triageId: '2', triageMessage: 'have you done your physical therapy stretches' },
      { triageId: '3', triageMessage: 'How many times did you wake up last night to urinate?' }];


  $scope.triageResponses = [
      { triageId: '1', date: '9/1/2015', response: '3' },
      { triageId: '1', date: '9/2/2015', response: '4' },
      { triageId: '1', date: '9/3/2015', response: '3' },
      { triageId: '1', date: '9/4/2015', response: '2' },
      { triageId: '1', date: '9/5/2015', response: '7' },
      { triageId: '1', date: '9/6/2015', response: '6' },
      { triageId: '2', date: '9/15/2015', response: 'yes' },
      { triageId: '2', date: '9/16/2015', response: 'yes' },
      { triageId: '2', date: '9/17/2015', response: 'no' },
      { triageId: '2', date: '9/18/2015', response: 'yes' },
      { triageId: '3', date: '9/20/2015', response: '2' },
      { triageId: '3', date: '9/21/2015', response: '2' },
      { triageId: '3', date: '9/22/2015', response: '3' },
      { triageId: '3', date: '9/23/2015', response: '7' },
      { triageId: '3', date: '9/24/2015', response: '6' },
      { triageId: '3', date: '9/25/2015', response: '5' },
      
  ];

  
  $scope.triageFollowup = [
  {fuId: '1', fuMessage: 'test follow up Message 1'},
  {fuId: '2', fuMessage: 'test follow up Message 2'},
  {fuId: '3', fuMessage: 'test follow up Message 3'}];

  $scope.eventsCalendar = [
 { response: 'true', id: 1, date: today },
 { response: 'false', id: 2, date: yesterday },
 { response: 'no response', id: 3, date: dby },
 
  ];


  var date = new Date(); var d = date.getDate(); var m = date.getMonth(); var y = date.getFullYear();
  $scope.equalsTracker = 0;

  $scope.calEvents = {
      //color: '#00ff00',
      events: [
           { color: '#00ff00', type: 'party', title: 'Yes', start: new Date(y, m, 10, 12, 0), end: new Date(y, m, 10, 14, 0), allDay: true },
         { color: '#00ff00', type: 'party', title: 'Yes', start: new Date(y, m, 11, 12, 0), end: new Date(y, m, 11, 14, 0), allDay: true },
          { color: '#0000ff', type: 'party', title: 'No', start: new Date(y, m, 12, 13, 0), end: new Date(y, m, 12, 14, 0), allDay: true },
          { color: '#0000ff', type: 'party', title: 'No', start: new Date(y, m, 13, 13, 0), end: new Date(y, m, 13, 14, 0), allDay: true },
         { color: '#f00', type: 'party', title: '???', start: new Date(y, m, 14), end: new Date(y, m, 14) },
      { color: '#f00', type: 'party', title: '???', start: new Date(y, m, 15), end: new Date(y, m, 15) }
      ]
  };

  $scope.calEvents2 = [
       { color: '#00ff00', type: 'test', title: '8', start: new Date(y, m, 20, 12, 0), end: new Date(y, m, 20, 14, 0), allDay: true },
        { color: '#00ff00', type: 'test', title: '7', start: new Date(y, m, 21, 12, 0), end: new Date(y, m, 21, 14, 0), allDay: true },
       { color: '#0000ff', type: 'test', title: '5', start: new Date(y, m, 22), end: new Date(y, m, 22)},
       { color: '#f00', type: 'test', title: '???', start: new Date(y, m, 23), end: new Date(y, m, 23) },
       { color: '#f00', type: 'test', title: '???', start: new Date(y, m, 24), end: new Date(y, m, 24) }
  ];

  $scope.addEvent = function () {
      $scope.calEvents.events.push({
          title: 'Click for Google ' + $scope.calEvents.events.length,
          start: new Date(y, m, d),
          end: new Date(y, m, d),
          url: 'http://google.com/'
      });
  };

  $scope.addEvent2 = function () {
      $scope.calEvents2.push({
          className: 'green',
          title: 'Click for Google ' + $scope.calEvents.events.length,
          start: new Date(y, m, d + 1),
          end: new Date(y, m, d + 1),
          url: 'http://google.com/'
      });
  };

  $scope.changeView = function (view) {
      $scope.calendar.fullCalendar('changeView', view);
  };

  $scope.eventSources = [$scope.calEvents2, $scope.calEvents];

    //with this you can handle the events that generated by clicking the day(empty spot) in the calendar
  $scope.dayClick = function( date, allDay, jsEvent, view ){
      $scope.$apply(function(){
          $scope.alertMessage = ('Day Clicked ' + date);
      });
  };
 
  $scope.uiConfig = {
      fullCalendar: {
          height: 650,
          header: {
              left: 'month basicWeek basicDay agendaWeek agendaDay',
              center: 'title',
              right: 'today prev,next'
          },

          editable: true,
          eventDrop: $scope.addOnDrop,
          eventResize: $scope.addOnResize,
          selectable: true,
          dayClick: $scope.dayClick
                   
      }
  };

  $scope.startMonth = new Date(2015, 10 - 1);
  $scope.nextMonth = function () {
  $scope.startMonth = new Date(2015, $scope.startMonth.getMonth() + 1);
  };
  $scope.prevMonth = function () {
      $scope.startMonth = new Date(2015, $scope.startMonth.getMonth() - 1);
  };

  $scope.alertDay = function (day) {
      //   alert('You clicked ' + $scope.startMonth.getDate());

      var alertPopup = $ionicPopup.alert({
          title: 'Don\'t eat that!',
          template: 'It might taste good'
      });
      alertPopup.then(function (res) {
          console.log('Thank you for not eating my delicious ice cream cone');
      });
  };

  $scope.calendarOptions = {
      defaultDate: "2015-10-10",
      minDate: new Date(),
      maxDate: new Date([2020, 12, 31]),
      dayNamesLength: 1, // How to display weekdays (1 for "M", 2 for "Mo", 3 for "Mon"; 9 will show full day names; default is 1)
      eventClick: $scope.alertDay /*,
      dateClick: $scope.dateClick */
  };

  $scope.events = [
      { title: 'TRUE', date: new Date([2015, 10, 22]) },
      { title: 'FALSE', date: new Date([2015, 10, 27]) },
      { title: 'No Resp', date: new Date([2015, 10, 23]) }
  ];

  
  checkForTriage();
  function checkForTriage() {
      var triagestuff = dataFactory.triages().success(function (triages) {
          $scope.triageData = triages;
          
      }).error(function (error) {
          $scope.status = 'Unable to load customer data: ' + error.message;
      });


      var triageJson = JSON.parse($scope.triageData);


      $scope.data = { response: 0 };
      
      var promptPopup = $ionicPopup.prompt({

          title: triageJson[0].Title,
          subTitle: triageJson[0].TriageMessage,
          inputType: 'number',
          scope: $scope
          });

      promptPopup.then(function (res) {
         var groupJson = JSON.parse($scope.groupData);
         var calendarJson = JSON.parse($scope.calService);
         var contactJson = JSON.parse($scope.contactData);
         var group = getGroup("ketebwoy@gmail.com", groupJson);
         var contact = getContact(triageJson[0].ContactID, contactJson); 
         var response = $scope.data.response;
         var triageId = triageJson[0].TriageId;
             

              if (response) {

                  $scope.triageResponse = response;
                  triageCache.setValue("triageResponse", { response: response, triage: triageJson[0], group: group, contact: contact });
                  triageCache.setValue("calendarInfo", { "calendarJson": calendarJson });
                  
                


                  $state.go('app.triageResponse');
                
                  console.log('Your input is ', response);

              } else {

                  console.log('Please enter input');

              }

          });

      };
  
})

.controller('PlaylistCtrl', function ($scope, $ionicPopup, $stateParams) {
})

.controller('TriageCtrl', function ($scope, $rootScope, $ionicPopup, $filter, $http, dataFactory, triageCache, dataFactory)
{
   
       

    var triageData = triageCache.getValue("triageResponse");

   
    var key = CryptoJS.enc.Utf8.parse($rootScope.devicePhoneNumber);
    var iv = CryptoJS.enc.Utf8.parse($rootScope.deviceId);

    var encryptedData = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(triageData.response), key,
    {
        keySize: 128,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });

    $scope.encryptedReply = encryptedData.toString()
    var encryptedPassword = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse("password"), key,
    {
        keySize: 128,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
    });

   

   
        var decrypReply = dataFactory.sendEncrypted(encryptedData, encryptedPassword).success(function (decryptedResponse) {

            $scope.decryptedData = decryptedResponse;

        });



    

       

    $scope.decryptedResponse = $scope.decryptedData; 
    $scope.response = triageData.response;
    $scope.group = triageData.group;
    $scope.groupTitle = triageData.group[0].CompanyName;
    $scope.triage = triageData.triage;
    $scope.contact = triageData.contact;
    $scope.triageResponse = "";
    $scope.model = {};
    $scope.model.date1 = "";

    switch ($scope.triage.TriageScaleType) {

        case 1:
            if ($scope.response >= $scope.triage.TriggerPoint) {
                $scope.triageResponse = $scope.triage.GoodResponseMessage;
                document.getElementById('selectCalDates').style.display = 'none';
            }
            else {
                $scope.triageResponse = $scope.triage.BadResponseMessage;
                document.getElementById('selectCalDates').style.display = 'block';
            }
            break;
        case 2:
            if ($scope.response <= $scope.triage.TriggerPoint) {
                $scope.triageResponse = $scope.triage.GoodResponseMessage;
                document.getElementById('selectCalDates').style.display = 'none';
            }
            else {
                $scope.triageResponse = $scope.triage.BadResponseMessage;
                document.getElementById('selectCalDates').style.display = 'block';
            }
            break;
        case 3:
            if ($scope.response = true) {
                $scope.triageResponse = $scope.triage.GoodResponseMessage;
                document.getElementById('selectCalDates').style.display = 'none';
            }
            else {
                $scope.triageResponse = $scope.triage.BadResponseMessage;
                document.getElementById('selectCalDates').style.display = 'block';
            }
            break;
            //default:
            //default code block
    }

    $scope.insertGoogleEvent = function () {

        var calData = triageCache.getValue("calendarInfo");
        
        var dates = { "date1": $scope.model.date1, "date2": $scope.model.date2, "date3": $scope.model.date3 };
        
        var urlBase = 'http://localhost/PatientHarmony.Web/Mobile/';
        var calendarId = 'ketebwoy@gmail.com'; //calendarJson[0].id;
        var groupInfo = "groupID: " + $scope.group[0].id; + "groupTitle: " + $scope.group[0].CompanyName;
        var attendeesObject = JSON.stringify({ "group": { 'email': $scope.group[0].Email, 'name': $scope.group[0].CompanyName }, "patient": { 'email': $scope.contact[0].EmailAddress, 'name': $scope.contact[0].FirstName + $scope.contact[0].LastName } });
        var url = urlBase + 'InsertCalendarEvent';
        $scope.newEventList = new Array();
        var i = 0;
        angular.forEach(dates, function (value, key) {
            var params = {
                calendarId: calendarId,
                title: 'Patient Harmony Triage Appointment',
                location: 'somewhere',
                description: 'Appointment Consideration time and Date',
                startDate: moment(value),
                endDate: moment(value).add(1, 'hour'),
                attendeesObject: attendeesObject
            }

            $http.post(url, params).success(function (newEvent) {
                if (newEvent) {
                    var temp = new Object()
                    temp = newEvent;
                    $scope.newEventList.push(temp);

                    if ($scope.newEventList.length > 2) {
                        var promptTriagePopup = $ionicPopup.alert({
                            title: "something", //$scope.group[0].CompanyName,
                            subTitle: "Has been notified of your response, and proposed appointment dates"
                        });
                        promptTriagePopup.then(function (res) {

                            //reload calendar data
                            $scope.insReturn = res;
                            $scope.url = "https://www.google.com/calendar/embed?src=ketebwoy%40gmail.com&ctz=America/New_York";
                            //datafactory.calEventService().success(function (events) {
                            //    var test = events;
                            //});
                        });
                    }
                }

            }).error(function (error) {
                $scope.status = 'Unable to load customer data: ' + error.message;
            });
             
        });

      
        

    };
})

function getGroup(code, groupJson) {
    return groupJson.filter(
        function (groupJson) { return groupJson.Email == code }
    );
};

function getContact(code, contactJson) {
    return contactJson.filter(
        function (contactJson) {return contactJson.ContactId == code }
        );
};


function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
};





