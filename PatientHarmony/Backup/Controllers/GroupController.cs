﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;
using PatientHarmony.Data.Managers;
using PatientHarmony.Data.Entities;
using PatientHarmony.Web.Filters; 
using PatientHarmony.Web.Models; 
using WebMatrix.WebData;
using System.Globalization;
using System.Web.Security;
using PatientHarmony.Web.Logging.NLog;
using Telerik.Web.Mvc;
using Telerik.Web.Mvc.UI;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using Newtonsoft.Json;


namespace PatientHarmony.Web.Controllers
{
    [InitializeSimpleMembership]
    public class GroupController : Controller
    {
       private DataManager _ctx = DataManager.Instance;
       private NLogLogger logger = new NLogLogger();
       

        #region Views

        //
        // GET: /Group/

        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Index()
        {
      
            int _userId = WebSecurity.CurrentUserId;
            var model = new GroupIndexModel();

            if (Roles.GetRolesForUser().Contains("Administrator"))
            {
                ViewBag.Role = "Administrator";
                var modelList = model.BuildIndexModel(_ctx.Groups.GetAllGroups());
                modelList.CampaignTemplates = _ctx.Templates.GetAllCampaignTemplateList();
                modelList.ProcedureTemplates = _ctx.Templates.GetAllProcedureTemplateList(); 
                
                return View(modelList);
              
            }
            else
            {
                ViewBag.Role = "Standard";
                var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                var modelList = model.BuildIndexModel(_ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups));

                foreach( var group in modelList.GroupObjects)
                {
                    modelList.CampaignTemplates = _ctx.Templates.GetAllCampaignTemplateList().FindAll(p => p.GroupID == group.GroupId);
                }

                
                var tempProcTemplateList = _ctx.Templates.GetAllProcedureTemplateList();

                foreach (var cTemplate in modelList.CampaignTemplates)
                {
                    modelList.ProcedureTemplates.AddRange(tempProcTemplateList.FindAll(p => p.CampaignTemplateID == cTemplate.CampaignTemplateId));
                }

                return View(modelList);
            }

      
        }

        //
        // GET: /Group/Manage/5

        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Manage(int id = 0, int gId = 0)
        {
            int _userId = WebSecurity.CurrentUserId;
            var group = new GroupObject();
            var manageGroupViewModel = new ManageGroupModel();  
            var campaigns = new List<Campaign>();
            var triageObject = new TriageObject();
            var templateList = new List<CampaignTemplateListTable>();

            //get all procedure templates
            var procedureTemplates = _ctx.Templates.GetAllProcedureTemplateList();

            //Get Campapign Templates 
            var campaignTemplates = _ctx.Templates.GetAllCampaignTemplateList();
            
            if (gId == 0)
            {
                manageGroupViewModel.Group = manageGroupViewModel.BuildGroupObject(_ctx.Groups.Get(id));
                manageGroupViewModel.ContactSelected = _ctx.ManyToManyRelationShips.GetAllContactsForGroup(id);
                manageGroupViewModel.Campaigns = _ctx.Campaigns.GetAllList().Where(p => p.GroupID == id).ToList<Campaign>();
                manageGroupViewModel.Triages = _ctx.Triages.GetTriagesForGroup(id);
                manageGroupViewModel.TriageTemplates = _ctx.Templates.GetAllTriageTemplateList().FindAll(p => p.GroupID == id); 
               manageGroupViewModel.Procedures = _ctx.Procedures.GetAllProcedureList();
               
               var templatesForGroup = campaignTemplates.FindAll(p => p.GroupID == id);
               
                //Build templates display model
               foreach (var template in templatesForGroup)
               {
                   var temp = new CampaignTemplateListTable();
                   temp.CampaignTemplateId = template.CampaignTemplateId;
                   temp.CampaignTemplateMessage = template.CampaignTemplateMessage;
                   temp.CampaignTemplateTitle = template.CampaignTemplateTitle;
                   temp.CreatedDate = template.CreatedDate;
                   temp.GroupID = template.GroupID;
                   temp.GroupTitle = manageGroupViewModel.Group.CompanyName;
                   temp.LastModifiedDate = template.LastModifiedDate;
                   temp.NumOfProcedures = procedureTemplates.FindAll(p => p.CampaignTemplateID == template.CampaignTemplateId).Count();
                   manageGroupViewModel.CampaignTemplates.Add(temp);

               }


            }
            else
            {
                manageGroupViewModel.Group = manageGroupViewModel.BuildGroupObject(_ctx.Groups.Get(gId));
                manageGroupViewModel.ContactSelected = _ctx.ManyToManyRelationShips.GetAllContactsForGroup(gId);
                manageGroupViewModel.Campaigns = _ctx.Campaigns.GetAllList().Where(p => p.GroupID == gId).ToList<Campaign>();
                manageGroupViewModel.Triages = _ctx.Triages.GetTriagesForGroup(gId);
                manageGroupViewModel.TriageTemplates = _ctx.Templates.GetAllTriageTemplateList().FindAll(p => p.GroupID == gId); 
                manageGroupViewModel.Procedures = _ctx.Procedures.GetAllProcedureList();

                var templatesForGroup = campaignTemplates.FindAll(p => p.GroupID == gId);

                //Build templates display model
                foreach (var template in templatesForGroup)
                {
                    var temp = new CampaignTemplateListTable();
                    temp.CampaignTemplateId = template.CampaignTemplateId;
                    temp.CampaignTemplateMessage = template.CampaignTemplateMessage;
                    temp.CampaignTemplateTitle = template.CampaignTemplateTitle;
                    temp.CreatedDate = template.CreatedDate;
                    temp.GroupID = template.GroupID;
                    temp.GroupTitle = group.CompanyName;
                    temp.LastModifiedDate = template.LastModifiedDate;
                    temp.NumOfProcedures = procedureTemplates.FindAll(p => p.CampaignTemplateID == template.CampaignTemplateId).Count();
                    manageGroupViewModel.CampaignTemplates.Add(temp);

                }

            }
          
            
            //groupManageViewModel.Schedules = _ctx.Schedules.Get

            if (Roles.GetRolesForUser().Contains("Administrator")) //Admin can get all contacts across application
            {
                var allContacts = _ctx.Contacts.GetAllList();
                manageGroupViewModel.AllContacts = allContacts;
                
            }
            else//standard user can only see contacts from groups they are assigned to
            {
                //get list of groups user belongs to
                var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                var groupList = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);

                //Get Contacts for all those groups
                var contactsForUser = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);

                manageGroupViewModel.AllContacts = contactsForUser;
            }

            if (id == 0)
            {
                return HttpNotFound();
            }

            //get triage objects with all triage info 
            manageGroupViewModel.TriageObjects = triageObject.TriageObjectList(manageGroupViewModel.Triages, manageGroupViewModel.AllContacts);
            return View(manageGroupViewModel);
        }



        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult GroupPatientControl(int Id = 0)
        {
            int _userId = WebSecurity.CurrentUserId;
            var group = new GroupObject();
            var manageGroupViewModel = new ManageGroupModel();
            var campaigns = new List<Campaign>();

            if (Id != 0)
            {
                manageGroupViewModel.Group = manageGroupViewModel.BuildGroupObject(_ctx.Groups.Get(Id));
                manageGroupViewModel.ContactSelected = _ctx.ManyToManyRelationShips.GetAllContactsForGroup(Id);
                
            }

            if (Roles.GetRolesForUser().Contains("Administrator")) //Admin can get all contacts across application
            {
                var allContacts = _ctx.Contacts.GetAllList();
                manageGroupViewModel.AllContacts = allContacts;
            }

            else//standard user can only see contacts from groups they are assigned to
            {
                //get list of groups user belongs to
                var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                var groupList = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);

                //Get Contacts for all those groups
                var contactsForUser = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);

                manageGroupViewModel.AllContacts = contactsForUser;
            }

            return View(manageGroupViewModel); 
        }

        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult GroupPatientControl(int Id, string submitPatientChanges, FormCollection collection)
        {
            
            List<int> contactIdList = new List<int>();
            List<string> contactIds = new List<string>();

            if (submitPatientChanges.Equals("Save Patients") && Id != 0)
            {

                if (collection.GetValues("patient[]").Count() > 0)
                {
                    contactIds = collection.GetValues("patient[]").ToList<string>();
                    contactIdList = contactIds.Select(int.Parse).Distinct().ToList();
                }

                _ctx.ManyToManyRelationShips.AddContactsToGroup(Id, contactIdList);

                return RedirectToAction("Manage", new { id = Id });
            }
            else
            {
                return RedirectToAction("Manage", new { id = Id });
            }
        }

        [Authorize(Roles = "Standard, Administrator")]
        [HttpGet]
        public ActionResult CreateTriage(int Id = 0)
        {
            int _userId = WebSecurity.CurrentUserId;
            var createTriageModel = new AddTriageModel();
            var activeTriages = new TriageObject(); 

            if (Id != 0)
            {
                createTriageModel.group = _ctx.Groups.Get(Id);

                //find out which patients already have active triages
                var tempList = new List<Triage>(); 
                   tempList = _ctx.Triages.GetAllTriage().FindAll(p => p.StartDate.Date >= DateTime.Now.Date);
                var triages = tempList.Where(c => c.GroupID == Id && c.EndDate >= DateTime.Today && c.EndMarker == false).ToList<Triage>();

                foreach (var triage in triages)
                {
                    createTriageModel.contactsWithTriage.Add(triage.ContactID);
                }

                //all available contacts
                createTriageModel.AllContacts = _ctx.ManyToManyRelationShips.GetAllContactsForGroup(Id);
                var allContacts = _ctx.Contacts.GetAllList(); 

                
                createTriageModel.activeTriagesForGroup = activeTriages.TriageObjectList(triages, createTriageModel.AllContacts);
                createTriageModel.triageTestList = activeTriages.TriageObjectList(tempList, allContacts);
            }
            return View(createTriageModel);
        }

        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult CreateTriagePost(string submitTriageButton, FormCollection collection)
        {
            var Id = Convert.ToInt32(collection["groupId"]);
            List<int> contactIdList = new List<int>();
            List<string> contactIds = new List<string>(); 
            var templateFlag = false;
            var globalTemplateFlag = false; 
            var templateTitle = string.Empty; 
            var triageList = new List<Triage>(); 

            if (submitTriageButton.Equals("Create Triage") && Id != 0)
            {
              
                if (collection.GetValues("patient[]").Count() > 0)
                {
                    contactIds = collection.GetValues("patient[]").ToList<string>();
                    contactIdList = contactIds.Select(int.Parse).Distinct().ToList();
                }

                var triage = new Triage();
                triage.Title = collection["NewTriage.Title"];
                triage.TriageMessage = collection["NewTriage.TriageMessage"];
                triage.GoodResponseMessage = collection["NewTriage.GoodResponseMessage"];
                triage.BadResponseMessage = collection["NewTriage.BadResponseMessage"];
                triage.SendDateTime = Convert.ToDateTime(collection["TriageSendTime"]); 
                triage.StartDate = Convert.ToDateTime(collection["TriageStartDate"]);
                triage.EndDate = Convert.ToDateTime(collection["TriageEndDate"]);
                triage.TriggerPoint = Convert.ToInt32(collection["TriggerPoint"]);
                triage.DisableTriageExtension = collection["enableExtension"].Contains("true"); 
                triage.TriageScaleType = (TriageScaleType)Enum.Parse(typeof(TriageScaleType), collection["scaleType"]);
                
                triage.Frequency = (TriageFrequencyType)Enum.Parse(typeof(TriageFrequencyType), collection["Frequency"]);


                //get send days
                for (var i = 0; i < 7; i++)
                {
                    switch (i)
                    {
                        case 0:
                            if (collection["Sunday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 1:
                            if (collection["Monday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 2:
                            if (collection["Tuesday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 3:
                            if (collection["Wednesday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 4:
                            if (collection["Thursday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 5:
                            if (collection["Friday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 6:
                            if (collection["Saturday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                    }
                }
                
                #region BuildFollowUps
                var followUps = new List<TriageFollowUp>();
                if (collection["AddFollowup"].Contains("true"))
                {
                    

                    string[] upTitles = null;
                    string[] upMessages = null;
                    string[] upGoodResponseMessages = null;
                    string[] upBadResponseMessages = null;

                    var followupStartDates = new List<DateTime>();
                    var followupEndDates = new List<DateTime>();
                    var followupsendTimes = new List<DateTime>(); 
                    

                    //build a list of procedures
                    foreach (var key in collection.AllKeys)
                    {
                        if (key.Contains("followupTitle[]"))
                        {
                            upTitles = collection.GetValues(key);
                        }

                        if (key.Contains("followupMessage[]"))
                        {
                            upMessages = collection.GetValues(key);
                        }

                        if (key.Contains("followupGoodResponseMessage[]"))
                        {
                            upGoodResponseMessages = collection.GetValues(key);
                        }

                        if (key.Contains("followupBadResponseMessage[]"))
                        {
                            upBadResponseMessages = collection.GetValues(key);
                        }

                        if (key.StartsWith("followupStartDate"))
                        {
                            var date = collection.GetValue(key);
                            var rawValue = collection.GetValues(key)[0];
                            var z = Convert.ToDateTime(rawValue);
                            followupStartDates.Add(z);

                        }

                        if (key.StartsWith("followupEndDate"))
                        {
                            var date = collection.GetValue(key);
                            var rawValue = collection.GetValues(key)[0];
                            var z = Convert.ToDateTime(rawValue);
                            followupEndDates.Add(z);

                        }

                        if (key.StartsWith("followupsendTime"))
                        {
                            var date = collection.GetValue(key);
                            var rawValue = collection.GetValues(key)[0];
                            var z = Convert.ToDateTime(rawValue);
                            followupsendTimes.Add(z);

                        }
                    }



                    if (upTitles.ToString() != string.Empty || upMessages.ToString() != string.Empty || upGoodResponseMessages.ToString() != string.Empty
                            || upBadResponseMessages.ToString() != string.Empty || followupsendTimes.Count > 0 || followupStartDates.Count > 0 || followupEndDates.Count > 0)
                    {
                        for (int x = 0; x <= upTitles.Length - 1; x++)
                        {
                            //build followUp


                            var fup = new TriageFollowUp();
                            fup.Title = upTitles[x];
                            fup.TriageFollowUpMessage = upMessages[x];
                            fup.GoodResponseMessage = upGoodResponseMessages[x];
                            fup.BadResponseMessage = upBadResponseMessages[x];
                            fup.StartDate = Convert.ToDateTime(followupStartDates[x]);
                            fup.EndDate = Convert.ToDateTime(followupEndDates[x]);
                            fup.SendDateTime = Convert.ToDateTime(followupsendTimes[x]);
                            fup.CreatedDate = DateTime.Now;
                            fup.LastModifiedDate = DateTime.Now;
                            


                            //save to List
                            followUps.Add(fup);

                        }
                    }

                    
                }
                #endregion

                triage.GroupID = Id;

                foreach (var patientID in contactIdList)
                {
                    triage.ContactID = patientID;
                    var tId = _ctx.Triages.Add(triage);

                    foreach(var followup in followUps)
                    {
                        followup.ContactID = patientID;
                        followup.TriageID = tId;
                        _ctx.TriageFollowUps.AddFollowUp(followup);
                    }

                    triageList.Add(triage);
                }

              


                #region BuildTemplates

                templateFlag = collection["templateFlag"].Contains("true");
                templateTitle = collection["templateTitle"];
                globalTemplateFlag = collection["globalTemplateFlag"].Contains("true");

             
                    if (templateFlag == true && (templateTitle != string.Empty || templateTitle != ""))
                    {
                        var triageTemplate = new TriageTemplate();
                        triageTemplate.GroupID = triage.GroupID;
                        triageTemplate.TriageTemplateTitle = triage.Title;
                        triageTemplate.TriageTemplateMessage = triage.TriageMessage;
                        triageTemplate.GoodResponseMessage = triage.GoodResponseMessage;
                        triageTemplate.BadResponseMessage = triage.BadResponseMessage;
                        triageTemplate.StartDate = triage.StartDate;
                        triageTemplate.EndDate = triage.EndDate;
                        triageTemplate.SendDateTime = triage.SendDateTime;
                        triageTemplate.TriggerPoint = triage.TriggerPoint;
                        triageTemplate.DisableTriageExtension = triage.DisableTriageExtension;
                        triageTemplate.TriageScaleType = triage.TriageScaleType;
                        triageTemplate.SendDays = triage.SendDays;
                        triageTemplate.Frequency = triage.Frequency;
                        
                        //if checked as global template
                        if (globalTemplateFlag)
                        {
                            triageTemplate.GlobalTemplate = true;
                        }
                        else
                        {
                            triageTemplate.GlobalTemplate = false;
                        }


                        var templateTriageId = _ctx.Templates.AddTriageTemplate(triageTemplate);


                        if (followUps.Count > 0)
                        {
                            foreach (var fu in followUps)
                            {

                                var followUpTemplate = new FollowUpTemplate();
                                followUpTemplate.Title = fu.Title;
                                followUpTemplate.TemplateFollowUpMessage = fu.TriageFollowUpMessage;
                                followUpTemplate.GoodResponseMessage = fu.GoodResponseMessage;
                                followUpTemplate.BadResponseMessage = fu.BadResponseMessage;
                                followUpTemplate.StartDate = fu.StartDate;
                                followUpTemplate.EndDate = fu.EndDate; 
                                followUpTemplate.SendDateTime = fu.SendDateTime;
                                followUpTemplate.TriageTemplateID = templateTriageId;

                                //if checked as global template
                                if (globalTemplateFlag)
                                {
                                    followUpTemplate.GlobalTemplate = true;
                                }
                                else
                                {
                                    followUpTemplate.GlobalTemplate = false;
                                }

                                _ctx.Templates.AddFollowUpTemplate(followUpTemplate);

                            }
                        }
                    }
                

                if(collection["startNow"].Contains("true"))
                {
                   ExecuteTriageNow(triageList);
                }

                #endregion


                return RedirectToAction("Manage", new { id = Id });
            }
            else
            {
                return RedirectToAction("Manage", new { id = Id });
            }

        }

        [Authorize(Roles = "Standard, Administrator")]
        [HttpGet]
        public ActionResult CreateTemplateTriage(int Id = 0)
        {
            int _userId = WebSecurity.CurrentUserId;
            var createTriageTemplateModel = new CreateTriageTemplateModel(); 
            
            var createTriageModel = new AddTriageModel();
            var activeTriages = new TriageObject();

            if (Id != 0)
            {
                createTriageTemplateModel.TriageTemplate = _ctx.Templates.GetTriageTemplate(Id);

                //build display info
                var temp1ateDisplay = new TraigeTemplateDisplayValueModel();
                temp1ateDisplay.TriageTemplateId = createTriageTemplateModel.TriageTemplate.TriageTemplateId;
                temp1ateDisplay.TriageTemplateTitle = createTriageTemplateModel.TriageTemplate.TriageTemplateTitle;
                temp1ateDisplay.TriageTemplateMessage = createTriageTemplateModel.TriageTemplate.TriageTemplateMessage;
                temp1ateDisplay.GoodResponseMessage = createTriageTemplateModel.TriageTemplate.GoodResponseMessage;
                temp1ateDisplay.BadResponseMessage = createTriageTemplateModel.TriageTemplate.BadResponseMessage;
                temp1ateDisplay.StartDate = createTriageTemplateModel.TriageTemplate.StartDate;
                temp1ateDisplay.EndDate = createTriageTemplateModel.TriageTemplate.EndDate;
                temp1ateDisplay.SendDateTime = createTriageTemplateModel.TriageTemplate.SendDateTime;
                temp1ateDisplay.TriggerPoint = createTriageTemplateModel.TriageTemplate.TriggerPoint;
                temp1ateDisplay.DisTriageExtension = createTriageTemplateModel.TriageTemplate.DisableTriageExtension;
                temp1ateDisplay.TriageScaleType = createTriageTemplateModel.TriageTemplate.TriageScaleType;
                temp1ateDisplay.TriageFrequencyType = createTriageTemplateModel.TriageTemplate.Frequency;
                temp1ateDisplay.SendDays = createTriageTemplateModel.TriageTemplate.SendDays;


                createTriageTemplateModel.TriageTemplateDisplay = temp1ateDisplay; 

                createTriageTemplateModel.Group = _ctx.Groups.Get(createTriageTemplateModel.TriageTemplate.GroupID);


                //find out which patients already have active triages
                var tempList = _ctx.Triages.GetAllTriage();
                var triages = tempList.Where(c => c.GroupID == createTriageTemplateModel.TriageTemplate.GroupID && c.EndDate >= DateTime.Today && c.EndMarker == false).ToList<Triage>();

                foreach (var triage in triages)
                {
                    createTriageTemplateModel.contactsWithTriage.Add(triage.ContactID);
                }

                //all available contacts
                createTriageTemplateModel.AllContacts = _ctx.ManyToManyRelationShips.GetAllContactsForGroup(createTriageTemplateModel.TriageTemplate.GroupID);


                createTriageTemplateModel.activeTriagesForGroup = activeTriages.TriageObjectList(triages, createTriageTemplateModel.AllContacts);

                createTriageTemplateModel.FollowUps = _ctx.Templates.GetAllFollowUpTemplateList().FindAll(p => p.TriageTemplateID == Id);
            }
            return View(createTriageTemplateModel);
        }

        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult CreateTemplateTriage(int Id, string submitTriageButton, FormCollection collection)
        {

            List<int> contactIdList = new List<int>();
            List<string> contactIds = new List<string>();
            var triageTemplate = _ctx.Templates.GetTriageTemplate(Id);
            var templateFlag = false;
            var globalTemplateFlag = false; 
            var templateTitle = string.Empty; 

            var gId = triageTemplate.GroupID;
            var triageList = new List<Triage>(); 

            if (submitTriageButton.Equals("Create Triage") && Id != 0)
            {
          

                if (collection.GetValues("patient[]").Count() > 0)
                {
                    contactIds = collection.GetValues("patient[]").ToList<string>();
                    contactIdList = contactIds.Select(int.Parse).Distinct().ToList();
                }

                var triage = new Triage();
                triage.Title = collection["TriageTemplateDisplay.TriageTemplateTitle"];
                triage.TriageMessage = collection["TriageTemplateDisplay.TriageTemplateMessage"];
                triage.GoodResponseMessage = collection["TriageTemplateDisplay.GoodResponseMessage"];
                triage.BadResponseMessage = collection["TriageTemplateDisplay.BadResponseMessage"];
                triage.SendDateTime = Convert.ToDateTime(collection["TriageSendTime"]); triage.StartDate = Convert.ToDateTime(collection["TriageStartDate"]);
                triage.EndDate = Convert.ToDateTime(collection["TriageEndDate"]);
                triage.TriggerPoint = Convert.ToInt32(collection["TriggerPoint"]);
                triage.DisableTriageExtension = collection["DisableExtension"].Contains("true");
                triage.TriageScaleType = (TriageScaleType)Enum.Parse(typeof(TriageScaleType), collection["scaleType"]);
                triage.Frequency = (TriageFrequencyType)Enum.Parse(typeof(TriageFrequencyType), collection["Frequency"]);
                triage.GroupID = gId;

                //get send days
                for (var i = 0; i < 7; i++)
                {
                    switch (i)
                    {
                        case 0:
                            if (collection["Sunday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 1:
                            if (collection["Monday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 2:
                            if (collection["Tuesday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 3:
                            if (collection["Wednesday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 4:
                            if (collection["Thursday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 5:
                            if (collection["Friday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 6:
                            if (collection["Saturday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                    }
                }

                #region BuildFollowUps
                var followUps = new List<TriageFollowUp>();
                if (collection["AddFollowup"].Contains("true"))
                {


                    List<string> upTitles = new List<string>();
                    List<string> upMessages = new List<string>();
                    List<string> upGoodResponseMessages = new List<string>();
                    List<string> upBadResponseMessages = new List<string>();
                    var followupsendTimes = new List<DateTime>();
                    var followupStartDates = new List<DateTime>();
                    var followupEndDates = new List<DateTime>();

                            
                    
                    //build a list of procedures
                    foreach (var key in collection.AllKeys)
                    {
                        if (key.StartsWith("followupTit"))
                        {
                            upTitles.AddRange(collection.GetValues(key));
                        }

                        if (key.StartsWith("followupMess"))
                        {
                            upMessages.AddRange(collection.GetValues(key));
                        }

                        if (key.StartsWith("followupGood"))
                        {
                            upGoodResponseMessages.AddRange(collection.GetValues(key));
                        }

                        if (key.StartsWith("followupBad"))
                        {
                            upBadResponseMessages.AddRange(collection.GetValues(key));
                        }

                        if (key.StartsWith("followupStartDate"))
                        {
                            var date = collection.GetValue(key);
                            var rawValue = collection.GetValues(key)[0];
                            var z = Convert.ToDateTime(rawValue);
                            followupStartDates.Add(z);

                        }

                        if (key.StartsWith("followupEndDate"))
                        {
                            var date = collection.GetValue(key);
                            var rawValue = collection.GetValues(key)[0];
                            var z = Convert.ToDateTime(rawValue);
                            followupEndDates.Add(z);

                        }

                        if (key.StartsWith("followupsendTime"))
                        {
                            var date = collection.GetValue(key);
                            var rawValue = collection.GetValues(key)[0];
                            var z = Convert.ToDateTime(rawValue);
                            followupsendTimes.Add(z);

                        }
                    }



                    if (upTitles.Count <= 0 || upMessages.Count <= 0 || upGoodResponseMessages.Count <= 0 || upBadResponseMessages.Count <= 0 || followupsendTimes.Count > 0)
                    {
                        for (int x = 0; x < upTitles.Count; x++)
                        {
                            //build followUp
                            var fup = new TriageFollowUp();
                            fup.Title = upTitles[x];
                            fup.TriageFollowUpMessage = upMessages[x];
                            fup.GoodResponseMessage = upGoodResponseMessages[x];
                            fup.BadResponseMessage = upBadResponseMessages[x];
                            fup.StartDate = Convert.ToDateTime(followupStartDates[x]);
                            fup.EndDate = Convert.ToDateTime(followupEndDates[x]);
                            fup.SendDateTime = Convert.ToDateTime(followupsendTimes[x]); fup.CreatedDate = DateTime.Now;
                            fup.LastModifiedDate = DateTime.Now;


                            //save to List
                            followUps.Add(fup);

                        }
                    }


                }
                #endregion

                foreach (var patientID in contactIdList)
                {
                    triage.ContactID = patientID;
                    var tId = _ctx.Triages.Add(triage);

                    foreach (var followup in followUps)
                    {
                        followup.ContactID = patientID;
                        followup.TriageID = tId;
                        _ctx.TriageFollowUps.AddFollowUp(followup);
                    }

                    triageList.Add(triage);
                }

                #region BuildTemplates

                templateFlag = collection["templateFlag"].Contains("true");
                templateTitle = collection["templateTitle"];

                 globalTemplateFlag = collection["globalTemplateFlag"].Contains("true");
               

                if (templateFlag == true && (templateTitle != string.Empty || templateTitle != ""))
                {
                    var newTriageTemplate = new TriageTemplate();
                    newTriageTemplate.GroupID = triage.GroupID;
                    newTriageTemplate.TriageTemplateTitle = triage.Title;
                    newTriageTemplate.TriageTemplateMessage = triage.TriageMessage;
                    newTriageTemplate.GoodResponseMessage = triage.GoodResponseMessage;
                    newTriageTemplate.BadResponseMessage = triage.BadResponseMessage;
                    newTriageTemplate.StartDate = triage.StartDate;
                    newTriageTemplate.EndDate = triage.EndDate;
                    newTriageTemplate.SendDateTime = triage.SendDateTime;
                    newTriageTemplate.TriggerPoint = triage.TriggerPoint;
                    newTriageTemplate.DisableTriageExtension = triage.DisableTriageExtension;
                    newTriageTemplate.TriageScaleType = triage.TriageScaleType;
                    newTriageTemplate.SendDays = triage.SendDays;
                    newTriageTemplate.Frequency = triage.Frequency;

                    //if checked as global template
                    if (globalTemplateFlag)
                    {
                        newTriageTemplate.GlobalTemplate = true;
                    }
                    else
                    {
                        newTriageTemplate.GlobalTemplate = false;
                    }

                    var templateTriageId = _ctx.Templates.AddTriageTemplate(newTriageTemplate);


                    if (followUps.Count > 0)
                    {
                        foreach (var fu in followUps)
                        {

                            var followUpTemplate = new FollowUpTemplate();
                            followUpTemplate.Title = fu.Title;
                            followUpTemplate.TemplateFollowUpMessage = fu.TriageFollowUpMessage;
                            followUpTemplate.GoodResponseMessage = fu.GoodResponseMessage;
                            followUpTemplate.BadResponseMessage = fu.BadResponseMessage;
                            followUpTemplate.StartDate = fu.StartDate;
                            followUpTemplate.EndDate = fu.EndDate;
                            followUpTemplate.SendDateTime = fu.SendDateTime;
                            followUpTemplate.TriageTemplateID = templateTriageId;

                            //if checked as global template
                            if(globalTemplateFlag)
                            {
                                followUpTemplate.GlobalTemplate = true; 
                            }
                            else
                            {
                                followUpTemplate.GlobalTemplate = false; 
                            }

                            _ctx.Templates.AddFollowUpTemplate(followUpTemplate);

                        }
                    }
                }

                #endregion 

                if (collection["startNow"].Contains("true"))
                {
                    ExecuteTriageNow(triageList);
                }

                return RedirectToAction("Manage", new { id = gId });
            }
            else
            {
                return RedirectToAction("Manage", new { id = gId });
            }

        }

        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult EditTriage(int triageId)
        {
            int _userId = WebSecurity.CurrentUserId;
            var triage = _ctx.Triages.GetTriage(triageId);

            var group = _ctx.Groups.Get(triage.GroupID);
            triage.Group = group;

            var followUpList = _ctx.TriageFollowUps.GetAllFollowUpsForTriage(triageId);

            ViewBag.FollowUpList = followUpList; 
                    
            return View(triage);
        }

        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult EditTriage(int Id, string submitTriageButton, FormCollection collection)
        {

            var triagePassedIn = _ctx.Triages.GetTriage(Id);
            var groupId = triagePassedIn.GroupID;

            if (submitTriageButton.Equals("Save Triage"))
            {
                var triageId = Convert.ToInt32(collection["TriageId"]);
               
                
                var triage = new Triage();
                triage.Title = collection["Title"];
                triage.TriageMessage = collection["TriageMessage"];
                triage.GoodResponseMessage = collection["GoodResponseMessage"];
                triage.BadResponseMessage = collection["BadResponseMessage"];
                triage.SendDateTime = Convert.ToDateTime(collection["TriageSendTime"]); 
                triage.StartDate = Convert.ToDateTime(collection["TriageStartDate"]);
                triage.EndDate = Convert.ToDateTime(collection["TriageEndDate"]);
                triage.TriggerPoint = Convert.ToInt32(collection["TriggerPoint"]);
                triage.DisableTriageExtension = collection["DisableExtension"].Contains("true");
                triage.TriageScaleType = (TriageScaleType)Enum.Parse(typeof(TriageScaleType), collection["scaleType"]);
                triage.Frequency = (TriageFrequencyType)Enum.Parse(typeof(TriageFrequencyType), collection["Frequency"]);
                triage.GroupID = groupId;
                triage.TriageId = triageId;
                triage.ContactID = triagePassedIn.ContactID;

                #region BuildFollowUps
                var followUps = new List<TriageFollowUp>();
                if (collection["AddFollowup"].Contains("true"))
                {


                    List<string> upTitles = new List<string>();
                    List<string> upMessages = new List<string>();
                    List<string> upGoodResponseMessages = new List<string>();
                    List<string> upBadResponseMessages = new List<string>();

                    var followupStartDates = new List<DateTime>();
                    var followupEndDates = new List<DateTime>();
                    var followupsendTimes = new List<DateTime>();



                    //build a list of procedures
                    foreach (var key in collection.AllKeys)
                    {
                        if (key.StartsWith("followupTit"))
                        {
                            upTitles.AddRange(collection.GetValues(key));
                        }

                        if (key.StartsWith("followupMess"))
                        {
                            upMessages.AddRange(collection.GetValues(key));
                        }

                        if (key.StartsWith("followupGood"))
                        {
                            upGoodResponseMessages.AddRange(collection.GetValues(key));
                        }

                        if (key.StartsWith("followupBad"))
                        {
                            upBadResponseMessages.AddRange(collection.GetValues(key));
                        }

                        
                        if (key.StartsWith("followupStartDate"))
                        {
                            var date = collection.GetValue(key);
                            var rawValue = collection.GetValues(key)[0];
                            var z = Convert.ToDateTime(rawValue);
                            followupStartDates.Add(z);

                        }

                        if (key.StartsWith("followupEndDate"))
                        {
                            var date = collection.GetValue(key);
                            var rawValue = collection.GetValues(key)[0];
                            var z = Convert.ToDateTime(rawValue);
                            followupEndDates.Add(z);

                        }

                        if (key.StartsWith("followupsendTime"))
                        {
                            var date = collection.GetValue(key);
                            var rawValue = collection.GetValues(key)[0];
                            var z = Convert.ToDateTime(rawValue);
                            followupsendTimes.Add(z);

                        }
                    }



                    if (upTitles.Count <= 0 || upMessages.Count <= 0 || upGoodResponseMessages.Count <= 0 || upBadResponseMessages.Count <= 0 || followupsendTimes.Count > 0 || followupStartDates.Count > 0 || followupEndDates.Count > 0)
                    {
                        for (int x = 0; x < upTitles.Count; x++)
                        {
                            //build followUp
                            var fup = new TriageFollowUp();
                            fup.Title = upTitles[x];
                            fup.TriageFollowUpMessage = upMessages[x];
                            fup.GoodResponseMessage = upGoodResponseMessages[x];
                            fup.BadResponseMessage = upBadResponseMessages[x];
                            fup.StartDate = Convert.ToDateTime(followupStartDates[x]);
                            fup.EndDate = Convert.ToDateTime(followupEndDates[x]);
                            fup.SendDateTime = Convert.ToDateTime(followupsendTimes[x]);
                            fup.CreatedDate = DateTime.Now;
                            fup.LastModifiedDate = DateTime.Now;


                            //save to List
                            followUps.Add(fup);

                        }
                    }


                }
                #endregion
                

                
                //get send days
                for (var i = 0; i < 7; i++)
                {
                    switch (i)
                    {
                        case 0:
                            if (collection["Sunday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 1:
                            if (collection["Monday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 2:
                            if (collection["Tuesday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 3:
                            if (collection["Wednesday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 4:
                            if (collection["Thursday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 5:
                            if (collection["Friday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                        case 6:
                            if (collection["Saturday"].Contains("true"))
                            {
                                triage.SendDays = string.Format(triage.SendDays + "{0}", i);
                            }
                            break;
                    }
                }

                _ctx.Triages.Update(triage);

                #region Update FollowUps

                var oldFOllowUpList = _ctx.TriageFollowUps.GetAllFollowUpsForTriage(triageId).FindAll(p => p.ContactID == triagePassedIn.ContactID); 

                //delete old followups
                foreach(var fu in oldFOllowUpList)
                {
                    _ctx.TriageFollowUps.Delete(fu.TriageFollowUpId);
                }

                //add 
                foreach(var fu in followUps)
                {
                    fu.ContactID = triagePassedIn.ContactID;
                    fu.TriageID = triageId;
                    _ctx.TriageFollowUps.AddFollowUp(fu);
                }

                #endregion 


                return RedirectToAction("Manage", new { id = groupId });
            }
            else
            {
                return RedirectToAction("Manage", new { id = groupId });
            }
        }




        //
        // GET: /Group/Create
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Create()
        {
            var contactList = _ctx.Contacts.GetAllList();
            var contactDictionary = contactList.ToDictionary((keyItem) => keyItem.ContactId, (valueItem) => valueItem.FirstName + " " + valueItem.LastName);

            ViewData["Contacts"] = new MultiSelectList(contactDictionary, "Key", "Value");

            return PartialView("_CreateGroup");
        }

        //
        // POST: /Group/Create
        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Create(Group group, FormCollection collection)
        {
            int _userId = WebSecurity.CurrentUserId;
            group.CreatedDate = DateTime.Now;
            group.LastModifiedDate = DateTime.Now;

            var availablePhoneNum = _ctx.Groups.FindNewTwilioNumber();
            var phoneNumBuy = _ctx.Groups.PurchaseNewTwilioNumber(availablePhoneNum);
            group.PhoneNumber =  phoneNumBuy;

            int gId = _ctx.Groups.Add(group);

            List<int> contactIdList = new List<int>();
            var contactIdItems = new List<string>();

            foreach (var key in collection.AllKeys)
            {
                if (key.Contains("Contacts"))
                {
                    List<string> contactIds = collection.GetValues(key).ToList<string>();
                    contactIdList = contactIds.Select(int.Parse).ToList();
                }
            }

            _ctx.ManyToManyRelationShips.AddContactsToGroup(gId, contactIdList);
         
            //TODO add checkbox to make this group default for current user
            //   _ctx.ManyToManyRelationShips.AddDefaultUserGroup(_userId, group.GroupId);

            return RedirectToAction("Manage", new { id = gId });
        }

//
        // GET: /Group/Edit/
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Edit(int id = 0)
        {
            int _userId = WebSecurity.CurrentUserId;
            Group group = _ctx.Groups.Get(id);
            var campaigns = new List<Campaign>();
            var contactsForGroup = _ctx.ManyToManyRelationShips.GetAllContactsForGroup(id);
            var campaignsForGroup = _ctx.Campaigns.GetAllList().Where(p => p.GroupID == id);

            if (Roles.GetRolesForUser().Contains("Administrator")) //Admin can get all contacts across application
            {
                var allContacts = _ctx.Contacts.GetAllList();
                ViewData["CampaignsSelected"] = campaignsForGroup;
                ViewData["ContactsSelected"] = contactsForGroup;
                ViewData["Contacts"] = allContacts;
            }
            else//standard user can only see contacts from groups they are assigned to
            {
                //get list of groups user belongs to
                var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                var groupList = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);

                //Get Contacts for all those groups
                var contactsForUser = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);

                ViewData["CampaignsSelected"] = campaignsForGroup;
                ViewData["ContactsSelected"] = contactsForGroup;
                ViewData["Contacts"] = contactsForUser;
            }

            if (group == null)
            {
                return HttpNotFound();
            }

            return View(group);
        }

        //
        // POST: /Group/Edit/5

        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Edit(Group group, FormCollection collection)
        {
            group.LastModifiedDate = DateTime.Now;

            _ctx.Groups.UpdateGroup(group);

            List<int> contactIdList = new List<int>();

            List<string> contactIds = collection.GetValues("Contacts").ToList<string>();
            contactIdList = contactIds.Select(int.Parse).ToList();

            _ctx.ManyToManyRelationShips.AddContactsToGroup(group.GroupId, contactIdList);

            return RedirectToAction("Index");
        }


        //
        // POST: /Group/Delete/5
        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var group = _ctx.Groups.Get(id);

            _ctx.Groups.DeleteGroup(id);
            return RedirectToAction("Index");
        }
        




        #endregion administrator Group views

        #region group details edit functions

        public ActionResult UpdateGroupDetails(Group group)
        {
            group.LastModifiedDate = DateTime.Now;
            _ctx.Groups.UpdateGroup(group);

            return RedirectToAction("Index");

        }

        
       
        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost]
        public ActionResult CreateContact(string submitContactButton, Contact contact, FormCollection collection)
        {
            int _userId = WebSecurity.CurrentUserId;
            int gId = Int32.Parse(collection["gId"]);
            if (submitContactButton == "Create Patient")
            {
                contact.FirstName = collection["NewContact.FirstName"];
                contact.LastName = collection["NewContact.LastName"];
                contact.EmailAddress = collection["NewContact.EmailAddress"];
                contact.PhoneNumber = collection["NewContact.PhoneNumber"];
                contact.InsurerEmailAddress = collection["NewContact.InsurerEmailAddress"]; 
                int cId = _ctx.Contacts.Add(contact);

                var contactGroup = new ContactsGroup();
                var contactList = _ctx.ManyToManyRelationShips.GetAllContactsForGroup(gId);
                var contactIdList = new List<int>();
                contactIdList.Add(cId);

                if(contactList.Count > 0)
                foreach (var contactAdd in contactList)
                {
                    contactIdList.Add(contactAdd.ContactId);
                }


                _ctx.ManyToManyRelationShips.AddContactsToGroup(gId, contactIdList);

                string url = Request.UrlReferrer.AbsolutePath;
                return RedirectToAction("Manage", new { id = gId });
            }
            else
            {
                return RedirectToAction("Manage", new { id = gId });
            }
        }



        [Authorize(Roles = "Standard, Administrator")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditContact(int gId, FormCollection form)
        {
            List<string> contactIds = form.GetValues("ContactId").ToList<string>();
            List<int> contactIdList = contactIds.Select(int.Parse).Distinct().ToList();
            
            var cId = contactIdList[0];
            var contact = _ctx.Contacts.Get(cId);
            contact.FirstName = form["FirstName"];
            contact.LastName = form["LastName"];
            contact.EmailAddress = form["EmailAddress"];
            contact.PhoneNumber = form["PhoneNumber"];
            contact.InsurerEmailAddress = form["InsurerEmailAddress"];

            _ctx.Contacts.UpdateContact(contact); 
            
               
            return RedirectToAction("Manage", new { id = gId });
            
        }

        [Authorize(Roles = "Standard, Administrator")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteContactInGroup(int id, int gId = 0)
        {
            //List<string> contactIds = form.GetValues("ContactId").ToList<string>();
            //List<int> contactIdList = contactIds.Select(int.Parse).Distinct().ToList();
            
            //var cId = contactIdList[0];
            _ctx.Contacts.DeleteContact(id); 
        
              return RedirectToAction("Manage", new { id = gId });
        }

        [Authorize(Roles = "Standard, Administrator")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteTriageInGroup(int id, int gId)
        {
            _ctx.Triages.Remove(id);

            return RedirectToAction("Manage", new { id = gId });
        }

        [Authorize(Roles = "Standard, Administrator")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditCampaign(int gId, FormCollection form)
        {
            List<string> campaignIds = form.GetValues("CampaignId").ToList<string>();
            List<int> campaignIdList = campaignIds.Select(int.Parse).Distinct().ToList();

            var campId = campaignIdList[0];
            var campaign = _ctx.Campaigns.GetCampaign(campId);
            campaign.CampaignTitle = form["CampaignTitle"];
            campaign.CampaignMessage = form["CampaignMessage"];
            

            _ctx.Campaigns.UpdateCampaign(campaign);


            return RedirectToAction("Manage", new { id = gId });

        }

        [Authorize(Roles = "Standard, Administrator")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DeleteCampaignInGroup(int id, int gId = 0)
        {
            //List<string> contactIds = form.GetValues("ContactId").ToList<string>();
            //List<int> contactIdList = contactIds.Select(int.Parse).Distinct().ToList();

            //var cId = contactIdList[0];
            _ctx.Campaigns.Delete(id);

            return RedirectToAction("Manage", new { id = gId });
        }


        public void CreateCampaign(Campaign campaign, int userID, int gID, List<int> patientIds, List<Procedure> procList = null, string templateTitle = "", bool globalMarker = false)
        {

            try
            {
                //add campaign
                int campaignId = _ctx.Campaigns.Add(campaign);

                //add contacts to campaign
                _ctx.ManyToManyRelationShips.AddContactstoCampaigns(campaignId, patientIds);

                // var contacts = _ctxFunctions.ManyToManyRelationShips.GetAllContactsForCampaign(campaignId);

                List<Contact> triagePatients = new List<Contact>();

                foreach (var contact in patientIds)
                {
                    var patient = _ctx.Contacts.Get(contact);
                    triagePatients.Add(patient);
                }

                //add procedures 
                foreach (var procedure in procList)
                {
                    procedure.CampaignID = campaignId;
                    _ctx.Procedures.Add(procedure);
                }

                //add templates
                if (templateTitle != "")
                {
                    //create campaign template
                    var tempCampTemplate = new CampaignTemplate();
                    tempCampTemplate.CampaignTemplateTitle = templateTitle;
                    tempCampTemplate.CampaignTemplateMessage = campaign.CampaignMessage;
                    tempCampTemplate.GroupID = campaign.GroupID;
                    tempCampTemplate.SendDateTime = campaign.SendDateTime;
                    tempCampTemplate.GlobalTemplate = globalMarker; 
                    _ctx.Templates.AddCampaignTemplate(tempCampTemplate);

                    //make procedures into procedure Templates
                    foreach (var procedure in procList)
                    {
                        var tempProcTemplate = new ProcedureTemplate();
                        tempProcTemplate.CampaignTemplateID = tempCampTemplate.CampaignTemplateId;
                        tempProcTemplate.ProcedureTemplateTitle = procedure.ProcedureTitle;
                        tempProcTemplate.ProcedureTemplateMessage = procedure.ProcedureMessage;
                        tempProcTemplate.StartDate = procedure.StartDate;
                        tempProcTemplate.EndDate = procedure.EndDate;
                        tempProcTemplate.GlobalTemplate = globalMarker; 
                        tempProcTemplate.SendDateTime = procedure.SendDateTime;


                        _ctx.Templates.AddProcedureTemplate(tempProcTemplate);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured creating campaign in manage group", ex);

            }
            
        }


        #endregion group details edit functions

        #region Group Ajax Operations

        [GridAction]
        public ActionResult _AjaxBindingGroup()
        {
            var model = new GroupModel();
            model.Groups = new List<Group>();
            
            int _userId = WebSecurity.CurrentUserId;

            if (Roles.GetRolesForUser().Contains("Administrator"))
            {
                //model.Groups = _ctx.Groups.GetAllGroups();
                //ViewBag.Role = "Administrator";
                return View(_ctx.Groups.GetAllGroups());
            }
           
            else
            {
                var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                return View(new GridModel(_ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups)));            
            }
           
           
        }

      
        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _InsertAjaxGroup(FormCollection collection)
        {
           
            int _userId = WebSecurity.CurrentUserId;
            var group = new Group(); 
            group.CreatedDate = DateTime.Now;
            group.LastModifiedDate = DateTime.Now;

            var availablePhoneNum = _ctx.Groups.FindNewTwilioNumber();
            var phoneNumBuy = _ctx.Groups.PurchaseNewTwilioNumber(availablePhoneNum);
            group.PhoneNumber =  phoneNumBuy;
            group.CompanyName = collection["CompanyName"];
            group.Email = collection["Email"];

            int gId = _ctx.Groups.Add(group);

             var modelScaffold = new GroupModel();

            modelScaffold = modelScaffold.BuildIndexData(); 

            return View(new GridModel(modelScaffold.GroupObjects));

        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxEditingGroup(int id)
        {

            var group = _ctx.Groups.Get(id);

            _ctx.Groups.DeleteGroup(id);

            var modelScaffold = new GroupModel();

            modelScaffold = modelScaffold.BuildIndexData(); 

            return View(new GridModel(modelScaffold.GroupObjects));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingGroup(int id, FormCollection collection)
        {

            var group = _ctx.Groups.Get(id);

            group.CompanyName = collection["CompanyName"];
            group.Email = collection["Email"];
           

            _ctx.Groups.UpdateGroup(group);

            var modelScaffold = new GroupModel();

            modelScaffold = modelScaffold.BuildIndexData();

            return View(new GridModel(modelScaffold.GroupObjects));
        }

        #endregion

        #region Campaign Ajax Operations

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxEditingCampaign(int id)
        {

            var campaign = _ctx.Campaigns.GetCampaign(id);

            _ctx.Campaigns.Delete(id);

            var campaigns = _ctx.Campaigns.GetAllList().FindAll(p => p.GroupID == campaign.GroupID);

            return View(new GridModel(campaigns));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxDeleteCampaignTemplate(int id)
        {

            var campaignTemplate = _ctx.Templates.GetCampaignTemplate(id);
            var group = _ctx.Groups.Get(campaignTemplate.GroupID);

            _ctx.Templates.DeleteCampaignTemplate(id);

            var campaignTemplates = _ctx.Templates.GetAllCampaignTemplateList().FindAll(p => p.GroupID == campaignTemplate.GroupID);
            var procedureTemplates = _ctx.Templates.GetAllProcedureTemplateList(); 

            var templateDisplayModel = new List<CampaignTemplateListTable>();

            //Build templates display model
            foreach (var template in campaignTemplates)
            {
                var temp = new CampaignTemplateListTable();
                temp.CampaignTemplateId = template.CampaignTemplateId;
                temp.CampaignTemplateMessage = template.CampaignTemplateMessage;
                temp.CampaignTemplateTitle = template.CampaignTemplateTitle;
                temp.CreatedDate = template.CreatedDate;
                temp.GroupID = template.GroupID;
                temp.GroupTitle = group.CompanyName;
                temp.LastModifiedDate = template.LastModifiedDate;
                temp.NumOfProcedures = procedureTemplates.FindAll(p => p.CampaignTemplateID == template.CampaignTemplateId).Count();
                templateDisplayModel.Add(temp);

            }



            return View(new GridModel(templateDisplayModel));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingCampaign(int id, FormCollection collection)
        {

            var campaign = _ctx.Campaigns.GetCampaign(id);

            campaign.CampaignTitle = collection["CampaignTitle"];
            campaign.SendDateTime = Convert.ToDateTime(collection["SendDateTime"]);
            campaign.CampaignMessage = collection["CampaignMessage"];

            _ctx.Campaigns.UpdateCampaign(campaign);

            var campaigns = _ctx.Campaigns.GetAllList().FindAll(p => p.GroupID == campaign.GroupID);

            return View(new GridModel(campaigns));
        }

        #endregion 

        #region Contact Ajax Operations

        [GridAction]
        public ActionResult _AjaxBindingContact(int gId)
        {
            var model = new GroupModel();
            model.Group = _ctx.Groups.Get(gId);
            model.ContactSelected = new List<Contact>();
            
            return View(new GridModel(_ctx.ManyToManyRelationShips.GetAllContactsForGroup(gId)));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _InsertAjaxContact(int gId, FormCollection collection)
        {

            int _userId = WebSecurity.CurrentUserId;
            var contact = new Contact();
            contact.CreatedDate = DateTime.Now;
            contact.LastModifiedDate = DateTime.Now;

            contact.FirstName = collection["FirstName"];
            contact.LastName = collection["LastName"];
            contact.PhoneNumber = collection["PhoneNumber"];
            contact.EmailAddress = collection["EmailAddress"];
            contact.InsurerEmailAddress = collection["InsurerEmailAddress"];

            int cId = _ctx.Contacts.Add(contact);
            var idList = new List<int>();
            var contactList = _ctx.ManyToManyRelationShips.GetAllContactsForGroup(gId);

            foreach (var patient in contactList)
            {
                idList.Add(patient.ContactId);
            }

            idList.Add(cId);
            _ctx.ManyToManyRelationShips.AddContactsToGroup(gId, idList);

            return View(new GridModel(_ctx.ManyToManyRelationShips.GetAllContactsForGroup(gId)));

        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxEditingContact(int id, int gId)
        {

            var contact  = _ctx.Contacts.Get(id);

            _ctx.Contacts.DeleteContact(id);
                        
            return View(new GridModel(_ctx.ManyToManyRelationShips.GetAllContactsForGroup(gId)));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingContact(int id, int gId, FormCollection collection)
        {

            _ctx.Groups.Get(gId);

            var contact = _ctx.Contacts.Get(id);

            contact.FirstName = collection["FirstName"];
            contact.LastName = collection["LastName"];
            contact.PhoneNumber = collection["PhoneNumber"];
            contact.EmailAddress = collection["EmailAddress"];
            contact.InsurerEmailAddress = collection["InsurerEmailAddress"];

            _ctx.Contacts.UpdateContact(contact);


            return View(new GridModel(_ctx.ManyToManyRelationShips.GetAllContactsForGroup(gId)));
        }


        #endregion

        #region Triage Ajax Operations

        //
        // GET: /AjaxTest/
        [HttpGet]
        public ActionResult EditInfo(string triageId)
        {
            var triage = _ctx.Triages.GetAllTriage();
            JavaScriptSerializer jss = new JavaScriptSerializer();
           
            ViewData["Triages"] = triage;

            string output = jss.Serialize(triage);

            return  Json(output, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult FirstAjax(string a)
        {

            return Json("chamara", JsonRequestBehavior.AllowGet);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingTriage(int id, FormCollection collection)
        {

            var triage = _ctx.Triages.GetTriage(id);

            triage.Title = collection["Title"];
            triage.TriageMessage = collection["TriageMessage"];
            triage.GoodResponseMessage = collection["GoodResponseMessage"];
            triage.BadResponseMessage = collection["BadResponseMessage"];
            //   triage.SendDateTime = collection[""];

            triage.StartDate = Convert.ToDateTime(collection["StartDate"]);
            triage.EndDate = Convert.ToDateTime(collection["EndDate"]);
            triage.SendDateTime = Convert.ToDateTime(collection["SendDateTime"]);
            triage.TriggerPoint = Convert.ToInt32(collection["TriggerPoint"]);
            triage.DisableTriageExtension= collection["DisableExtension"].Contains("true");
            triage.TriageScaleType = (TriageScaleType)Enum.Parse(typeof(TriageScaleType), collection["scaleType"]);
            _ctx.Triages.Update(triage);

            var model = new GroupModel();

            model.Triages = _ctx.Triages.GetTriagesForGroup(triage.GroupID);

            return View(new GridModel(model.Triages));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxEditingTriage(int id)
        {

            var triage = _ctx.Triages.GetTriage(id);
            int gId = triage.GroupID; 
            _ctx.Triages.Remove(id);

            var triages = _ctx.Triages.GetTriagesForGroup(gId);

              return View(new GridModel(triages));
        }

        
        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxDeleteTriageTemplate(int id)
        {

            var triageTemplate = _ctx.Templates.GetTriageTemplate(id);
           _ctx.Templates.DeleteTriageTemplate(id);

            var triages = _ctx.Templates.GetAllTriageTemplateList().FindAll(p => p.GroupID == triageTemplate.GroupID);

            return View(new GridModel(triages));
        }



        private void ExecuteTriageNow(List<Triage> triages)
        {
            foreach (var triage in triages)
            {
                Contact patient = _ctx.Contacts.Get(triage.ContactID);

                //need list for sendSms
                var patientList = new List<Contact>();
                patientList.Add(patient);

                var group = _ctx.Groups.Get(triage.GroupID);
                var sendTime = triage.SendDateTime - triage.SendDateTime.Date;

                //Send Triage Immediately
                var sms = new SMSOutgoing();
                sms.Message = triage.TriageMessage;
                sms.TriageID = triage.TriageId;
                sms.FromPhoneNumber = group.PhoneNumber;
                _ctx.SMSmessages.sendSMS(sms, patientList);
                System.Threading.Thread.Sleep(500);
                triage.LastDateProcessed = DateTime.Now;
                _ctx.Triages.Update(triage);

            }

        }
        
                    

        #endregion
        
        #region Add Campaign Actions 

        [HttpGet]
        public ActionResult CreateCampaignGroup(int gId)
        {
            int _userId = WebSecurity.CurrentUserId;
            var model = new ManageGroupModel();
            model.Group = model.BuildGroupObject(_ctx.Groups.Get(gId));

            if (Roles.GetRolesForUser().Contains("Administrator")) //Admin can get all contacts across application
            {
                var allContacts = _ctx.Contacts.GetAllList();
                model.AllContacts = allContacts;

            }
            else//standard user can only see contacts from groups they are assigned to
            {
                //get list of groups user belongs to
                var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                var groupList = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);

                //Get Contacts for all those groups
                var contactsForUser = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);

                model.AllContacts = contactsForUser;
            }

           return View(model);
        }

        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost]
        public ActionResult CreateCampaignGroup(string submitCampaignButton, Campaign campaign, FormCollection collection)
        {
            int _userId = WebSecurity.CurrentUserId;
            string[] procTitles = null;
            string[] procMessages = null;
            int gId = Convert.ToInt32(collection["gId"]);
            var procList = new List<Procedure>();
            var contactIdList = new List<int>();
            var contactIds = new List<string>();
            var templateFlag = false;
            var templateTitle = string.Empty; 
            var globalTemplateFlag = false; 

            if (submitCampaignButton.Equals("Create Campaign"))
            {
                #region Build Patients

                if (collection.GetValues("patient[]").Count() > 0)
                {
                    contactIds = collection.GetValues("patient[]").ToList<string>();
                    contactIdList = contactIds.Select(int.Parse).Distinct().ToList();
                }

                #endregion

                #region Build Campaign


                campaign.CampaignTitle = collection["NewCampaign.CampaignTitle"];
                campaign.CampaignMessage = collection["NewCampaign.CampaignTitle"];
                campaign.CampaignMessage = collection["NewCampaign.CampaignMessage"];
                campaign.GroupID = gId;
                string time = collection["SendDateTime"];

                campaign.SendDateTime = Convert.ToDateTime(time);

                var startDates = new List<DateTime>();
                var endDates = new List<DateTime>();
                var sendTimes = new List<DateTime>();
                string[] compare = new string[10];
                #endregion

                #region Build Procedures

                //build a list of procedures
                foreach (var key in collection.AllKeys)
                {
                    if (key.Contains("procedureTitle[]"))
                    {
                        procTitles = collection.GetValues(key);
                    }

                    if (key.Contains("procedureMessage[]"))
                    {
                        procMessages = collection.GetValues(key);
                    }

                    if (key.StartsWith("startDates"))
                    {
                        var date = collection.GetValue(key).RawValue;
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        startDates.Add(z);


                        //   startDates.Add(Convert.ToDateTime(date));
                    }

                    if (key.StartsWith("endDates"))
                    {
                        var date = collection.GetValue(key);
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        endDates.Add(z);

                    }

                    if (key.StartsWith("sendTimes"))
                    {
                        var date = collection.GetValue(key);
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        sendTimes.Add(z);

                    }
                }



                if (procTitles.ToString() != string.Empty || procMessages.ToString() != string.Empty)
                {
                    for (int x = 0; x <= procTitles.Length - 1; x++)
                    {
                        //build procedres
                        var proc = new Procedure();
                        proc.ProcedureTitle = procTitles[x];
                        proc.ProcedureMessage = procMessages[x];
                        proc.CampaignID = campaign.CampaignId;
                        proc.SendDateTime = campaign.SendDateTime;
                        proc.StartDate = Convert.ToDateTime(startDates[x]);
                        proc.EndDate = Convert.ToDateTime(endDates[x]);
                        proc.SendDateTime = Convert.ToDateTime(sendTimes[x]);
                        proc.CreatedDate = DateTime.Now;
                        proc.LastModifiedDate = DateTime.Now;


                        //save to List
                        procList.Add(proc);

                    }
                }
                #endregion

                #region BuildTemplates
                templateFlag = collection["templateFlag"].Contains("true");
                templateTitle = collection["templateTitle"];
                globalTemplateFlag = collection["globalTemplateFlag"].Contains("true");


                if (templateFlag == true && (templateTitle != string.Empty || templateTitle != ""))
                    CreateCampaign(campaign, _userId, gId, contactIdList, procList, templateTitle, globalTemplateFlag);
                else
                    CreateCampaign(campaign, _userId, gId, contactIdList, procList, "", globalTemplateFlag);

                #endregion

               return RedirectToAction("Manage", "Group", new { id = gId });
            }
            else
            {
                return RedirectToAction("Manage", "Group", new { id = gId });
            }
        }

        [HttpGet]
        public ActionResult CreateTemplateCampaign(int CampaignTemplateId = 0)
        {
            int _userId = WebSecurity.CurrentUserId;
            var model = new CreateTemplateCampaignModel();
            var list = new List<MyGroupListTable>();
          

            var campaignTemplate = _ctx.Templates.GetCampaignTemplate(CampaignTemplateId);
            model.Group = _ctx.Groups.Get(campaignTemplate.GroupID);

            if (Roles.GetRolesForUser().Contains("Administrator")) //Admin can get all contacts across application
            {

               model.AllContacts = _ctx.Contacts.GetAllList();

            }
            else//standard user can only see contacts from groups they are assigned to
            {
                //get list of groups user belongs to
                var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                var groupList = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);

                //Get Contacts for all those groups
                var contactsForUser = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);

                model.AllContacts = contactsForUser;
                model.Groups = groupList;
            }

            //create campaign values
            model.CampaignTemplate.CampaignTemplateTitle = campaignTemplate.CampaignTemplateTitle;
            model.CampaignTemplate.CampaignTemplateMessage = campaignTemplate.CampaignTemplateMessage;
            model.CampaignTemplate.SendDateTime = campaignTemplate.SendDateTime;

            var procTemplates = _ctx.Templates.GetAllProcedureTemplatesForCampaignTemplate(CampaignTemplateId);

            //Get procedureTemplates
            foreach (var template in procTemplates)
            {
                var tempHolder = new ProcedureTemplateValueModel();
                tempHolder.ProcedureTemplateTitle = template.ProcedureTemplateTitle;
                tempHolder.ProcedureTemplateMessage = template.ProcedureTemplateMessage;
                tempHolder.StartDate = template.StartDate;
                tempHolder.EndDate = template.EndDate;
                tempHolder.SendDateTime = template.SendDateTime;
                model.ProcedureTemplates.Add(tempHolder);
            }

           return View(model);
        }

        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost]
        public ActionResult CreateTemplateCampaign(string submitCampaignButton, Campaign campaign, FormCollection collection)
        {

            int _userId = WebSecurity.CurrentUserId;
            string[] procTitles = null;
            string[] procMessages = null;
            // string[] startDates = null;
            //string[] endDates = null;
            var procList = new List<Procedure>();
            var contactIdList = new List<int>();
            var contactIds = new List<string>();
            int gId = Convert.ToInt32(collection["gId"]);
            var templateFlag = false;
            var templateTitle = string.Empty;
            var globalTemplateFlag = false; 


            if (submitCampaignButton.Equals("Create Campaign"))
            {
                #region Build Patients

                if (collection.GetValues("patient[]").Count() > 0)
                {
                    contactIds = collection.GetValues("patient[]").ToList<string>();
                    contactIdList = contactIds.Select(int.Parse).Distinct().ToList();
                }

                #endregion

                #region Build Campaign


                campaign.CampaignTitle = collection["CampaignTemplate.CampaignTemplateTitle"];
                campaign.CampaignMessage = collection["CampaignTemplate.CampaignTemplateMessage"];
                string time = collection["SendDateTime"];

                campaign.SendDateTime = Convert.ToDateTime(time);
                campaign.GroupID = gId;

                #endregion

                #region Build Procedures

                var startDates = new List<DateTime>();
                var endDates = new List<DateTime>();
                var sendTimes = new List<DateTime>();
                string[] compare = new string[10];

                //build a list of procedures
                foreach (var key in collection.AllKeys)
                {
                    if (key.Contains("procedureTitle"))
                    {
                        procTitles = collection.GetValues(key);
                    }

                    if (key.Contains("procedureMessage"))
                    {
                        procMessages = collection.GetValues(key);
                    }

                    if (key.StartsWith("startDates"))
                    {
                        var date = collection.GetValue(key).RawValue;
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        startDates.Add(z);

                    }

                    if (key.StartsWith("endDates"))
                    {
                        var date = collection.GetValue(key);
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        endDates.Add(z);

                    }

                    if (key.StartsWith("sendTimes"))
                    {
                        var date = collection.GetValue(key);
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        sendTimes.Add(z);

                    }
                }



                if (procTitles.ToString() != string.Empty || procMessages.ToString() != string.Empty)
                {
                    for (int x = 0; x <= procTitles.Length - 1; x++)
                    {
                        //build procedres
                        var proc = new Procedure();
                        proc.ProcedureTitle = procTitles[x];
                        proc.ProcedureMessage = procMessages[x];
                        proc.CampaignID = campaign.CampaignId;
                        proc.SendDateTime = Convert.ToDateTime(sendTimes[x]);
                        proc.StartDate = Convert.ToDateTime(startDates[x]);
                        proc.EndDate = Convert.ToDateTime(endDates[x]);
                        proc.CreatedDate = DateTime.Now;
                        proc.LastModifiedDate = DateTime.Now;


                        //save to List
                        procList.Add(proc);

                    }
                }
                #endregion

                #region BuildTemplates
                templateFlag = collection["templateFlag"].Contains("true");
                templateTitle = collection["templateTitle"];
                globalTemplateFlag = collection["globalTemplateFlag"].Contains("true");


                if (templateFlag == true && (templateTitle != string.Empty || templateTitle != ""))
                    CreateCampaign(campaign, _userId, gId, contactIdList, procList, templateTitle, globalTemplateFlag);
                else
                    CreateCampaign(campaign, _userId, gId, contactIdList, procList, "", globalTemplateFlag);

                #endregion

                            

                return RedirectToAction("Manage", "Group", new { id = gId });
            }
            else
            {
                return RedirectToAction("Manage", "Group", new { id = gId });
            }
        }



       #endregion
        
        #region Chat Actions

        [HttpPost]
        public ActionResult SendMessage(int patientId, int groupId, string message)
        {
            //needed fields
            var sms = new SMSOutgoing();
            var chatMessage = new ChatMessage(); 
            var messages = new List<ChatMessage>();
            var model = new ChatMessageModel(); 
            var messagesModel = new List<ChatMessageModel>(); 
            var patientList = new List<Contact>();
            var patient = _ctx.Contacts.Get(patientId);
            var group = _ctx.Groups.Get(groupId);
            patientList.Add(patient);

            //build sms
            sms.ToPhoneNumber = patient.PhoneNumber;
            sms.FromPhoneNumber = group.PhoneNumber;
            sms.Message = message;

            //build chatMessage
            
            chatMessage.FromPhoneNumber = group.PhoneNumber;
            chatMessage.ToPhoneNumber = patient.PhoneNumber;
            chatMessage.Message = message;
            chatMessage.Type = false;
            chatMessage.CreatedDate = DateTime.Now; 
           

            // send sms for chat message                         
            _ctx.SMSmessages.sendSMS(sms, patientList);

            //add chat message to database
            _ctx.SMSmessages.AddMessage(chatMessage);           

            //get new list of messages, build model for disaplay and return
            messages = _ctx.SMSmessages.GetAllChatMessages().FindAll(p => p.ToPhoneNumber == patient.PhoneNumber || p.FromPhoneNumber == patient.PhoneNumber).FindAll(p => p.ToPhoneNumber == group.PhoneNumber || p.FromPhoneNumber == group.PhoneNumber);

            messagesModel = model.BuildModel(messages, patient.FirstName + " " + patient.LastName, group.CompanyName);
            
            return Json(messagesModel);

        }

        [HttpGet]
        public ActionResult GetMessages(int patientId, int groupId)
        {
            var messages = new List<ChatMessage>();
            var model = new ChatMessageModel();
            var messagesModel = new List<ChatMessageModel>(); 

            var patient = _ctx.Contacts.Get(patientId);
            var group = _ctx.Groups.Get(groupId);

           messages = _ctx.SMSmessages.GetAllChatMessages().FindAll(p => p.ToPhoneNumber == patient.PhoneNumber  || p.FromPhoneNumber == patient.PhoneNumber).FindAll(p => p.ToPhoneNumber == group.PhoneNumber || p.FromPhoneNumber == group.PhoneNumber && p.CreatedDate.Date >= DateTime.Now.AddDays(-7));

           messagesModel = model.BuildModel(messages, patient.FirstName + " " + patient.LastName, group.CompanyName);

           return Json(messagesModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetGroupMessages(int groupId)
        {
            var messages = new List<ChatMessage>();
            var model = new ChatMessageModel();
            var messagesModel = new List<ChatMessageModel>();

            ///TODO: Connection is closed cauing javascript 500 error 
            var group = _ctx.Groups.Get(groupId);

            messages = _ctx.SMSmessages.GetAllChatMessages().FindAll(p => p.Processed == false && p.Type == true);

            //mark each message as read
            _ctx.SMSmessages.ProcessMessages(messages);

            messages.FindAll(p => p.ToPhoneNumber == group.PhoneNumber && p.CreatedDate.Date >= DateTime.Now.AddDays(-7));

            return Json(messages, JsonRequestBehavior.AllowGet);
        }


        




        #endregion 

       
    }


    public class GroupModel
    {
        private DataManager _ctx = DataManager.Instance;

        public List<Group> Groups { get; set; }
        public List<GroupObject> GroupObjects { get; set; }
        public Group Group { get; set; }
        public List<Contact> AllContacts { get; set; }
        public List<Contact> ContactSelected { get; set; }
        public List<Campaign> Campaigns { get; set; }
        public List<Triage> Triages { get; set; }
        public List<Procedure> Procedures { get; set; }
        public Campaign NewCampaign { get; set; }
        public Procedure NewProcedure { get; set; }
        public Contact NewContact { get; set; }
        public Triage NewTriage { get; set; }


        public GroupModel()
        {
            this.Group = new Group();
            this.GroupObjects = new List<GroupObject>();
            this.AllContacts = new List<Contact>();
            this.ContactSelected = new List<Contact>();
            this.Triages = new List<Triage>();
            this.Procedures = new List<Procedure>();
            this.NewCampaign = new Campaign();
            this.NewProcedure = new Procedure();
            this.NewContact = new Contact();
            this.NewTriage = new Triage(); 
        }

        public GroupModel BuildIndexData()
        {
            var model = new GroupModel();
            model.Groups = new List<Group>();


            int _userId = WebSecurity.CurrentUserId;
            if (Roles.GetRolesForUser().Contains("Administrator"))
            {
                model.Groups = _ctx.Groups.GetAllGroups();

                foreach (var group in model.Groups)
                {
                    var grpObj = new GroupObject();
                    grpObj.GroupId = group.GroupId;
                    grpObj.CompanyName = group.CompanyName;
                    grpObj.Email = group.Email;
                    grpObj.PhoneNumber = group.PhoneNumber;
                    grpObj.CreatedDate = group.CreatedDate;
                    grpObj.LastModifiedDate = group.LastModifiedDate;
                    model.GroupObjects.Add(grpObj);
                }

            }

            else
            {
                var groups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);

                foreach (var userGroup in groups)
                {
                    var group = _ctx.Groups.Get(userGroup.GroupId);
                    model.Groups.Add(group);
                }

                foreach (var group in model.Groups)
                {
                    var grpObj = new GroupObject();
                    grpObj.GroupId = group.GroupId;
                    grpObj.CompanyName = group.CompanyName;
                    grpObj.Email = group.Email;
                    grpObj.PhoneNumber = group.PhoneNumber;
                    grpObj.CreatedDate = group.CreatedDate;
                    grpObj.LastModifiedDate = group.LastModifiedDate;
                    model.GroupObjects.Add(grpObj);
                }

            }

            return model;
        }

        public GroupModel BuildManageData()
        {
            return new GroupModel(); 
        }

    }

  


}
