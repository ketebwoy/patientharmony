﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PatientHarmony.Web.Logging.NLog;
using PatientHarmony.Data.Managers;
using PatientHarmony.Data.Entities;
using PatientHarmony.Web.Filters;
using PatientHarmony.Data.Models;
using WebMatrix.WebData;
using System.Globalization;
using System.Web.Security;
using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Enums;
using System.Drawing;
using Point = DotNet.Highcharts.Options.Point;
using DotNet.Highcharts.Helpers;



namespace PatientHarmony.Web.Controllers
{
    [InitializeSimpleMembership]
    public class HomeController : Controller
    {
        NLogLogger logger = new NLogLogger();
        private DataManager _ctx = DataManager.Instance;
        
        public ActionResult Index()
        {
            int _userId = WebSecurity.CurrentUserId;
            var indexModel = new HomeIndexModel();

            indexModel.smsIncomingList = new List<SMSIncoming>();
            indexModel.smsOutgoingList = new List<SMSOutgoing>();
            
            indexModel.indexContacts = new List<Contact>(); 

            var tempList = _ctx.Triages.GetAllTriage(); 
            if (Roles.GetRolesForUser(WebSecurity.CurrentUserName).Contains("Administrator"))
            {
                //Get Contacts and group entities for user
                indexModel.indexContacts = _ctx.Contacts.GetAllList();
                indexModel.triages = tempList; 
                indexModel.smsIncomingList = _ctx.SMSmessages.GetAllincoming();
                indexModel.smsOutgoingList = _ctx.SMSmessages.GetAllOutgoing();
                indexModel.patientsImproving = CalculateAmountImproving(indexModel.triages);
                indexModel.patientsNotImproving = CalculateAmountNotImproving(indexModel.triages);
                indexModel.patientReplyRate = CalculateReplyRate(indexModel.smsOutgoingList, indexModel.smsIncomingList);
                                
            }   
            
            if (Roles.GetRolesForUser().Contains("Standard"))
            {
                //get list of groups user belongs to
                var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                var groupList = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);

                //Get Contacts and group entities for user
                indexModel.indexContacts = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);
                indexModel.smsIncomingList = _ctx.SMSmessages.GetAllIncomingForUser(userGroups);
                indexModel.smsOutgoingList = _ctx.SMSmessages.GetAllOutgoingForUser(userGroups);

                
                foreach (var group in groupList)
                {
                    indexModel.triages.AddRange(tempList.Where(p => p.GroupID == group.GroupId).ToList<Triage>());
                }
                
            }
           
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            #region Column chart
           
            var sendArray = indexModel.smsOutgoingList
                            .Select(x => x.CreatedDate)
                            .OrderByDescending ( s => s)
                            .ToArray(); 

            var recievedArray = indexModel.smsIncomingList
                            .Select(x => x.CreatedDate)
                            .OrderByDescending ( s => s)
                            .ToArray();

            var sendOBject = new object[12];
            var recObject = new object[12];

            
            
                for (int i = 1; i <= DateTime.Parse("December 1, 2013").Month; i++)
                {
                    if (sendArray.Length > 0)
                    {
                        var sendCount = sendArray.Where(x => x.Month == i).Count();
                        sendOBject[i-1] = sendCount;
                    }

                    if (recievedArray.Length > 0)
                    {
                        var recCount = recievedArray.Where(x => x.Month == i).Count();
                        recObject[i-1] = recCount;
                    }
                }
            

          
                 Highcharts chart = new Highcharts("chart")
                .InitChart(new Chart { DefaultSeriesType = ChartTypes.Column, ZoomType = ZoomTypes.Xy })
                .SetTitle(new Title { Text = "SMS Usage" })
                .SetSubtitle(new Subtitle { Text = "Patient Harmony DB " + DateTime.Today.Year })
                .SetXAxis(new XAxis { Categories = new[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" } })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = "Num SMS sent" },
                    })
                //.SetTooltip(new Tooltip { Formatter = "function() {return ''+ this.x +' cm, '+ this.y +' kg'; }" })
                .SetLegend(new Legend
                {
                    Layout = Layouts.Vertical,
                    Align = HorizontalAligns.Left,
                    VerticalAlign = VerticalAligns.Top,
                    X = 100,
                    Y = 50,
                    Floating = true,
                   // BackgroundColor = System.Drawing.ColorTranslator.FromHtml("#FFF"),
                   Shadow = true,
                   BorderWidth = 1
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        PointPadding = 0.2,
                        BorderWidth = 0
                    }
                })
                
     
                .SetSeries(new[]
                {
                    new Series
                    {
                        Name = "Sent SMS",
                         Data = new DotNet.Highcharts.Helpers.Data(sendOBject)
                        //Data = new DotNet.Highcharts.Helpers.Data(new object[] {sendArray}.ToArray())
                    }, 
                    new Series
                    {
                        Name = "Recieved SMS", 
                         Data = new DotNet.Highcharts.Helpers.Data(recObject)
                        //Data = new DotNet.Highcharts.Helpers.Data(new object[] {recievedArray}.ToArray() )
                    }
                });

            #endregion

            #region Pie Chart
                 Highcharts pieChart = new Highcharts("pieChart")
                    .InitChart(new Chart { PlotShadow = false })
                    .SetTitle(new Title { Text = "Patient Harmony Patient Health Stats" })
                    .SetTooltip(new Tooltip { Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %'; }" })
                    .SetPlotOptions(new PlotOptions
                    {
                        Pie = new PlotOptionsPie
                        {
                            AllowPointSelect = true,
                            Cursor = Cursors.Pointer,
                            DataLabels = new PlotOptionsPieDataLabels
                            {
                                Color = ColorTranslator.FromHtml("#000000"),
                                ConnectorColor = ColorTranslator.FromHtml("#000000"),
                                Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %'; }"
                            }
                        }
                    })
                    .SetSeries(new Series
                    {
                        Type = ChartTypes.Pie,
                        Name = "Browser share",
                        Data = new DotNet.Highcharts.Helpers.Data(new object[]
                                               {
                                                   new object[] { "Patients Improving", indexModel.patientsImproving },
                                                   new object[] { "Patients on Decline ", indexModel.patientsNotImproving },
                                                                                                     
                                               })
                    });

            #endregion

                 #region Reply Pie Chart
                 Highcharts pieReplyChart = new Highcharts("pieReplyChart")
                    .InitChart(new Chart { PlotShadow = false })
                    .SetTitle(new Title { Text = "Patient Harmony Patient Reply Stats" })
                    .SetTooltip(new Tooltip { Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %'; }" })
                    .SetPlotOptions(new PlotOptions
                    {
                        Pie = new PlotOptionsPie
                        {
                            AllowPointSelect = true,
                            Cursor = Cursors.Pointer,
                            DataLabels = new PlotOptionsPieDataLabels
                            {
                                Color = ColorTranslator.FromHtml("#000000"),
                                ConnectorColor = ColorTranslator.FromHtml("#000000"),
                                Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage.toFixed(2) +' %'; }"
                            }
                        }
                    })
                    .SetSeries(new Series
                    {
                        Type = ChartTypes.Pie,
                        Name = "Browser share",
                        Data = new DotNet.Highcharts.Helpers.Data(new object[]
                                               {
                                                   new object[] { "Patients Compliance Rate", indexModel.patientReplyRate },
                                                   new object[] { "Patients Ignore Rate ", 100 - indexModel.patientReplyRate},
                                                                                                     
                                               })
                    });

    
            #endregion


                 ViewBag.ColumnChart = chart;
                 ViewBag.PieChart = pieChart;
                 ViewBag.PieReplyRateChart = pieReplyChart;

            return View(indexModel);
        }

      
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        private int CalculateAmountImproving(List<Triage> triages)
        {
            int x = 0;
            var lastRating = 0;  
            var dayBeforeRating = 0;

            foreach (var triage in triages)
            {


                if (triage.HealthTracker != null)
                {
                    var healthRecord = triage.HealthTracker.Trim().ToCharArray();
                    var recordLength = healthRecord.Length;
                    for (var i = recordLength; i > recordLength - 1; i--)
                    {
                        if (i == recordLength - 1 && recordLength >= 1)
                        {
                            if (healthRecord[recordLength - 2].Equals('*'))
                            {
                                dayBeforeRating = 10;
                            }
                            else
                            {
                                var result1 = healthRecord[recordLength - 2];
                                string result1conv = ((char)result1).ToString();
                                dayBeforeRating = Convert.ToInt32(result1);
                            }
                        }


                        if (i == recordLength && recordLength >= 1)
                        {
                            if (healthRecord[recordLength - 1].Equals('*'))
                            {
                                lastRating = 10;
                            }
                            else
                            {
                                var result2 = (int)healthRecord[recordLength - 1];
                                string result2conv = ((char)result2).ToString();
                                lastRating = Convert.ToInt32(result2conv);
                            }
                        }

                            var result = compareHealth(dayBeforeRating, lastRating, healthRecord);

                            if (!result) x++;
                        }
                    }
                }
            return x; 
        }

        private int CalculateAmountNotImproving(List<Triage> triages)
        {
            int x = 0;
            var lastRating = 0;
            var dayBeforeRating = 0;

            foreach (var triage in triages)
            {
                if(triage.HealthTracker != null)
                {
                    var healthRecord = triage.HealthTracker.ToCharArray();
                    var recordLength = healthRecord.Length;
                    for (var i = recordLength; i > recordLength - 1; i--)
                    {
                        if (i == recordLength - 1)
                        {
                            if (healthRecord[recordLength - 2].Equals('*'))
                            {
                                dayBeforeRating = 10;
                            }
                            else
                            {
                                var result1 = healthRecord[recordLength - 2];
                                string result1conv = ((char)result1).ToString();
                                dayBeforeRating = Convert.ToInt32(result1);
                            }
                        }
                        if (i == recordLength)
                        {
                            if (healthRecord[recordLength - 1].Equals('*'))
                            {
                                lastRating = 10;
                            }
                            else
                            {
                                var result2 = (int)healthRecord[recordLength - 1];
                                string result2conv = ((char)result2).ToString();
                                lastRating = Convert.ToInt32(result2conv);
                            }
                        }

                        var result = compareHealth(dayBeforeRating, lastRating, healthRecord);

                        if (result) x++;
                    }
                }
            }

            return x;
        }

        private int CalculateReplyRate(List<SMSOutgoing> outGoings, List<SMSIncoming> inComings)
        {
            int percentage = 0;
            int seperateOuts = 0;
            int seperateIns = 0; 
            
            if(outGoings != null)
            {
             seperateOuts = outGoings.Where(x => (x.CreatedDate > x.CreatedDate.AddDays(-3) && x.CreatedDate < x.CreatedDate.AddDays(3) && x.TriageID > 0)).GroupBy(x => new { x.ToPhoneNumber, x.TriageID }).Count();
            }


            if (inComings != null)
            {
                seperateIns = inComings.Where(x => (x.CreatedDate > x.CreatedDate.AddDays(-3) && x.CreatedDate < x.CreatedDate.AddDays(3))).GroupBy(x => new { x.FromPhoneNumber, x.ToPhoneNumber }).Count();
            }


            if(seperateOuts > 0 && seperateIns > 0)
            percentage = seperateIns * 100 / seperateOuts;

            return percentage; 
        }

     
        /* compare sms health status to health status on record */
        private bool compareHealth(int oldStatus, int newStatus, char[] healthRecord = null)
        {
            bool result = false;
            var intList = new List<int>();
            int numParse;

            if (healthRecord.Length > 0)
            {
                foreach (var record in healthRecord)
                {
                    if (Int32.TryParse(record.ToString(), out numParse))
                    {
                        intList.Add(numParse);
                    }
                    else if (record.Equals('*'))
                    {
                        intList.Add(10);
                    }
                }

            }

            if (intList.Count > 0)
            {
                if (oldStatus > newStatus || intList.Max() - newStatus >= 3)
                    result = true;
            }
            return result;
        }

    }
}
