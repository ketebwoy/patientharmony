﻿using PatientHarmony.Data.Managers;
using PatientHarmony.Data.Entities; 
using PatientHarmony.Web.Logging.NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Twilio.Mvc;
using Twilio.TwiML;
using Twilio.TwiML.Mvc; 

namespace PatientHarmony.Web.Controllers
{
    public class PhoneCallController : TwilioController
    {
        NLogLogger logger = new NLogLogger();
        private DataManager _ctx = DataManager.Instance;
        private Triage triage = new Triage(); 
        private Campaign campaign = new Campaign(); 
        private TriageFollowUp fu = new TriageFollowUp();
        private Procedure procedure = new Procedure(); 
        //
        // GET: /PhoneCall/

        public ActionResult CallHandler(VoiceRequest request)
        {
            var patient = _ctx.Contacts.GetAllList().First(p => p.PhoneNumber == request.To); 
            var group = _ctx.Groups.GetAllGroups().First(p => p.PhoneNumber == request.From); 
            var requestObject = _ctx.VoiceCalls.GetAllCalls().FirstOrDefault(p => p.ToPhoneNumber == request.To && p.CallInitiated == true && p.CallEnded == false && p.CallStartDate.Date == DateTime.Now.Date);
            var response = new TwilioResponse();

            if(requestObject != null)
            {
                var requestType = requestObject.Type;
                response.Say(string.Format("Hello {0} {1} this is {2}", patient.FirstName, patient.LastName, group.CompanyName));
                
                
                switch(requestType)
                {
                    case ObjectType.Campaign:
                        {
                            campaign = _ctx.Campaigns.GetCampaign(requestObject.ObjectId);
                            response.Redirect("http://my.patientharmony.com/PhoneCall/CampaignHandler");
                            break; 
                        }
                    case ObjectType.Procedure:
                        {
                            procedure = _ctx.Procedures.Get(requestObject.ObjectId);
                            response.Redirect("http://my.patientharmony.com/PhoneCall/ProcedureHandler");
                            break; 
                        }
                    case ObjectType.Triage:
                        {
                            triage = _ctx.Triages.GetTriage(requestObject.ObjectId);
                            response.Redirect("http://my.patientharmony.com/PhoneCall/TriageHandler");
                            break; 
                        }
                    case ObjectType.TriageFollowUp:
                        {
                            fu = _ctx.TriageFollowUps.Get(requestObject.ObjectId);
                            response.Redirect("http://my.patientharmony.com/PhoneCall/FollowUpHandler");
                            break; 
                        }
                }

            }

            else
            {
               response.Hangup();
                               
            }

            return TwiML(response);
        }

       
        public ActionResult CampaignHandler()
        {
            var response = new TwilioResponse();

            response.Say(campaign.CampaignMessage);
            response.Hangup(); 
            Console.WriteLine("Executed phone call for Campaign: " + campaign.CampaignTitle);
            return TwiML(response);
        }

        public ActionResult ProcedureHandler()
        {
            var response = new TwilioResponse();

            response.Say(procedure.ProcedureMessage);
            response.Hangup();
            Console.WriteLine("Executed phone call for procedure: " + procedure.ProcedureTitle);
            return TwiML(response);
        }

        public ActionResult TriageHandler()
        {
            var response = new TwilioResponse();

            response.BeginGather(new { action = "http://my.patientharmony.com/PhoneCall/TriageResponseHandler" });
            response.Say(triage.TriageMessage);
            response.EndGather();
            Console.WriteLine("Executed Phonecall for Triage: " + triage.Title);
            return TwiML(response);
        }

        public ActionResult FollowUpHandler()
        {
            var response = new TwilioResponse();

            response.BeginGather(new { action = "http://my.patientharmony.com/PhoneCall/FollowUpResponseHandler" });
            response.Say(fu.TriageFollowUpMessage);
            response.EndGather();
            Console.WriteLine("Executed Phonecall for FollowUp: " + fu.Title);
            return TwiML(response);
        }

        public ActionResult TriageResponseHandler(string digits)
        {
            var defaultDate = Convert.ToDateTime("1753-01-01 00:00:00");
            int x = 0;

            var response = new TwilioResponse();

            var scaleType = triage.TriageScaleType;
            var triggerPoint = triage.TriggerPoint;
            var healthRecord = triage.HealthTracker;
            var entry = 0;
            bool digitResult = Int32.TryParse(digits, out entry);

            
            int oldHealthStatus = 0;


            if ((scaleType == TriageScaleType.Ascending || scaleType == TriageScaleType.Descending) && (!digitResult && !digits.Contains("*")))
            {
                response.BeginGather(new { action = "http://my.patientharmony.com/PhoneCall/TriageResponseHandler" });
                response.Say("Please Enter a number 1 through 9 or * for 10");
                response.EndGather();
            }

            if(scaleType == TriageScaleType.Decision && (!digitResult || (digitResult && entry > 2)))
            {
                response.BeginGather(new { action = "http://my.patientharmony.com/PhoneCall/TriageResponseHandler" });
                response.Say("Please Enter 1 for Yes or 2 for No ");
                response.EndGather();
            }
            else
            {


                var triageResult = _ctx.SMSmessages.compareHealth(oldHealthStatus, entry, triggerPoint, scaleType, healthRecord.ToCharArray());

                //get health stats 
                if (!String.IsNullOrEmpty(triage.HealthTracker.Trim()))
                {
                    var currentHealthStatus = triage.HealthTracker.ToCharArray().Last();

                    if (currentHealthStatus == '*')
                    {
                        oldHealthStatus = 10;
                    }
                    else
                        oldHealthStatus = Int32.Parse(currentHealthStatus.ToString());
                }

                switch (scaleType)
                {
                    case TriageScaleType.Ascending:
                        {
                            if (!triageResult)
                            {
                                response.Say(triage.GoodResponseMessage);
                                _ctx.SMSmessages.addToHealthRecord(triage, entry);
                                _ctx.SMSmessages.processTriages(triage, triageResult);
                            }
                            else
                            {
                                response.Say(triage.BadResponseMessage);
                                _ctx.SMSmessages.addToHealthRecord(triage, entry);
                                _ctx.SMSmessages.processTriages(triage, triageResult);
                            }
                            break;
                        }

                    case TriageScaleType.Descending:
                        {
                            if (triageResult)
                            {
                                response.Say(triage.GoodResponseMessage);
                                _ctx.SMSmessages.addToHealthRecord(triage, entry);
                                _ctx.SMSmessages.processTriages(triage, triageResult);
                            }
                            else
                            {
                                response.Say(triage.BadResponseMessage);
                                _ctx.SMSmessages.addToHealthRecord(triage, entry);
                                _ctx.SMSmessages.processTriages(triage, triageResult);
                            }
                            break;
                        }

                    case TriageScaleType.Decision:
                        {
                            if (entry == 1)
                            {
                                response.Say(triage.GoodResponseMessage);
                                _ctx.SMSmessages.addToHealthRecord(triage, entry);
                                _ctx.SMSmessages.processTriages(triage, triageResult);
                            }
                            if (entry == 2)
                            {
                                response.Say(triage.BadResponseMessage);
                                _ctx.SMSmessages.addToHealthRecord(triage, entry);
                                _ctx.SMSmessages.processTriages(triage, triageResult);
                            }
                           
                            break;
                        }
                }
            }
            

            return TwiML(response); 
        }

        public ActionResult FollowUpResponseHandler(string digits)
        {
            var response = new TwilioResponse();

            return TwiML(response);
        }

       

    }
}
