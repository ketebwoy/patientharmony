﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PatientHarmony.Data.Managers;
using PatientHarmony.Data.Entities;
using PatientHarmony.Web.Filters;
using PatientHarmony.Web.Models;
using WebMatrix.WebData;
using System.Globalization;
using System.Web.Security;
using PatientHarmony.Web.Logging.NLog;
using PatientHarmony.Web.Models;
using Telerik.Web.Mvc;
using Telerik.Web.Mvc.UI;
using System.ComponentModel.DataAnnotations;


namespace PatientHarmony.Web.Controllers
{
    public class ReportController : Controller
    {
        private DataManager _ctx = DataManager.Instance;
        private NLogLogger logger = new NLogLogger();
        private ReportsIndexModel reportData = new ReportsIndexModel(); 

        //
        // GET: /Report/

        public ActionResult Index()
        {
           int _userId = WebSecurity.CurrentUserId;

            var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
            
            var reportModel = new ReportsIndexModel();

            reportModel = reportModel.BuildIndexModel(_userId, userGroups);
            reportData = reportModel;

            return View(reportModel);
        }

        public ActionResult CampaignReport(int campID = 0)
        {
            var model = new ReportsIndexModel();
            var campaign = _ctx.Campaigns.GetCampaign(campID); 
            var group = _ctx.Groups.Get(campaign.GroupID);

            model.Groups.Add(group);

            model.Campaigns.Add(campaign);

            model.Procedures.AddRange(_ctx.Procedures.GetAllProceduresForCampaign(campID));

            model.Patients.AddRange(_ctx.ManyToManyRelationShips.GetAllContactsForCampaign(campID));

            return new RazorPDF.PdfResult(model, "CampaignReport");
        }

        public ActionResult TriageReport(int triageID)
        {
            var model = new ReportsIndexModel();
            var triage = _ctx.Triages.GetTriage(triageID);
            var group = _ctx.Groups.Get(triage.GroupID);

            model.Groups.Add(group);

            model.Triages.Add(triage);

            model.Patients.Add(_ctx.Contacts.Get(triage.ContactID));

            model.SmsIncomings.AddRange(_ctx.SMSmessages.GetAllincoming().FindAll(p => p.TriageID == triageID));

            return new RazorPDF.PdfResult(model, "TriageReport");
        }

        public ActionResult GroupReport(int groupID)
        {
            var model = new ReportsIndexModel();
            
            var group = _ctx.Groups.Get(groupID);

            model.Groups.Add(group);

            model.Campaigns.AddRange(_ctx.Campaigns.GetAllList().FindAll(p => p.GroupID == groupID));

            model.Triages.AddRange(_ctx.Triages.GetTriagesForGroup(groupID));

            model.Patients.AddRange(_ctx.ManyToManyRelationShips.GetAllContactsForGroup(groupID));

            model.SmsIncomings.AddRange(_ctx.SMSmessages.GetAllincoming().FindAll(p => p.ToPhoneNumber == group.PhoneNumber));

            model.SmsOutgoings.AddRange(_ctx.SMSmessages.GetAllOutgoing().FindAll(p => p.FromPhoneNumber == group.PhoneNumber));

            return new RazorPDF.PdfResult(model, "GroupReport");
        }

        public ActionResult PatientReport(int ContactId)
        {
            var model = new ReportsIndexModel();
            var patient = _ctx.Contacts.Get(ContactId);
            var groups = _ctx.ManyToManyRelationShips.GetAllGroupsForContact(ContactId);
            var triages = _ctx.Triages.GetAllTriage().FindAll(p => p.ContactID == ContactId);

            model.Patients.Add(patient);
            model.Groups.AddRange(groups);

            model.Triages.AddRange(triages);

            foreach(var triage in model.Triages)
            {
                triage.Group = _ctx.Groups.Get(triage.GroupID);
            }

            model.SmsIncomings.AddRange(_ctx.SMSmessages.GetAllincoming().FindAll(p => p.FromPhoneNumber == patient.PhoneNumber));
            model.SmsOutgoings.AddRange(_ctx.SMSmessages.GetAllOutgoing().FindAll(p => p.ToPhoneNumber == patient.PhoneNumber));

            return new RazorPDF.PdfResult(model, "PatientReport");
        }

    }


}
