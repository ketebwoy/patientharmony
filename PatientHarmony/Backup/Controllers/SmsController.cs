﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PatientHarmony.Web.OverRides;
using WebMatrix.WebData;
using System.Web.Security;
using Telerik.Web.Mvc;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Managers;
using PatientHarmony.Web.Models;
using PatientHarmony.Web.Filters;
using PhoneNumbers;

namespace PatientHarmony.Web.Controllers
{
    [InitializeSimpleMembership]
    public class SmsController : Controller
    {
        private DataManager _ctx = DataManager.Instance;

        [ValidateRequest("327479b3f55f99741637618b8d737df0")]
        [HttpPost]
        public ActionResult IncomingSMS(string From, string To, string Body)
        {
            var util = PhoneNumberUtil.GetInstance();
            var ToNumber = util.Parse(To, "US").NationalNumber.ToString();
            var FromNumber = util.Parse(From, "US").NationalNumber.ToString();

            var patient = _ctx.Contacts.GetAllList().First(p => p.PhoneNumber.Equals(FromNumber));
            var patientList = new List<Contact>();
            patientList.Add(patient); 
            var group =  _ctx.Groups.GetAllGroups().First(p => p.PhoneNumber.Equals(ToNumber));

            var today = DateTime.Now;
            var newFollowUpDefaultDate = Convert.ToDateTime("1753-01-01 00:00:00");


            //build incoming sms entity
            var incoming = new SMSIncoming();
            incoming.Message = Body;
            incoming.CreatedDate = DateTime.Now;
            incoming.FromPhoneNumber = FromNumber.ToString();
            incoming.ToPhoneNumber = ToNumber.ToString();

            var openUnfinishedTriagesNotProcessedToday = _ctx.Triages.GetAllTriage().FindAll(p => p.ContactID == patient.ContactId && p.GroupID == group.GroupId);
                openUnfinishedTriagesNotProcessedToday = openUnfinishedTriagesNotProcessedToday.FindAll(p=> p.LastDateProcessed < today && p.EndMarker == false);
                openUnfinishedTriagesNotProcessedToday = openUnfinishedTriagesNotProcessedToday.FindAll(p => p.EndDate.Date >= today.Date);

            var openFollowupsunporocessed = _ctx.TriageFollowUps.GetAllTriageFollowUpList().FindAll(p => p.ContactID == patient.ContactId
                                                                                                        && p.ProcessedDate.Date < today.Date && p.EndMarker == false && p.EndDate.Date >= today.Date);  
                                                                                                        

            #region StopMessages

            //if text includes 'stop' cancel all text messages to go out
            if(incoming.Message.ToLower().Contains("stop"))
            {
                //cancel campaigns and procedures
                var campaings = _ctx.ManyToManyRelationShips.GetAllCampaignForContact(patient.ContactId).FindAll(p => p.DateSMSProcessed.Date >= today.Date || p.DateSMSProcessed == newFollowUpDefaultDate);
               
                foreach(var c in campaings)
                {
                    _ctx.ManyToManyRelationShips.DeleteContactFromCampaign(patient.ContactId);
                }

                //cancel triages and followups
                var triages = _ctx.Triages.GetAllTriage().FindAll(p => p.ContactID == patient.ContactId && p.GroupID == group.GroupId && p.EndMarker == false);
                foreach(var t in triages)
                {
                    t.EndMarker = true;
                    _ctx.Triages.Update(t);

                    //cancel followups 
                    var followUpsFuture = _ctx.TriageFollowUps.GetAllFollowUpsForTriage(t.TriageId).FindAll(p => p.EndMarker == false);
                     foreach(var f in followUpsFuture)
                     {
                         f.EndMarker = true; 
                         f.ProcessedDate = today; 
                         _ctx.TriageFollowUps.UpdateFollowUp(f);
                     }

                }

                //process response
                incoming.processed = true; 
                _ctx.SMSmessages.Addincoming(incoming);

                //form reply letting them know they have been unsuscribed
                var sms = new SMSOutgoing();
                sms.Message = "You have been unsuscribed from future communication";
                sms.FromPhoneNumber = group.PhoneNumber;
                _ctx.SMSmessages.sendSMS(sms, patientList);
                

               
            }

            #endregion 

            #region AddSMS

            if (openUnfinishedTriagesNotProcessedToday.Count > 0  || openUnfinishedTriagesNotProcessedToday.Count > 0)
            {
                //process response
                _ctx.SMSmessages.Addincoming(incoming);
            }
            else //if no open unfinished triages then process as chatMessage
            {
                //process response
                incoming.processed = true;
                _ctx.SMSmessages.Addincoming(incoming);
                
                //build chatMessage object
                var chatMessage = new ChatMessage();
                chatMessage.FromPhoneNumber = patient.PhoneNumber;
                chatMessage.ToPhoneNumber = group.PhoneNumber;
                chatMessage.Message = incoming.Message;
                chatMessage.Type = true;
                chatMessage.CreatedDate = DateTime.Now;
                chatMessage.Processed = false; 
                incoming.processed = true;

                _ctx.SMSmessages.AddMessage(chatMessage);
            }

            #endregion 
            
            return null;
        }

     

        //
        // GET: /Sms/
        [Authorize(Roles = "Administrator, Standard")]
        public ActionResult Index(SMSIndexModel model = null)
        {
             int _userId = WebSecurity.CurrentUserId;
             var _groups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
             var contacts = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(_groups);

             if (model.Incoming.Count <= 0 && model.Outgoing.Count <= 0 && model.Emails.Count <= 0)
             {
                 model = new SMSIndexModel();

                 if (Roles.GetRolesForUser().Contains("Administrator")) //Admin can get all emails and sms
                 {
                     model.Emails = _ctx.EmailMessages.GetAllEmails();
                     model.Incoming = _ctx.SMSmessages.GetAllincoming();
                     model.Outgoing = _ctx.SMSmessages.GetAllOutgoing(); 
                 }
                 else
                 {

                     model.Incoming = _ctx.SMSmessages.GetAllIncomingForUser(_groups);
                     model.Outgoing = _ctx.SMSmessages.GetAllOutgoingForUser(_groups);
                     model.Emails = new List<Email>();

                     var emailCollection = _ctx.EmailMessages.GetAllEmails();

                     if (emailCollection.Count > 0)
                     {
                         foreach (var group in _groups)
                         {
                             var groupSinglet = _ctx.Groups.Get(group.GroupId);

                             model.Emails.AddRange(emailCollection.FindAll(p => p.GroupID == groupSinglet.GroupId));

                         }
                     }
                 }
             }
            
            return View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult EmailAdministrator(SMSIndexModel email, FormCollection collection)
        {
            int _userId = WebSecurity.CurrentUserId;
            var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
            var contacts = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);


            if (email.Incoming.Count <= 0 && email.Outgoing.Count <= 0 && email.Emails.Count <= 0)
            {
                email = new SMSIndexModel();

                email.Incoming = _ctx.SMSmessages.GetAllIncomingForUser(userGroups);
                email.Outgoing = _ctx.SMSmessages.GetAllOutgoingForUser(userGroups);
                email.Emails = new List<Email>();

                var emailCollection = _ctx.EmailMessages.GetAllEmails();

                if (emailCollection.Count > 0)
                {
                    foreach (var group in userGroups)
                    {
                        var groupSinglet = _ctx.Groups.Get(group.GroupId);
                        email.Emails.AddRange(emailCollection.FindAll(p => p.GroupID == groupSinglet.GroupId));


                    }
                }
            }


            var emailTemplate = new Email();

            emailTemplate.Subject = collection["Subject"];
            emailTemplate.Message = collection["Message"];
            emailTemplate.ToEmail = "PatientHarmonyAdmin@patientharmony.com";
            
            
            var defaultGroup = _ctx.ManyToManyRelationShips.GetDefaultGroup(_userId);
            
            emailTemplate.FromEmail = defaultGroup.Email;
            emailTemplate.GroupID = defaultGroup.GroupId;
            emailTemplate.EmailId = _ctx.EmailMessages.EmailAdmin(emailTemplate);

            email.Emails.Add(emailTemplate);


            return View(new GridModel(email.Emails));
        }

    }
}
