﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using System.Web.Security;
using PatientHarmony.Data.Managers;
using PatientHarmony.Web.Logging.NLog;
using PatientHarmony.Data.Entities;
using PatientHarmony.Web.Models;
using PatientHarmony.Web.Filters;
using PatientHarmony.Data.Context;
using Telerik.Web.Mvc;

namespace PatientHarmony.Web.Controllers
{
    [InitializeSimpleMembership]
    public class ContactController : Controller
    {
        private DataManager _ctx = DataManager.Instance;
        private NLogLogger logger = new NLogLogger();

        #region Index and Manage Views 
       
        //
        // GET: /Contact/
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Index()
        {
            int _userId = WebSecurity.CurrentUserId;

            var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
            var model = new ContactIndexModel();
            model.Patients = new List<Contact>(); 
          
            if (Roles.GetRolesForUser().Contains("Administrator"))
            {
                 model.Patients = _ctx.Contacts.GetAllList();
                 return View(model);
            }
            
            else
            {
                var groups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);

                var patients = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(groups);

                foreach (var patient in patients)
                {
                    model.Patients.Add(patient);
                }

                return View(model);
            }
        }

        //
        // GET: /Contact/Manage/5

        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Manage(int id)
        {
            int _userId = WebSecurity.CurrentUserId;
            var patient = new Contact();
            var triageObject = new TriageObject(); 

            var manageContactViewModel = new ContactManageModel();
            manageContactViewModel.EContact = _ctx.Contacts.Get(id);
            var tempList = new List<Group>(); 

            if (id == 0)
            {
                return HttpNotFound();
            }
            else
            {
                tempList = _ctx.Groups.GetAllGroups(); 
                DataContext _context = new DataContext(); 
                                               
                var groupList = _ctx.ManyToManyRelationShips.GetAllGroupsForContact(id);
                //var groupList | _ctx.GetType

                foreach (var group in groupList)
                {
                    manageContactViewModel.Groups.Add(group);
                }

                               
                manageContactViewModel.Campaigns = _ctx.ManyToManyRelationShips.GetAllCampaignForContact(id).ToList<Campaign>();
                manageContactViewModel.Triages = _ctx.Triages.GetAllTriage().FindAll(p => p.ContactID == id);

                manageContactViewModel.TriageObjects = triageObject.TriageObjectList(manageContactViewModel.Triages, _ctx.Contacts.GetAllList());
                
                return View(manageContactViewModel);
            }

        }

        #endregion

        #region Contact Ajax Actions 

        [GridAction]
        public ActionResult _AjaxBindingContact()
        {
            int _userId = WebSecurity.CurrentUserId;
            var model = new ContactIndexModel();
            model.Patients = new List<Contact>();

            if (Roles.GetRolesForUser().Contains("Administrator"))
            {
                model.Patients = _ctx.Contacts.GetAllList();
               
            }

            else
            {
                var groups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);

                var patients = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(groups);

                foreach (var patient in patients)
                {
                    model.Patients.Add(patient);
                }

               
            }

            return View(new GridModel(model.Patients));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _InsertAjaxContact(FormCollection collection)
        {

            int _userId = WebSecurity.CurrentUserId;
            var contact = new Contact();
            contact.CreatedDate = DateTime.Now;
            contact.LastModifiedDate = DateTime.Now;

            contact.FirstName = collection["FirstName"];
            contact.LastName = collection["LastName"];
            contact.PhoneNumber = collection["PhoneNumber"];
            contact.EmailAddress = collection["EmailAddress"];
            contact.InsurerEmailAddress = collection["InsurerEmailAddress"];

            int cId = _ctx.Contacts.Add(contact);
            var idList = new List<int>();

            var defaultGroup = _ctx.ManyToManyRelationShips.GetDefaultGroup(_userId);
            var userGroup = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);


            var contactList = _ctx.ManyToManyRelationShips.GetAllContactsForGroup(defaultGroup.GroupId);

            foreach (var patient in contactList)
            {
                idList.Add(patient.ContactId);
            }

            idList.Add(cId);
            _ctx.ManyToManyRelationShips.AddContactsToGroup(defaultGroup.GroupId, idList);

            if (Roles.GetRolesForUser().Contains("Administrator"))
            {
                return View(new GridModel( _ctx.Contacts.GetAllList()));
            }

            else
            {
                var groups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                
                return View(new GridModel(_ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroup)));

            }

            

        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxEditingContact(int id)
        {
            int _userId = WebSecurity.CurrentUserId;
            var contact = _ctx.Contacts.Get(id);

            _ctx.Contacts.DeleteContact(id);

            var userGroup = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);

            if (Roles.GetRolesForUser().Contains("Administrator"))
            {
                return View(new GridModel(_ctx.Contacts.GetAllList()));
            }

            else
            {
                var groups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                return View(new GridModel(_ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroup)));
            }
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingContact(int id, FormCollection collection)
        {

            int _userId = WebSecurity.CurrentUserId;

            var contact = _ctx.Contacts.Get(id);

            contact.FirstName = collection["FirstName"];
            contact.LastName = collection["LastName"];
            contact.PhoneNumber = collection["PhoneNumber"];
            contact.EmailAddress = collection["EmailAddress"];
            contact.InsurerEmailAddress = collection["InsurerEmailAddress"];

            _ctx.Contacts.UpdateContact(contact);
            var userGroup = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);

            if (Roles.GetRolesForUser().Contains("Administrator"))
            {
                return View(new GridModel(_ctx.Contacts.GetAllList()));
            }

            else
            {
                var groups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                return View(new GridModel(_ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroup)));
            }
        }



        #endregion

      

        #region Group Ajax Actions

        [GridAction]
        public ActionResult _AjaxBindingGroup(int contactId)
        {
            int _userId = WebSecurity.CurrentUserId;
            var patient = new Contact();
            var manageContactViewModel = new ContactManageModel();
            manageContactViewModel.EContact = _ctx.Contacts.Get(contactId);
            var tempList = new List<Group>();

            if (contactId == 0)
            {
                return HttpNotFound();
            }
            else
            {
                tempList = _ctx.Groups.GetAllGroups();
                DataContext _context = new DataContext();

                var groupList = _ctx.ManyToManyRelationShips.GetAllGroupsForContact(contactId);

                return View(new GridModel(groupList));
            }
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingGroup(int id, int contactId, FormCollection collection)
        {

            var group = _ctx.Groups.Get(id);

            group.CompanyName = collection["CompanyName"];
            group.Email = collection["Email"];

            _ctx.Groups.UpdateGroup(group);

            var groupList = _ctx.ManyToManyRelationShips.GetAllGroupsForContact(contactId);

            return View(new GridModel(groupList));
        }

        #endregion

        #region Campaign Ajax Actions

       

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingCampaign(int id, int contactId, FormCollection collection)
        {

            var campaign = _ctx.Campaigns.GetCampaign(id);

            campaign.CampaignTitle = collection["CampaignTitle"];
            campaign.SendDateTime = Convert.ToDateTime(collection["SendDateTime"]);
            campaign.CampaignMessage = collection["CampaignMessage"];

            _ctx.Campaigns.UpdateCampaign(campaign);

            var campaigns = _ctx.ManyToManyRelationShips.GetAllCampaignForContact(contactId);  

            return View(new GridModel(campaigns));
        }

        
        #endregion

        #region Triage Ajax Actions

       

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingTriage(int id, int contactId, FormCollection collection)
        {

            var triage = _ctx.Triages.GetTriage(id);

            triage.Title = collection["Title"];
            triage.TriageMessage = collection["TriageMessage"];
            triage.GoodResponseMessage = collection["GoodResponseMessage"];
            triage.BadResponseMessage = collection["BadResponseMessage"];
            //   triage.SendDateTime = collection[""];

            triage.StartDate = Convert.ToDateTime(collection["StartDate"]);
            triage.EndDate = Convert.ToDateTime(collection["EndDate"]);
            triage.SendDateTime = Convert.ToDateTime(collection["SendDateTime"]);
            _ctx.Triages.Update(triage);

            var model = new List<Triage>(); 

            model = _ctx.Triages.GetAllTriage().FindAll(p => p.ContactID == contactId);

            return View(new GridModel(model));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxEditingTriage(int id, int contactId)
        {

            var triage = _ctx.Triages.GetTriage(id);

            _ctx.Triages.Remove(id);

            var model = new List<Triage>();


            model = _ctx.Triages.GetAllTriage().FindAll(p => p.ContactID == contactId);

            return View(new GridModel(model));
        }

        #endregion


        //
        // POST: /Contact/Create
        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Create(Contact contact, FormCollection collection)
        {
            int _userId = WebSecurity.CurrentUserId;
            contact.CreatedDate = DateTime.Now;
            contact.LastModifiedDate = DateTime.Now;

            int cId = _ctx.Contacts.Add(contact);
            
            var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);

            var patients = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);

            var contactList = new List<int>();

            foreach (var patient in patients)
            {
                contactList.Add(patient.ContactId);
            }

            contactList.Add(cId);

            _ctx.ManyToManyRelationShips.AddContactsToGroup(userGroups[0].GroupId, contactList);

            return RedirectToAction("Index", new { id = cId });
        }


        //
        // POST: /Contact/Edit/5
        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Edit(Contact contact, FormCollection collection)
        {
            contact.LastModifiedDate = DateTime.Now;

            _ctx.Contacts.UpdateContact(contact);

            return RedirectToAction("Index");
        }

        //
        // POST: /Contact/Delete/5
        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            var patient = _ctx.Contacts.Get(id);

            if (patient != null)
            {
                _ctx.Contacts.DeleteContact(id);
            }
            
            return RedirectToAction("Index");
        }




    }
}
