﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;

namespace PatientHarmony.Web.Models
{
    public class ChatMessageModel
    {
       
        public int MessageId { get; set; }
                        
        public string FromPhoneNumber { get; set; }

        public string ToPhoneNumber { get; set; }

        public string Message { get; set; }

        public System.DateTime CreatedDate { get; set; }

        // 0 = outgoing 1 = incoming
        public bool Type { get; set; }

        public string PatientName { get; set; }

        public string GroupName { get; set; }

        public List<ChatMessageModel> BuildModel(List<ChatMessage> messages, string patientName, string groupName)
        {
            var model = new List<ChatMessageModel>();

            foreach (var message in messages)
            {
                var temp = new ChatMessageModel();
                temp.MessageId = message.MessageId;
                temp.CreatedDate = message.CreatedDate;
                temp.FromPhoneNumber = message.FromPhoneNumber;
                temp.ToPhoneNumber = message.ToPhoneNumber;
                temp.Message = message.Message;
                temp.Type = message.Type; 
                temp.PatientName = patientName;
                temp.GroupName = groupName;
                model.Add(temp);

            }


            return model; 
        }
    }


}