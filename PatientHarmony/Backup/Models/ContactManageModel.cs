﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;

namespace PatientHarmony.Web.Models
{
    public class ContactManageModel
    {
        public List<Group> Groups { get; set;  }
        public List<Triage> Triages { get; set;  }
        public List<Campaign> Campaigns { get; set;  }
        public Contact EContact { get; set;  }
        public List<Procedure> Procedures { get; set;  }
        public List<TriageObject> TriageObjects { get; set; }
     
        
        public ContactManageModel()
        {
            this.Groups = new List<Group>();
            this.Campaigns = new List<Campaign>();
            this.Triages = new List<Triage>();
            this.EContact = new Contact();
            this.Procedures = new List<Procedure>();
            this.TriageObjects = new List<TriageObject>(); 


        }
    }
}