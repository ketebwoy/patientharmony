﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;
using System.ComponentModel.DataAnnotations;
using PatientHarmony.Data.Managers;

namespace PatientHarmony.Web.Models
{
    public class ReportsIndexModel
    {
        private DataManager _ctx = DataManager.Instance;

        public List<GroupObject> GroupObjects { get; set; }
        public List<Group> Groups { get; set; }
        public List<Campaign> Campaigns { get; set; }
        public List<CampaignTemplate> CampaignTemplates { get; set; }
        public List<Procedure> Procedures { get; set; }
        public List<Triage> Triages { get; set; }
        public List<SMSIncoming> SmsIncomings { get; set; }
        public List<SMSOutgoing> SmsOutgoings { get; set; }
        public List<Contact> Patients { get; set; }


        public ReportsIndexModel()
        {
            this.GroupObjects = new List<GroupObject>();
            this.Groups = new List<Group>();
            this.Campaigns = new List<Campaign>();
            this.CampaignTemplates = new List<CampaignTemplate>(); 
            this.Procedures = new List<Procedure>();
            this.Triages = new List<Triage>();
            this.SmsIncomings = new List<SMSIncoming>();
            this.SmsOutgoings = new List<SMSOutgoing>();
            this.Patients = new List<Contact>(); 
        }

        public ReportsIndexModel BuildIndexModel(int userID, List<UserDefaultGroup> userGroups)
        {
            var model = new ReportsIndexModel();
            var groupManageTemp = new ManageGroupModel(); 
            
            model.Groups = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);
                        

            foreach (var group in model.Groups)
            {
                //build group objects 
                var groupObject = new GroupObject();
                model.GroupObjects.Add(groupManageTemp.BuildGroupObject(group));

                //get campaigns
                model.Campaigns.AddRange(_ctx.Campaigns.GetAllList().FindAll(p => p.GroupID == group.GroupId));

                //get triages
                model.Triages.AddRange(_ctx.Triages.GetAllTriage().FindAll(p => p.GroupID == group.GroupId));
            }

                    
                //get incoming sms for user
                model.SmsIncomings.AddRange(_ctx.SMSmessages.GetAllIncomingForUser(userGroups));
            
                //get outgoing sms for user 
                model.SmsOutgoings.AddRange(_ctx.SMSmessages.GetAllOutgoingForUser(userGroups));

               //get procedures
                foreach(var campaign in model.Campaigns)
                {
                    var group = model.Groups.First(p => p.GroupId == campaign.GroupID);
                    
                    campaign.Group = new Group();
                    campaign.Group = group; 

                    model.Procedures.AddRange(_ctx.Procedures.GetAllProceduresForCampaign(campaign.CampaignId)); 
                }

               //get patients 
                model.Patients.AddRange(_ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups));

            return model; 
        }
    }
}