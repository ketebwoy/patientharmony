﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatientHarmony.Data.Entities;
using PatientHarmony.Web.Models;


namespace PatientHarmony.Web.Models
{
    public class ManageGroupModel
    {
        public GroupObject Group { get; set; }
        public List<Contact> AllContacts { get; set; }
        public List<Contact> ContactSelected { get; set; }
        public List<Campaign> Campaigns { get; set; }
        public List<Triage> Triages { get; set; }
        public List<TriageObject> TriageObjects { get; set; }
        public List<Procedure> Procedures { get; set; }
        public Campaign NewCampaign { get; set; }
        public Procedure NewProcedure { get; set; }
        public Contact NewContact { get; set; }
        public Triage NewTriage { get; set; }
        public List<CampaignTemplateListTable> CampaignTemplates { get; set; }
        public List<TriageTemplate> TriageTemplates { get; set; }
      

        public ManageGroupModel()
        {
            this.Group = new GroupObject(); 
            this.AllContacts = new List<Contact>();
            this.ContactSelected = new List<Contact>();
            this.Triages = new List<Triage>();
            this.TriageObjects = new List<TriageObject>(); 
            this.Procedures = new List<Procedure>();
            this.NewCampaign = new Campaign();
            this.NewProcedure = new Procedure();
            this.NewContact = new Contact();
            this.NewTriage = new Triage();
            this.CampaignTemplates = new List<CampaignTemplateListTable>();
            this.TriageTemplates = new List<TriageTemplate>(); 
        }

        public GroupObject BuildGroupObject (Group group)
        {
         
                var model = new GroupObject();
                model.GroupId = group.GroupId;
                model.CompanyName = group.CompanyName;
                model.Email = group.Email;
                model.PhoneNumber = group.PhoneNumber;
                model.CreatedDate = group.CreatedDate;
                model.LastModifiedDate = group.LastModifiedDate;
                
            

            return model;
        }
    }

    
}
