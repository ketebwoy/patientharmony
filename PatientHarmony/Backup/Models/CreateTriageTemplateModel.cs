﻿using PatientHarmony.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientHarmony.Web.Models
{
    public class CreateTriageTemplateModel
    {
        public Group Group { get; set; }
        public List<Contact> AllContacts { get; set; }
        public List<int> contactsWithTriage { get; set; }
        public TriageTemplate TriageTemplate { get; set; }
        public List<TriageObject> activeTriagesForGroup { get; set; }
        public TraigeTemplateDisplayValueModel TriageTemplateDisplay { get; set;}
        public List<FollowUpTemplate> FollowUps { get; set; }

        public CreateTriageTemplateModel()
        {
            this.Group = new Group();
            this.AllContacts = new List<Contact>();
            this.TriageTemplate = new TriageTemplate();
            this.contactsWithTriage = new List<int>();
            this.activeTriagesForGroup = new List<TriageObject>(); 
            this.TriageTemplateDisplay = new TraigeTemplateDisplayValueModel();
            this.FollowUps = new List<FollowUpTemplate>(); 
            
        }

    }

    public class TraigeTemplateDisplayValueModel
    {
        public int TriageTemplateId { get; set; }
        
        private string _triageTemplateTitle = string.Empty;
        public string TriageTemplateTitle
        {
            get
            {
                return _triageTemplateTitle;
            }
            set
            {
                _triageTemplateTitle = value;
            }
        }

        private string _triageTempleMessage = string.Empty; 
        public string TriageTemplateMessage
        {
            get
            {
                return _triageTempleMessage;
            }
            set
            {
                _triageTempleMessage = value;
            }
        }

        private string _goodResponseMessage = string.Empty; 
        public string GoodResponseMessage
        {
            get
            {
                return _goodResponseMessage;
            }
            set
            {
                _goodResponseMessage = value;
            }
        }

        
        private string _badResponseMessage = string.Empty; 
        public string BadResponseMessage
        {
            get
            {
                return _badResponseMessage;
            }
            set
            {
                _badResponseMessage = value;
            }
        }
        
        private DateTime _startDate = DateTime.Now; 
        public DateTime StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }
        
        private DateTime _endDate = DateTime.Now; 
        public DateTime EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                _endDate = value;
            }
        }
        
        private DateTime _sendDateTime = DateTime.Now; 
        public DateTime SendDateTime
        {
            get
            {
                return _sendDateTime;
            }
            set
            {
                _sendDateTime = value;
            }
        }

        private int _triggerPoint = 0;
        public int TriggerPoint
        {
            get
            {
                return _triggerPoint;
            }
            set
            {
                _triggerPoint = value;
            }
        }

        private bool _disableTriageExtension = true;
        public bool DisTriageExtension
        {
            get
            {
                return _disableTriageExtension;
            }
            set
            {
                _disableTriageExtension = value;
            }
        }

        private TriageScaleType _triageScaleType = TriageScaleType.Ascending;
        public TriageScaleType TriageScaleType
        {
            get
            {
                return _triageScaleType;
            }
            set
            {
                _triageScaleType = value;
            }
        }

        private TriageFrequencyType _triageFrequencyType = TriageFrequencyType.Daily;
        public TriageFrequencyType TriageFrequencyType
        {
            get
            {
                return _triageFrequencyType;
            }
            set
            {
                _triageFrequencyType = value;
            }
        }

        private string _sendDays = string.Empty;
        public string SendDays
        {
            get
            {
                return _sendDays;
            }
            set
            {
                _sendDays = value;
            }
        }


    }
}