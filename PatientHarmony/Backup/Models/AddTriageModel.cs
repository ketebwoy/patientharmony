﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;
using System.Web.Mvc;


namespace PatientHarmony.Web.Models
{
    public class AddTriageModel
    {
        public Group group { get; set; }
        public List<Contact> AllContacts { get; set; }
        public Triage NewTriage { get; set; }
        public List<int> contactsWithTriage { get; set; }
        public List<TriageObject> activeTriagesForGroup { get; set; }
        public List<TriageObject> triageTestList { get; set; }
        
        public AddTriageModel()
        {
            this.group = new Group();
            this.AllContacts = new List<Contact>();
            this.NewTriage = new Triage();
            this.activeTriagesForGroup = new List<TriageObject>(); 
            this.contactsWithTriage = new List<int>();
            this.triageTestList = new List<TriageObject>(); 
        }
    }
}