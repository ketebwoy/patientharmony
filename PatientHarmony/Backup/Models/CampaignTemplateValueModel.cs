﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PatientHarmony.Web.Models
{
    public class CampaignTemplateValueModel
    {
        public int CampaignTemplateId { get; set; }

        private string _campaignTemplateTitle = string.Empty;
        public string CampaignTemplateTitle
        {
            get
            {
                return _campaignTemplateTitle;
            }
            set
            {
                _campaignTemplateTitle = value;
            }
        }

        private DateTime _sendDateTime = DateTime.Now;
        public System.DateTime SendDateTime
        {
            get
            {
                return _sendDateTime;
            }
            set
            {
                _sendDateTime = value;
            }
        }

        private string _campaignTemplateMessage = string.Empty;
        public string CampaignTemplateMessage
        {
            get
            {
                return _campaignTemplateMessage;
            }
            set
            {
                _campaignTemplateMessage = value;
            }
        }
    }
}