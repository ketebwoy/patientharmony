﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;
using PatientHarmony.Web.Models;
using System.Web.Mvc;

namespace PatientHarmony.Web.Models
{
    public class CreateTemplateCampaignModel
    {
        public Campaign Campaign { get; set; }
        public Group Group { get; set; }
        public List<Group> Groups { get; set; }
        public List<Contact> AllContacts { get; set; }
        public CampaignTemplateValueModel CampaignTemplate { get; set; }
        public List<ProcedureTemplateValueModel> ProcedureTemplates { get; set; }
        public IEnumerable<SelectListItem> GroupDropDown { get; set; }

        public CreateTemplateCampaignModel()
        {
            this.Campaign = new Campaign();
            this.Groups = new List<Group>();
            this.Group = new Group(); 
            this.AllContacts = new List<Contact>();
            this.CampaignTemplate = new CampaignTemplateValueModel();
            this.ProcedureTemplates = new List<ProcedureTemplateValueModel>(); 
                       
        }
    }

   
}