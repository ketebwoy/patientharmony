﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace PatientHarmony.Web.Models
{
    public class CampaignIndexModel
    {
        public Campaign campaign { get; set; }
        public List<Group> Groups { get; set; }
        public List<Contact> AllContacts { get; set; }
        public List<Contact> ContactSelected { get; set; }
        public List<Campaign> Campaigns { get; set; }
        public Campaign NewCampaign { get; set; }
        public List<int> NewGroups { get; set; }
        public Triage NewTriage { get; set; }
        public List<Procedure> NewProcedures { get; set; }
        public IEnumerable<SelectListItem> GroupDropDown { get; set; }
        public List<CampaignTemplateListTable> CampaignTemplates { get; set; }
        public List<ProcedureTemplate> ProcedureTemplates { get; set; }
        

        public CampaignIndexModel()
        {
            this.campaign = new Campaign(); 
            this.Campaigns = new List<Campaign>(); 
            this.Groups = new List<Group>(); 
            this.AllContacts = new List<Contact>();
            this.ContactSelected = new List<Contact>();
            this.NewCampaign = new Campaign();
            this.NewGroups = new List<int>();
            this.CampaignTemplates = new List<CampaignTemplateListTable>();
            this.ProcedureTemplates = new List<ProcedureTemplate>(); 
            
          
        }
    }

    //template display Model need Group Name
    public class CampaignTemplateListTable
    {
        
        public int CampaignTemplateId { get; set; }
        public string CampaignTemplateTitle { get; set; }
        public string GroupTitle { get; set; }
        public int GroupID { get; set; }
        public System.DateTime SendDateTime { get; set; }
        public System.DateTime DateSMSProcessed { get; set; }
        public string CampaignTemplateMessage { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime LastModifiedDate { get; set; }
        public int NumOfProcedures { get; set; }

    }



}