﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace PatientHarmony.Web.OverRides
{
    public class TwimlResult : ActionResult
    {
        public XDocument Response { get; set; }

        public TwimlResult(XDocument response)
        {
            Response = response;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = "text/xml";
            Response.Save(context.HttpContext.Response.Output);
        }
    }
}