﻿using PatientHarmony.Data.Managers;
namespace PatientHarmony.ContactImportTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.browseButton = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.filepathtxtBox = new System.Windows.Forms.TextBox();
            this.importButton = new System.Windows.Forms.Button();
            this.groupListDropDown = new System.Windows.Forms.ComboBox();
            this.groupSelectLabel = new System.Windows.Forms.Label();
            this.FileSelectLabel = new System.Windows.Forms.Label();
            this.importPreview = new System.Windows.Forms.DataGridView();
            this.ImportLabel = new System.Windows.Forms.Label();
            this.progressLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.importPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(176, 29);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(95, 20);
            this.browseButton.TabIndex = 0;
            this.browseButton.Text = "Choose File";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(32, 479);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(675, 25);
            this.progressBar1.TabIndex = 1;
            this.progressBar1.Visible = false;
            // 
            // filepathtxtBox
            // 
            this.filepathtxtBox.Location = new System.Drawing.Point(298, 29);
            this.filepathtxtBox.Name = "filepathtxtBox";
            this.filepathtxtBox.Size = new System.Drawing.Size(377, 20);
            this.filepathtxtBox.TabIndex = 3;
            // 
            // importButton
            // 
            this.importButton.Location = new System.Drawing.Point(252, 444);
            this.importButton.Name = "importButton";
            this.importButton.Size = new System.Drawing.Size(209, 29);
            this.importButton.TabIndex = 4;
            this.importButton.Text = "Start Import";
            this.importButton.UseVisualStyleBackColor = true;
            this.importButton.Click += new System.EventHandler(this.importButton_Click);
            // 
            // groupListDropDown
            // 
            this.groupListDropDown.FormattingEnabled = true;
            this.groupListDropDown.Location = new System.Drawing.Point(303, 304);
            this.groupListDropDown.Name = "groupListDropDown";
            this.groupListDropDown.Size = new System.Drawing.Size(209, 21);
            this.groupListDropDown.TabIndex = 5;
            this.groupListDropDown.Visible = false;
            this.groupListDropDown.SelectedIndexChanged += new System.EventHandler(this.groupListDropDown_SelectedIndexChanged);
            // 
            // groupSelectLabel
            // 
            this.groupSelectLabel.AutoSize = true;
            this.groupSelectLabel.Location = new System.Drawing.Point(29, 312);
            this.groupSelectLabel.Name = "groupSelectLabel";
            this.groupSelectLabel.Size = new System.Drawing.Size(115, 13);
            this.groupSelectLabel.TabIndex = 6;
            this.groupSelectLabel.Text = "Step 2 - Choose Group";
            this.groupSelectLabel.Visible = false;
            // 
            // FileSelectLabel
            // 
            this.FileSelectLabel.AutoSize = true;
            this.FileSelectLabel.Location = new System.Drawing.Point(29, 36);
            this.FileSelectLabel.Name = "FileSelectLabel";
            this.FileSelectLabel.Size = new System.Drawing.Size(128, 13);
            this.FileSelectLabel.TabIndex = 8;
            this.FileSelectLabel.Text = "Step 1 - Select Import File";
            // 
            // importPreview
            // 
            this.importPreview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.importPreview.Location = new System.Drawing.Point(32, 65);
            this.importPreview.Name = "importPreview";
            this.importPreview.Size = new System.Drawing.Size(675, 225);
            this.importPreview.TabIndex = 10;
            // 
            // ImportLabel
            // 
            this.ImportLabel.AutoSize = true;
            this.ImportLabel.Location = new System.Drawing.Point(29, 452);
            this.ImportLabel.Name = "ImportLabel";
            this.ImportLabel.Size = new System.Drawing.Size(101, 13);
            this.ImportLabel.TabIndex = 11;
            this.ImportLabel.Text = "Step 3 - Start Import";
            this.ImportLabel.Visible = false;
            // 
            // progressLabel
            // 
            this.progressLabel.AutoSize = true;
            this.progressLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressLabel.Location = new System.Drawing.Point(294, 508);
            this.progressLabel.Name = "progressLabel";
            this.progressLabel.Size = new System.Drawing.Size(51, 20);
            this.progressLabel.TabIndex = 12;
            this.progressLabel.Text = "label1";
            this.progressLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.progressLabel.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 537);
            this.Controls.Add(this.progressLabel);
            this.Controls.Add(this.ImportLabel);
            this.Controls.Add(this.importPreview);
            this.Controls.Add(this.FileSelectLabel);
            this.Controls.Add(this.groupSelectLabel);
            this.Controls.Add(this.groupListDropDown);
            this.Controls.Add(this.importButton);
            this.Controls.Add(this.filepathtxtBox);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.browseButton);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.importPreview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox filepathtxtBox;
        private System.Windows.Forms.Button importButton;
        private System.Windows.Forms.ComboBox groupListDropDown;
        private System.Windows.Forms.Label groupSelectLabel;
        private System.Windows.Forms.Label FileSelectLabel;
        private System.Windows.Forms.DataGridView importPreview;
        private System.Windows.Forms.Label ImportLabel;
        private System.Windows.Forms.Label progressLabel;
    }
}

