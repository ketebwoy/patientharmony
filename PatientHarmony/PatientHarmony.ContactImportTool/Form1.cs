﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Transactions;
using System.Diagnostics;
using PatientHarmony.Data.Context;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Managers;
using Excel;
using System.Data.Entity.Core.Objects;

namespace PatientHarmony.ContactImportTool
{
    public partial class Form1 : Form
    {
        private BackgroundWorker _bw;
        private Stopwatch _sw = new Stopwatch();
        private string _filePath = string.Empty;
        private DataManager _ctx = DataManager.Instance;
        private List<Contact> _contacts = new List<Contact>();
        private int _groupId = 0; 



        public Form1()
        {
            InitializeComponent();
            importButton.Visible = false; 

        }

        #region Import Helpers

        private void browseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog(); string filename = "";
            ofd.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            ofd.FilterIndex = 2;

            string path = "";
            if (ofd.ShowDialog() == DialogResult.OK)
            {

                _filePath = System.IO.Path.GetFullPath(ofd.FileName);

                filepathtxtBox.Text = _filePath;
                groupSelectLabel.Visible = true;
                groupListDropDown.Visible = true;

                var dt = GetExcelData(_filePath);

                //remove empty rows from Data Table
                dt.AsEnumerable().Where(row => String.IsNullOrEmpty(row.Field<string>("First Name"))).ToList().ForEach(row => row.Delete());
                dt.AcceptChanges(); 
 
                importPreview.DataSource = dt;
                importPreview.DefaultCellStyle.WrapMode =  DataGridViewTriState.False; 
                importPreview.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
                importPreview.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                importPreview.ColumnHeadersDefaultCellStyle.WrapMode = DataGridViewTriState.False;
                importPreview.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                _contacts = BuildContactListFromDataSet(dt);
               

                groupListDropDown.DataSource = _ctx.Groups.GetAllGroups();
                groupListDropDown.DisplayMember = "CompanyName";
                groupListDropDown.ValueMember = "GroupId";

            }
                        
        }

        private DataTable GetExcelData(string filePath)
        {
            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);

            excelReader.IsFirstRowAsColumnNames = true;
            DataTable dt = excelReader.AsDataSet().Tables[0];

            return dt;
        }

        private void ProcessDataIntoEntities(List<Contact> contacts, int groupId)
        {
            List<int> contactIds = new List<int>();
            
            progressBar1.Visible = true;
            progressLabel.Visible = true;
            progressLabel.Update(); 
            progressBar1.Minimum = 1;
            progressBar1.Maximum = contacts.Count();
            progressBar1.Value = 1;
            progressBar1.Step = 1;

            
            if (contacts.Count > 0)
            {
                using (var _ctx2 = new DataContext())
                {
                    using (var dbcontextTransaction = _ctx2.Database.BeginTransaction())
                    {
                        try
                        {
                            //Import Contacts
                            foreach (var contact in contacts)
                            {

                                progressLabel.Text = "Importing " + contact.FirstName + " " + contact.LastName;
                                progressLabel.Update();
                                contact.CreatedDate = DateTime.Now;
                                contact.LastModifiedDate = DateTime.Now;

                                _ctx2.Contacts.Add(contact);
                                _ctx2.SaveChanges();
                                progressBar1.ForeColor = Color.Silver;
                                progressBar1.Style = ProgressBarStyle.Continuous;
                                progressBar1.PerformStep();

                                int conId = contact.ContactId;
                                contactIds.Add(conId);
                            }

                            int x = 0;

                            //Add Contacts to group
                            progressBar1.Value = 1;
                            progressLabel.Text = "adding contacts to " + groupListDropDown.Text;
                            progressLabel.Update();
                            foreach (var item in contactIds)
                            {
                                var contactGroup = new ContactsGroup();
                                contactGroup.GroupId = groupId;
                                contactGroup.ContactId = contactIds[x];
                                _ctx2.ContactsGroups.Add(contactGroup);
                                _ctx2.SaveChanges();
                                progressBar1.ForeColor = Color.Green;
                                progressBar1.Style = ProgressBarStyle.Continuous;
                                progressBar1.PerformStep();

                                x++;
                            }

                            //dbcontextTransaction.Rollback();
                            dbcontextTransaction.Commit();
                            x = 0;
                        }
                        catch (Exception ex)
                        {
                            progressBar1.ForeColor = Color.Red;
                            progressBar1.Value = _contacts.Count;
                            progressBar1.Style = ProgressBarStyle.Continuous;
                            progressLabel.Text = "Import Failed";
                            progressLabel.Update();
                            Console.WriteLine("An Error occured importing contacts");
                            dbcontextTransaction.Rollback();
                        }

                    }
                }
            }
        }

        private List<Contact> BuildContactListFromDataSet(DataTable dt)
        {
            var listContacts = new List<Contact>();

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.AsEnumerable())
                {
                    var contact = new Contact();
                    contact.FirstName = dr.Field<string>("First Name");
                    contact.LastName = dr.Field<string>("Last Name");
                    contact.EmailAddress = dr.Field<string>("Email Address");
                    contact.PhoneNumber = dr.Field<string>("Phone Number");
                    contact.InsurerEmailAddress = dr.Field<string>("Issurer Email Address");
                    listContacts.Add(contact);
                }
            }

            _contacts = listContacts; 
            return listContacts;
        }

       
        private void txtImportFilePath_TextChanged(object sender, EventArgs e)
        {
            RefreshUi();
        }

        private void RefreshUi()
        {
         
        }

        private void _bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _sw.Stop();
            WriteStatus(string.Format("Process Complete - Elapsed time in seconds {0}", _sw.Elapsed.TotalSeconds));
            RefreshUi();
        }

        private void WriteStatus(string message)
        {
            //if (txtOutput.Text.Length != 0)
            //{
            //    txtOutput.AppendText(Environment.NewLine);
            //}
            //string date = DateTime.Now.ToString("hh:mm:ss.FF");
            //txtOutput.AppendText(string.Format("{0} -  {1}", date, message));
        }

 
        private void importButton_Click(object sender, EventArgs e)
        {
            _groupId = (int)groupListDropDown.SelectedValue;
            ProcessDataIntoEntities(_contacts, _groupId);

            progressLabel.Font = new Font(progressLabel.Font, FontStyle.Bold);
            progressLabel.Text = "Successful Import";
            progressLabel.Update();         
        }
        #endregion

        private void groupListDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            importButton.Visible = true;
        }

       

      



     
    }
}
