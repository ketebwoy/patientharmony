﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PatientHarmony.Web;
using PatientHarmony.Web.Controllers;
using PatientHarmony.Data.Managers;
using PatientHarmony.Data.Entities;
using PatientHarmony.Web.Filters;
using PatientHarmony.Web.Models;
using System.Globalization;
using System.Web.Security;
using PatientHarmony.Web.Logging.NLog;
using PatientHarmony.Web.Models;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;


namespace PatientHarmony.Web.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        private DataManager _ctx = DataManager.Instance;

        [TestMethod]
        
        public void FullTriageReply()
        {
            //// createTriage and add//
            //var random = Guid.NewGuid().ToString("d").Substring(1,8);

            //var triage = new Triage();
            //triage.Title = string.Format("Test Triage{0}", random);
            //triage.TriageMessage = "Test Message";
            //triage.GoodResponseMessage = "GoodResponseMessage";
            //triage.BadResponseMessage = "BadResponseMessage";
            //triage.SendDateTime = DateTime.Now.AddMinutes(5);
            //triage.StartDate = DateTime.Now;
            //triage.EndDate = DateTime.Now.AddDays(30);
            //triage.TriggerPoint = 5;
            //triage.DisableTriageExtension = true;
            //triage.GroupID = _ctx.Groups.GetAllGroups().Find(p => p.Email == "ketebwoy@gmail.com").GroupId;
            //triage.ContactID = _ctx.Contacts.GetAllList().Find(p => p.EmailAddress == "ketebwoy@gmail.com").ContactId;


            //var daily = TriageFrequencyType.Daily;
            //var weekly = TriageFrequencyType.Weekly;
            //var biweekly = TriageFrequencyType.BiWeekly;
            
            //7 



            
            //HomeController controller = new HomeController();

            //// Act
            //ViewResult result = controller.Index() as ViewResult;

            //// Assert
            //Assert.AreEqual("Modify this template to jump-start your ASP.NET MVC application.", result.ViewBag.Message);
        }

        [TestMethod]
        [Ignore]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [Ignore]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        private bool AbleToSend(Triage triage)
        {
            var today = DateTime.Now;

            var passed = false;

            if (allowSendBasedonDays(triage.SendDays))
            {

                switch (triage.Frequency)
                {
                    case TriageFrequencyType.BiWeekly:
                        var minSendDate = DateTime.Today.AddDays(-14);

                        if (triage.LastDateProcessed < minSendDate)
                        {
                            passed = true;
                        }
                        break;

                    case TriageFrequencyType.Weekly:
                        if (triage.LastDateProcessed < DateTime.Today.AddDays(7))
                        {
                            passed = true;
                        }

                        passed = true;
                        break;

                    case TriageFrequencyType.Daily:
                        passed = true;
                        break;

                }

            }
            return passed;
        }

        private bool allowSendBasedonDays(string sendDays)
        {
            var daylist = new List<DayofWeek>();
            var today = DateTime.Today.DayOfWeek;
            var allow = false;

            var sendArray = sendDays.ToCharArray();

            if (sendArray.Contains('0') && today.Equals(DayofWeek.Sunday))
            {
                return true;
            }

            if (sendArray.Contains('1') && today.Equals(DayofWeek.Monday))
            {
                return true;
            }
            if (sendArray.Contains('2') && today.Equals(DayofWeek.Tuesday))
            {
                return true;
            }
            if (sendArray.Contains('3') && today.Equals(DayofWeek.Wednesday))
            {
                return true;
            }
            if (sendArray.Contains('4') && today.Equals(DayofWeek.Thursday))
            {
                return true;
            }
            if (sendArray.Contains('5') && today.Equals(DayofWeek.Friday))
            {
                return true;
            }

            if (sendArray.Contains('6') && today.Equals(DayofWeek.Saturday))
            {
                return true;
            }


            return allow;
        }
    }
}
