﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Context;
using PatientHarmony.Data.Managers;


namespace PatientHarmony.Web.Tests.SMSTests
{
    [TestClass]
    public class SMSTests
    {
        private DataManager _ctxFunctions = DataManager.Instance;

        [TestMethod]
        public void SplitSMSTest()
        {
            var testSMS = "Place a cooler next to your bed at night filled with the frozen peas or blue packs. Place one or two of the bags over your knee and set your alarm for approx 2 hours later.";

            var split = splitLongMessage(testSMS);

            Assert.IsTrue(split.Count == 2);

            var testSMS2 = "This first 48 hours is critical, if you haven't been propping your leg up on a cushion after cleaning your incision, you need to do so as soon as possible.";

            split = splitLongMessage(testSMS2);

            Assert.IsTrue(split.Count == 2);

            var testSMS3 = "Use rest, ice, compression, and elevation (RICE) to treat pain and swelling. Wear a sling for the first 48 hours after the injury, if it makes you more comfortable and supports the injured area. If you feel you need to use a sling for more than 48 hours, discuss your symptoms with your doctor. An elbow support, such as an elbow sleeve, forearm wrap, or arm sling, may help rest your elbow joint, relieve stress on your forearm muscles, and protect your joint during activity. A counterforce brace may be helpful for tennis elbow symptoms. Follow the manufacturer's directions for using the brace. Gently massage or rub the area to relieve pain and encourage blood flow. Do not massage the injured area if it causes pain. For the first 48 hours after an injury, avoid things that might increase swelling, such as hot showers, hot tubs, hot packs, or alcoholic beverages. After 48 to 72 hours, if swelling is gone, apply heat and begin gentle exercise with the aid of moist heat to help restore and maintain flexibility. Some experts recommend alternating between hot and cold treatments. If applying ice to your elbow does not reduce the swelling, talk with your doctor about hydrocortisone gel treatments (phonophoresis) with a physical therapist.";

            split = splitLongMessage(testSMS3);

            Assert.IsTrue(split.Count == 9);

            var testSMS4 = "please enter only a number ranging from 1 - 10";

            split = splitLongMessage(testSMS4);

            Assert.IsTrue(split.Count == 1);


        }

        public List<string> splitLongMessage(string message)
        {
            var tempList = new List<String>();
            var splitMessageList = new List<string>();
            var whitespaceIndexes = new List<int>();
            int stringLength = message.Length;
            int position = 0;
            int chunkSize = 155;
            var tempPosition = 0;


            if (stringLength > 0)
            {

                if (stringLength < chunkSize)
                {
                    tempList.Add(message);
                }

                else
                {
                    //split long message in 155 character chunks 
                    //get position of white space before 155 index
                    //5 characters to append sequence number/message count
                    for (int i = 0; i < stringLength; i += position)
                    {
                        // i = position + chunkSize; 
                        if (!(stringLength - i < chunkSize))
                        {
                            var temp = message.Substring(i, chunkSize);
                            position = temp.LastIndexOf(" ", chunkSize);

                            //position = (tempPosition + position);
                            whitespaceIndexes.Add(position);
                            tempList.Add(message.Substring(i, position));
                            //  i = position;

                            if (stringLength - i < chunkSize)
                            {
                                tempList.Add(message.Substring(i));
                            }
                        }
                        else
                        {
                            tempList.Add(message.Substring(i));
                        }

                    }
                }

                if (tempList.Count > 1)
                {
                    foreach (var tempMessage in tempList)
                    {
                        var sms = tempMessage + " " + (tempList.IndexOf(tempMessage) + 1) + "/" + tempList.Count;
                        splitMessageList.Add(sms);
                    }
                }
                else
                {
                    splitMessageList.Add(tempList.First());
                }
                System.Diagnostics.Debug.WriteLine("");




            }

            return splitMessageList;
        }
        
    }
}
