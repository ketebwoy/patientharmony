﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Managers;
using Newtonsoft.Json;



namespace PHWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {

        private DataManager _ctx = DataManager.Instance;



        public Group GetGroupData(string id)
        {
            var value = Convert.ToInt32(id);
            return _ctx.Groups.Get(value);
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public Group GetGroup(int value)
        {
            return _ctx.Groups.Get(value);
        }

        public string GetAllGroups()
        {
           
            var groups = _ctx.Groups.GetAllGroups();
            var groupTypes = new List<GroupType>(); 


            foreach(var group in groups)
            {
               var gt = new GroupType(); 
                gt.GroupId = group.GroupId;
                gt.CompanyName = group.CompanyName;
                gt.Email = group.Email;
                gt.PhoneNumber = group.PhoneNumber;
                
                groupTypes.Add(gt); 
            }

            var jsonResult = JsonConvert.SerializeObject(groupTypes, Formatting.Indented,
    new JsonSerializerSettings
    {
        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
    });

            return jsonResult;
        }





      
    }
}
