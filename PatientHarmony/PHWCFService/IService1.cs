﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using PatientHarmony.Data.Entities;

namespace PHWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        #region JSON Examples

        [OperationContract]
        [WebInvoke(Method = "GET", 
            ResponseFormat = WebMessageFormat.Json, 
            BodyStyle = WebMessageBodyStyle.Wrapped, 
            UriTemplate = "getGroupData/{id}")]
        Group GetGroupData(string id);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "getAllGroups")]
        string GetAllGroups();


        #endregion 

    }


        // TODO: Add your service operations here
    


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class GroupType
    {
        int groupId = 0;
        string companyName = string.Empty;
        string email = string.Empty;
        string phoneNumber = string.Empty;
        

        [DataMember]
        public int  GroupId
        {
            get { return groupId; }
            set { groupId = value; }
        }

        [DataMember]
        public string CompanyName
        {
            get { return companyName; }
            set { companyName = value; }
        }

        [DataMember]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        [DataMember]
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }
    }
}
