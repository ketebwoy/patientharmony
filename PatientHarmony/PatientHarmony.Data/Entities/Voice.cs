﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace PatientHarmony.Data.Entities
{
    public class Voice
    {
        private const string RegExPhonePattern =
   @"^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$";

        [Key]
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Required]        
        public int ObjectId { get; set; }

        [Required]
        public ObjectType Type { get; set; }

        [Required]
        public bool CallInitiated { get; set; }

        [Required]
        public bool CallEnded { get; set; }

        [StringLength(30)]
        [Required, RegularExpression(RegExPhonePattern, ErrorMessage = "Phone Number format must be ###-###-####")]
        public string FromPhoneNumber { get; set; }

        [StringLength(30)]
        [Required, RegularExpression(RegExPhonePattern, ErrorMessage = "Phone Number format must be ###-###-####")]
        public string ToPhoneNumber { get; set; }

        [Required]
        public System.DateTime CallStartDate { get; set; }

        [Required]
        public System.DateTime CallEndDate { get; set; }
        
    }

    public enum ObjectType
    {
        Campaign = 0,
        Procedure = 1,
        Triage = 2,
        TriageFollowUp = 3,
    }

}
