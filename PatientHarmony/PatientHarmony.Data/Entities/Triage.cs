﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientHarmony.Data.Entities
{
    public class Triage
    {
        [Key]
        [ScaffoldColumn(false)]
        public int TriageId { get; set; }

        [StringLength(200)]
        [Required]
        public string Title { get; set; }


        [Required]
        public string TriageMessage { get; set; }

        [Required]
        public string GoodResponseMessage { get; set; }

        [Required]
        public string BadResponseMessage { get; set; }


        [ForeignKey("GroupID")]
        public Group Group { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public int GroupID { get; set; }

        [ForeignKey("ContactID")]
        public Contact Contact { get; set; }

        public string HealthTracker { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public int ContactID { get; set; }

        [Required]
        public System.DateTime StartDate { get; set; }

        [Required]
        public string SendDays { get; set; }

        [Required]
        public System.DateTime EndDate { get; set; }

        [Required]
        public System.DateTime CreatedDate { get; set; }

        [Required]
        public bool EndMarker { get; set; }

        [Required]
        public bool VoiceEnabled { get; set; }


        public System.DateTime LastDateProcessed { get; set; }

        public System.DateTime LastReplyDate { get; set; }

      //  public System.DateTime YearlySendDate { get; set; }

        [Required]
        public TriageFrequencyType Frequency { get; set; }
                

        [DataType(DataType.Time)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a SMS send time")]
        public System.DateTime SendDateTime { get; set; }

        [Required]
        public int TriggerPoint { get; set; }

        [Required]
        public bool DisableTriageExtension { get; set; }

        // This property will be mapped
        public TriageScaleType TriageScaleType { get; set; }
    }

    public enum TriageScaleType
    {
        // 1 (no pain) - 10 (Most Pain) 
        Ascending = 1,

        //10 (Feeling Great) - 1 (Feeling Crappy)
        Descending = 2, 
        
        //1 = Yes, 2 = No
        Decision = 3
    }

    public enum TriageFrequencyType
    {
        Daily = 1,
                
        Weekly = 2,
                
        BiWeekly = 3

    }

    public enum DayofWeek
    {
        Sunday = 0,
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6

    }


}
