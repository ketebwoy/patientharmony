﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace PatientHarmony.Data.Entities
{
    public class CampaignTemplate
    {
        [Key]
        [ScaffoldColumn(false)]
        public int CampaignTemplateId { get; set; }

        [StringLength(30)]
        [Required]
        public string CampaignTemplateTitle { get; set; }

        [ForeignKey("GroupID")]
        public Group Group { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public int GroupID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a Date to send SMS")]
        public System.DateTime SendDateTime { get; set; }

        [Required]
        public string CampaignTemplateMessage { get; set; }

        [Required]
        public System.DateTime CreatedDate { get; set; }

        [Required]
        public System.DateTime LastModifiedDate { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public bool GlobalTemplate { get; set; }


    }
}
