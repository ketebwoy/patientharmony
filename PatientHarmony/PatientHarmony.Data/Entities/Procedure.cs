﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientHarmony.Data.Entities
{
    public class Procedure
    {
        [Key]
        [ScaffoldColumn(false)]
        public int ProcedureId { get; set; }

        [StringLength(30)]
        [Required]
        public string ProcedureTitle { get; set; }

        [ForeignKey("CampaignID")]
        public Campaign Campaign { get; set; }

        [Required]
        public int CampaignID { get; set; }

        [DataType(DataType.Time)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a start time")]
        public System.DateTime SendDateTime { get; set; }


        public System.DateTime DateSMSProcessed { get; set; }

        [Required]
        public System.DateTime StartDate { get; set; }

        [Required]
        public System.DateTime EndDate { get; set; }


        [Required]
        public string ProcedureMessage { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime CreatedDate { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime LastModifiedDate { get; set; }

        [Required]
        public bool VoiceEnabled { get; set; }
        
        
    }
}
