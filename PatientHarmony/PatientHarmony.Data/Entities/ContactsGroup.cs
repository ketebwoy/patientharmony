﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientHarmony.Data.Entities
{
    public class ContactsGroup
    {
        [Key]
        [Column(Order = 0)]
        public int ContactId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int GroupId { get; set; }
       
    }
}
