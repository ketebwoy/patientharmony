﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientHarmony.Data.Entities
{
    public class AutoResponder
    {
        [Key]
        [ScaffoldColumn(false)]
        public int ResponseId { get; set; }

        [StringLength(160)]
        [Required]
        public string ResponseMessage { get; set; }

        [ForeignKey("TriageID")]
        public Triage Triage { get; set; }

        [Required]
        public int TriageID { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime CreatedDate { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime LastModifiedDate { get; set; }
    }
}
