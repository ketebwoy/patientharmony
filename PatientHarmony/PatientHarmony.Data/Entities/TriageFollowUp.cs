﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientHarmony.Data.Entities
{
    public class TriageFollowUp
    {
        [Key]
        [ScaffoldColumn(false)]
        public int TriageFollowUpId { get; set; }

        [StringLength(200)]
        [Required]
        public string Title { get; set; }


        [Required]
        public string TriageFollowUpMessage { get; set; }

        [Required]
        public string GoodResponseMessage { get; set; }

        [Required]
        public string BadResponseMessage { get; set; }

        [Required]
        public System.DateTime StartDate { get; set; }

       [Required]
        public System.DateTime EndDate { get; set; }
        
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a SMS send time")]
        [DataType(DataType.Time)]
        public System.DateTime SendDateTime { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public int TriageID { get; set; }

        [ForeignKey("ContactID")]
        public Contact Contact { get; set; }

        public string ContactResponse { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public int ContactID { get; set; }

        [Required]
        public System.DateTime CreatedDate { get; set; }
               
        public System.DateTime ProcessedDate { get; set; }

        public System.DateTime LastDateExecuted { get; set; }

        public bool EndMarker { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime LastModifiedDate { get; set; }

        [Required]
        public bool VoiceEnabled { get; set; }



                
     

        
    }

    
}
