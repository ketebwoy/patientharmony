﻿using System.ComponentModel.DataAnnotations;

namespace PatientHarmony.Data.Entities
{
    public class SMSOutgoing
    {
        private const string RegExPhonePattern =
   @"^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$";

        [Key]
        [ScaffoldColumn(false)]
        public int SmsId { get; set; }

        [StringLength(30)]
        [Required, RegularExpression(RegExPhonePattern, ErrorMessage = "Phone Number format must be ###-###-####")]
        public string FromPhoneNumber { get; set; }

        [StringLength(30)]
        [Required, RegularExpression(RegExPhonePattern, ErrorMessage = "Phone Number format must be ###-###-####")]
        public string ToPhoneNumber { get; set; }

        [Required]
        public string Message { get; set; }

        public int CampaignID { get; set; }

        public int TriageID { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime CreatedDate { get; set; }
    }
}
