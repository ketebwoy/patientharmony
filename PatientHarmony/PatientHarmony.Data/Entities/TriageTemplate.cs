﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace PatientHarmony.Data.Entities
{
    public class TriageTemplate
    {
        [Key]
        [ScaffoldColumn(false)]
        public int TriageTemplateId { get; set; }

        [StringLength(200)]
        [Required]
        public string TriageTemplateTitle { get; set; }

        [Required]
        public string TriageTemplateMessage { get; set; }

        [Required]
        public string GoodResponseMessage { get; set; }

        [Required]
        public string BadResponseMessage { get; set; }

        [ForeignKey("GroupID")]
        public Group Group { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public int GroupID { get; set; }

        [Required]
        public System.DateTime StartDate { get; set; }

        [Required]
        public System.DateTime EndDate { get; set; }

        [Required]
        public string SendDays { get; set; }


        [DataType(DataType.Time)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a SMS send time")]
        public System.DateTime SendDateTime { get; set; }

        [Required]
        public System.DateTime CreatedDate { get; set; }

        [Required]
        public System.DateTime LastModifiedDate { get; set; }

        [Required]
        public int TriggerPoint { get; set; }

        [Required]
        public bool DisableTriageExtension { get; set; }

        // This property will be mapped
        public TriageScaleType TriageScaleType { get; set; }

        [Required]
        public TriageFrequencyType Frequency { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public bool GlobalTemplate { get; set; }


    }

}

