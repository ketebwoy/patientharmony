﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientHarmony.Data.Entities
{
    public class ContactsCampaigns
    {
        [Key]
        [Column(Order = 0)]
        public int ContactId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int CampaignId { get; set; }
       
    }
}
