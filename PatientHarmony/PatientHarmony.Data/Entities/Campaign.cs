﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace PatientHarmony.Data.Entities
{
    public class Campaign
    {
        [Key]
        [ScaffoldColumn(false)]
        public int CampaignId { get; set; }

        [StringLength(30)]
        [Required]
        public string CampaignTitle { get; set; }

        [ForeignKey("GroupID")]
        public Group Group { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public int GroupID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a Date to send SMS")]
        public System.DateTime SendDateTime { get; set; }


        public System.DateTime DateSMSProcessed { get; set; }

        
        [Required]
        public string CampaignMessage { get; set; }

        [Required]
        public System.DateTime CreatedDate { get; set; }

        [Required]
        public System.DateTime LastModifiedDate { get; set; }


        public virtual ICollection<Contact> Contacts { get; set; }

        [Required]
        public bool VoiceEnabled { get; set; }
    }
}
