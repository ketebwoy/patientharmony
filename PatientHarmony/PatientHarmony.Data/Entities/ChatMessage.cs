﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace PatientHarmony.Data.Entities
{
    public class ChatMessage
    {
        private const string RegExPhonePattern =
    @"^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$";

        [Key]
        [ScaffoldColumn(false)]
        public int MessageId { get; set; }

        [StringLength(30)]
        [Required, RegularExpression(RegExPhonePattern, ErrorMessage = "Phone Number format must be ###-###-####")]
        public string FromPhoneNumber { get; set; }

        [StringLength(30)]
        [Required, RegularExpression(RegExPhonePattern, ErrorMessage = "Phone Number format must be ###-###-####")]
        public string ToPhoneNumber { get; set; }

        [Required]
        public string Message { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime CreatedDate { get; set; }

        [Required]
        // 0 = outgoing 1 = incoming
        public bool Type { get; set; }

        [Required]
        public bool Processed { get; set; }

        
    }
}
