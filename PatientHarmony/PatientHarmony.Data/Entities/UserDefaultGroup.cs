﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace PatientHarmony.Data.Entities
{
    public class UserDefaultGroup
    {
        [Key]
        [Column(Order = 0)]
        public int UserId { get; set; }

        [Key]
        [Column(Order = 1)]
        public int GroupId { get; set; }

        public bool Type { get; set; }
    }
}
