﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientHarmony.Data.Entities
{
    public class FollowUpTemplate
    {
        [Key]
        [ScaffoldColumn(false)]
        public int FollowUpTemplateId { get; set; }

        [StringLength(200)]
        [Required]
        public string Title { get; set; }


        [Required]
        public string TemplateFollowUpMessage { get; set; }

        [Required]
        public string GoodResponseMessage { get; set; }

        [Required]
        public string BadResponseMessage { get; set; }

        [Required]
        public System.DateTime StartDate { get; set; }

        [Required]
        public System.DateTime EndDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a SMS send time")]
        [DataType(DataType.Time)]
        public System.DateTime SendDateTime { get; set; }


        [Required]
        [ScaffoldColumn(false)]
        public int TriageTemplateID { get; set; }

        [Required]
        public System.DateTime CreatedDate { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime LastModifiedDate { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public bool GlobalTemplate { get; set; }



    }
}
