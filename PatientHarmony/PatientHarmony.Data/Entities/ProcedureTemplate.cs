﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace PatientHarmony.Data.Entities
{
    public class ProcedureTemplate
    {
        [Key]
        [ScaffoldColumn(false)]
        public int ProcedureTemplateId { get; set; }

        [StringLength(30)]
        [Required]
        public string ProcedureTemplateTitle { get; set; }

        [Required]
        public int CampaignTemplateID { get; set; }

        [DataType(DataType.Time)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a start time")]
        public System.DateTime SendDateTime { get; set; }

        
        [Required]
        public System.DateTime StartDate { get; set; }

        [Required]
        public System.DateTime EndDate { get; set; }


        [Required]
        public string ProcedureTemplateMessage { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime CreatedDate { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime LastModifiedDate { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public bool GlobalTemplate { get; set; }
        
        
    }
}
