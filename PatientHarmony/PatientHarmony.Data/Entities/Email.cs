﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PatientHarmony.Data.Entities
{
    public class Email
    {
        private const string RegExEmailPattern = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
         //@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";

        [Key]
        [ScaffoldColumn(false)]
        public int EmailId { get; set; }

        [Required, RegularExpression(RegExEmailPattern, ErrorMessage = "Email Format must be userName@Domain.server ex: wellbeingsmsadmin@wellbeingsms.com")]
        public string FromEmail { get; set; }

        [Required, RegularExpression(RegExEmailPattern, ErrorMessage = "Email Format must be userName@Domain.server ex: wellbeingsmsadmin@wellbeingsms.com")]
        public string ToEmail { get; set; }

        [StringLength(160)]
        [Required]
        public string Subject { get; set; }

        [Required]
        public string Message { get; set; }

        [Required]
        public System.DateTime CreatedDate { get; set; }

        public int GroupID { get; set; }
    }
}
