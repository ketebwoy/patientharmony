﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Context;
using PatientHarmony.Data.Logging.NLog;
using PatientHarmony.Data.Managers;
using Twilio;

namespace PatientHarmony.Data.Managers
{
    public class CampaignsManager
    {

        private DataContext _ctx = new DataContext(); 

        //real account tokens
        private static string _ACCOUNT_SID = "ACe4219381d5754ac49d9b6a8768901b44";
        private static string _AUTH_TOKEN = "327479b3f55f99741637618b8d737df0";
        NLogLogger logger = new NLogLogger();


        public Campaign GetCampaign(int campaignId)
        {
            try
            {
               return _ctx.Campaigns.First(p => p.CampaignId == campaignId);
               
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting campaign", ex);
                //Console.WriteLine("An Error occured getting campaign" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<Campaign> GetAllList()
        {
            try
            {
                    if (_ctx.Campaigns.OrderBy(p => p.CampaignTitle).ToList<Campaign>().Count > 0)
                        return _ctx.Campaigns.OrderBy(p => p.CampaignTitle).ToList<Campaign>();
                    else
                        return new List<Campaign>(); 
               
            }

            catch (Exception ex)
            {
                logger.Error("An Error occured getting campaigns", ex);
                //Console.WriteLine("An Error occured getting campaigns" + System.Environment.NewLine + ex);
                return null;
            }

        }

        public int Add(Campaign eCampaign)
        {
            int newCamapignId;
            eCampaign.LastModifiedDate = DateTime.Now;
            eCampaign.CreatedDate = DateTime.Now;
            eCampaign.DateSMSProcessed = DateTime.Parse("1753-01-01 00:00:00");

            try
            {
                _ctx.Campaigns.Add(eCampaign);
                _ctx.SaveChanges();
                newCamapignId = eCampaign.CampaignId;

                return newCamapignId;
               
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding a campaign", ex);
                //Console.WriteLine("An Error occured adding campaigns" + System.Environment.NewLine + ex);
                return 0;
            }
        }

        public void UpdateCampaign(Campaign eCampaign)
        {
            try
            {
                    var updev = _ctx.Campaigns.First(p => p.CampaignId == eCampaign.CampaignId);

                    if (updev.CampaignTitle != eCampaign.CampaignTitle)
                        updev.CampaignTitle = eCampaign.CampaignTitle;

                    if (updev.SendDateTime != eCampaign.SendDateTime)
                        updev.SendDateTime = eCampaign.SendDateTime;

                    if (updev.DateSMSProcessed != eCampaign.DateSMSProcessed)
                        updev.DateSMSProcessed = eCampaign.DateSMSProcessed;

                    if (updev.CampaignMessage != eCampaign.CampaignMessage)
                        updev.CampaignMessage = eCampaign.CampaignMessage;


                    if (updev.VoiceEnabled != eCampaign.VoiceEnabled)
                        updev.VoiceEnabled = eCampaign.VoiceEnabled;

                    updev.LastModifiedDate = DateTime.Now;

                    _ctx.SaveChanges();
                
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating a campaign", ex);
                //Console.WriteLine("An Error occured updating campaign" + System.Environment.NewLine + ex);
                return;
            }
            
        }

        public void Delete(int id)
        {
            try
            {
                    Campaign campaign = _ctx.Campaigns.First(p => p.CampaignId == id);
                    _ctx.Campaigns.Remove(campaign);
                    DeleteCampaignFromRelationships(id);
                    _ctx.SaveChanges();
               
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting campaign", ex);
                //Console.WriteLine("An Error occured deleting campaign" + System.Environment.NewLine + ex);
                return;
            }
            
        }
        public void CampaignTaskExecute()
        {
            var today = DateTime.Now;
            var newCampaignDefaultDate = Convert.ToDateTime("1753-01-01 00:00:00");

            try
            {
                var camapigns = _ctx.Campaigns.OrderBy(p => p.CampaignId).Where(p => p.DateSMSProcessed == newCampaignDefaultDate);

                foreach (var campaign in camapigns)
                {
                    //get patients in campaign needed list for sendSms
                    var sendTime = campaign.SendDateTime - campaign.SendDateTime.Date;
                    System.Diagnostics.Debug.WriteLine("sendTime = {0}", sendTime.ToString());
                    var rightNow = today - today.Date;
                    System.Diagnostics.Debug.WriteLine("rightNow = {0}", rightNow.ToString());
                    var patientList = GetAllContactsForCampaign(campaign.CampaignId);
                    var group = _ctx.Groups.First(p => p.GroupId == campaign.GroupID);

                    //Make sure sms sends at schedule time of day
                    if ((campaign.SendDateTime - campaign.SendDateTime.Date) >= (today.AddMinutes(-5) - today.Date) && (campaign.SendDateTime - campaign.SendDateTime.Date) <= (today.AddMinutes(5) - today.Date))
                    {
                        Console.WriteLine("Executing campaign " + campaign.CampaignTitle + " " + " for " + group.CompanyName);
                        var sms = new SMSOutgoing();
                        sms.Message = campaign.CampaignMessage;
                        sms.CampaignID = campaign.CampaignId;
                        sms.FromPhoneNumber = group.PhoneNumber;
                        sendSMS(sms, patientList);
                        System.Threading.Thread.Sleep(500);
                        campaign.DateSMSProcessed = DateTime.Now;
                        UpdateCampaign(campaign);
                        Console.WriteLine("campaign " + campaign.CampaignTitle + " processed at: " + campaign.DateSMSProcessed);
                    }

                }
            }

            catch (Exception ex)
            {
                logger.Error("An Error occured executing campaign scheduled task", ex);
                //Console.WriteLine("An Error occured deleting campaign" + System.Environment.NewLine + ex);
            }

        }

        public void DeleteCampaignFromRelationships(int campaignId)
        {
            try
            {
                    var contactCampaigns = _ctx.ContactsCamapigns.Where(p => p.CampaignId == campaignId);
                    var procedures = _ctx.Procedures.Where(p => p.CampaignID == campaignId);
                   
                    foreach (var item in contactCampaigns)
                        _ctx.ContactsCamapigns.Remove(item);

                    foreach (var item in procedures)
                        _ctx.Procedures.Remove(item);

                   _ctx.SaveChanges();
            
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting Campaign relationships", ex);
                //Console.WriteLine("An Error occured deleting Campaign relationbships" + System.Environment.NewLine + ex);
                return;
            }
        }

        public List<Contact> GetAllContactsForCampaign(int campaignId)
        {
            var contactList = new List<Contact>();

            try
            {
                    var contactCampaignIds = _ctx.ContactsCamapigns.Where(p => p.CampaignId == campaignId).ToList<ContactsCampaigns>();

                    foreach (var item in contactCampaignIds)
                    {
                        var contactItem = _ctx.Contacts.First(p => p.ContactId == item.ContactId);
                        if (!contactList.Contains(contactItem))
                            contactList.Add(contactItem);

                    }
                    return contactList;
            
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured contacts for campaign", ex);
                //Console.WriteLine("An Error occured getting contacts for campaign" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public void sendSMS(SMSOutgoing message, List<Contact> contacts)
        {
            try
            {
                var twilClient = new TwilioRestClient(_ACCOUNT_SID, _AUTH_TOKEN);
                int x = 0;
                // iterate over all patients
                if (contacts != null)
                    foreach (var patient in contacts)
                    {
                        twilClient.SendSmsMessage(
                            message.FromPhoneNumber,
                            contacts[x].PhoneNumber,
                            message.Message
                            );
                        System.Threading.Thread.Sleep(500);
                        message.ToPhoneNumber = contacts[x].PhoneNumber;
                        AddOutgoing(message);
                        x++;
                    }
                x = 0;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured sending sms", ex);
                //Console.WriteLine("An Error occured sending sms" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void AddOutgoing(SMSOutgoing eSMSOutgoing)
        {

            eSMSOutgoing.CreatedDate = DateTime.Now;

            try
            {
                using (var _ctx = new DataContext())
                {
                    _ctx.SMSOutgoings.Add(eSMSOutgoing);
                    _ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding outgoign sms", ex);
                //Console.WriteLine("An Error occured adding outgoing sms" + System.Environment.NewLine + ex);
                return;
            }

        }


    }
}
