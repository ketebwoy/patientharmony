﻿using PatientHarmony.Data.Context;
using PatientHarmony.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System;
using PatientHarmony.Data.Logging.NLog;

namespace PatientHarmony.Data.Managers
{
    public class ManyToManyRelationshipManager
    {
        NLogLogger logger = new NLogLogger();
        private DataContext _ctx = new DataContext();
        private DataManager _ctxFunctins = DataManager.Instance; 

        #region Campaign Operations

        public List<Campaign> GetAllCampaignForContact(int contactId)
        {
            var tempList = new List<Campaign>();

            try
            {
                var contactCampaignIds = _ctx.ContactsCamapigns.Where(p => p.ContactId == contactId).ToList<ContactsCampaigns>();

                tempList = _ctxFunctins.Campaigns.GetAllList();

                var campaignList = new List<Campaign>(); 

                foreach (var item in tempList)
                {
                    if (contactCampaignIds.Where(p => p.CampaignId == item.CampaignId).Count() > 0)
                    {
                       var campaign = new Campaign();
                       campaign.CampaignId = item.CampaignId;
                       campaign.CampaignMessage = item.CampaignMessage;
                       campaign.CampaignTitle = item.CampaignTitle;
                       campaign.CreatedDate = item.CreatedDate;
                       campaign.SendDateTime = item.SendDateTime;
                       campaign.GroupID = item.GroupID;
                       
                       campaignList.Add(campaign);
                    
                    }
                } 
                
                return campaignList;
            }
            
            catch (Exception ex)
            {
                logger.Error("An Error occured getting Campaign contacts", ex);
                //Console.WriteLine("An Error occured getting Campaign contacts" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<Contact> GetAllContactsForCampaign(int campaignId)
        {
            var contactList = new List<Contact>();

            try
            {
                var contactCampaignIds = _ctx.ContactsCamapigns.Where(p => p.CampaignId == campaignId).ToList<ContactsCampaigns>();

                foreach (var item in contactCampaignIds)
                {
                    var contactItem = _ctx.Contacts.First(p => p.ContactId == item.ContactId);
                    if (!contactList.Contains(contactItem))
                        contactList.Add(contactItem);

                }
                return contactList;

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured contacts for campaign", ex);
                //Console.WriteLine("An Error occured getting contacts for campaign" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public void AddContactstoCampaigns(int campaignId, List<int> contactIds)
        {
            DeleteCampaignFromRelationships(campaignId);

            try
            {
                int x = 0;

                foreach (var item in contactIds)
                {
                    var contactCampaign = new ContactsCampaigns();
                    contactCampaign.CampaignId = campaignId;
                    contactCampaign.ContactId = System.Convert.ToInt32(contactIds[x]);
                    _ctx.ContactsCamapigns.Add(contactCampaign);
                    _ctx.SaveChanges();
                    x++;
                }
                x = 0;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding contacts to campaign", ex);
                //Console.WriteLine("An Error occured adding contacts to campaign" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void DeleteContactFromCampaign(int contactId)
        {
            try
            {
                var contactCampaigns = _ctx.ContactsCamapigns.Where(p => p.ContactId == contactId);

                foreach (var item in contactCampaigns)
                    _ctx.ContactsCamapigns.Remove(item);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting Campaign Contacts", ex);
                //Console.WriteLine("An Error occured deleting Campaign contacts" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void DeleteCampaignFromRelationships(int campaignId)
        {
            try
            {
                var contactCampaigns = _ctx.ContactsCamapigns.Where(p => p.CampaignId == campaignId);

                foreach (var item in contactCampaigns)
                    _ctx.ContactsCamapigns.Remove(item);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting Campaign relationships", ex);
                //Console.WriteLine("An Error occured deleting Campaign relationbships" + System.Environment.NewLine + ex);
                return;
            }
        }

        #endregion

        #region Groups Operations

        public List<Group> GetAllGroupsForContact(int contactId)
        {
            var tempList = new List<Group>(); 

            try
            {
                var contactGroupIds = _ctx.ContactsGroups.ToList<ContactsGroup>().FindAll(p => p.ContactId == contactId);
 
           
                tempList = _ctxFunctins.Groups.GetAllGroups();

                //used to store groups that equal groupId in contactGroup 
                var groupList = new List<Group>();
       
                foreach (var item in tempList)
                {
                    if (contactGroupIds.Where(p => p.GroupId == item.GroupId).Count() > 0)
                    {
                        var group = new Group();
                        group.GroupId = item.GroupId;
                        group.CompanyName = item.CompanyName;
                        group.Email = item.Email;
                        group.PhoneNumber = item.PhoneNumber;
                        group.CreatedDate = item.CreatedDate;
                        group.LastModifiedDate = item.LastModifiedDate;

                        groupList.Add(group);
                    }

                }
                return groupList;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting contact groups", ex);
                //Console.WriteLine("An Error occured getting contact groups" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<Contact> GetAllContactsForGroup(int groupId)
        {
            var contactList = new List<Contact>();

            try
            {
                //error check for null contactsgroups
                if (_ctx.ContactsGroups.Where(p => p.GroupId == groupId).ToList<ContactsGroup>().Count > 0)
                {
                    var contactGroupIds = _ctx.ContactsGroups.Where(p => p.GroupId == groupId).ToList<ContactsGroup>();

                    foreach (var item in contactGroupIds)
                    {
                        var contactItem = _ctx.Contacts.First(p => p.ContactId == item.ContactId);
                        if (!contactList.Contains(contactItem))
                        {
                            contactList.Add(contactItem);
                        }
                    }

                }
                if (contactList.Count <= 0)
                {
                    return new List<Contact>();
                }
                else
                    return contactList;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting group contacts", ex);
                //Console.WriteLine("An Error occured getting group contacts" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public void AddGroupstoContacts(int contactId, List<int> groupIds)
        {
            try
            {
                DeleteContactFromGroups(contactId);

                int x = 0;

                foreach (var item in groupIds)
                {
                    var contactGroup = new ContactsGroup();
                    contactGroup.ContactId = contactId;
                    contactGroup.GroupId = groupIds[x];
                    _ctx.ContactsGroups.Add(contactGroup);
                    _ctx.SaveChanges();
                    x++;
                }
                x = 0;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured ading group to contacts", ex);
                //Console.WriteLine("An Error occured adding group to contacts" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void DeleteContactFromGroups(int contactId)
        {
            try
            {
                var contactGroups = _ctx.ContactsGroups.Where(p => p.ContactId == contactId);

                foreach (var item in contactGroups)
                    _ctx.ContactsGroups.Remove(item);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting contact from groups", ex);
                //Console.WriteLine("An Error occured deleting contact from groups" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void DeleteGroups(int groupId)
        {
            try
            {
                var contactGroups = _ctx.ContactsGroups.Where(p => p.GroupId == groupId);

                foreach (var item in contactGroups)
                    _ctx.ContactsGroups.Remove(item);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting groups", ex);
                //Console.WriteLine("An Error occured deleting groups" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void AddContactsToGroup(int groupId, List<int> contactIds)
        {

            try
            {
                DeleteGroups(groupId);

                int x = 0;

                foreach (var item in contactIds)
                {
                    var contactGroup = new ContactsGroup();
                    contactGroup.GroupId = groupId;
                    contactGroup.ContactId = contactIds[x];
                    _ctx.ContactsGroups.Add(contactGroup);
                    _ctx.SaveChanges();
                    x++;
                }
                x = 0;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding contact to groups", ex);
                //Console.WriteLine("An Error occured adding contacts to group" + System.Environment.NewLine + ex);
                return;
            }
        }

         #endregion

        #region User, Role, Group Relationships

        public List<UserDefaultGroup> GetUserGroups(int userId)
        {
            try
            {
                return _ctx.UserGroups.OrderBy(p => p.UserId).Where(p => p.UserId == userId).ToList();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting User Groups", ex);
                //Console.WriteLine("An Error occured getting User Groups" + System.Environment.NewLine + ex);
                return null;
            }

        }
        
        public void AdduserGroup(int userId, int groupId, bool type = false)
        {
            try
            {
                var userGroup = new UserDefaultGroup();
                userGroup.UserId = userId;
                userGroup.GroupId = groupId;
                userGroup.Type = type;
                _ctx.UserGroups.Add(userGroup);
                _ctx.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding User Groups", ex);
                //Console.WriteLine("An Error occured Adding user Groups" + System.Environment.NewLine + ex);
                return;
            }
                         
        }
        
        public void AddDefaultUserGroup(int userId, int groupId)
        {
            try
            {
                //turn get current default group and change its type to false
                var defaultGroup = _ctx.UserGroups.First(p => p.Type == true && p.GroupId == groupId && p.UserId == userId);
                if (defaultGroup.UserId > 0 && defaultGroup.GroupId > 0)
                {
                    defaultGroup.Type = false;
                    _ctx.SaveChanges();
                }

                //add new default group
                var userGroup = new UserDefaultGroup();
                userGroup.UserId = userId;
                userGroup.GroupId = groupId;
                userGroup.Type = true;
                _ctx.UserGroups.Add(userGroup);
                _ctx.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding default User Group", ex);
                //Console.WriteLine("An Error occured adding Defaualt UserGroup" + System.Environment.NewLine + ex);
                return;
            }
                         
        }

        public void ChangeDefaultUserGroup(UserDefaultGroup userGroup)
        {
            try
            {
                //turn get current default user group and change its type to false
                var defaultGroup = _ctx.UserGroups.First(p => p.Type == true && p.GroupId == userGroup.GroupId && p.UserId == userGroup.UserId);
                if (defaultGroup.UserId > 0 && defaultGroup.GroupId > 0)
                {
                    defaultGroup.Type = false;
                    _ctx.SaveChanges();
                }

                //make passed in userGroup the default
                userGroup.Type = true;
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured changing default userGroup", ex);
                //Console.WriteLine("An Error occured changing default userGroup" + System.Environment.NewLine + ex);
                return;
            }
        }

        public Group GetDefaultGroup(int userId)
        {
            var defaultGroup = new Group(); 
            var groups = GetUserGroups(userId);

            try
            {
                foreach (var group in groups)
                {
                    if (group.Type == true)
                    {
                        defaultGroup = _ctx.Groups.First(p => p.GroupId == group.GroupId);
                    }
                }
                return defaultGroup;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting default Group", ex);
                //Console.WriteLine("An Error occured Getting default Group" + System.Environment.NewLine + ex);
                return null;
            }

        }

        public bool DeleteGroupsForUser(int groupIdToDelete)
        {
            try
            {
                //get a list of groups that match id to be deleted
                var groups = _ctx.UserGroups.OrderBy(p => p.GroupId).Where(p => p.GroupId == groupIdToDelete);

                //check to see if this group is a default group somewhere
                bool isDefault = false;
                foreach (var group in groups)
                {
                    if (group.Type == true)
                        isDefault = true;
                }

                //if it is not a default group delete group relationships and return true for confirmation
                if (isDefault == false)
                {
                    foreach (var group in groups)
                    {
                        _ctx.UserGroups.Remove(group);
                        _ctx.SaveChanges();
                    }
                    return true;
                }

                //return false could not delete default group
                return false;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting group for user", ex);
                // Console.WriteLine("An Error occured deleting group for User" + System.Environment.NewLine + ex);
                return false;
            }
        }

        public List<Group> GetGroupsEntitiesForUser(List<UserDefaultGroup> userGroups)
        {
            var groupList = new List<Group>();

            try
            {

                foreach (var userGroup in userGroups)
                {
                    var addGroup = _ctx.Groups.First(group => group.GroupId == userGroup.GroupId);
                    groupList.Add(addGroup);
                }
                return groupList;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting Group Entities for User", ex);
                //Console.WriteLine("An Error occured getting group Entities for User" + System.Environment.NewLine + ex);
                return null;
            }

            
        }

        public List<Contact> GetContactEntitiesForUser(List<UserDefaultGroup> userGroups)
        {
            var contactList = new List<Contact>();

            try
            {
                foreach (var userGroup in userGroups)
                {
                    var addContactList = GetAllContactsForGroup(userGroup.GroupId);

                    contactList.AddRange(addContactList);

                }
                return contactList;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting Contact Entrities for User", ex);
                //Console.WriteLine("An Error occured getting Contact Entitues for User" + System.Environment.NewLine + ex);
                return null;
            }
        }
        #endregion

    }
}
