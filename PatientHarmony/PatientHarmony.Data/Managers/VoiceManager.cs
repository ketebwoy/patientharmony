﻿using PatientHarmony.Data.Context;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Logging.NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Twilio;

namespace PatientHarmony.Data.Managers
{
    public class VoiceManager
    {
        private static string _ACCOUNT_SID = "ACe4219381d5754ac49d9b6a8768901b44";
        private static string _AUTH_TOKEN = "327479b3f55f99741637618b8d737df0";
        private DataContext _ctx = new DataContext();
        NLogLogger logger = new NLogLogger();


        public Voice GetCall(int id)
        {
            try
            {
                var call = new Voice();
                if (_ctx.VoiceCalls.First(p => p.Id == id) != null)
                {
                    call = _ctx.VoiceCalls.First(p => p.Id == id);
                }
                return call;

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting a call", ex);
                //Console.WriteLine("An Error occured getting the Contact" + System.Environment.NewLine + ex);
                return null;
            }
        }


        public List<Voice> GetAllCalls()
        {
            try
            {
                return _ctx.VoiceCalls.OrderBy(p => p.Id).ToList<Voice>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting voice calls", ex);
                //Console.WriteLine("An Error occured getting Contacts" + System.Environment.NewLine + ex);
                return null;
            }
        }


        public int Add(Voice eVoice)
        {
            int newVoiceId;

            try
            {
                if(eVoice.CallStartDate == null)
                eVoice.CallStartDate = DateTime.Parse("1753-01-01 00:00:00");

                if (eVoice.CallEndDate == null)
                eVoice.CallEndDate = DateTime.Parse("1753-01-01 00:00:00");

                _ctx.VoiceCalls.Add(eVoice);
                _ctx.SaveChanges();

                newVoiceId = eVoice.Id;
                return newVoiceId;

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding a voice call", ex);
                // Console.WriteLine("An Error occured adding Contact" + System.Environment.NewLine + ex);
                return -1;
            }
        }

        public void UpdateVoiceCall(Voice eVoice)
        {
            try
            {
                //using (var _ctx = new DataContext())
                //{
                var updev = _ctx.VoiceCalls.First(p => p.Id == eVoice.Id);

                if (updev.ObjectId != eVoice.ObjectId)
                    updev.ObjectId = eVoice.ObjectId;

                if (updev.Type!= eVoice.Type)
                    updev.Type = eVoice.Type;

                if (updev.CallInitiated!= eVoice.CallInitiated)
                    updev.CallInitiated = eVoice.CallInitiated;

                if (updev.CallEnded != eVoice.CallEnded)
                    updev.CallEnded = eVoice.CallEnded;


                if (updev.CallStartDate != eVoice.CallStartDate)
                    updev.CallStartDate = eVoice.CallStartDate;

                if (updev.CallEndDate != eVoice.CallEndDate)
                    updev.CallEndDate = eVoice.CallEndDate;

                if (updev.ToPhoneNumber != eVoice.ToPhoneNumber)
                    updev.ToPhoneNumber = eVoice.ToPhoneNumber;

                if (updev.FromPhoneNumber != eVoice.FromPhoneNumber)
                    updev.FromPhoneNumber = eVoice.FromPhoneNumber;


                _ctx.SaveChanges();
                //}
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating voice call", ex);
                //Console.WriteLine("An Error occured updating Contact" + System.Environment.NewLine + ex);
                return;
            }
        }

        
        public void callContacts(List<Contact> contacts, Group group, Triage triage = null, Campaign campaign = null, Procedure procedure = null, TriageFollowUp followup = null)
        {
            var voiceRecord = new Voice(); 
            
           
            var options = new CallOptions();
            // options.Url = "http://demo.twilio.com/docs/voice.xml";
            options.Url = "http://my.patientharmony.com/PhoneCall/CallHandler";
            options.From = group.PhoneNumber;
            options.Timeout = 30;
            options.IfMachine = "Hangup";
            voiceRecord.CallEnded = false;
            voiceRecord.CallInitiated = true;
            voiceRecord.CallStartDate = DateTime.Now; 
            
            
            if(campaign != null)
            {
                voiceRecord.ObjectId = campaign.CampaignId;
                voiceRecord.Type = ObjectType.Campaign;
                
            }

            if (procedure != null)
            {
                voiceRecord.ObjectId = procedure.ProcedureId;
                voiceRecord.Type = ObjectType.Procedure;
            }

            if (triage != null)
            {
                voiceRecord.ObjectId = triage.TriageId;
                voiceRecord.Type = ObjectType.Triage;
            }

            if (followup != null)
            {
                voiceRecord.ObjectId = followup.TriageFollowUpId;
                voiceRecord.Type = ObjectType.TriageFollowUp;
            }

            try
            {
                foreach (var contact in contacts)
                {
                    var twilio = new TwilioRestClient(_ACCOUNT_SID, _AUTH_TOKEN);
                    options.To = contact.PhoneNumber;
                    voiceRecord.ToPhoneNumber = contact.PhoneNumber;
                    voiceRecord.FromPhoneNumber = group.PhoneNumber; 
                    var call = twilio.InitiateOutboundCall(options);
                    System.Threading.Thread.Sleep(2500);
                    _ctx.VoiceCalls.Add(voiceRecord);
                    Console.WriteLine("Placed call to " + contact.PhoneNumber);
                }

                

            }
            
            catch(Exception ex)
            {
                logger.Error("An Error occured iniating a voice call", ex);
            }



        }
       
        public void callTest(Contact contact, Group group)
        {

            var options = new CallOptions();
            // options.Url = "http://demo.twilio.com/docs/voice.xml";
            options.Url = "http://my.patientharmony.com/PhoneCall/CallHandler";
            options.From = group.PhoneNumber;
            options.Timeout = 30;
            options.IfMachine = "Hangup";

            var twilio = new TwilioRestClient(_ACCOUNT_SID, _AUTH_TOKEN);
            options.To = contact.PhoneNumber;
            var call = twilio.InitiateOutboundCall(options);
        }
    }
}
