﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatientHarmony.Data.Managers
{
    public class DataManager
    {
        private static DataManager _instance;

        private DataManager() { }

        public static DataManager Instance
        {
            get
            {
                return _instance ?? (_instance = new DataManager());
            }
        }

        private ContactsManager _contactManager;

        public ContactsManager Contacts
        {
            get
            {
                if (_contactManager == null)
                    _contactManager = new ContactsManager();
                return _contactManager;
            }
        }

        private GroupsManager _groupManager;

        public GroupsManager Groups
        {
            get
            {
                if (_groupManager == null)
                    _groupManager = new GroupsManager();
                return _groupManager;
            }
        }

        private CampaignsManager _campaignManager;

        public CampaignsManager Campaigns
        {
            get
            {
                if (_campaignManager == null)
                    _campaignManager = new CampaignsManager();
                return _campaignManager;
            }
        }

        private ProceduresManager _procedureManager;

        public ProceduresManager Procedures
        {
            get
            {
                if (_procedureManager == null)
                    _procedureManager = new ProceduresManager();
                return _procedureManager;
            }
        }


        private SMSManager _smsManager;

        public SMSManager SMSmessages
        {
            get
            {
                if (_smsManager == null)
                    _smsManager = new SMSManager();
                return _smsManager;
            }
        }

        private EmailsManager _emailManager;

        public EmailsManager EmailMessages
        {
            get
            {
                if (_emailManager == null)
                    _emailManager = new EmailsManager();
                return _emailManager;
            }
        }

        private TriagesManager _triagesManager;

        public TriagesManager Triages
        {
            get
            {
                if (_triagesManager == null)
                    _triagesManager = new TriagesManager();
                return _triagesManager;
            }
        }

        private TriageFollowUpManager _triagesFollowUpManager;

        public TriageFollowUpManager TriageFollowUps
        {
            get
            {
                if (_triagesFollowUpManager == null)
                    _triagesFollowUpManager = new TriageFollowUpManager();
                return _triagesFollowUpManager;
            }
        }


        private AutoResponderManager _autoResponderManager;

        public AutoResponderManager AutoResponder
        {
            get
            {
                if (_autoResponderManager == null)
                    _autoResponderManager = new AutoResponderManager();
                return _autoResponderManager;
            }
        }

        private ManyToManyRelationshipManager _manyToManyRelationshipManager;

        public ManyToManyRelationshipManager ManyToManyRelationShips
        {
            get
            {
                if (_manyToManyRelationshipManager == null)
                    _manyToManyRelationshipManager = new ManyToManyRelationshipManager();
                return _manyToManyRelationshipManager;
            }
        }

        private TemplateManager _templateManager;

        public TemplateManager Templates
        {
            get
            {
                if (_templateManager == null)
                    _templateManager = new TemplateManager();
                return _templateManager;
            }
        }


        private VoiceManager _voiceManager;

        public VoiceManager VoiceCalls
        {
            get
            {
                if (_voiceManager == null)
                    _voiceManager = new VoiceManager();
                return _voiceManager;
            }
        }
    }
}
