﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Context;
using PatientHarmony.Data.Logging.NLog;

namespace PatientHarmony.Data.Managers
{
    public class AutoResponderManager
    {
        NLogLogger logger = new NLogLogger();

        public int Add(AutoResponder eAutoResponder)
        {
            int newResponderId;

            try
            {
                using (var _ctx = new DataContext())
                {
                    eAutoResponder.CreatedDate = DateTime.Now;
                    eAutoResponder.LastModifiedDate = DateTime.Now;

                    _ctx.AutoResponders.Add(eAutoResponder);
                    _ctx.SaveChanges();
                    newResponderId = eAutoResponder.ResponseId;
                }
                return newResponderId;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding a AutoResponder", ex);
                //Console.WriteLine("An Error occured adding AutoResponder" + System.Environment.NewLine + ex);
                return -1;
            }
        }

        public AutoResponder Get(int triageId)
        {
            try
            {
                using (var _ctx = new DataContext())
                {
                    return _ctx.AutoResponders.First(p => p.TriageID == triageId);
                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting AutoResponder", ex);
               // Console.WriteLine("An Error occured getting AutoResponder" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public void UpdateResponder(AutoResponder eAutoResponder)
        {
            try
            {
                using (var _ctx = new DataContext())
                {
                    var updev = _ctx.AutoResponders.First(p => p.ResponseId == eAutoResponder.ResponseId);

                    updev.LastModifiedDate = DateTime.Now;

                    _ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured Updating AutoResponder", ex);
                //Console.WriteLine("An Error occured updating AutoResponder" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void DeleteResponder(int responseId)
        {
            try
            {
                using (var _ctx = new DataContext())
                {
                    AutoResponder response = _ctx.AutoResponders.First(p => p.ResponseId == responseId);
                    _ctx.AutoResponders.Remove(response);
                    _ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured Deleting AutoResponder", ex);
                //Console.WriteLine("An Error occured deleting AutoResponder" + System.Environment.NewLine + ex);
                return;
            }
        }

    }
}
