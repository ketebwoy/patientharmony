﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Context;
using System.Net.Mail;
using System.Net;
using PatientHarmony.Data.Logging.NLog;

namespace PatientHarmony.Data.Managers
{
    public class EmailsManager
    {
        NLogLogger logger = new NLogLogger();
        private DataContext _ctx = new DataContext(); 

        public Email GetEmailDetail(int emailId)
        {
            try
            {
                return _ctx.Emails.First(p => p.EmailId == emailId);
                
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting Email Detail", ex);
                //Console.WriteLine("An Error occured getting Email Detail" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<Email> GetAllEmails()
        {
            try
            {
              return _ctx.Emails.OrderBy(p => p.CreatedDate).ToList<Email>();
              
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting Emails", ex);
                //Console.WriteLine("An Error occured getting Emails" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public int EmailAdmin(Email email)
        {
            try
            {
                email.ToEmail = "PatientHarmonyAdmin@patientharmony.com";
                email.CreatedDate = DateTime.Now;
                _ctx.Emails.Add(email);
                _ctx.SaveChanges();

                return email.EmailId;

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding email Admin Message", ex);
                //Console.WriteLine("An Error occured adding email" + System.Environment.NewLine + ex);
                return 0;
            }
        }


        public int AddEmail(Email email)
        {
            try
            {
                email.FromEmail = "PatientHarmonyAdmin@patientharmony.com";
                email.CreatedDate = DateTime.Now;
                _ctx.Emails.Add(email);
                _ctx.SaveChanges();

                return email.EmailId;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding email", ex);
                //Console.WriteLine("An Error occured adding email" + System.Environment.NewLine + ex);
                return 0;
            }
        }


        public void SendEmail(Email email, List<string> recipients = null, List<string> ccRecipients = null)
        {

            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("mail.patientharmony.com", 465);
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new NetworkCredential("PatientHarmonyAdmin@patientharmony.com", "S3attlesnows!");

                SmtpServer.Port = 587;



                mail.From = new MailAddress("PatientHarmonyAdmin@patientharmony.com");
                mail.Subject = email.Subject;
                mail.Body = email.Message;


                if (recipients == null)
                {
                    mail.To.Add(email.ToEmail);
                    SmtpServer.Send(mail);
                    AddEmail(email);
                }

                if (recipients.Count > 0)
                {
                    foreach (var rec in recipients)
                    {
                        mail.To.Add(rec);
                        
                    }
                    SmtpServer.Send(mail);
                    AddEmail(email);
                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured sending email", ex);
                //Console.WriteLine("An Error occured sending email" + System.Environment.NewLine + ex);
                return;
            }

        }
    }
}
