﻿using PatientHarmony.Data.Context;
using PatientHarmony.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System;
using PatientHarmony.Data.Logging.NLog;

namespace PatientHarmony.Data.Managers
{
    public class TemplateManager
    {
        NLogLogger logger = new NLogLogger();
        private DataContext _ctx = new DataContext();
        private DataManager _ctxFunctins = DataManager.Instance;

        #region Campaign Templates Functions 
        
        public CampaignTemplate GetCampaignTemplate(int campaignTemplateId)
        {
            try
            {
                return _ctx.CampaignTemplates.First(p => p.CampaignTemplateId == campaignTemplateId);

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting campaignTemplate", ex);
                //Console.WriteLine("An Error occured getting campaignTemplate" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<CampaignTemplate> GetAllCampaignTemplateList()
        {
            try
            {
                if (_ctx.CampaignTemplates.OrderBy(p => p.CampaignTemplateTitle).ToList<CampaignTemplate>().Count > 0)
                    return _ctx.CampaignTemplates.OrderBy(p => p.CampaignTemplateTitle).ToList<CampaignTemplate>();
                else
                    return new List<CampaignTemplate>();

            }

            catch (Exception ex)
            {
                logger.Error("An Error occured getting campaignTemplates", ex);
                //Console.WriteLine("An Error occured getting campaignTemplates" + System.Environment.NewLine + ex);
                return null;
            }

        }

        public int AddCampaignTemplate(CampaignTemplate eCampaignTemplate)
        {
            int newCampaignTemplateId;
            eCampaignTemplate.LastModifiedDate = DateTime.Now;
            eCampaignTemplate.CreatedDate = DateTime.Now;
           
            try
            {
                _ctx.CampaignTemplates.Add(eCampaignTemplate);
                _ctx.SaveChanges();
                newCampaignTemplateId = eCampaignTemplate.CampaignTemplateId;

                return newCampaignTemplateId;

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding a campaignTemplate", ex);
                //Console.WriteLine("An Error occured adding campaignTemplates" + System.Environment.NewLine + ex);
                return 0;
            }
        }

        public void UpdateCampaignTemplate(CampaignTemplate eCampaignTemplate)
        {
            try
            {
                var updev = _ctx.CampaignTemplates.First(p => p.CampaignTemplateId == eCampaignTemplate.CampaignTemplateId);

                if (updev.CampaignTemplateTitle != eCampaignTemplate.CampaignTemplateTitle)
                    updev.CampaignTemplateTitle = eCampaignTemplate.CampaignTemplateTitle;

                if (updev.SendDateTime != eCampaignTemplate.SendDateTime)
                    updev.SendDateTime = eCampaignTemplate.SendDateTime;

                if (updev.CampaignTemplateMessage != eCampaignTemplate.CampaignTemplateMessage)
                    updev.CampaignTemplateMessage = eCampaignTemplate.CampaignTemplateMessage;

                if (updev.GlobalTemplate!= eCampaignTemplate.GlobalTemplate)
                    updev.GlobalTemplate = eCampaignTemplate.GlobalTemplate;

                updev.LastModifiedDate = DateTime.Now;

                _ctx.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating a campaignTemplate", ex);
                //Console.WriteLine("An Error occured updating campaignTemplate" + System.Environment.NewLine + ex);
                return;
            }

        }

        public void DeleteCampaignTemplate(int camapignTemplateid)
        {
            try
            {
                CampaignTemplate campaignTemplate = _ctx.CampaignTemplates.First(p => p.CampaignTemplateId == camapignTemplateid);
                _ctx.CampaignTemplates.Remove(campaignTemplate);
                _ctx.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting campaignTemplate", ex);
                //Console.WriteLine("An Error occured deleting campaignTemplate" + System.Environment.NewLine + ex);
                return;
            }

        }

       
        #endregion

        #region Procedure Templates

        public ProcedureTemplate Get(int procedureTemplateId)
        {
            try
            {
                return _ctx.ProcedureTemplates.First(p => p.ProcedureTemplateId == procedureTemplateId);
            }
            catch (Exception ex)
            {

                logger.Error("An Error occured getting procedureTemplate", ex);
                //Console.WriteLine("An Error occured getting procedureTemplate" + System.Environment.NewLine + ex);
                return null;
            }
        }


        public List<ProcedureTemplate> GetAllProcedureTemplateList()
        {
            try
            {
                return _ctx.ProcedureTemplates.OrderBy(p => p.ProcedureTemplateTitle).ToList<ProcedureTemplate>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting procedureTemplates", ex);
                //Console.WriteLine("An Error occured getting procedureTemplates" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<ProcedureTemplate> GetAllProcedureTemplatesForCampaignTemplate(int campaignTemplateId)
        {
            var procedureTemplateList = new List<ProcedureTemplate>();

            try
            {
                procedureTemplateList = _ctx.ProcedureTemplates.Where(p => p.CampaignTemplateID == campaignTemplateId).ToList<ProcedureTemplate>();

                return procedureTemplateList;

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured contacts for campaignTemplate", ex);
                //Console.WriteLine("An Error occured getting contacts for campaignTemplate" + System.Environment.NewLine + ex);
                return null;
            }
        }


        public int AddProcedureTemplate(ProcedureTemplate eProcedureTemplate)
        {
            int newProcedureTemplateId;
            eProcedureTemplate.LastModifiedDate = DateTime.Now;
            eProcedureTemplate.CreatedDate = DateTime.Now;
           
            try
            {
                _ctx.ProcedureTemplates.Add(eProcedureTemplate);
                _ctx.SaveChanges();
                newProcedureTemplateId = eProcedureTemplate.ProcedureTemplateId;
                return newProcedureTemplateId;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding a procedureTemplate", ex);
                //Console.WriteLine("An Error occured adding procedureTemplate" + System.Environment.NewLine + ex);
                return -1;
            }
        }

        public void UpdateProcedureTemplate(ProcedureTemplate eProcedureTemplate)
        {
            try
            {
                var updev = _ctx.ProcedureTemplates.First(p => p.ProcedureTemplateId == eProcedureTemplate.ProcedureTemplateId);

                if (updev.ProcedureTemplateTitle != eProcedureTemplate.ProcedureTemplateTitle)
                    updev.ProcedureTemplateTitle = eProcedureTemplate.ProcedureTemplateTitle;

                if (updev.SendDateTime != eProcedureTemplate.SendDateTime)
                    updev.SendDateTime = eProcedureTemplate.SendDateTime;

                if (updev.ProcedureTemplateMessage != eProcedureTemplate.ProcedureTemplateMessage)
                    updev.ProcedureTemplateMessage = eProcedureTemplate.ProcedureTemplateMessage;

                if (updev.StartDate != eProcedureTemplate.StartDate)
                    updev.StartDate = eProcedureTemplate.StartDate;

                if (updev.EndDate != eProcedureTemplate.EndDate)
                    updev.EndDate = eProcedureTemplate.EndDate;

                if (updev.GlobalTemplate != eProcedureTemplate.GlobalTemplate)
                    updev.GlobalTemplate = eProcedureTemplate.GlobalTemplate;

                updev.LastModifiedDate = DateTime.Now;

                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating procedureTemplate", ex);
                //Console.WriteLine("An Error occured updating procedureTemplate" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void DeleteProcedureTemplate(int procedureTemplateId)
        {
            try
            {
                ProcedureTemplate procedureTemplate = _ctx.ProcedureTemplates.First(p => p.ProcedureTemplateId == procedureTemplateId);
                _ctx.ProcedureTemplates.Remove(procedureTemplate);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting procedureTemplate", ex);
                //Console.WriteLine("An Error occured deleting procedureTemplate" + System.Environment.NewLine + ex);
                return;
            }
        }

        #endregion 

        #region Triage Templates

        public TriageTemplate GetTriageTemplate(int triageTemplateId)
        {
            try
            {
                return _ctx.TriageTemplates.First(p => p.TriageTemplateId == triageTemplateId);

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting Triage Template", ex);
                return null;
            }
        }

        public List<TriageTemplate> GetAllTriageTemplateList()
        {
            try
            {
                if (_ctx.TriageTemplates.OrderBy(p => p.TriageTemplateTitle).ToList<TriageTemplate>().Count > 0)
                    return _ctx.TriageTemplates.OrderBy(p => p.TriageTemplateTitle).ToList<TriageTemplate>();
                else
                    return new List<TriageTemplate>();

            }

            catch (Exception ex)
            {
                logger.Error("An Error occured getting Triage Templates", ex);
                return null;
            }

        }

        public int AddTriageTemplate(TriageTemplate eTriageTemplate)
        {
            int newTriageTemplateId;
            eTriageTemplate.LastModifiedDate = DateTime.Now;
            eTriageTemplate.CreatedDate = DateTime.Now;

            try
            {
                _ctx.TriageTemplates.Add(eTriageTemplate);
                _ctx.SaveChanges();
                newTriageTemplateId = eTriageTemplate.TriageTemplateId;

                return newTriageTemplateId;

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding a campaignTemplate", ex);
                //Console.WriteLine("An Error occured adding campaignTemplates" + System.Environment.NewLine + ex);
                return 0;
            }
        }

        public void UpdateCampaignTemplate(TriageTemplate eTriageTemplate)
        {
            try
            {
                var updev = _ctx.TriageTemplates.First(p => p.TriageTemplateId == eTriageTemplate.TriageTemplateId);

                if (updev.TriageTemplateTitle != eTriageTemplate.TriageTemplateTitle)
                    updev.TriageTemplateTitle = eTriageTemplate.TriageTemplateTitle;

                if (updev.TriageTemplateMessage != eTriageTemplate.TriageTemplateMessage)
                    updev.TriageTemplateMessage = eTriageTemplate.TriageTemplateMessage;

                if (updev.GoodResponseMessage != eTriageTemplate.GoodResponseMessage)
                    updev.GoodResponseMessage = eTriageTemplate.GoodResponseMessage;

                if (updev.BadResponseMessage != eTriageTemplate.BadResponseMessage)
                    updev.BadResponseMessage = eTriageTemplate.BadResponseMessage;

                if (updev.SendDateTime != eTriageTemplate.SendDateTime)
                    updev.SendDateTime = eTriageTemplate.SendDateTime;

                if (updev.EndDate != eTriageTemplate.EndDate)
                    updev.EndDate = eTriageTemplate.EndDate;

                if (updev.GroupID != eTriageTemplate.GroupID)
                    updev.GroupID = eTriageTemplate.GroupID;

                if (updev.StartDate != eTriageTemplate.StartDate)
                    updev.StartDate = eTriageTemplate.StartDate;

                if (updev.SendDateTime != eTriageTemplate.SendDateTime)
                    updev.SendDateTime = eTriageTemplate.SendDateTime;

                if (updev.TriageScaleType != eTriageTemplate.TriageScaleType)
                    updev.TriageScaleType = eTriageTemplate.TriageScaleType;

                if (updev.TriggerPoint != eTriageTemplate.TriggerPoint)
                    updev.TriggerPoint = eTriageTemplate.TriggerPoint;

                if (updev.DisableTriageExtension != eTriageTemplate.DisableTriageExtension)
                    updev.DisableTriageExtension = eTriageTemplate.DisableTriageExtension;

                if (updev.Frequency != eTriageTemplate.Frequency)
                    updev.Frequency = eTriageTemplate.Frequency;


                if (updev.SendDays != eTriageTemplate.SendDays)
                    updev.SendDays = eTriageTemplate.SendDays;

                if (updev.GlobalTemplate != eTriageTemplate.GlobalTemplate)
                    updev.GlobalTemplate = eTriageTemplate.GlobalTemplate;

                updev.LastModifiedDate = DateTime.Now;

                _ctx.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating a Triage Template", ex);
                //Console.WriteLine("An Error occured updating campaignTemplate" + System.Environment.NewLine + ex);
                return;
            }

        }

        public void DeleteTriageTemplate(int eTriageTemplateId)
        {
            try
            {
                TriageTemplate triageTemplate = _ctx.TriageTemplates.First(p => p.TriageTemplateId == eTriageTemplateId);
                _ctx.TriageTemplates.Remove(triageTemplate);
                _ctx.SaveChanges();

                //delete associated followUp Templates
                var fupL = GetAllFollowUpTemplateList().FindAll(p => p.TriageTemplateID == eTriageTemplateId);

                foreach(var fu in fupL)
                {
                    DeleteFollowUpTemplate(fu.FollowUpTemplateId);
                }

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting Triage Template", ex);
                return;
            }

        }       

        #endregion 

        #region FollowUp Templates

        public FollowUpTemplate GetFollowUpTemplate(int followUpTemplateId)
        {
            try
            {
                return _ctx.FollowUpTemplate.First(p => p.FollowUpTemplateId == followUpTemplateId);

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting FollowUp Template", ex);
                return null;
            }
        }

        public List<FollowUpTemplate> GetAllFollowUpTemplateList()
        {
            try
            {
                if (_ctx.FollowUpTemplate.OrderBy(p => p.Title).ToList<FollowUpTemplate>().Count > 0)
                    return _ctx.FollowUpTemplate.OrderBy(p => p.Title).ToList<FollowUpTemplate>();
                else
                    return new List<FollowUpTemplate>();

            }

            catch (Exception ex)
            {
                logger.Error("An Error occured getting FollowUp Templates", ex);
                return null;
            }

        }

        public int AddFollowUpTemplate(FollowUpTemplate eFollowUpTemplate)
        {
            int newFollowUpTemplateId;
            eFollowUpTemplate.LastModifiedDate = DateTime.Now;
            eFollowUpTemplate.CreatedDate = DateTime.Now;

            try
            {
                _ctx.FollowUpTemplate.Add(eFollowUpTemplate);
                _ctx.SaveChanges();
                newFollowUpTemplateId = eFollowUpTemplate.FollowUpTemplateId;

                return newFollowUpTemplateId;

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding a FollowUp Template", ex);
                //Console.WriteLine("An Error occured adding campaignTemplates" + System.Environment.NewLine + ex);
                return 0;
            }
        }

        public void UpdateFollowUpTemplate(FollowUpTemplate eFollowUpTemplate)
        {
            try
            {
                var updev = _ctx.FollowUpTemplate.First(p => p.FollowUpTemplateId == eFollowUpTemplate.FollowUpTemplateId);

                if (updev.Title != eFollowUpTemplate.Title)
                    updev.Title = eFollowUpTemplate.Title;

                if (updev.TemplateFollowUpMessage != eFollowUpTemplate.TemplateFollowUpMessage)
                    updev.TemplateFollowUpMessage = eFollowUpTemplate.TemplateFollowUpMessage;

                if (updev.GoodResponseMessage != eFollowUpTemplate.GoodResponseMessage)
                    updev.GoodResponseMessage = eFollowUpTemplate.GoodResponseMessage;

                if (updev.BadResponseMessage != eFollowUpTemplate.BadResponseMessage)
                    updev.BadResponseMessage = eFollowUpTemplate.BadResponseMessage;

                if (updev.StartDate != eFollowUpTemplate.StartDate)
                    updev.StartDate = eFollowUpTemplate.StartDate;

                if (updev.EndDate != eFollowUpTemplate.EndDate)
                    updev.EndDate = eFollowUpTemplate.EndDate;

                if (updev.SendDateTime != eFollowUpTemplate.SendDateTime)
                    updev.SendDateTime = eFollowUpTemplate.SendDateTime;

                if (updev.TriageTemplateID != eFollowUpTemplate.TriageTemplateID)
                    updev.TriageTemplateID = eFollowUpTemplate.TriageTemplateID;

                if (updev.GlobalTemplate != eFollowUpTemplate.GlobalTemplate)
                    updev.GlobalTemplate= eFollowUpTemplate.GlobalTemplate;

              
                updev.LastModifiedDate = DateTime.Now;

                _ctx.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating a FollowUp Template", ex);
                //Console.WriteLine("An Error occured updating campaignTemplate" + System.Environment.NewLine + ex);
                return;
            }

        }

        public void DeleteFollowUpTemplate(int eFollowUpTemplateId)
        {
            try
            {
                FollowUpTemplate followUpTemplate = _ctx.FollowUpTemplate.First(p => p.FollowUpTemplateId == eFollowUpTemplateId);
                _ctx.FollowUpTemplate.Remove(followUpTemplate);
                _ctx.SaveChanges();

            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting Following Template", ex);
                return;
            }

        }       

        #endregion 

    }
}
