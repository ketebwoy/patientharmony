﻿using PatientHarmony.Data.Context;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Logging.NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatientHarmony.Data.Managers
{
    public class TriageFollowUpManager
    {
        //private DataManager _ctxFunctions = DataManager.Instance;
        NLogLogger logger = new NLogLogger();
        //real account tokens
        private static string _ACCOUNT_SID = "ACe4219381d5754ac49d9b6a8768901b44";
        private static string _AUTH_TOKEN = "327479b3f55f99741637618b8d737df0";
        private DataContext _ctx = new DataContext();
        private DataManager _ctxFunctions = DataManager.Instance;



        public TriageFollowUp Get(int triageFollowUpId)
        {
            try
            {
                return _ctx.TriageFollowUp.First(p => p.TriageFollowUpId == triageFollowUpId);
            }
            catch (Exception ex)
            {

                logger.Error("An Error occured getting triage followup", ex);
                //Console.WriteLine("An Error occured getting procedure" + System.Environment.NewLine + ex);
                return null;
            }
        }


        public List<TriageFollowUp> GetAllTriageFollowUpList()
        {
            try
            {
                return _ctx.TriageFollowUp.OrderBy(p => p.Title).ToList<TriageFollowUp>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting triage followups", ex);
                //Console.WriteLine("An Error occured getting procedures" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<TriageFollowUp> GetAllFollowUpsForTriage(int triageId)
        {
            var tfuList = new List<TriageFollowUp>();

            try
            {
                return _ctx.TriageFollowUp.Where(p => p.TriageID == triageId).OrderBy(p => p.Title).ToList<TriageFollowUp>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured ", ex);
                // Console.WriteLine("An Error occured getting procedures for campaigns" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public int AddFollowUp(TriageFollowUp eFollowUp)
        {
            int newTFUId = 0;
            eFollowUp.CreatedDate = DateTime.Now;
            eFollowUp.LastDateExecuted = DateTime.Parse("1753-01-01 00:00:00");
            eFollowUp.ProcessedDate = DateTime.Parse("1753-01-01 00:00:00");
            eFollowUp.EndMarker = false; 
            

            try
            {
                _ctx.TriageFollowUp.Add(eFollowUp);
                _ctx.SaveChanges();
                newTFUId = eFollowUp.TriageFollowUpId;
                return newTFUId;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding followUp", ex);
                //Console.WriteLine("An Error occured adding procedure" + System.Environment.NewLine + ex);
                return -1;
            }
        }

        public void UpdateFollowUp(TriageFollowUp eFollowUp)
        {
            try
            {
                var updev = _ctx.TriageFollowUp.First(p => p.TriageFollowUpId == eFollowUp.TriageFollowUpId);

                if (updev.Title != eFollowUp.Title)
                    updev.Title = eFollowUp.Title;

                if (updev.TriageFollowUpMessage != eFollowUp.TriageFollowUpMessage)
                    updev.TriageFollowUpMessage = eFollowUp.TriageFollowUpMessage;

                if (updev.GoodResponseMessage != eFollowUp.GoodResponseMessage)
                    updev.GoodResponseMessage = eFollowUp.GoodResponseMessage;

                if (updev.BadResponseMessage != eFollowUp.BadResponseMessage)
                    updev.BadResponseMessage = eFollowUp.BadResponseMessage;

                if (updev.StartDate != eFollowUp.StartDate)
                    updev.StartDate = eFollowUp.StartDate;

                if (updev.EndDate != eFollowUp.EndDate)
                    updev.EndDate = eFollowUp.EndDate;

                if (updev.SendDateTime != eFollowUp.SendDateTime)
                    updev.SendDateTime = eFollowUp.SendDateTime;

                if (updev.EndMarker != eFollowUp.EndMarker)
                    updev.EndMarker = eFollowUp.EndMarker;

                if (updev.ProcessedDate != eFollowUp.ProcessedDate)
                    updev.ProcessedDate = eFollowUp.ProcessedDate;

                if (updev.LastDateExecuted != eFollowUp.LastDateExecuted)
                    updev.LastDateExecuted = eFollowUp.LastDateExecuted;

                if (updev.VoiceEnabled != eFollowUp.VoiceEnabled)
                    updev.VoiceEnabled = eFollowUp.VoiceEnabled;

                updev.LastModifiedDate = DateTime.Now;

                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating follow Up", ex);
                //Console.WriteLine("An Error occured updating procedure" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void Delete(int id)
        {
            try
            {
                TriageFollowUp fu = _ctx.TriageFollowUp.First(p => p.TriageFollowUpId == id);
                _ctx.TriageFollowUp.Remove(fu);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting follow up", ex);
                //Console.WriteLine("An Error occured deleting procedure" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void FollowUpExecute()
        {
            var today = DateTime.Now;
            var newFollowUpDefaultDate = Convert.ToDateTime("1753-01-01 00:00:00");


            try
            {
                var followUps = _ctx.TriageFollowUp.OrderBy(p => p.TriageFollowUpId).Where(p => p.SendDateTime.Date == today.Date);

                foreach (var followUp in followUps)
                {
                    //get patients in procedure needed list for sendSms
                    var sendTime = followUp.SendDateTime - followUp.SendDateTime.Date;
                    System.Diagnostics.Debug.WriteLine("sendTime = {0}", sendTime.ToString());
                    var rightNow = today - today.Date;
                    System.Diagnostics.Debug.WriteLine("rightNow = {0}", rightNow.ToString());
                    var patient = _ctx.Contacts.First(p => p.ContactId == followUp.ContactID);
                    var triage = _ctx.Triages.First(p => p.TriageId == followUp.TriageID);

                    var group = _ctx.Groups.First(g => g.GroupId == triage.GroupID);

                    //Make sure sms sends at schedule time of day
                    if ((followUp.SendDateTime - followUp.SendDateTime.Date) >= (today.AddMinutes(-5) - today.Date) && (followUp.SendDateTime - followUp.SendDateTime.Date) <= (today.AddMinutes(5) - today.Date))
                    {
                        var pList = new List<Contact>();
                        pList.Add(patient); 
                        Console.WriteLine("Executing Triage Follow up: " + followUp.Title + " " + " for " + group.CompanyName);
                        var sms = new SMSOutgoing();
                        sms.Message = followUp.TriageFollowUpMessage;
                        sms.TriageID = followUp.TriageID;
                        sms.FromPhoneNumber = group.PhoneNumber;
                        _ctxFunctions.SMSmessages.sendSMS(sms, pList);
                        System.Threading.Thread.Sleep(500);
                        followUp.ProcessedDate = DateTime.Now;
                        UpdateFollowUp(followUp);

                    }

                }
            }

            catch (Exception ex)
            {
                logger.Error("An Error occured executing FollowUp task", ex);
                //Console.WriteLine("An Error occured executing Procedure task" + System.Environment.NewLine + ex);
                return;
            }

        }

        
    }
}
