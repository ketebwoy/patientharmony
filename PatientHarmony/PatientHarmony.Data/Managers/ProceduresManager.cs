﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Context;
using PatientHarmony.Data.Logging.NLog;
using PatientHarmony.Data.Models;
using Newtonsoft.Json;
using Twilio;

namespace PatientHarmony.Data.Managers
{
    public class ProceduresManager
    {
        //private DataManager _ctxFunctions = DataManager.Instance;
        NLogLogger logger = new NLogLogger();
        //real account tokens
        private static string _ACCOUNT_SID = "ACe4219381d5754ac49d9b6a8768901b44";
        private static string _AUTH_TOKEN = "327479b3f55f99741637618b8d737df0";
        private DataContext _ctx = new DataContext();        

     

        public Procedure Get(int procedureId)
        {
            try
            {
                return _ctx.Procedures.First(p => p.ProcedureId == procedureId);
            }
            catch (Exception ex)
            {

                logger.Error("An Error occured getting procedure", ex);
                //Console.WriteLine("An Error occured getting procedure" + System.Environment.NewLine + ex);
                return null;
            }
        }


        public List<Procedure> GetAllProcedureList()
        {
            try
            {
                return _ctx.Procedures.OrderBy(p => p.ProcedureTitle).ToList<Procedure>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting procedures", ex);
                //Console.WriteLine("An Error occured getting procedures" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<Procedure> GetAllProceduresForCampaign(int campaignId)
        {
            var procedureList = new List<Procedure>();

            try
            {
                return _ctx.Procedures.Where(p => p.CampaignID == campaignId).OrderBy(p => p.ProcedureTitle).ToList<Procedure>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting procedures for campaigns", ex);
                // Console.WriteLine("An Error occured getting procedures for campaigns" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public int Add(Procedure eProcedure)
        {
            int newProcedureId;
            eProcedure.LastModifiedDate = DateTime.Now;
            eProcedure.CreatedDate = DateTime.Now;
            eProcedure.DateSMSProcessed = DateTime.Parse("1753-01-01 00:00:00");

            try
            {
                _ctx.Procedures.Add(eProcedure);
                _ctx.SaveChanges();
                newProcedureId = eProcedure.ProcedureId;
                return newProcedureId;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding a procedure", ex);
                //Console.WriteLine("An Error occured adding procedure" + System.Environment.NewLine + ex);
                return -1;
            }
        }

        public void UpdateProcedure(Procedure eProcedure)
        {
            try
            {
                var updev = _ctx.Procedures.First(p => p.ProcedureId == eProcedure.ProcedureId);

                if (updev.ProcedureTitle != eProcedure.ProcedureTitle)
                    updev.ProcedureTitle = eProcedure.ProcedureTitle;

                if (updev.SendDateTime != eProcedure.SendDateTime)
                    updev.SendDateTime = eProcedure.SendDateTime;

                if (updev.DateSMSProcessed != eProcedure.DateSMSProcessed)
                    updev.DateSMSProcessed = eProcedure.DateSMSProcessed;

                if (updev.ProcedureMessage != eProcedure.ProcedureMessage)
                    updev.ProcedureMessage = eProcedure.ProcedureMessage;

                if (updev.StartDate != eProcedure.StartDate)
                    updev.StartDate = eProcedure.StartDate;

                if (updev.EndDate != eProcedure.EndDate)
                    updev.EndDate = eProcedure.EndDate;

                if (updev.VoiceEnabled != eProcedure.VoiceEnabled)
                    updev.VoiceEnabled = eProcedure.VoiceEnabled;

                updev.LastModifiedDate = DateTime.Now;

                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating procedure", ex);
                //Console.WriteLine("An Error occured updating procedure" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void Delete(int id)
        {
            try
            {
                Procedure procedure = _ctx.Procedures.First(p => p.ProcedureId == id);
                _ctx.Procedures.Remove(procedure);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting procedure", ex);
                //Console.WriteLine("An Error occured deleting procedure" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void ProcedureExecute()
        {
            var today = DateTime.Now;
            var newProcedureDefaultDate = Convert.ToDateTime("1753-01-01 00:00:00");


            try
            {
                var procedures = _ctx.Procedures.OrderBy(p => p.ProcedureId).Where(p => p.DateSMSProcessed == newProcedureDefaultDate);

                foreach (var procedure in procedures)
                {
                    //get patients in procedure needed list for sendSms
                    var sendTime = procedure.SendDateTime - procedure.SendDateTime.Date;
                    System.Diagnostics.Debug.WriteLine("sendTime = {0}", sendTime.ToString());
                    var rightNow = today - today.Date;
                    System.Diagnostics.Debug.WriteLine("rightNow = {0}", rightNow.ToString());
                    var patientList = GetAllContactsForCampaign(procedure.CampaignID);

                    var campaign = _ctx.Campaigns.First(p => p.CampaignId == procedure.CampaignID);
                    var group = _ctx.Groups.First(g => g.GroupId == campaign.GroupID);

                    //Make sure sms sends at schedule time of day
                    if ((procedure.SendDateTime - procedure.SendDateTime.Date) >= (today.AddMinutes(-5) - today.Date) && (procedure.SendDateTime - procedure.SendDateTime.Date) <= (today.AddMinutes(5) - today.Date))
                    {
                        Console.WriteLine("Executing Procedure " + procedure.ProcedureTitle + " " + " for " + group.CompanyName);
                        var sms = new SMSOutgoing();
                        sms.Message = procedure.ProcedureMessage;
                        sms.CampaignID = procedure.CampaignID;
                        sms.FromPhoneNumber = group.PhoneNumber;
                        sendSMS(sms, patientList);
                        System.Threading.Thread.Sleep(500);
                        procedure.DateSMSProcessed = DateTime.Now;
                        UpdateProcedure(procedure);

                    }

                }
            }

            catch (Exception ex)
            {
                logger.Error("An Error occured executing Procedure task", ex);
                //Console.WriteLine("An Error occured executing Procedure task" + System.Environment.NewLine + ex);
                return;
            }

        }

        public CalendarModel GetCalendarData(int year, int month)
        {
            var model = new List<DayModel>();

            var start = new DateTime(year, month, 1);
            var prev = start.AddMonths(-1);
            var next = start.AddMonths(1);


            var start0 = start;

            if (start.DayOfWeek != DayOfWeek.Monday)
            {
                while (start0.DayOfWeek != DayOfWeek.Monday)
                {
                    start0 = start0.AddDays(-1);
                }
            }

            while (start0 != start)
            {
                DateTime temp = start0.AddDays(1);
                model.Add(new DayModel
                {
                    IsActive = false,
                    EventsCount = _ctx.Procedures.Count(p => (p.StartDate.Day == start0.Day && p.StartDate.Month == start0.Month && p.StartDate.Year == start0.Year
                        || p.EndDate.Day == start0.Day && p.EndDate.Month == start0.Month && p.EndDate.Year == start0.Year) || (p.StartDate < start0 && p.EndDate > temp)),
                    Date = start0
                });
                start0 = temp;
            }

            while (start.Month == month)
            {
                DateTime temp = start.AddDays(1);

                model.Add(new DayModel
                {
                    IsActive = true,
                    EventsCount = _ctx.Procedures.Count(p => (p.StartDate.Day == start.Day && p.StartDate.Month == start.Month && p.StartDate.Year == start.Year
                        || p.EndDate.Day == start.Day && p.EndDate.Month == start.Month && p.EndDate.Year == start.Year) || (p.StartDate < start && p.EndDate > temp)),
                    Date = start
                });
                start = temp;
            }

            if (start.DayOfWeek != DayOfWeek.Monday)
            {
                while (start.DayOfWeek != DayOfWeek.Monday)
                {
                    DateTime temp = start.AddDays(1);
                    model.Add(new DayModel
                    {
                        IsActive = false,
                        EventsCount = _ctx.Procedures.Count(p => (p.StartDate.Day == start.Day && p.StartDate.Month == start.Month && p.StartDate.Year == start.Year
                        || p.EndDate.Day == start.Day && p.EndDate.Month == start.Month && p.EndDate.Year == start.Year) || (p.StartDate < start && p.EndDate > temp)),
                        Date = start
                    });
                    start = temp;
                }
            }


            var ylist = new List<int>();

            if (!(GetAllProcedureList().ToList().Count <= 0))
            {
                for (int i = _ctx.Procedures.Min(x => x.StartDate.Year); i <= _ctx.Procedures.Max(x => x.EndDate.Year); ++i)
                    if (i != year)
                        ylist.Add(i);
            }
            var mlist = (new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }).ToList();
            mlist.Remove(month);

            var cm = new CalendarModel
            {
                Days = model,
                PrevMonth = prev.Month,
                PrevYear = prev.Year,
                NextMonth = next.Month,
                NextYear = next.Year,
                Month = (new DateTime(year, month, 1)).ToString("MMMM"),
                Year = year,
                MonthNum = month,
                Years = ylist,
                Months = mlist
            };

            return cm;
        }

        public string GetSimpleJsonDay(DateTime start)
        {
            var temp = start.AddDays(1);

            var list = _ctx.Procedures.Where(
                   p =>
                   (p.StartDate.Day == start.Day && p.StartDate.Month == start.Month &&
                    p.StartDate.Year == start.Year
                    || p.EndDate.Day == start.Day && p.EndDate.Month == start.Month && p.EndDate.Year == start.Year) ||
                   (p.StartDate < start && p.EndDate > temp)).OrderByDescending(p => p.CreatedDate).ToList();

            var res = new List<SimpleJsonEvent>();

            foreach (var x in list)
            {
                res.Add(new SimpleJsonEvent
                {
                    Title = x.ProcedureTitle,
                    Start = x.StartDate.ToString("dddd, dd MMMM yyyy HH:mm"),
                    End = x.EndDate.ToString("dddd, dd MMMM yyyy HH:mm")
                });
            }


            return JsonConvert.SerializeObject(res);
        }
        public DayProcedureModel Get(DateTime start)
        {

            var temp = start.AddDays(1);
            var list =
                _ctx.Procedures.Include("Campaign").Where(
                    p =>
                    (p.StartDate.Day == start.Day && p.StartDate.Month == start.Month &&
                     p.StartDate.Year == start.Year
                     || p.EndDate.Day == start.Day && p.EndDate.Month == start.Month && p.EndDate.Year == start.Year) ||
                    (p.StartDate < start && p.EndDate > temp)).OrderByDescending(p => p.CreatedDate).ToList();

            var model = new DayProcedureModel
            {
                Date = start.ToString("dddd, dd MMMM yyyy")
            };
            model.ProcedureList = list;

            model.Procedures = list.GroupBy(x => x.Campaign).Select(p => new GroupProceduresModel { Campaign = p.Key.CampaignTitle, Procedures = p.ToList() }).OrderBy(o => o.Campaign).ToList();
            return model;
        }
        public List<Contact> GetAllContactsForCampaign(int campaignId)
        {
            var contactList = new List<Contact>();

            try
            {
                var contactCampaignIds = _ctx.ContactsCamapigns.Where(p => p.CampaignId == campaignId).ToList<ContactsCampaigns>();

                foreach (var item in contactCampaignIds)
                {
                    var contactItem = _ctx.Contacts.First(p => p.ContactId == item.ContactId);
                    if (!contactList.Contains(contactItem))
                        contactList.Add(contactItem);

                }
                return contactList;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured contacts for campaign", ex);
                //Console.WriteLine("An Error occured getting contacts for campaign" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public void sendSMS(SMSOutgoing message, List<Contact> contacts)
        {
            try
            {
                var twilClient = new TwilioRestClient(_ACCOUNT_SID, _AUTH_TOKEN);
                int x = 0;
                // iterate over all patients
                if (contacts != null)
                    foreach (var patient in contacts)
                    {
                        twilClient.SendSmsMessage(
                            message.FromPhoneNumber,
                            contacts[x].PhoneNumber,
                            message.Message
                            );
                        System.Threading.Thread.Sleep(500);
                        message.ToPhoneNumber = contacts[x].PhoneNumber;
                        AddOutgoing(message);
                        x++;
                    }
                x = 0;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured sending sms", ex);
                //Console.WriteLine("An Error occured sending sms" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void AddOutgoing(SMSOutgoing eSMSOutgoing)
        {

            eSMSOutgoing.CreatedDate = DateTime.Now;

            try
            {
                using (var _ctx = new DataContext())
                {
                    _ctx.SMSOutgoings.Add(eSMSOutgoing);
                    _ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding outgoign sms", ex);
                //Console.WriteLine("An Error occured adding outgoing sms" + System.Environment.NewLine + ex);
                return;
            }

        }

    }
}
