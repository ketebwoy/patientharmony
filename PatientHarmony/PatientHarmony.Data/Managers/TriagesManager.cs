﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatientHarmony.Data.Context;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Logging.NLog;
using PatientHarmony.Data.Models;
using Newtonsoft.Json;
using Twilio;
using System.Net.Mail;
using System.Net;



namespace PatientHarmony.Data.Managers
{
    public class TriagesManager
    {
     //   private DataManager _ctxFunctions = DataManager.Instance;
        NLogLogger logger = new NLogLogger();
        //real account tokens
        private static string _ACCOUNT_SID = "ACe4219381d5754ac49d9b6a8768901b44";
        private static string _AUTH_TOKEN = "327479b3f55f99741637618b8d737df0";
        private DataContext _ctx = new DataContext();
        private DataManager _ctxFunctions = DataManager.Instance; 

        

        public List<Triage> GetAllTriage()
        {
            try
            {
                return _ctx.Triages.ToList<Triage>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting all triages", ex);
                //Console.WriteLine("An Error occured getting group triages" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<Triage> GetTriagesForGroup(int groupId)
        {
            try
            {
                return _ctx.Triages.OrderBy(p => p.StartDate).Where(p => p.GroupID == groupId).ToList<Triage>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting group triages", ex);
                //Console.WriteLine("An Error occured getting group triages" + System.Environment.NewLine + ex);
                return null;
            }

        }

        public CalendarModel GetCalendarData(int year, int month)
        {
            var model = new List<DayModel>();

            var start = new DateTime(year, month, 1);
            var prev = start.AddMonths(-1);
            var next = start.AddMonths(1);


            var start0 = start;

            if (start.DayOfWeek != DayOfWeek.Monday)
            {
                while (start0.DayOfWeek != DayOfWeek.Monday)
                {
                    start0 = start0.AddDays(-1);
                }
            }

            while (start0 != start)
            {
                DateTime temp = start0.AddDays(1);
                model.Add(new DayModel
                {
                    IsActive = false,
                    EventsCount = _ctx.Triages.Count(p => (p.StartDate.Day == start0.Day && p.StartDate.Month == start0.Month && p.StartDate.Year == start0.Year
                        || p.EndDate.Day == start0.Day && p.EndDate.Month == start0.Month && p.EndDate.Year == start0.Year) || (p.StartDate < start0 && p.EndDate > temp)),
                    Date = start0
                });
                start0 = temp;
            }

            while (start.Month == month)
            {
                DateTime temp = start.AddDays(1);

                model.Add(new DayModel
                {
                    IsActive = true,
                    EventsCount = _ctx.Triages.Count(p => (p.StartDate.Day == start.Day && p.StartDate.Month == start.Month && p.StartDate.Year == start.Year
                        || p.EndDate.Day == start.Day && p.EndDate.Month == start.Month && p.EndDate.Year == start.Year) || (p.StartDate < start && p.EndDate > temp)),
                    Date = start
                });
                start = temp;
            }

            if (start.DayOfWeek != DayOfWeek.Monday)
            {
                while (start.DayOfWeek != DayOfWeek.Monday)
                {
                    DateTime temp = start.AddDays(1);
                    model.Add(new DayModel
                    {
                        IsActive = false,
                        EventsCount = _ctx.Triages.Count(p => (p.StartDate.Day == start.Day && p.StartDate.Month == start.Month && p.StartDate.Year == start.Year
                        || p.EndDate.Day == start.Day && p.EndDate.Month == start.Month && p.EndDate.Year == start.Year) || (p.StartDate < start && p.EndDate > temp)),
                        Date = start
                    });
                    start = temp;
                }
            }


            var ylist = new List<int>();

            if (!(GetAllTriage().ToList().Count <= 0))
            {
                for (int i = _ctx.Triages.Min(x => x.StartDate.Year); i <= _ctx.Triages.Max(x => x.EndDate.Year); ++i)
                    if (i != year)
                        ylist.Add(i);
            }
            var mlist = (new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }).ToList();
            mlist.Remove(month);

            var cm = new CalendarModel
            {
                Days = model,
                PrevMonth = prev.Month,
                PrevYear = prev.Year,
                NextMonth = next.Month,
                NextYear = next.Year,
                Month = (new DateTime(year, month, 1)).ToString("MMMM"),
                Year = year,
                MonthNum = month,
                Years = ylist,
                Months = mlist
            };

            return cm;
        }

        public string GetSimpleJsonDay(DateTime start)
        {
            var temp = start.AddDays(1);

            var list = _ctx.Triages.Where(
                   p =>
                   (p.StartDate.Day == start.Day && p.StartDate.Month == start.Month &&
                    p.StartDate.Year == start.Year
                    || p.EndDate.Day == start.Day && p.EndDate.Month == start.Month && p.EndDate.Year == start.Year) ||
                   (p.StartDate < start && p.EndDate > temp)).OrderByDescending(p => p.CreatedDate).ToList();

            var res = new List<SimpleJsonEvent>();

            foreach (var x in list)
            {
                res.Add(new SimpleJsonEvent
                {
                    Title = x.Title,
                    Start = x.StartDate.ToString("dddd, dd MMMM yyyy HH:mm"),
                    End = x.EndDate.ToString("dddd, dd MMMM yyyy HH:mm")
                });
            }


            return JsonConvert.SerializeObject(res);
        }

        public DayTriageModel Get(DateTime start)
        {
            var triageObject = new TriageObject();
            var temp = start.AddDays(1);
            var list =
                _ctx.Triages.Include("Group").Where(
                    p =>
                    (p.StartDate.Day == start.Day && p.StartDate.Month == start.Month &&
                     p.StartDate.Year == start.Year
                     || p.EndDate.Day == start.Day && p.EndDate.Month == start.Month && p.EndDate.Year == start.Year) ||
                    (p.StartDate < start && p.EndDate > temp)).OrderByDescending(p => p.CreatedDate).ToList();

            var model = new DayTriageModel
            {
                Date = start.ToString("dddd, dd MMMM yyyy"),
                AjaxDate = start
            };

            model.TriageList = list;
            model.TriageObjects = triageObject.TriageObjectList(model.TriageList, _ctxFunctions.Contacts.GetAllList());


            foreach (var item in model.TriageList)
            {
                item.Group = _ctxFunctions.Groups.Get(item.GroupID);
            }

            model.Triages = list.GroupBy(x => x.Group).Select(p => new GroupTriagesModel { group = p.Key.CompanyName, Triages = p.ToList() }).OrderBy(o => o.group).ToList();

           
            return model;
        }

        public List<Triage> GetTriagesForCampaign(int campaignId)
        {
            var triageList = new List<Triage>(); 
            try
            {
                var camapign = _ctxFunctions.Campaigns.GetCampaign(campaignId);
                var contactsCampaigns = _ctxFunctions.ManyToManyRelationShips.GetAllContactsForCampaign(campaignId);
                
                foreach(var contact in contactsCampaigns)
                {
                    triageList.Add(_ctx.Triages.First(p => p.ContactID == contact.ContactId && p.GroupID == camapign.GroupID));
                }

                return triageList;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting campaign triages", ex);
                //Console.WriteLine("An Error occured getting group triages" + System.Environment.NewLine + ex);
                return null;
            }

        }



        public Triage GetTriage(int id)
        {
            try
            {
                return _ctx.Triages.First(p => p.TriageId == id);
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting triage", ex);
                //Console.WriteLine("An Error occured getting triage" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public int Add(Triage eTriage)
        {
            eTriage.LastDateProcessed = DateTime.Parse("1753-01-01 00:00:00");
            eTriage.LastReplyDate = DateTime.Parse("1753-01-01 00:00:00");
            
            try
            {

                eTriage.CreatedDate = DateTime.Now;
             
                using (var context = new DataContext())
                {
                    context.Triages.Add(eTriage);
                    context.SaveChanges();
                }
                return eTriage.TriageId;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding a triage", ex);
                //Console.WriteLine("An Error occured adding a triage" + System.Environment.NewLine + ex);
                return -1;
            }
        }

        public void Update(Triage eTriage)
        {
            try
            {
                var updev = _ctx.Triages.First(p => p.TriageId == eTriage.TriageId);
                updev.Group = null; 

                if (updev.Title != eTriage.Title)
                    updev.Title = eTriage.Title;

                if (updev.TriageMessage != eTriage.TriageMessage)
                    updev.TriageMessage = eTriage.TriageMessage;

                if (updev.GoodResponseMessage != eTriage.GoodResponseMessage)
                    updev.GoodResponseMessage = eTriage.GoodResponseMessage;

                if (updev.BadResponseMessage != eTriage.BadResponseMessage)
                    updev.BadResponseMessage = eTriage.BadResponseMessage;

                if (updev.StartDate != eTriage.StartDate)
                    updev.StartDate = eTriage.StartDate;
                if (updev.StartDate.Year <= 1753)
                    updev.StartDate = new DateTime().AddYears(1752);


                if (updev.EndDate != eTriage.EndDate)
                    updev.EndDate = eTriage.EndDate;
                if (updev.EndDate.Year <= 1753)
                    updev.EndDate = new DateTime().AddYears(1752); 


                if (updev.GroupID != eTriage.GroupID)
                    updev.GroupID = eTriage.GroupID;

                if (updev.LastDateProcessed != eTriage.LastDateProcessed)
                    updev.LastDateProcessed = eTriage.LastDateProcessed;
                if (updev.LastDateProcessed.Year <= 1753)
                    updev.LastDateProcessed = new DateTime().AddYears(1752);


                if (updev.SendDateTime != eTriage.SendDateTime)
                    updev.SendDateTime = eTriage.SendDateTime;
                if (updev.SendDateTime.Year <= 1753)
                    updev.SendDateTime = new DateTime().AddYears(1752);

                               
                if (updev.EndMarker != eTriage.EndMarker)
                    updev.EndMarker = eTriage.EndMarker;

                if (updev.HealthTracker != eTriage.HealthTracker)
                    updev.HealthTracker = eTriage.HealthTracker;

                if (updev.TriageScaleType != eTriage.TriageScaleType)
                    updev.TriageScaleType = eTriage.TriageScaleType;

                if (updev.TriggerPoint != eTriage.TriggerPoint)
                    updev.TriggerPoint = eTriage.TriggerPoint;

                if (updev.DisableTriageExtension != eTriage.DisableTriageExtension)
                    updev.DisableTriageExtension = eTriage.DisableTriageExtension;

                if (updev.Frequency != eTriage.Frequency)
                    updev.Frequency = eTriage.Frequency;


                if (updev.SendDays != eTriage.SendDays)
                    updev.SendDays = eTriage.SendDays;
                
                if (updev.VoiceEnabled != eTriage.VoiceEnabled)
                    updev.VoiceEnabled = eTriage.VoiceEnabled;
                

                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating Triage", ex);
            }
        }

        public void Remove(int id)
        {
            try
            {
                using (var context = new DataContext())
                {
                var triage = context.Triages.First(p => p.TriageId == id); 
                context.Triages.Remove(triage);
                context.SaveChanges();

                var associatedFollowUps = _ctxFunctions.TriageFollowUps.GetAllFollowUpsForTriage(id);

                if (associatedFollowUps.Count > 0)
                {
                    foreach (var fu in associatedFollowUps)
                    {
                        _ctxFunctions.TriageFollowUps.Delete(fu.TriageFollowUpId);
                    }
                }
                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting a triage", ex);
            }
        }


  
        public void TriageExecute()
        {
            var today = DateTime.Now;
            var newTriageDefaultDate = Convert.ToDateTime("1753-01-01 00:00:00");

            try
            {
                var Triages = GetAllTriage().OrderBy(p => p.StartDate).Where(p => p.StartDate <= today && p.EndDate >= today);
                foreach (var Triage in Triages)
                {
                    Contact patient = _ctx.Contacts.First(p => p.ContactId == Triage.ContactID);

                    //need list for sendSms
                    var patientList = new List<Contact>();
                    patientList.Add(patient);

                    var group = _ctx.Groups.First(g => g.GroupId == Triage.GroupID);

                    //Make sure Triage has not been processed today
                    if (Triage.LastDateProcessed <= Triage.EndDate && Triage.LastDateProcessed.Date < today.Date || Triage.LastDateProcessed == newTriageDefaultDate)
                    {

                        //Make sure sms sends at Triage time of day
                        if ((Triage.SendDateTime - Triage.SendDateTime.Date) >= (today.AddMinutes(-5) - today.Date) && (Triage.SendDateTime - Triage.SendDateTime.Date) <= (today.AddMinutes(5) - today.Date))
                        {
                            Console.WriteLine("Executing Triage " + Triage.Title + " " + " for " + group.CompanyName);
                            var sms = new SMSOutgoing();
                            sms.Message = Triage.TriageMessage;
                            sms.TriageID = Triage.TriageId;
                            sms.FromPhoneNumber = group.PhoneNumber;
                            sendSMS(sms, patientList);
                            System.Threading.Thread.Sleep(500);
                            Triage.LastDateProcessed = DateTime.Now;
                            Update(Triage);
                            Console.WriteLine("Triage " + Triage.Title + " processed at: " + Triage.LastDateProcessed);
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine("An Error occured executing Triage task" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void TriageLastDayExecute()
        {
            var today = DateTime.Now;
            var yesterday = DateTime.Today.AddDays(-1);

            try
            {
                //grab Triages that finished yesterday
                var Triages = _ctx.Triages.OrderBy(p => p.EndDate).Where(p => p.EndDate == yesterday && p.EndMarker == false);

                foreach (var Triage in Triages)
                {

                    Contact patient = _ctx.Contacts.First(p => p.ContactId == Triage.ContactID);
                    Group group = _ctx.Groups.First(g => g.GroupId == Triage.GroupID);

                    sendTriageFinishEmail(patient, group, Triage.HealthTracker);
                    Triage.EndMarker = true;
                    Update(Triage);
                    Console.WriteLine("Triage " + Triage.Title + " processed at: " + Triage.LastDateProcessed);

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("An Error occured executing Last Day Triage task" + System.Environment.NewLine + ex);
                return;
            }
        }

        /*form and send email to doctor notifying him/her patient feels worse */
        private void sendTriageFinishEmail(Contact patient, Group group, string patientHealthRecord)
        {

            var newEmail = new Email();

            string emailTemplate = @"
                                                                               
                                        Patient ##PATIENTNAME## has reached the end of triage tracking. 

                                        Through out the triage they responded via SMS with a scale of 1 - 10 based on how they felt 1 being worst health 10 being best health. 
                                        
                                        Their responses in sorted by recieving date =: ##HEALTHRECORD##
                                       
                                        Their contact info is below: 

                                        ##PATIENTNAME##
                                        ##EMAILADDRESS##
                                        ##PHONENUMBER##

                                        Best regards, 
                                        ##ADMINNAME##";

            string emailBody = emailTemplate
                .Replace("##ClientName##", group.CompanyName)
                .Replace("##PATIENTNAME##", patient.FirstName + " " + patient.LastName)
                .Replace("##EMAILADDRESS##", patient.EmailAddress)
                .Replace("##PHONENUMBER##", patient.PhoneNumber)
                .Replace("##HEALTHRECORD##", printHealthRecord(patientHealthRecord.ToCharArray()))
                .Replace("##ADMINNAME##", "WellbeingSMS Administrator");

            newEmail.ToEmail = group.Email + "," + patient.InsurerEmailAddress;
            newEmail.Subject = "Patient Health Status Notification";
            newEmail.Message = emailBody;


           SendEmail(newEmail);
        }

        private string printHealthRecord(char[] healthRecord)
        {
            var sb = new StringBuilder();

            foreach (var stat in healthRecord)
            {
                var recordData = string.Empty;
                if (stat.Equals('*'))
                {
                    recordData = "10";
                }
                else
                { recordData = stat.ToString(); }

                sb.Append("[" + recordData + "] ");

            }
            return sb.ToString();
        }

        public void sendSMS(SMSOutgoing message, List<Contact> contacts)
        {
            try
            {
                var twilClient = new TwilioRestClient(_ACCOUNT_SID, _AUTH_TOKEN);
                int x = 0;
                // iterate over all patients
                if (contacts != null)
                    foreach (var patient in contacts)
                    {
                        twilClient.SendSmsMessage(
                            message.FromPhoneNumber,
                            contacts[x].PhoneNumber,
                            message.Message
                            );
                        System.Threading.Thread.Sleep(500);
                        message.ToPhoneNumber = contacts[x].PhoneNumber;
                        AddOutgoing(message);
                        x++;
                    }
                x = 0;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured sending sms", ex);
                //Console.WriteLine("An Error occured sending sms" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void AddOutgoing(SMSOutgoing eSMSOutgoing)
        {

            eSMSOutgoing.CreatedDate = DateTime.Now;

            try
            {
                _ctx.SMSOutgoings.Add(eSMSOutgoing);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding outgoign sms", ex);
                //Console.WriteLine("An Error occured adding outgoing sms" + System.Environment.NewLine + ex);
                return;
            }

        }

        public void SendEmail(Email email, List<string> recipients = null, List<string> ccRecipients = null)
        {

            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("mail.wellbeingsms.com", 465);
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new NetworkCredential("wellbeingadmin@wellbeingsms.com", "S3attlesnows!");

                SmtpServer.Port = 587;



                mail.From = new MailAddress("wellbeingadmin@wellbeingsms.com");
                mail.Subject = email.Subject;
                mail.Body = email.Message;


                if (recipients == null)
                {
                    mail.To.Add(email.ToEmail);
                    SmtpServer.Send(mail);
                    AddEmail(email);
                }

                if (recipients.Count > 0)
                {
                    foreach (var rec in recipients)
                    {
                        mail.To.Add(rec);

                    }
                    SmtpServer.Send(mail);
                    AddEmail(email);
                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured sending email", ex);
                //Console.WriteLine("An Error occured sending email" + System.Environment.NewLine + ex);
                return;
            }

        }

        public int AddEmail(Email email)
        {
            try
            {
                email.FromEmail = "wellbeingadmin@wellbeingsms.com";
                email.CreatedDate = DateTime.Now;
                _ctx.Emails.Add(email);
                _ctx.SaveChanges();

                return email.EmailId;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding email", ex);
                //Console.WriteLine("An Error occured adding email" + System.Environment.NewLine + ex);
                return 0;
            }
        }

        public bool TriageExistCheck(int patientID, int groupID)
        {
            var triageExist = false;

            var triages = _ctx.Triages.Where(p => p.ContactID == patientID && p.GroupID == groupID);

            if (triages.Count() > 0)
                triageExist = true; 

            return triageExist; 
        }
    }
}

