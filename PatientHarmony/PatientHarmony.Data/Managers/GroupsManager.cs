﻿using System;
using System.Collections.Generic;
using System.Linq;
using PatientHarmony.Data.Context;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Managers;
using Twilio;
using PatientHarmony.Data.Logging.NLog;
using PhoneNumbers;

namespace PatientHarmony.Data.Managers
{
    public class GroupsManager
    {
        //private static string _TestAccountSID = "ACbe315fdb25d3431fef6b58c69fea1499";
        //private static string _TestAuthToken = "dd23663513ff033848413e68419e19f6";

        //real account tokens
        private static string _ACCOUNT_SID = "ACe4219381d5754ac49d9b6a8768901b44";
        private static string _AUTH_TOKEN = "327479b3f55f99741637618b8d737df0";

        NLogLogger logger = new NLogLogger();
        private DataContext _ctx = new DataContext(); 


        public Group Get(int groupId)
        {
            
            try
            {
                var group = new Group();
                var temp = _ctx.Groups.First(p => p.GroupId == groupId);

                if (temp != null)
                {
                    group.GroupId = temp.GroupId;
                    group.CompanyName = temp.CompanyName;
                    group.Email = temp.Email;
                    group.PhoneNumber = temp.PhoneNumber;
                    group.CreatedDate = temp.CreatedDate;
                    group.LastModifiedDate = temp.LastModifiedDate;
                   
                }
                return group;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting a group", ex);
                // Console.WriteLine("An Error occured getting a group" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<Group> GetAllGroups()
        {
            try
            {
                var groupList = _ctx.Groups.OrderBy(p => p.CompanyName).ToList<Group>();

                return groupList;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting groups", ex);
                //Console.WriteLine("An Error occured getting groups" + System.Environment.NewLine + ex);
                return null;
            }

        }


        public int Add(Group eGroup)
        {
            int newGroupId;

            try
            {
                _ctx.Groups.Add(eGroup);
                _ctx.SaveChanges();
                newGroupId = eGroup.GroupId;

                return newGroupId;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding group", ex);
                //Console.WriteLine("An Error occured adding group" + System.Environment.NewLine + ex);
                return -1;
            }
        }

        public void UpdateGroup(Group eGroup)
        {
            try
            {
                var updev = _ctx.Groups.First(p => p.GroupId == eGroup.GroupId);

                if (updev.CompanyName != eGroup.CompanyName)
                    updev.CompanyName = eGroup.CompanyName;

                if (updev.Email != eGroup.Email)
                    updev.Email = eGroup.Email;

                if (updev.PhoneNumber != eGroup.PhoneNumber)
                    updev.PhoneNumber = eGroup.PhoneNumber;

                updev.LastModifiedDate = DateTime.Now;

                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating group", ex);
                //  Console.WriteLine("An Error occured updating group" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void DeleteGroup(int id)
        {
            try
            {
                Group group = _ctx.Groups.First(p => p.GroupId == id);
                _ctx.Groups.Remove(group);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting a group", ex);
                //Console.WriteLine("An Error occured deleting groups" + System.Environment.NewLine + ex);
                return;
            }
        }

        public AvailablePhoneNumber FindNewTwilioNumber(string friendlyName = null, string areaCode = null, string zipcode = null, string nearNumber = null, string containsNumbers = null)
        {
            var twilClient = new TwilioRestClient(_ACCOUNT_SID, _AUTH_TOKEN);
            var options = new Twilio.AvailablePhoneNumberListRequest();
            
            if(areaCode != null)
            {
                options.AreaCode = areaCode; 
            }
                        
            // Get the account and phone number factory class
            Account acct = twilClient.GetAccount();

            try
            {
                /* Initiate US Local PhoneNumber search with $SearchParams list */
                var availableNumbers = new AvailablePhoneNumberResult(); 
                   availableNumbers = twilClient.ListAvailableLocalPhoneNumbers("US", options);

                /* If we did not find any phone numbers let the user know */
                if (availableNumbers.AvailablePhoneNumbers.Count <= 0)
                {
                    logger.Info("no phone numbers for a new group were found by that search");
                    //Console.WriteLine("We didn't find any phone numbers by that search");
                    return null;
                }
                else
                {
                    // return the first number in the list
                    return availableNumbers.AvailablePhoneNumbers[0]; ;
                }
            }

            catch (Exception ex)
            {
                logger.Error("An Error occured finding a new SMS number for a group", ex);
                //   Console.WriteLine("An Error Occurred finding a new SMS number for a group" + System.Environment.NewLine + ex);
                return null;
            }

        }

        public string PurchaseNewTwilioNumber(AvailablePhoneNumber availableNumber)
        {
            var twilClient = new TwilioRestClient(_ACCOUNT_SID, _AUTH_TOKEN);

            var purchaseOptions = new PhoneNumberOptions();
            purchaseOptions.PhoneNumber = availableNumber.PhoneNumber;
            purchaseOptions.SmsUrl = "http://my.patientharmony.com//Sms/IncomingSMS";

            var number = twilClient.AddIncomingPhoneNumber(purchaseOptions);

            var util = PhoneNumberUtil.GetInstance(); 

            var phoneNumber = util.Parse(number.PhoneNumber, "US");

        
            return phoneNumber.NationalNumber.ToString();



        }

    }
}

