﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Context;
using Twilio;
using PhoneNumbers;
using PatientHarmony.Data.Logging.NLog;
using System.Net.Mail;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;

namespace PatientHarmony.Data.Managers
{
    public class SMSManager
    {
        #region Constants

        private static string _TestAccountSID = "ACbe315fdb25d3431fef6b58c69fea1499";
        private static string _TestAuthToken = "dd23663513ff033848413e68419e19f6";

        //real account tokens
        private static string _ACCOUNT_SID = "ACe4219381d5754ac49d9b6a8768901b44";
        private static string _AUTH_TOKEN = "327479b3f55f99741637618b8d737df0";

        private static string _TwilioFromNumber = "19043375299";
        NLogLogger logger = new NLogLogger();
        private DataContext _ctx = new DataContext();
        private DataManager _ctxFunctions = DataManager.Instance;

        #endregion

        #region Outgoing

        public List<SMSOutgoing> GetAllOutgoingForUser(List<UserDefaultGroup> userGroups)
        {
            var outgoingForUser = new List<SMSOutgoing>();
            try
            {
                var userGroupList = GetGroupsEntitiesForUser(userGroups);

                foreach (var group in userGroupList)
                {
                    var smsList = _ctx.SMSOutgoings.Where(x => x.FromPhoneNumber == group.PhoneNumber).ToList<SMSOutgoing>();

                    outgoingForUser.AddRange(smsList);
                }

                return outgoingForUser;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting all outgoing sms for user", ex);
                //Console.WriteLine("An Error occured getting all outgoing SMS" + System.Environment.NewLine + ex);
                return null;
            }
        }


        public SMSOutgoing GetOutgoing(int smsId)
        {
            try
            {
                return _ctx.SMSOutgoings.First(p => p.SmsId == smsId);
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting outgoing sms", ex);
                //Console.WriteLine("An Error occured getting outgoing SMS" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<SMSOutgoing> GetAllOutgoing()
        {
            try
            {
                return _ctx.SMSOutgoings.OrderBy(p => p.CreatedDate).ToList<SMSOutgoing>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting all outgoing sms", ex);
                //Console.WriteLine("An Error occured getting all outgoing SMS" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public SMSOutgoing GetoutgoingDetail(int smsId)
        {
            try
            {
                return _ctx.SMSOutgoings.First(p => p.SmsId == smsId);
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting outgoing sms details", ex);
                //Console.WriteLine("An Error occured getting outgoing SMS Detail" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public void AddOutgoing(SMSOutgoing eSMSOutgoing)
        {

            eSMSOutgoing.CreatedDate = DateTime.Now;

            try
            {
                _ctx.SMSOutgoings.Add(eSMSOutgoing);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding outgoign sms", ex);
                //Console.WriteLine("An Error occured adding outgoing sms" + System.Environment.NewLine + ex);
                return;
            }

        }

        public void sendTestSMS(SMSOutgoing message, List<Contact> patientList)
        {
            try
            {
                var twilClient = new TwilioRestClient(_TestAccountSID, _TestAuthToken);
                var splitMessageList = new List<String>();
                int stringLength = message.Message.Length;
                
                if (patientList.Count > 0 && patientList != null && stringLength > 0)
                {
                    splitMessageList = splitLongMessage(message.Message);

                    foreach (var patient in patientList)
                    {
                        if(message.ToPhoneNumber == null)
                        message.ToPhoneNumber = patient.PhoneNumber;

                        foreach (var sms in splitMessageList)
                        {
                            twilClient.SendSmsMessage(message.FromPhoneNumber, message.ToPhoneNumber, sms);
                            AddOutgoing(message);
                            Thread.Sleep(15000);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured sending test SMS", ex);
                //Console.WriteLine("An Error occured sending test sms" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void sendSMS(SMSOutgoing message, List<Contact> patientList)
        {
            try
            {
                var twilClient = new TwilioRestClient(_ACCOUNT_SID, _AUTH_TOKEN);
                var splitMessageList = new List<String>();
                int stringLength = message.Message.Length;
                                
                if (patientList.Count > 0 && patientList != null && stringLength > 0)
                {
                   splitMessageList = splitLongMessage(message.Message);

                   foreach (var patient in patientList)
                   {
                       if (message.ToPhoneNumber == null)
                       message.ToPhoneNumber = patient.PhoneNumber;

                       foreach (var sms in splitMessageList)
                       {
                           twilClient.SendSmsMessage(message.FromPhoneNumber, message.ToPhoneNumber, sms);
                           var smsTemp = new SMSOutgoing();
                           smsTemp.ToPhoneNumber = message.ToPhoneNumber;
                           smsTemp.FromPhoneNumber = message.FromPhoneNumber;
                           smsTemp.Message = sms;
                           smsTemp.TriageID = message.TriageID;
                           smsTemp.CampaignID = message.CampaignID;
                           AddOutgoing(smsTemp);
                           Thread.Sleep(5000);
                       }
                   }
                  
                }
            }               
            catch (Exception ex)
            {
                logger.Error("An Error occured sending sms", ex);
                //Console.WriteLine("An Error occured sending sms" + System.Environment.NewLine + ex);
                return;
            }
        
        }
        
        #endregion

        #region incoming SMS

        public SMSIncoming GetincomingDetail(int smsId)
        {
            try
            {
                return _ctx.SMSIncomings.First(p => p.SmsId == smsId);
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting incoming sms detail", ex);
                //Console.WriteLine("An Error occured getting incoming smsdetail" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<SMSIncoming> GetAllIncomingForUser(List<UserDefaultGroup> userGroups)
        {
            var incomingForUser = new List<SMSIncoming>();
            try
            {
                var userGroupList = GetGroupsEntitiesForUser(userGroups);

                foreach (var group in userGroupList)
                {
                    var smsList = _ctx.SMSIncomings.Where(x => x.ToPhoneNumber == group.PhoneNumber).ToList<SMSIncoming>();

                    incomingForUser.AddRange(smsList);
                }

                return incomingForUser;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting all outgoing sms for user", ex);
                //Console.WriteLine("An Error occured getting all outgoing SMS" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public List<SMSIncoming> GetAllincoming()
        {
            try
            {
                return _ctx.SMSIncomings.OrderBy(p => p.CreatedDate).ToList<SMSIncoming>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting getting incoming sms List", ex);
                //Console.WriteLine("An Error occured getting incoming sms List" + System.Environment.NewLine + ex);
                return null;
            }
        }

        public void Addincoming(SMSIncoming eSMSIncoming)
        {
            try
            {
                var util = PhoneNumberUtil.GetInstance();
                var ToNumber = util.Parse(eSMSIncoming.ToPhoneNumber, "US");
                var FromNumber = util.Parse(eSMSIncoming.FromPhoneNumber, "US");

                eSMSIncoming.ToPhoneNumber = ToNumber.NationalNumber.ToString();
                eSMSIncoming.FromPhoneNumber = FromNumber.NationalNumber.ToString();
               
                //this is set in chat message during incoming
                if(eSMSIncoming.processed != true)
                eSMSIncoming.processed = false;

                _ctx.SMSIncomings.Add(eSMSIncoming);
                _ctx.SaveChanges();
                // string response = triageProcessing(eSMSIncoming);
                // return response; 
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting incoming sms", ex);
                //Console.WriteLine("An Error occured adding incoming sms" + System.Environment.NewLine + ex);
                return;
            }

        }

        public void MarkProcessed(SMSIncoming eSMSIncoming, int triageId = 0)
        {
            var updev = _ctx.SMSIncomings.First(p => p.SmsId == eSMSIncoming.SmsId);

            if(triageId > 0) { updev.TriageID = triageId; }

            updev.processed = true;
            _ctx.SaveChanges();
        }

        /*
         * get incoming message
         * compare incoming message to current Health
         * append new status to health record
         * if health status has dropped more than 2 digits prolong triag by a week,  
         * extra parameter false for incoming sms reply, need to see how system responds if a triage is available
         */
        public string triageProcessing(SMSIncoming eSMSIncoming, bool communicate = true)
        {
            
            string autoReply = string.Empty; 
            var defaultDate = Convert.ToDateTime("1753-01-01 00:00:00");

            int x = 0;
          
            //find out who sent text and get thier current health rating
            var patient = _ctx.Contacts.First(p => p.PhoneNumber == eSMSIncoming.FromPhoneNumber);

            //find out what group the sms was sent to
            var group = _ctx.Groups.First(p => p.PhoneNumber == eSMSIncoming.ToPhoneNumber);


            //find triage that patient belongs to that has not been processed today
            var triage = new Triage();

            var triageList = _ctxFunctions.Triages.GetAllTriage().Where(p => (p.ContactID == patient.ContactId) && (p.EndDate.Date >= DateTime.Now.Date) && (p.GroupID == group.GroupId) && (p.EndMarker == false));;

            triage = triageList.FirstOrDefault(p => (p.LastReplyDate == defaultDate)  || (p.LastReplyDate.Date < DateTime.Now.Date));

           if(triage == null)
           {
               triage = new Triage(); 
           }

            if(triage.HealthTracker == null)
            {
                triage.HealthTracker = ""; 
            }

            if (triage == default(Triage)) // null for reference types.
            {
                if(communicate)
                MarkProcessed(eSMSIncoming);
                return null; 
            }

            var incomingHealthStatus = extractTriageRating(eSMSIncoming, triage);

            //send error sms to sender if format was wrong
            if (incomingHealthStatus is string && triage.LastReplyDate.Date < DateTime.Now.Date)
            {
                if(communicate)
                MarkProcessed(eSMSIncoming);
                autoReply = Convert.ToString(incomingHealthStatus);
            }

            else
            {
                var tryParseRating = Int32.TryParse(incomingHealthStatus.ToString(), out x);

                var scaletype = triage.TriageScaleType;
                var triggerpoint = triage.TriggerPoint;

                bool prolongTriage = false;
                int oldHealthStatus = 0;

                //Process the triage this patient belongs to

                //get health stats 
                if (!String.IsNullOrEmpty(triage.HealthTracker.Trim()))
                {
                    var currentHealthStatus = triage.HealthTracker.ToCharArray().Last();

                    if (currentHealthStatus == '*')
                    {
                        oldHealthStatus = 10;
                    }
                    else
                        oldHealthStatus = Int32.Parse(currentHealthStatus.ToString());
                }

                if (tryParseRating)
                {
                    int newHealthStatus = x;

                    if (triage.HealthTracker != null)
                    {
                        prolongTriage = compareHealth(oldHealthStatus, newHealthStatus, triggerpoint, scaletype, triage.HealthTracker.ToCharArray());
                    }
                    else
                    {
                        prolongTriage = compareHealth(oldHealthStatus, newHealthStatus, triggerpoint, scaletype, null);
                    }

                    if (communicate)
                    {
                        //handle Triages, email, and patient records
                        addToHealthRecord(triage, newHealthStatus);
                        MarkProcessed(eSMSIncoming, triage.TriageId);
                    }   
                    
                    autoReply = processTriages(triage, prolongTriage, communicate);
                    
                }

                else if (incomingHealthStatus is char)
                {
                    if (triage.HealthTracker != null)
                    {
                        prolongTriage = compareHealth(oldHealthStatus, 10, triggerpoint, scaletype, triage.HealthTracker.ToCharArray());
                    }
                    else
                    {
                        prolongTriage = compareHealth(oldHealthStatus, 10, triggerpoint, scaletype, null);
                    }

                    if(communicate)
                    {
                        addToHealthRecord(triage, 10);
                        MarkProcessed(eSMSIncoming, triage.TriageId);
                        autoReply = processTriages(triage, prolongTriage);
                    }
                }

            }
                

            return autoReply;
        }

        public string followUpProcessing(SMSIncoming eSMSIncoming)
        {
            string reply = string.Empty;

            int x = 0;

            //find out who sent text and get thier current health rating
            var patient = _ctx.Contacts.First(p => p.PhoneNumber == eSMSIncoming.FromPhoneNumber);

            //find out what group the sms was sent to
            var group = _ctx.Groups.First(p => p.PhoneNumber == eSMSIncoming.ToPhoneNumber);


            //find triage that patient belongs to that has not been processed today
            var followUp = new TriageFollowUp();

            var followUpList = _ctxFunctions.TriageFollowUps.GetAllTriageFollowUpList();

            followUp = followUpList.FirstOrDefault(p => p.ContactID == patient.ContactId && p.EndMarker == false);

            var patientAnswer = extractFollowUpRating(eSMSIncoming);

            //send error sms to sender if format was wrong
            if (patientAnswer is string)
            {
                MarkProcessed(eSMSIncoming);
                string answerParse = Convert.ToString(patientAnswer).ToLower();                            
                
                switch(answerParse)
                {
                    case "yes":
                        reply = followUp.GoodResponseMessage;
                        break;

                    case "no":
                        reply = followUp.BadResponseMessage;
                        break;

                    case "please reply with 'yes' or 'no' ":
                        reply = answerParse;
                        break;
                }
                              
                                      
                
            }

            return reply; 
        }

        public bool smsMeantForTriage(SMSIncoming sms)
        {

            //find out who sent text and get thier current health rating
            var patient = _ctx.Contacts.First(p => p.PhoneNumber == sms.FromPhoneNumber);

            //find out what group the sms was sent to
            var group = _ctx.Groups.First(p => p.PhoneNumber == sms.ToPhoneNumber);

            //find triage that patient belongs to that has not been processed today
            var triage = new Triage();
            triage = _ctxFunctions.Triages.GetAllTriage().FirstOrDefault(p => p.ContactID == patient.ContactId && p.EndDate.Date >= DateTime.Now.Date && p.GroupID == group.GroupId && p.EndMarker == false && p.LastReplyDate.Date < DateTime.Now.Date);

            if (triage != null)
            {
                var triageMessage = extractTriageRating(sms, triage);  //extractTriageRating() _ctxFunctions.SMSmessages.triageProcessing(sms);
                var followUpMessage = Convert.ToString(extractFollowUpRating(sms));
                string extractFromMessage = ExtractNumbers(sms.Message);
                int extractedRating = 0;
                var tryTriageRating = Int32.TryParse(extractFromMessage, out extractedRating);

                if (!tryTriageRating)
                {
                    if (triageMessage is string)
                    {
                        var triMess = Convert.ToString(triageMessage);

                        if ((followUpMessage.ToLower().Contains("yes"))
                                || (followUpMessage.ToLower().Contains("no"))
                                        && (triMess.ToLower().Contains("please reply with '1' for yes or '2' for no ")) || (triMess.ToLower().Contains("please enter only a number ranging from 1 - 10")))
                        {
                            return false; //sms meant for followup
                        }
                        else
                        {
                            return true; //only Triages have numbers;
                        }
                    }
                }
                else
                {
                    return true; //only Triages have numbers;
                }
            }

            return false; // if no triages available to answer, must be for a followup
        }
    
        /*pull health rating out of patient incoming text*/
        private object extractTriageRating(SMSIncoming message, Triage triage)
        {
            string rawMessage = message.Message;
            object result = new object();

            var switchBool = false;

            if (triage.TriageScaleType == TriageScaleType.Decision)
            {
                switchBool = true;
            }


            //look for 1/2 answer
            if (switchBool)
            {

                if (rawMessage.ToLower().Contains("1") || rawMessage.ToLower().Contains("2"))
                {
                    if (rawMessage.ToLower().Contains("1") && !rawMessage.ToLower().Contains("2"))
                    {
                        result = 1;

                    }
                    else if (!rawMessage.ToLower().Contains("1") && rawMessage.ToLower().Contains("2"))
                    {
                        result = 2;

                    }
                    else
                    {
                        result = "Please reply with '1' for yes or '2' for no ";

                    }
                
                }
                else
                {
                    result = "Please reply with '1' for yes or '2' for no ";

                }
                return result; 
            }

            else
            {
                //look for numbers
                string extractFromMessage = ExtractNumbers(rawMessage);
                int extractedRating = 0;

                //   Regex re = new Regex(@"[^1-9]+");
                var tryRating = Int32.TryParse(extractFromMessage, out extractedRating);
                if (extractedRating > 0 && extractedRating <= 10)
                {
                    if (extractedRating == 10)
                    { result = '*'; }
                    else
                    { result = extractedRating; }
                    return result;
                }
                else
                {
                    result = "please enter only a number ranging from 1 - 10";
                    return result;
                }
            }

        }

        /*pull health rating out of patient incoming text*/
        public object extractFollowUpRating(SMSIncoming message)
        {
            string rawMessage = message.Message;
            object result = new object();

            
            //look for yes/no answer
            
                if (rawMessage.ToLower().Contains("yes") || rawMessage.ToLower().Contains("no"))
                {
                    if (rawMessage.ToLower().Contains("yes") && !rawMessage.ToLower().Contains("no"))
                    {
                        result = "yes";

                    }
                    else if (!rawMessage.ToLower().Contains("yes") && rawMessage.ToLower().Contains("no"))
                    {
                        result = "no";

                    }
                    else
                    {
                        result = "Please reply with 'yes' or 'no' ";

                    }

                }
                else
                {
                    result = "Please reply with 'yes' or 'no' ";

                }
                return result;
                     

        }

        public static string ExtractNumbers(string expr)
        {
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
        }

        /* compare sms health status to health status on record */
        public bool compareHealth(int oldStatus, int newStatus, int triggerPoint, TriageScaleType triageScaleType, char[] healthRecord = null)
        {
            bool result = false;
            var intList = new List<int>();
            var maxNum = 0; 
            int numParse;

            if (healthRecord != null)
            {
                foreach (var record in healthRecord)
                {
                    if (Int32.TryParse(record.ToString(), out numParse))
                    {
                        intList.Add(numParse);
                    }
                    else if (record.Equals('*'))
                    {
                        intList.Add(10);
                    }
                }

            }

            if(intList.Count <= 0 )
            {
                maxNum = triggerPoint;
            }
            else
            {
                maxNum = intList.Max(); 
            }

            /// TODO: Add sliding scale for ascending, descending, and boolean Responses

            if (triageScaleType == TriageScaleType.Ascending)
            {
                if (healthRecord != null)
                {
                    if (oldStatus - newStatus > 2 || maxNum - newStatus >= 3 || newStatus >= triggerPoint)
                        result = true;
                }
                else
                {
                    if (newStatus <= triggerPoint)
                        result = true;
                }
            }

            if(triageScaleType == TriageScaleType.Descending)
            {
                if (healthRecord != null)
                {
                    if (oldStatus - newStatus > 2 || maxNum - newStatus >= 3 || newStatus <= triggerPoint)
                        result = true;
                }
                else
                {
                    if (newStatus <= triggerPoint)
                        result = true;
                }
            }

            if(triageScaleType == TriageScaleType.Decision)
            {
                if (newStatus == 1)
                {
                    result = true;
                }
                if(newStatus == 2)
                {
                    result = false; 
                }
                
            }



            return result;
        }

        /* add new health status to patient health record */
        public void addToHealthRecord(Triage triage, int newStat = 0)
        {
            string newHealthRecord = string.Empty;
            string newHealthStat = string.Empty;

            if (newStat > 0 && !String.IsNullOrEmpty(triage.HealthTracker))
            {
                if (newStat == 10)
                {
                    var stat10 = "*";
                    triage.HealthTracker = triage.HealthTracker + stat10;
                }
                else
                {
                    triage.HealthTracker = triage.HealthTracker + newStat;
                }

                UpdateTriage(triage);
            }
            else
            {
                if (newStat == 10)
                {
                    var stat10 = "*";
                    triage.HealthTracker = stat10;
                }
                else
                {
                    triage.HealthTracker = newStat.ToString();
                }
                UpdateTriage(triage);
            }
        }

        /* process the triages based on customer reply and return reply message */
        public string processTriages(Triage triage, bool prolongTriage, bool communicate = true)
        {
            string replyMessage = string.Empty;
            
                // process the triage based on customer reply
                if (prolongTriage && triage.EndDate >= DateTime.Today)
                {
                    // will only pro long triage if endDate is less than a month  
                    if (triage.EndDate.Date < triage.StartDate.Date.AddDays(30) && triage.DisableTriageExtension.Equals(false))
                    {
                        triage.EndDate = triage.EndDate.AddDays(7);

                        if (communicate)
                        UpdateTriage(triage);
                    }

                    if (communicate)
                    sendEmail(triage);

                    replyMessage = triage.BadResponseMessage;
                }
                else
                {
                    replyMessage = triage.GoodResponseMessage;
                }

            
            return replyMessage;
        }

        /*form and send email to doctor notifying him/her patient feels worse */
        public void sendEmail(Triage triage)
        {
            var group = _ctx.Groups.First(p => p.GroupId == triage.GroupID);
            var patient = _ctx.Contacts.First(p => p.ContactId == triage.ContactID);
            var newEmail = new Email();

            string emailTemplate = @"
                                        Hello, ##ClientName##
                                        Your patient ##PATIENTNAME## has responded to our poll and may need to be contacted, or seen again.
                                        Their contact info is below: 

                                        ##PATIENTNAME##
                                        ##EMAILADDRESS##
                                        ##PHONENUMBER##

                                        Best regards, 
                                        ##ADMINNAME##";

            string emailBody = emailTemplate
                .Replace("##ClientName##", group.CompanyName)
                .Replace("##PATIENTNAME##", patient.FirstName + " " + patient.LastName)
                .Replace("##EMAILADDRESS##", patient.EmailAddress)
                .Replace("##PHONENUMBER##", patient.PhoneNumber)
                .Replace("##ADMINNAME##", "PatientHarmony Administrator");

            newEmail.ToEmail = group.Email;
            newEmail.Subject = "Patient Health Status Notification";
            newEmail.Message = emailBody;


            SendEmail(newEmail);
        }

        #endregion

        #region Chat Messages 

        
        public ChatMessage GetChatMessage(int messageId)
        {
            try
            {
                return _ctx.ChatMessages.First(p => p.MessageId == messageId);
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting chat message", ex);
                return null;
            }
        }

        public List<ChatMessage> GetAllChatMessages()
        {
            try
            {
                return _ctx.ChatMessages.OrderBy(p => p.CreatedDate).ToList<ChatMessage>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting all chat messages", ex);
                return null;
            }
        }

        public void AddMessage(ChatMessage eChatMessage)
        {
            if(eChatMessage.CreatedDate == null)
            eChatMessage.CreatedDate = DateTime.Now;

            try
            {
                _ctx.ChatMessages.Add(eChatMessage);
                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding chat message", ex);
                return;
            }

        }

        public void ProcessMessages(List<ChatMessage> eChatMessages)
        {
            
            try
            {
                foreach (var message in eChatMessages)
                {
                    message.Processed = true; 
                    _ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured processing chat messages", ex);
                return;
            }

        }






        #endregion

      

        public void SendEmail(Email email, List<string> recipients = null, List<string> ccRecipients = null)
        {

            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("mail.patientharmony.com", 465);
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new NetworkCredential("PatientHarmonyAdmin@patientharmony.com", "S3attlesnows!");

                SmtpServer.Port = 587;



                mail.From = new MailAddress("PatientHarmonyAdmin@patientharmony.com");
                mail.Subject = email.Subject;
                mail.Body = email.Message;


                if (recipients == null)
                {
                    recipients = new List<string>(); 
                    mail.To.Add(email.ToEmail);
                    SmtpServer.Send(mail);
                    AddEmail(email);
                }

                if (recipients.Count > 0)
                {
                    foreach (var rec in recipients)
                    {
                        mail.To.Add(rec);

                    }
                    SmtpServer.Send(mail);
                    AddEmail(email);
                }
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured sending email", ex);
                //Console.WriteLine("An Error occured sending email" + System.Environment.NewLine + ex);
                return;
            }

        }

        public int AddEmail(Email email)
        {
            try
            {
                email.FromEmail = "PatientHarmonyAdmin@patientharmony.com";
                email.CreatedDate = DateTime.Now;
                _ctx.Emails.Add(email);
                _ctx.SaveChanges();

                return email.EmailId;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding email", ex);
                //Console.WriteLine("An Error occured adding email" + System.Environment.NewLine + ex);
                return 0;
            }
        }

        public void UpdateTriage(Triage eTriage)
        {
            try
            {
                var updev = _ctx.Triages.First(p => p.TriageId == eTriage.TriageId);

                if (updev.Title != eTriage.Title)
                    updev.Title = eTriage.Title;

                if (updev.TriageMessage != eTriage.TriageMessage)
                    updev.TriageMessage = eTriage.TriageMessage;

                if (updev.GoodResponseMessage != eTriage.GoodResponseMessage)
                    updev.GoodResponseMessage = eTriage.GoodResponseMessage;

                if (updev.BadResponseMessage != eTriage.BadResponseMessage)
                    updev.BadResponseMessage = eTriage.BadResponseMessage;

                if (updev.StartDate != eTriage.StartDate)
                    updev.StartDate = eTriage.StartDate;

                if (updev.EndDate != eTriage.EndDate)
                    updev.EndDate = eTriage.EndDate;

                if (updev.GroupID != eTriage.GroupID)
                    updev.GroupID = eTriage.GroupID;

                if (updev.LastDateProcessed != eTriage.LastDateProcessed)
                    updev.LastDateProcessed = eTriage.LastDateProcessed;

                if (updev.SendDateTime != eTriage.SendDateTime)
                    updev.SendDateTime = eTriage.SendDateTime;

                if (updev.HealthTracker != eTriage.HealthTracker)
                    updev.HealthTracker = eTriage.HealthTracker;

                if (updev.EndMarker != eTriage.EndMarker)
                    updev.EndMarker = eTriage.EndMarker;

                updev.LastReplyDate = DateTime.Now;
                updev.LastDateProcessed = DateTime.Now;

                _ctx.SaveChanges();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating Triage", ex);
            }
        }

        
        public List<PatientHarmony.Data.Entities.Group> GetGroupsEntitiesForUser(List<UserDefaultGroup> userGroups)
        {
            var groupList = new List<PatientHarmony.Data.Entities.Group>();

            try
            {
                foreach (var userGroup in userGroups)
                {
                    var addGroup = _ctx.Groups.First(group => group.GroupId == userGroup.GroupId);
                    groupList.Add(addGroup);
                }
                return groupList;
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting Group Entities for User", ex);
                //Console.WriteLine("An Error occured getting group Entities for User" + System.Environment.NewLine + ex);
                return null;
            }


        }

        public List<string> splitLongMessage(string message)
        {
            var tempList = new List<String>();
            var splitMessageList = new List<string>();
            var whitespaceIndexes = new List<int>();
            int stringLength = message.Length;
            int position = 0;
            int chunkSize = 155;
            var tempPosition = 0;


            if (stringLength > 0)
            {

                if (stringLength < chunkSize)
                {
                    tempList.Add(message);
                }

                else
                {
                    //split long message in 155 character chunks 
                    //get position of white space before 155 index
                    //5 characters to append sequence number/message count
                    for (int i = 0; i < stringLength; i += position)
                    {
                        // i = position + chunkSize; 
                        if (!(stringLength - i < chunkSize))
                        {
                            var temp = message.Substring(i, chunkSize);
                            position = temp.LastIndexOf(" ", chunkSize);

                            //position = (tempPosition + position);
                            whitespaceIndexes.Add(position);
                            tempList.Add(message.Substring(i, position));
                            //  i = position;

                            if (stringLength - i < chunkSize)
                            {
                                tempList.Add(message.Substring(i));
                            }
                        }
                        else
                        {
                            tempList.Add(message.Substring(i));
                        }

                    }
                }

                if (tempList.Count > 1)
                {
                    foreach (var tempMessage in tempList)
                    {
                        var sms = tempMessage + " " + (tempList.IndexOf(tempMessage) + 1) + "/" + tempList.Count;
                        splitMessageList.Add(sms);
                    }
                }
                else
                {
                    splitMessageList.Add(tempList.First());
                }
                System.Diagnostics.Debug.WriteLine("");




            }

            return splitMessageList;
        }

      }

}
