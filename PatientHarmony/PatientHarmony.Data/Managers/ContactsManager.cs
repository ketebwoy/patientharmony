﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Context;
using PagedList;
using PatientHarmony.Data.Logging.NLog;
using PhoneNumbers;

namespace PatientHarmony.Data.Managers
{
    public class ContactsManager
    {
        private DataContext _ctx = new DataContext(); 
        NLogLogger logger = new NLogLogger();
        
     


        public Contact Get(int contactID)
        {
            try
            {
                 var contact = new Contact();
                    if (_ctx.Contacts.First(p => p.ContactId == contactID) != null)
                    {
                        contact = _ctx.Contacts.First(p => p.ContactId == contactID); 
                    }
                    return contact;
                
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting a contact", ex);
                //Console.WriteLine("An Error occured getting the Contact" + System.Environment.NewLine + ex);
                return null;
            }
        }

       
        public List<Contact> GetAllList()
        {
            try
            {
                 return _ctx.Contacts.OrderBy(p => p.LastName).ToList<Contact>();
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured getting contacts", ex);
                //Console.WriteLine("An Error occured getting Contacts" + System.Environment.NewLine + ex);
                return null;
            }
        }


        public int Add(Contact eContact)
        {
            int newContactId;

            try
            {
                    eContact.CreatedDate = DateTime.Now;
                    eContact.LastModifiedDate = DateTime.Now;

                    var util = PhoneNumberUtil.GetInstance();

                    var phoneNumber = util.Parse(eContact.PhoneNumber, "US");


                    eContact.PhoneNumber = phoneNumber.NationalNumber.ToString();

                    _ctx.Contacts.Add(eContact);
                    _ctx.SaveChanges();

                    newContactId = eContact.ContactId;
                    return newContactId;
    
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured adding a contact", ex);
               // Console.WriteLine("An Error occured adding Contact" + System.Environment.NewLine + ex);
                return -1;
            }
        }

        public bool DoesContactExist(Contact eContact)
        {
            var allContact = GetAllList(); 

            //foreach loop checking each item fields to eContact fields

            //if statement LINQ
            if((allContact.FindAll(p => p.FirstName == eContact.FirstName && p.LastName == eContact.LastName && p.PhoneNumber == eContact.PhoneNumber && p.EmailAddress == eContact.EmailAddress).Count <= 0))
            {
                return true;
            }
            return false; 
        }

        public void UpdateContact(Contact eContact)
        {
            try
            {
                //using (var _ctx = new DataContext())
                //{
                    var updev = _ctx.Contacts.First(p => p.ContactId == eContact.ContactId);

                    if (updev.FirstName != eContact.FirstName)
                        updev.FirstName = eContact.FirstName;

                    if (updev.LastName != eContact.LastName)
                        updev.LastName = eContact.LastName;

                    if (updev.EmailAddress != eContact.EmailAddress)
                        updev.EmailAddress = eContact.EmailAddress;

                    if (updev.PhoneNumber != eContact.PhoneNumber)
                        updev.PhoneNumber = eContact.PhoneNumber;

                   updev.LastModifiedDate = DateTime.Now;

                    _ctx.SaveChanges();
                //}
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured updating contact", ex);
                //Console.WriteLine("An Error occured updating Contact" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void DeleteContact(int id)
        {
            try
            {
                    Contact user = _ctx.Contacts.First(p => p.ContactId == id);
                    _ctx.Contacts.Remove(user);
                    DeleteContactFromCampaign(id);
                    DeleteContactFromGroups(id);
                    _ctx.SaveChanges();
    
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting a contact", ex);
               // Console.WriteLine("An Error occured deleting the Contact" + System.Environment.NewLine + ex);
                return;
            }
        }

        public bool EmailExists(int contactId, string email)
        {
            try
            {
                    Contact user = _ctx.Contacts.First(p => p.ContactId == contactId);
                    if (user.EmailAddress == email) return false;
                    {
                        return _ctx.Contacts.Any(p => p.EmailAddress == email);
                    }
    
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured checking preExisting Email", ex);
               // Console.WriteLine("An Error occured checking preExisting email" + System.Environment.NewLine + ex);
                return false;
            }
        }

        public void DeleteContactFromCampaign(int contactId)
        {
            try
            {
                var contactCampaigns = _ctx.ContactsCamapigns.Where(p => p.ContactId == contactId);

                foreach (var item in contactCampaigns)
                _ctx.ContactsCamapigns.Remove(item);
                
                _ctx.SaveChanges();
    
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting Campaign Contacts", ex);
                //Console.WriteLine("An Error occured deleting Campaign contacts" + System.Environment.NewLine + ex);
                return;
            }
        }

        public void DeleteContactFromGroups(int contactId)
        {
            try
            {
                
                    var contactGroups = _ctx.ContactsGroups.Where(p => p.ContactId == contactId);

                    foreach (var item in contactGroups)
                        _ctx.ContactsGroups.Remove(item);
                    _ctx.SaveChanges();
                
            }
            catch (Exception ex)
            {
                logger.Error("An Error occured deleting contact from groups", ex);
                //Console.WriteLine("An Error occured deleting contact from groups" + System.Environment.NewLine + ex);
                return;
            }
        }


    }
}


