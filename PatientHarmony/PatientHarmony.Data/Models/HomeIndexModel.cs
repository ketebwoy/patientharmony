﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatientHarmony.Data.Entities;



namespace PatientHarmony.Data.Models
{
    public class HomeIndexModel
    {
        public List<Contact> indexContacts { get; set; }
        public List<SMSIncoming> smsIncomingList {get; set; }
        public List<SMSOutgoing> smsOutgoingList { get; set; }
        public List<Triage> triages { get; set; }
        public object chart { get; set;  }

        public int patientsEnrolled { get; set; }
        public int patientsImproving { get; set; }
        public int patientsNotImproving { get; set; }
        public int patientReplyRate { get; set; }

        public HomeIndexModel()
        {
            this.indexContacts = new List<Contact>();
            this.smsIncomingList = new List<SMSIncoming>();
            this.smsOutgoingList = new List<SMSOutgoing>();
            this.triages = new List<Triage>(); 
            this.chart = new object();
            this.patientsEnrolled = 0;
            this.patientsImproving = 0;
            this.patientsNotImproving = 0;
            this.patientReplyRate = 0; 

        }
        
        
    }
}
