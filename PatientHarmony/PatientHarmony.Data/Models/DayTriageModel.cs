﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatientHarmony.Data.Entities;


namespace PatientHarmony.Data.Models
{
    public class DayTriageModel
    {
        public List<GroupTriagesModel> Triages { get; set; }
        public string Date { get; set; }
        public DateTime AjaxDate { get; set; }
        public List<Triage> TriageList { get; set; }
        public List<TriageObject> TriageObjects { get; set; }

        public DayTriageModel()
        {
            this.TriageList = new List<Triage>();
            this.TriageObjects = new List<TriageObject>();
            this.AjaxDate = new DateTime();
            this.Triages = new List<GroupTriagesModel>(); 
        }
    }

    public class GroupTriagesModel
    {
        public string group { get; set; }
        public List<Triage> Triages { get; set; }
        public List<TriageObject> TriageObjects { get; set; }

        public GroupTriagesModel()
        {
            this.TriageObjects = new List<TriageObject>();
            this.Triages = new List<Triage>(); 
        }
    }

   

}
