﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatientHarmony.Data.Models
{
    public class DayModel
    {
        public DateTime Date { get; set; }
        public int EventsCount { get; set; }
        public bool IsActive { get; set; }
        public int Id { get; set; }
        public string Day { get; set; }
    }
}
