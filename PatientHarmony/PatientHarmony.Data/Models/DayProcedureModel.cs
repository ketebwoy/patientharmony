﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PatientHarmony.Data.Entities;

namespace PatientHarmony.Data.Models
{
    public class DayProcedureModel
    {
        public List<GroupProceduresModel> Procedures { get; set; }
        public string Date { get; set; }
        public List<Procedure> ProcedureList { get; set; }
    }

    public class GroupProceduresModel
    {
        public string Campaign { get; set; }
        public List<Procedure> Procedures { get; set; }
    }
}
