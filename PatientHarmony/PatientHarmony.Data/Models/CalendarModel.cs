﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatientHarmony.Data.Models
{
    public class CalendarModel
    {
        public int PrevYear { get; set; }
        public int PrevMonth { get; set; }

        public int NextYear { get; set; }
        public int NextMonth { get; set; }

        public string Month { get; set; }
        public int Year { get; set; }

        public List<DayModel> Days { get; set; }
        public List<int> Years { get; set; }
        public int MonthNum { get; set; }

        public List<int> Months { get; set; }
    }
}
