﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PatientHarmony.Data.Models
{
        public class SimpleJsonEvent
    {
        public string Start { get; set; }
        public string End { get; set; }
        public string Title { get; set; }
    }
}
