﻿using System.Data.Entity;
using PatientHarmony.Data.Entities;

namespace PatientHarmony.Data.Context
{
    public class DataContext : DbContext
    {
        public DataContext() : base("PatientHarmonyDB") { }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<Procedure> Procedures { get; set; }
        public DbSet<SMSOutgoing> SMSOutgoings { get; set; }
        public DbSet<SMSIncoming> SMSIncomings { get; set; }
        public DbSet<AutoResponder> AutoResponders { get; set; }
        public DbSet<Triage> Triages { get; set; }
        public DbSet<TriageFollowUp> TriageFollowUp { get; set; }
        public DbSet<ContactsCampaigns> ContactsCamapigns { get; set; }
        public DbSet<ContactsGroup> ContactsGroups { get; set; }
        public DbSet<UserDefaultGroup> UserGroups { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<CampaignTemplate> CampaignTemplates { get; set; }
        public DbSet<ProcedureTemplate> ProcedureTemplates { get; set; }
        public DbSet<TriageTemplate> TriageTemplates { get; set; }
        public DbSet<FollowUpTemplate> FollowUpTemplate { get; set; }
        public DbSet<ChatMessage> ChatMessages { get; set; }
        public DbSet<Voice> VoiceCalls { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
                   
    }

    }
}
