angular.module('ionicApp', ['ionic'])



.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('tabs', {
          url: "/tab",
          abstract: true,
          templateUrl: "templates/tabs.html"
      })
      .state('tabs.account', {
          url: "/account",
          views: {
              'account-tab': {
                  templateUrl: "templates/account.html",
                  controller: 'AccountCtrl'
              }
          }
      })
      .state('tabs.home', {
          url: "/home",
          views: {
              'home-tab': {
                  templateUrl: "templates/home.html",
                  controller: 'HomeTabCtrl'
              }
          }
      })
      .state('tabs.campaigns', {
          url: "/campaigns",
          views: {
              'campaign-tab': {
                  templateUrl: "templates/campaigns.html",
                  controller: 'CampaignTabCtrl'
              }
          }
      })
      .state('tabs.procedures', {
          url: "/procedures",
          views: {
              'procedure-tab': {
                  templateUrl: "templates/procedures.html",
                  controller: 'HomeTabCtrl'
              }
          }
      })
     .state('tabs.triages', {
         url: "/triages",
         views: {
             'triage-tab': {
                 templateUrl: "templates/triages.html",
                 controller: 'TriageTabCtrl'
             }
         }
     })
     .state('triageResponse.html', {
         url: "/triageResponse.html",
         views: {
             'triageResponse': {
                 templateUrl: "templates/triageResponse.html",
                 controller: 'TriageResponseCtrl'
             }
         }
     })
     .state('tabs.followups', {
         url: "/followups",
         views: {
             'followup-tab': {
                 templateUrl: "templates/followups.html",
                 controller: 'HomeTabCtrl'
             }
         }
     })
      .state('tabs.about', {
          url: "/about",
          views: {
              'about-tab': {
                  templateUrl: "templates/about.html"
              }
          }
      })
      .state('tabs.navstack', {
          url: "/navstack",
          views: {
              'about-tab': {
                  templateUrl: "templates/nav-stack.html"
              }
          }
      })
      .state('tabs.contact', {
          url: "/contact",
          views: {
              'contact-tab': {
                  templateUrl: "templates/contact.html"
              }
          }
      });

    // $resourceProvider.defaults.stripTrailingSlashes = false;
    $urlRouterProvider.otherwise("account.html");

})

//.factory('PhResource', function () {
  //  return $resource("/Group/GetGroupsJson") })

.controller('AccountCtrl', function ($scope, $ionicPopup, $http, $rootScope, $state) {

    var x = 1;

    function getPosition(str, m, i) {
        return str.split(m, i).join(m).length;
    }


    $scope.accountStatus = function () {

        var username = "ketebwoy";
        var password = "yasmir";
        var pinVerify = "1282";


        $http.post('http://localhost/PatientHarmony.Web/Mobile/LoginMobile', { "userName": username, "password": password }).then(function (resp) {
            var success = resp.data;

            $scope.success = "";

            if (success) {


                if (x < 4) {

                    $ionicPopup.prompt({
                        title: 'Login Step 2:',
                        subTitle: 'Please Enter your Pin!',
                        inputText: 'I want this to be editable in prompt',
                        inputPlaceholder: 'input rules'
                    }).then(function (res) {
                        if (res == pinVerify) {
                            $ionicPopup.alert({
                                title: "Successful Login:",
                                template: res
                            });
                            $state.go('tabs.home');
                        }
                        else {

                            $ionicPopup.alert({
                                title: "Unsuccessful Login, write log, kill first step",
                                template: res
                            });
                            $http.post('http://localhost/PatientHarmony.Web/Mobile/LogoOffMobile').then(function (resp) {
                                if (resp.data) {
                                    $state.go('tabs.account');
                                    x = 0;
                                }
                            });
                            x++;
                        }
                    })

                }

                if (x >= 4) {

                    $ionicPopup.prompt({
                        title: 'Account Locked',
                        subTitle: 'Please try to login again in 1 Hour!'
                    }).then(function (res) {
                        $http.post('http://localhost/PatientHarmony.Web/Mobile/LogoOffMobile').then(function (resp) {
                            if (resp.data) {
                                $state.go('tabs.account');
                                x = 0;
                            }
                        });
                    })

                }
            }


        });
    }

})




.controller('HomeTabCtrl', function ($scope, $http, $state) {


    $http.get('http://localhost/PatientHarmony.Web/Mobile/GetContactsJson').then(function (resp) {

        var contacts = JSON.parse(resp.data);

        var E = Enumerable;
        var contact = E.From(contacts)
                     .Where(function (item) { return item.FirstName == 'Keith'; }).ToArray();

        $scope.contact = contact[0];

    }, function (err) {
        console.error('ERR', err);
        $state.go("~/Account/Login");
        // err.status will contain the status code
    })

    $scope.headingCaption = 'Patient Harmony';

    $scope.group = [

    { CompanyName: "Keith Test Group" },
    { CompanyName: "Keith Test Group2" }];

})

.controller('CampaignTabCtrl', function ($scope, $http) {

    var contactId = 2019;

    $http.get('http://localhost/PatientHarmony.Web/Mobile/GetCampaignForContactJson', { params: { "contactId": contactId } }).then(function (resp) {
        var campaigns = JSON.parse(resp.data);

        $scope.campaigns = campaigns;

    }, function (err) {
        console.error('ERR', err);
        // err.status will contain the status code
    })

    $scope.headingCaption = 'Patient Harmony';

    $scope.group = [

    { CompanyName: "Keith Test Group" },
    { CompanyName: "Keith Test Group2" }];

})

.controller('TriageTabCtrl', function ($scope, $http, $ionicPopup) {

    var contactId = 2019;

    $http.get('http://localhost/PatientHarmony.Web/Mobile/GetTriagesForContactJson', { params: { "contactId": contactId } }).then(function (resp) {
        var triages = JSON.parse(resp.data);

        $scope.triages = triages;

    }, function (err) {
        console.error('ERR', err);
        // err.status will contain the status code
    })

    $scope.processTriage = function (triageResponse) {

        var response = "";
        if (triageResponse >= 5) {
            response = $scope.triages[0].GoodResponseMessage;
        }
        else {
            response = $scope.triages[0].BadResponseMessage;
        }

        var alertPopup = $ionicPopup.alert({

            title: 'Triage Response!',
            content: response
        });
        //alert("Process Triage with a response of" + triageResponse);
        console.log(triageResponse);
    }

    //// Triggered on a button click, or some other target
    //$scope.processTriage = function (triageResponse) {
    //    var alertPopup = $ionicPopup.alert({
    //        title: 'Thanks for your response!',
    //        template: 'It might taste good'
    //    });
    //    alertPopup.then(function (res) {
    //        console.log('Thank you for not eating my delicious ice cream cone');
    //    });
    //}




    $scope.headingCaption = 'Patient Harmony';

    $scope.group = [

    { CompanyName: "Keith Test Group" },
    { CompanyName: "Keith Test Group2" }];

})

.controller('TriageResponseCtrl', function ($scope, $http) {

    var contactId = 2019;

    $http.get('http://localhost/PatientHarmony.Web/Mobile/GetTriagesForContactJson', { params: { "contactId": contactId } }).then(function (resp) {
        var triages = JSON.parse(resp.data);

        $scope.triages = triages;

    }, function (err) {
        console.error('ERR', err);
        // err.status will contain the status code
    })


    $scope.headingCaption = 'Patient Harmony';

    $scope.group = [

    { CompanyName: "Keith Test Group" },
    { CompanyName: "Keith Test Group2" }];

})