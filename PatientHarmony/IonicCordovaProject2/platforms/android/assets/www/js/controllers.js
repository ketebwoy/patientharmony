angular.module('account', ['ionic'])



.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
      .state('tabs', {
          url: "/tab",
          abstract: true,
          templateUrl: "templates/tabs.html"
      })
      .state('tabs.account', {
          url: "/account",
          views: {
              'account-tab': {
                  templateUrl: "templates/account.html",
                  controller: 'AccountCtrl'
              }
          }
      })
      .state('tabs.home', {
          url: "/home",
          views: {
              'home-tab': {
                  templateUrl: "templates/home.html",
                  controller: 'HomeTabCtrl'
              }
          }
      })
      .state('tabs.campaigns', {
          url: "/campaigns",
          views: {
              'campaign-tab': {
                  templateUrl: "templates/campaigns.html",
                  controller: 'CampaignTabCtrl'
              }
          }
      })
      .state('tabs.procedures', {
          url: "/procedures",
          views: {
              'procedure-tab': {
                  templateUrl: "templates/procedures.html",
                  controller: 'HomeTabCtrl'
              }
          }
      })
     .state('tabs.triages', {
         url: "/triages",
         views: {
             'triage-tab': {
                 templateUrl: "templates/triages.html",
                 controller: 'TriageTabCtrl'
             }
         }
     })
     .state('triageResponse.html', {
         url: "/triageResponse.html",
         views: {
             'triageResponse': {
                 templateUrl: "templates/triageResponse.html",
                 controller: 'TriageResponseCtrl'
             }
         }
     })
     .state('tabs.followups', {
         url: "/followups",
         views: {
             'followup-tab': {
                 templateUrl: "templates/followups.html",
                 controller: 'HomeTabCtrl'
             }
         }
     })
      .state('tabs.about', {
          url: "/about",
          views: {
              'about-tab': {
                  templateUrl: "templates/about.html"
              }
          }
      })
      .state('tabs.navstack', {
          url: "/navstack",
          views: {
              'about-tab': {
                  templateUrl: "templates/nav-stack.html"
              }
          }
      })
      .state('tabs.contact', {
          url: "/contact",
          views: {
              'contact-tab': {
                  templateUrl: "templates/contact.html"
              }
          }
      });

    // $resourceProvider.defaults.stripTrailingSlashes = false;
    $urlRouterProvider.otherwise("account.html");

})

//.factory('PhResource', function () {
  //  return $resource("/Group/GetGroupsJson") })

.controller('AccountCtrl', function ($scope, $ionicPopup, $http, $rootScope, $state) {

    var x = 1;

    function getPosition(str, m, i) {
        return str.split(m, i).join(m).length;
    }


    $scope.accountStatus = function () {

        var username = "ketebwoy";
        var password = "yasmir";
        var pinVerify = "1282";


        $http.post('http://localhost/PatientHarmony.Web/Mobile/LoginMobile', { "userName": username, "password": password }).then(function (resp) {
            var success = resp.data;

            $scope.success = "";

            if (success) {


                if (x < 4) {

                    $ionicPopup.prompt({
                        title: 'Login Step 2:',
                        subTitle: 'Please Enter your Pin!',
                        inputText: 'I want this to be editable in prompt',
                        inputPlaceholder: 'input rules'
                    }).then(function (res) {
                        if (res == pinVerify) {
                            $ionicPopup.alert({
                                title: "Successful Login:",
                                template: res
                            });
                            $state.go('tabs.home');
                        }
                        else {

                            $ionicPopup.alert({
                                title: "Unsuccessful Login, write log, kill first step",
                                template: res
                            });
                            $http.post('http://localhost/PatientHarmony.Web/Mobile/LogoOffMobile').then(function (resp) {
                                if (resp.data) {
                                    $state.go('tabs.account');
                                    x = 0;
                                }
                            });
                            x++;
                        }
                    })

                }

                if (x >= 4) {

                    $ionicPopup.prompt({
                        title: 'Account Locked',
                        subTitle: 'Please try to login again in 1 Hour!'
                    }).then(function (res) {
                        $http.post('http://localhost/PatientHarmony.Web/Mobile/LogoOffMobile').then(function (resp) {
                            if (resp.data) {
                                $state.go('tabs.account');
                                x = 0;
                            }
                        });
                    })

                }
            }


        });
    }

})


