﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;
using System.ComponentModel.DataAnnotations;

namespace PatientHarmony.Web.Models
{
    public class GroupIndexModel
    {
        public List<GroupObject> GroupObjects { get; set; }
        public List<Group> Groups { get; set; }
        public List<CampaignTemplate> CampaignTemplates { get; set; }
        public List<ProcedureTemplate> ProcedureTemplates { get; set; }
        
               

        public GroupIndexModel()
        {
            this.GroupObjects = new List<GroupObject>();
            this.Groups = new List<Group>();
            this.CampaignTemplates = new List<CampaignTemplate>();
            this.ProcedureTemplates = new List<ProcedureTemplate>(); 
        }


        public GroupIndexModel BuildIndexModel(List<Group> groups)
        {
            var modelList = new GroupIndexModel();
            modelList.Groups = groups; 

            
            foreach (var group in groups)
            {
                var model = new GroupObject();
                model.GroupId = group.GroupId;
                model.CompanyName = group.CompanyName;
                model.Email = group.Email;
                model.PhoneNumber = group.PhoneNumber;
                model.CreatedDate = group.CreatedDate;
                model.LastModifiedDate = group.LastModifiedDate;
                modelList.GroupObjects.Add(model); 
            }

            return modelList; 
        }

    }

   

}