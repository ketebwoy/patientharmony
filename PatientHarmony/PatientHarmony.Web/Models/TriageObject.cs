﻿using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PatientHarmony.Web.Models
{
    public class TriageObject
    {
        private DataManager _ctx = DataManager.Instance;
        private List<Group> _groups = new List<Group>();
      
        [Key]
        public int TriageId { get; set; }

        public string Title { get; set; }

        public string TriageMessage { get; set; }

        public string GoodResponseMessage { get; set; }

        public string BadResponseMessage { get; set; }

        public string GroupTitle { get; set; }

        public int GroupID { get; set; }

        public string ContactFullName { get; set; }

        public int ContactID { get; set; }

        public string HealthTracker { get; set; }
        
        public System.DateTime StartDate { get; set; }

        public System.DateTime EndDate { get; set; }

        public bool EndMarker { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public System.DateTime LastDateProcessed { get; set; }

        [DataType(DataType.Time)]
        public System.DateTime SendDateTime { get; set; }

        public bool DisableTriageExtension { get; set; }

        public int TriggerPoint { get; set; }

        public TriageScaleType ScaleType { get; set; }

        public TriageFrequencyType Frequency { get; set; }

        public string SendDays { get; set; }

        public List<TriageObject> TriageObjectList(List<Triage> triageList, List<Contact> patientList)
        {
            var triageObjectList = new List<TriageObject>();
            _groups = _ctx.Groups.GetAllGroups();

            if (triageList.Count > 0 && triageList != null)
            {
                foreach (var triage in triageList)
                {
                    var triageObject = new TriageObject();
                    var triageGroup = _groups.First(p => p.GroupId == triage.GroupID);
                    var triagePatient = patientList.First(p => p.ContactId == triage.ContactID);
                    triageObject.TriageId = triage.TriageId;
                    triageObject.Title = triage.Title;
                    triageObject.TriageMessage = triage.TriageMessage;
                    triageObject.GoodResponseMessage = triage.GoodResponseMessage;
                    triageObject.BadResponseMessage = triage.BadResponseMessage;
                    triageObject.GroupTitle = triageGroup.CompanyName;
                    triageObject.GroupID = triage.GroupID;
                    triageObject.ContactFullName = triagePatient.FirstName + " " + triagePatient.LastName;
                    triageObject.ContactID = triage.ContactID;
                    triageObject.SendDays = triage.SendDays;
                    triageObject.Frequency = triage.Frequency;
                    triageObject.ScaleType = triage.TriageScaleType;

                    if (triage.HealthTracker != null)
                    {
                        triageObject.HealthTracker = printHealthRecord(triage.HealthTracker.ToCharArray());
                    }
                    else
                    {
                        triageObject.HealthTracker = string.Empty;
                    }

                    triageObject.StartDate = triage.StartDate;
                    triageObject.EndDate = triage.EndDate;
                    triageObject.EndMarker = triage.EndMarker;
                    triageObject.CreatedDate = triage.CreatedDate;
                    triageObject.LastDateProcessed = triage.LastDateProcessed;
                    triageObject.SendDateTime = triage.SendDateTime;
                    triageObject.ScaleType = triage.TriageScaleType;
                    triageObject.TriggerPoint= triage.TriggerPoint;
                    triageObject.DisableTriageExtension = triage.DisableTriageExtension;
                    triageObjectList.Add(triageObject);


                }
            }
            return triageObjectList;
        }

        private string printHealthRecord(char[] healthRecord)
        {
            var sb = new StringBuilder();

            foreach (var stat in healthRecord)
            {
                var recordData = string.Empty;
                if (stat.Equals('*'))
                {
                    recordData = "10";
                }
                else
                { recordData = stat.ToString(); }

                sb.Append("[" + recordData + "] ");

            }
            return sb.ToString();
        }

    }
}
