﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PatientHarmony.Web.Models
{
    public class ProcedureTemplateValueModel
    {
        public int ProcedureTemplateId { get; set; }

        private string _procedureTemplateTitle = string.Empty;
        public string ProcedureTemplateTitle
        {
            get
            {
                return _procedureTemplateTitle;
            }
            set
            {
                _procedureTemplateTitle = value;
            }
        }

        private string _procedureTemplateMessage = string.Empty;
        public string ProcedureTemplateMessage
        {
            get
            {
                return _procedureTemplateMessage;
            }
            set
            {
                _procedureTemplateMessage = value;
            }
        }

        [DataType(DataType.Time)]
        private DateTime _sendDateTime = DateTime.Now;

        [DataType(DataType.Time)]
        public System.DateTime SendDateTime
        {
            get
            {
                return _sendDateTime;
            }
            set
            {
                _sendDateTime = value;
            }
        }

        private DateTime _startDate = DateTime.Now;
        public System.DateTime StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        private DateTime _endDate = DateTime.Now;
        public System.DateTime EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                _endDate = value;
            }
        }
    }
}