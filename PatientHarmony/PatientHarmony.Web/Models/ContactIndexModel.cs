﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;

namespace PatientHarmony.Web.Models
{
    public class ContactIndexModel
    {
        public List<Contact> Patients { get; set; }
        public List<SMSIncoming> IncomingMessageList { get; set; }
        public List<SMSOutgoing> OutGoingMessageList { get; set; }
    }
}