﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;
using System.Web.Mvc;


namespace PatientHarmony.Web.Models
{
    public class CampaignManageModel
    {
        public List<Triage> Triages { get; set; }
        public List<Procedure> Procedures { get; set; }
        public Campaign Campaign { get; set;  }
        public List<Contact> Patients { get; set; }
        public List<Contact> AllContacts { get; set; }
        public List<TriageObject> TriageObjects { get; set; }
        public List<CampaignTemplate> CampaignTemplates { get; set; }
        public List<ProcedureTemplate> ProcedureTemplates { get; set; }
      

        public CampaignManageModel()
        {
            this.Campaign = new Campaign();
            this.Procedures = new List<Procedure>();
            this.Triages = new List<Triage>();
            this.Patients = new List<Contact>();
            this.AllContacts = new List<Contact>();
            this.TriageObjects = new List<TriageObject>();
            this.CampaignTemplates = new List<CampaignTemplate>();
            this.ProcedureTemplates = new List<ProcedureTemplate>(); 
        }
    }
}