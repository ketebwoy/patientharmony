﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;

namespace PatientHarmony.Web.Models
{
        public class DayTriageModel
    {
        public List<GroupTriagesModel> Triages { get; set; }
        public string Date { get; set; }
        public DateTime AjaxDate { get; set; }
        public List<Triage> TriageList { get; set; }
        public List<TriageObject> TriageObjects { get; set; }
    }

    public class GroupTriagesModel
    {
        public string group { get; set; }
        public List<Triage> Triages { get; set; }
        public List<TriageObject> TriageObjects { get; set; }
    }
}