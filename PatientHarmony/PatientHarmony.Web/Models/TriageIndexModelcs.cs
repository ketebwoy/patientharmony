﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PatientHarmony.Web.Models
{
    public class TriageIndexModelcs : Controller
    {
        //
        // GET: /TriageIndexModelcs/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Manage()
        {
            return View(); 
        }

        public ActionResult DeleteTriage()
        {
            return View(); 
        }

        public ActionResult UpdateTriage()
        {
            return View();
        }

        public ActionResult DeleteProcedure()
        {
            return View(); 
        }

        public ActionResult UpdateProcedure()
        {
            return View(); 
        }


    }
}
