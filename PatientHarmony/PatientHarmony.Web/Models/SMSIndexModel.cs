﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;

namespace PatientHarmony.Web.Models
{
    public class SMSIndexModel
    {
        public List<SMSIncoming> Incoming { get; set; }
        public List<SMSOutgoing> Outgoing { get; set; }
        public List<Email> Emails { get; set; }
        public Email AdminRequest { get; set; }

        public SMSIndexModel()
        {
            this.Incoming = new List<SMSIncoming>();
            this.Outgoing = new List<SMSOutgoing>();
            this.Emails = new List<Email>();
            this.AdminRequest = new Email(); 
        }
    }
}