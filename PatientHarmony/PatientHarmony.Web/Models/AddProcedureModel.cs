﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PatientHarmony.Data.Entities;
using System.Web.Mvc;

namespace PatientHarmony.Web.Models
{
    public class AddProcedureModel
    {
        public Campaign campaign { get; set; }
        public List<Contact> AllContacts { get; set; }
                
        public AddProcedureModel()
        {
            this.campaign = new Campaign();
            this.AllContacts = new List<Contact>();
        }
    }
}