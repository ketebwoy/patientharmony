﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PatientHarmony.Web.Models
{
    public class GroupObject
    {
        private const string RegExEmailPattern = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";


        private const string RegExPhonePattern =
         @"^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$";

        [Key]
        [ScaffoldColumn(false)]
        public int GroupId { get; set; }

        [Required]
        public string CompanyName { get; set; }

        [Required, RegularExpression(RegExEmailPattern, ErrorMessage = "Email Format must be userName@Domain.server ex: wellbeingsmsadmin@wellbeingsms.com")]
        public string Email { get; set; }

        [Required, RegularExpression(RegExPhonePattern, ErrorMessage = "Phone Number format must be ###-###-####")]
        [ScaffoldColumn(false)]
        public string PhoneNumber { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime CreatedDate { get; set; }

        [Required]
        [ScaffoldColumn(false)]
        public System.DateTime LastModifiedDate { get; set; }
    }
}