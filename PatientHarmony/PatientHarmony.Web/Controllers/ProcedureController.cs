﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PatientHarmony.Data.Managers;
using PatientHarmony.Data.Entities;
using PatientHarmony.Data.Models;
using PatientHarmony.Web.Filters;
using Telerik.Web.Mvc;
using WebMatrix.WebData; 

namespace PatientHarmony.Web.Controllers
{
    [InitializeSimpleMembership]
    public class ProcedureController : Controller
    {
        private DataManager _ctx = DataManager.Instance;
        private List<Procedure> procedureList = new List<Procedure>();

        //
        // GET: /Procedure/

        public ActionResult Index(int id = 0, int month = 0)
        {
            if (id == 0 && month == 0)
            {
                id = DateTime.Now.Year;
                month = DateTime.Now.Month;
            }

            return View(_ctx.Procedures.GetCalendarData(id, month));
        }

        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult UpdateProcedureDetails(Procedure eProcedure, FormCollection collection, string returnUrl = null)
        {
            List<string> procedureIds = collection.GetValues("ProcedureId").ToList<string>();
            List<int> procedureIdList = procedureIds.Select(int.Parse).Distinct().ToList();

            var procedureId = procedureIdList[0];
                procedureId = eProcedure.ProcedureId;

            var procedure = _ctx.Procedures.Get(eProcedure.ProcedureId);

            procedure.ProcedureTitle = collection["ProcedureTitle"];
            procedure.ProcedureMessage = collection["ProcedureMessage"];
            procedure.StartDate = Convert.ToDateTime(collection["StartDate"]);
            procedure.EndDate = Convert.ToDateTime(collection["EndDate"]);
            procedure.SendDateTime = Convert.ToDateTime(collection["SendDateTime"]);

            _ctx.Procedures.UpdateProcedure(procedure);

            if (returnUrl == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
             return Redirect(returnUrl);
            }
        }

        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost]
        public ActionResult DeleteConfirmed(int id, string returnUrl = null)
        {

            _ctx.Procedures.Delete(id);

            if (returnUrl == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
               return Redirect(returnUrl);
            }
        }

        [HttpPost]
        public ActionResult Ajax()
        {
            return Content("AJAX CONTENT RETURNED");
        }

        [HttpPost]
        public ActionResult GetEvents(string d)
        {
            return Content(_ctx.Procedures.GetSimpleJsonDay(DateTime.Parse(d)));
        }

        public ActionResult GetEvents(int d, int m, int y)
        {
            return View("_DayProcedures", _ctx.Procedures.Get(new DateTime(y, m, d)));
        }

        #region Procedure Ajax operations

        public ActionResult AjaxProcedureBinding()
        {
            return View();
        }

        [GridAction]
        public ActionResult _AjaxBindingProcedure(DateTime date)
        {

            int _userId = WebSecurity.CurrentUserId;

            int y = date.Year;
            int m = date.Month;
            int d = date.Day;

            var procedures = _ctx.Procedures.Get(new DateTime(y, m, d));

            return View(new GridModel(procedures.ProcedureList));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingProcedure(int id, FormCollection collection, DateTime date)
        {

            var temp = date.AddDays(1);
            var procedure = _ctx.Procedures.Get(id);

            procedure.ProcedureTitle = collection["ProcedureTitle"];
            procedure.ProcedureMessage = collection["ProcedureMessage"];
            procedure.StartDate = Convert.ToDateTime(collection["StartDate"]);
            procedure.EndDate = Convert.ToDateTime(collection["EndDate"]);
            procedure.SendDateTime = Convert.ToDateTime(collection["SendDateTime"]);
            _ctx.Procedures.UpdateProcedure(procedure);

            int y = date.Year;
            int m = date.Month;
            int d = date.Day;

            var procedures = _ctx.Procedures.GetAllProcedureList().Where(
                    p =>
                    (p.StartDate.Day == date.Day && p.StartDate.Month == date.Month &&
                     p.StartDate.Year == date.Year
                     || p.EndDate.Day == date.Day && p.EndDate.Month == date.Month && p.EndDate.Year == date.Year) ||
                    (p.StartDate < date && p.EndDate > temp)).OrderByDescending(p => p.CreatedDate).ToList();



            return View(new GridModel(procedures));
        }



        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxEditingProcedure(int id, DateTime date)
        {
            var temp = date.AddDays(1);

            var procedure = _ctx.Procedures.Get(id);

            _ctx.Procedures.Delete(id);

            var procedures = _ctx.Procedures.GetAllProcedureList().Where(
                    p =>
                    (p.StartDate.Day == date.Day && p.StartDate.Month == date.Month &&
                     p.StartDate.Year == date.Year
                     || p.EndDate.Day == date.Day && p.EndDate.Month == date.Month && p.EndDate.Year == date.Year) ||
                    (p.StartDate < date && p.EndDate > temp)).OrderByDescending(p => p.CreatedDate).ToList();



            return View(new GridModel(procedures));
        }

        #endregion

    }
}
