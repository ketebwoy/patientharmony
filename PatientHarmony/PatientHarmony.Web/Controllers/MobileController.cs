﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Twilio;
using PatientHarmony.Data.Managers;
using PatientHarmony.Data.Entities;
using PatientHarmony.Web.Filters;
using PatientHarmony.Web.Models;
using WebMatrix.WebData;
using System.Globalization;
using System.Web.Security;
using PatientHarmony.Web.Logging.NLog;
using Telerik.Web.Mvc;
using Telerik.Web.Mvc.UI;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Google.Apis.Calendar;
using Google.Apis.Calendar.v3;
using Google.Apis.Authentication;
using Google.Apis.Authentication.OAuth2;
using Google.Apis.Authentication.OAuth2.DotNetOpenAuth;
using DotNetOpenAuth.OAuth2;
using System.Diagnostics;
using Google.Apis.Calendar.v3.Data;
using System.Security.Cryptography.X509Certificates;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Newtonsoft.Json.Linq;
using PatientHarmony.Web.AntiForgery;
using System.Text;

namespace PatientHarmony.Web.Controllers
{
    [InitializeSimpleMembership]
    public class MobileController : Controller
    {
   

        private DataManager _ctx = DataManager.Instance;
        private NLogLogger logger = new NLogLogger();
        private static string SERVICE_ACCOUNT_EMAIL =
        "174509411004-t392l9geh4cje5a95tudvhuokbf61sgd@developer.gserviceaccount.com";

        private static string path = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/PatientHarmony-4066af230831.p12");

        private static string SERVICE_ACCOUNT_PKCS12_FILE_PATH = path;

        private CalendarService calservice = AuthorizeCalAccess(); 

        #region Group JSON Actions

        
        [HttpGet]
        public JsonResult GetGroupsJson()
        {
            var model = new GroupModel(); 

            model.Groups = _ctx.Groups.GetAllGroups();

            foreach (var group in model.Groups)
            {
                var grpObj = new GroupObject();
                grpObj.GroupId = group.GroupId;
                grpObj.CompanyName = group.CompanyName;
                grpObj.Email = group.Email;
                grpObj.PhoneNumber = group.PhoneNumber;
                grpObj.CreatedDate = group.CreatedDate;
                grpObj.LastModifiedDate = group.LastModifiedDate;
                model.GroupObjects.Add(grpObj);
            }
            
            var list = model.GroupObjects; 

            
            string output = JsonConvert. SerializeObject(list, Formatting.Indented);
            
            return Json(output, JsonRequestBehavior.AllowGet);

        }

        #endregion 
        
        #region Triage JSON Actions

        
        [HttpGet]
        public JsonResult GetTriagesJson()
        {
            
            var triages = _ctx.Triages.GetAllTriage();

            string output = JsonConvert.SerializeObject(triages, Formatting.Indented);

            return Json(output, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public JsonResult GetTriagesForContactJson(int contactId)
        {
            var triages = _ctx.Triages.GetAllTriage().FindAll(p => p.ContactID == contactId && p.EndMarker == false);

            string output = JsonConvert.SerializeObject(triages, Formatting.Indented);

            return Json(output, JsonRequestBehavior.AllowGet);

        }

        #endregion 

        #region Triage FollowUp JSON Actions

        
        [HttpGet]
        public JsonResult GetFollowUpJson()
        {
            var followUps = _ctx.TriageFollowUps.GetAllTriageFollowUpList();

            string output = JsonConvert.SerializeObject(followUps, Formatting.Indented);

            return Json(output, JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Campaign JSON Actions

        [HttpGet]
        public JsonResult GetCampaignJson()
        {
            var campaigns = _ctx.Campaigns.GetAllList();

            string output = JsonConvert.SerializeObject(campaigns, Formatting.Indented);

            return Json(output, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public JsonResult GetCampaignForContactJson(int contactId)
        {
            var campaigns = _ctx.ManyToManyRelationShips.GetAllCampaignForContact(contactId);

            string output = JsonConvert.SerializeObject(campaigns, Formatting.Indented);

            return Json(output, JsonRequestBehavior.AllowGet);

        }

        #endregion

        #region Procedure Json Actions

        [HttpGet]
        public JsonResult GetProcedureJson()
        {
            var procedure = _ctx.Procedures.GetAllProcedureList();

            string output = JsonConvert.SerializeObject(procedure, Formatting.Indented);

            return Json(output, JsonRequestBehavior.AllowGet);

        }

        #endregion 

        #region Contact

        [HttpGet]
        public JsonResult GetContactsJson()
        {
            var contacts = _ctx.Contacts.GetAllList();

            string output = JsonConvert.SerializeObject(contacts, Formatting.Indented);

            return Json(output, JsonRequestBehavior.AllowGet);

        }

        #endregion 

        #region Account 

        [HttpPost]
        [AllowAnonymous]
       // [ValidateAntiForgeryToken]
        public bool LoginMobile(string userName, string password)
        {
            if (WebSecurity.Login(userName, password, persistCookie: false))
            {
                return true;
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return false;
        }


        [HttpPost]
        public bool LogOff()
        {
            try
            {
                WebSecurity.Logout();
                return true; 

            }

            catch
            {
                return false; 
            }
            
        }

        #endregion

        #region Google Calendar Events
        [HttpGet]
        public JsonResult GetCalendarJson()
        {

            var eventList = new List<IEnumerable<CalendarEvent>>();

            var calendarList = calservice.CalendarList.List().Execute();

            var calendarData = calendarList.Items.First(p => p.Id == "patientharmony@gmail.com");
            //var evList = GetEventsByDate(DateTime.Today.AddYears(-2), calservice, "patientharmony@gmail.com");
            if (calendarData != null)
            {
                string output = JsonConvert.SerializeObject(calendarData, Formatting.Indented);
                return Json(output, JsonRequestBehavior.AllowGet);
            }
            
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        #region Encryption
        [HttpPost]
        public JsonResult DecryptHttpTest(string cipher, string passPhrase)
        {
            //string cipher = "e372548e68e0cb6322e5f87b9f3131f1";
            //string passPhrase = "password"; 
            var keybytes = Encoding.UTF8.GetBytes("8080808080808080"); //("15618460337");
            var iv = Encoding.UTF8.GetBytes("8080808080808080"); //("3a20d2b9-6042-4c46-ace2-bc1d4ee555fb");



            var encryptedData = Convert.FromBase64String(cipher);
            var encryptedPass = Convert.FromBase64String(passPhrase);

            var testDecrypt = EncryptionManager.DecryptStringFromBytesAes(encryptedData, keybytes, iv); 

            return Json(testDecrypt, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public JsonResult EnCryptHttpTest(string plainText, string passPhrase)
        {
            var key = "0123456789abcdef";
            var iv = "fedcba9876543210";


            return Json("", JsonRequestBehavior.AllowGet);

        }
        #endregion

        [HttpGet]
        public JsonResult GetCalendarEventsJson()
        {
           
            var eventList = new List<IEnumerable<CalendarEvent>>();

            var calendarList = calservice.CalendarList.List().Execute();

            var evList = GetEventsByDate(DateTime.Today.AddYears(-2), calservice, "ketebwoy@gmail.com");
                           

            string output = JsonConvert.SerializeObject(evList, Formatting.Indented);

            return Json(output, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        [AllowAnonymous]
        // [ValidateAntiForgeryToken]
        public JsonResult InsertCalendarEvent(string calendarId, string title, string location, string description, DateTime startdate, DateTime enddate, string attendeesObject)
        {
            
            var newEvent = CreateNewEvent(calservice, calendarId, title, location, "session", startdate, enddate, attendeesObject);

            string output = JsonConvert.SerializeObject(newEvent, Formatting.Indented);

            return Json(output, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        // [ValidateAntiForgeryToken]
        public bool DeleteCalendarEvent(string calid, string eventId)
        {
            try
            {
                DeleteEvent(calservice, calid, eventId);
                return true;
            }
            catch
            {
                return false;
            }
        }

        [HttpPost]
        [AllowAnonymous]
        // [ValidateAntiForgeryToken]
        public bool UpdateCalendarEvent()
        {
            return false;
        }

        private static IAuthorizationState GetAuthentication(NativeApplicationClient arg)
        {
            // Get the auth URL:
            //IAuthorizationState state = new AuthorizationState(new[] { CalendarService.Scopes.Calendar.ToString() });
            IAuthorizationState state = new AuthorizationState(new[] { "https://www.google.com/calendar/feeds" });
            state.Callback = new Uri(NativeApplicationClient.OutOfBandCallbackUrl);
            Uri authUri = arg.RequestUserAuthorization(state);

            // Request authorization from the user (by opening a browser window):
            Process.Start(authUri.ToString());
            Console.Write("  Authorization Code: ");
            string authCode = Console.ReadLine();

            // Retrieve the access token by using the authorization code:
            return arg.ProcessUserAuthorization(authCode, state);
        }

        private static CalendarService AuthorizeCalAccess()
        {
        X509Certificate2 certificate = new X509Certificate2(SERVICE_ACCOUNT_PKCS12_FILE_PATH, "notasecret", X509KeyStorageFlags.Exportable);


            ServiceAccountCredential credential = new ServiceAccountCredential(
                   new ServiceAccountCredential.Initializer(SERVICE_ACCOUNT_EMAIL)
                   {
                       User = "ketebwoy@gmail.com",
                       Scopes = new[] { CalendarService.Scope.Calendar }

                   }.FromCertificate(certificate));

            // Create the service.
            return new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Keith Drummond",
            });


        }

       
        // Function to get events by date
        private static IEnumerable<CalendarEvent> GetEventsByDate(DateTime startDate, CalendarService calservice, string calendarId)
        {
            List<CalendarEvent> calendarEvents = new List<CalendarEvent>();
            try
            {
                EventsResource.ListRequest req = calservice.Events.List(calendarId);
                req.TimeMin = Convert.ToDateTime(startDate);
                req.TimeMax = Convert.ToDateTime(startDate).AddYears(3);
                req.SingleEvents = true;
                req.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;
                Events events = req.Execute();

                foreach (Event eventdata in events.Items)
                {
                    CalendarEvent item = new CalendarEvent
                    {
                        Id = eventdata.Id,
                        Title = eventdata.Summary,
                        Description = eventdata.Description,
                        Location = eventdata.Location,
                        IsBusy = (eventdata.Transparency != "transparent") ? true : false
                    };
                    EventDateTime start = eventdata.Start;
                    item.StartDate = Convert.ToString(start.DateTime);
                    item.StartTime = Convert.ToDateTime(start.DateTime).ToString("HH:mm");
                    EventDateTime end = eventdata.End;
                    item.EndDate = Convert.ToString(end.DateTime);
                    item.EndTime = Convert.ToDateTime(end.DateTime).ToString("HH:mm");

                    calendarEvents.Add(item);
                }
            }
            catch (Exception ex)
            {
                //CustomFunctionToLogException(ex);
            }

            return calendarEvents;
        }

        private static Event CreateNewEvent(CalendarService calservice, string calendarId, string title, string location, string description, DateTime startdate, DateTime enddate, string attendees)
        {
            try
            {
                var attendeesJson = JObject.Parse(attendees);
                Event eventdata = new Event();
                eventdata.Summary = title;
                eventdata.Location = location;
                eventdata.Description = description;

                EventDateTime start = new EventDateTime();
                start.DateTime = startdate;
                eventdata.Start = start;
                EventDateTime end = new EventDateTime();
                end.DateTime = enddate;
                eventdata.End = end;

                eventdata.Attendees = new List<EventAttendee>(); 
                foreach (var attendee in attendeesJson)
                {
                    var attendeeObject = JObject.Parse(attendee.Value.ToString());
                    
                    var attender = new EventAttendee();
                    attender.Email = attendeeObject.GetValue("email").ToString();
                    attender.DisplayName = attendeeObject.GetValue("name").ToString();
                    attender.Organizer = false;
                    attender.Resource = false; 
                                        
                    //attender.Email = attendee.Value.ToString(); 
                    eventdata.Attendees.Add(attender);
                }


                eventdata.Transparency = "transparent";

                var insertevent = calservice.Events.Insert(eventdata, calendarId);
                Event createdevent = insertevent.Execute();

                return createdevent;
            }
            catch (Exception ex)
            {
                // CustomFunctionToLogException(ex);
                return new Event(); ;
            }
        }

        private static void DeleteEvent(CalendarService calservice, string calendarId, string eventid)
        {
            calservice.Events.Delete(calendarId, eventid).Execute();
        }

        private static void UpdateEvent(CalendarService calservice, string calendarId, string eventid, Event eventdata)
        {
            calservice.Events.Update(eventdata, calendarId, eventid).Execute();
        }

        #endregion

       


        //
        // GET: /Mobile/

        public ActionResult Index()
        {
            return View();
        }

    }
}
