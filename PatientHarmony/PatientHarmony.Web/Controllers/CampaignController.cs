﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using System.Web.Security;
using PatientHarmony.Data.Managers;
using PatientHarmony.Web.Logging.NLog;
using PatientHarmony.Data.Entities;
using PatientHarmony.Web.Models;
using PatientHarmony.Web.Filters;
using Telerik.Web.Mvc;
using Telerik.Web.Mvc.UI;


namespace PatientHarmony.Web.Controllers
{
    [InitializeSimpleMembership]
    public class CampaignController : Controller
    {
        private DataManager _ctx = DataManager.Instance;
        private NLogLogger logger = new NLogLogger();



        #region Views


        //
        // GET: /Campaign/
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Index()
        {
            int _userId = WebSecurity.CurrentUserId;
            var model = new CampaignIndexModel();
            var list = new List<MyGroupListTable>();
            var templateList = new List<CampaignTemplateListTable>(); 

            model.GroupDropDown = new SelectList(list, "GroupId", "CompanyName");

            //get Groups
            var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);

            //get all procedure templates
             var procedureTemplates = _ctx.Templates.GetAllProcedureTemplateList();

            //Get Campapign Templates 
            var campaignTemplates = _ctx.Templates.GetAllCampaignTemplateList();

            //Admin can get all contacts campaigns, and groups across application
            if (Roles.GetRolesForUser().Contains("Administrator"))
            {
                //get Groups
                model.Groups = _ctx.Groups.GetAllGroups();

                //get Contacts
                model.AllContacts = _ctx.Contacts.GetAllList();

                //get all procedure templates
                model.ProcedureTemplates = procedureTemplates;


                //Get Campaigns 
                foreach (var group in model.Groups)
                {
                    var campaignsFG = _ctx.Campaigns.GetAllList();

                    model.Campaigns.AddRange(campaignsFG.FindAll(p => p.GroupID == group.GroupId));

                    var templatesForGroup = _ctx.Templates.GetAllCampaignTemplateList().FindAll(p => p.GroupID == group.GroupId);
                    list.Add(new MyGroupListTable
                        {
                            GroupId = group.GroupId,
                            CompanyName = group.CompanyName
                        });

                    //Build templates display model
                    foreach (var template in templatesForGroup)
                    {
                        var temp = new CampaignTemplateListTable();
                        temp.CampaignTemplateId = template.CampaignTemplateId;
                        temp.CampaignTemplateMessage = template.CampaignTemplateMessage;
                        temp.CampaignTemplateTitle = template.CampaignTemplateTitle;
                        temp.CreatedDate = template.CreatedDate;
                        temp.GroupID = template.GroupID;
                        temp.GroupTitle = group.CompanyName;
                        temp.LastModifiedDate = template.LastModifiedDate;
                        temp.NumOfProcedures = model.ProcedureTemplates.FindAll(p => p.CampaignTemplateID == template.CampaignTemplateId).Count();
                        model.CampaignTemplates.Add(temp);

                    }
                }

            }

            else if (Roles.GetRolesForUser().Contains("Standard"))
            {
                //Get Groups
                model.Groups = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);

                //get Contacts
                model.AllContacts = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);

                //Get campaigns
                foreach (var group in model.Groups)
                {
                    var campaignsFG = _ctx.Campaigns.GetAllList();
                    model.AllContacts = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);
                    model.Campaigns.AddRange(campaignsFG.FindAll(p => p.GroupID == group.GroupId));
                    var templatesForGroup = _ctx.Templates.GetAllCampaignTemplateList().FindAll(p => p.GroupID == group.GroupId);

                    //Build templates display model
                    foreach (var template in templatesForGroup)
                    {
                        var temp = new CampaignTemplateListTable();
                        temp.CampaignTemplateId = template.CampaignTemplateId;
                        temp.CampaignTemplateMessage = template.CampaignTemplateMessage;
                        temp.CampaignTemplateTitle = template.CampaignTemplateTitle;
                        temp.CreatedDate = template.CreatedDate;
                        temp.GroupID = template.GroupID;
                        temp.GroupTitle = group.CompanyName;
                        temp.LastModifiedDate = template.LastModifiedDate;
                        temp.NumOfProcedures = model.ProcedureTemplates.FindAll(p => p.CampaignTemplateID == template.CampaignTemplateId).Count();
                        model.CampaignTemplates.Add(temp);

                    }


                }
            }

            return View(model);
        }

        //
        // GET: /Group/Manage/5

        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Manage(int id = 0, int campId = 0)
        {
            int _userId = WebSecurity.CurrentUserId;
            var campaign = new Campaign();
            var triageObject = new TriageObject(); 

            var model = new CampaignManageModel();
            model.Campaign = _ctx.Campaigns.GetCampaign(id);

            if (model.Campaign == null && campId > 0)
            {
                model.Campaign = _ctx.Campaigns.GetCampaign(campId);
            }

            if (id == 0)
            {
                return HttpNotFound();
            }
            else
            {
               
                model.Procedures = _ctx.Procedures.GetAllProceduresForCampaign(id);
                model.Patients = _ctx.Campaigns.GetAllContactsForCampaign(id);
                model.CampaignTemplates = _ctx.Templates.GetAllCampaignTemplateList().FindAll(p => p.GroupID == campaign.GroupID);
                var tempProcTemplateList = _ctx.Templates.GetAllProcedureTemplateList(); 
                
                foreach(var cTemplate in model.CampaignTemplates)
                {
                    model.ProcedureTemplates.AddRange(tempProcTemplateList.FindAll(p => p.CampaignTemplateID == cTemplate.CampaignTemplateId));
                }
                
                return View(model);
            }

        }

        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult CampaignPatientControl(int Id = 0)
        {
            int _userId = WebSecurity.CurrentUserId;
            var CamapignViewModel = new CampaignManageModel();
            var campaigns = new List<Campaign>();

            if (Id != 0)
            {
                CamapignViewModel.Campaign = _ctx.Campaigns.GetCampaign(Id);
                CamapignViewModel.Patients = _ctx.ManyToManyRelationShips.GetAllContactsForCampaign(Id);

            }

            if (Roles.GetRolesForUser().Contains("Administrator")) //Admin can get all contacts across application
            {
                var allContacts = _ctx.Contacts.GetAllList();
                CamapignViewModel.AllContacts = allContacts;
            }

            else//standard user can only see contacts from groups they are assigned to
            {
                //get list of groups user belongs to
                var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                var groupList = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);

                //Get Contacts for all those groups
                var contactsForUser = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);

                CamapignViewModel.AllContacts = contactsForUser;
            }

            return View(CamapignViewModel);
        }

        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult CampaignPatientControl(int Id, string submitPatientChanges, FormCollection collection)
        {

            List<int> contactIdList = new List<int>();
            List<string> contactIds = new List<string>();

            if (submitPatientChanges.Equals("Save Patients") && Id != 0)
            {
                if (collection.GetValues("patient[]").Count() > 0)
                {
                    contactIds = collection.GetValues("patient[]").ToList<string>();
                    contactIdList = contactIds.Select(int.Parse).Distinct().ToList();
                }

                _ctx.ManyToManyRelationShips.AddContactstoCampaigns(Id, contactIdList);

                return RedirectToAction("Manage", new { id = Id });
            }
            else
            {
                return RedirectToAction("Manage", new { id = Id });
            }
            
        }

        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult CreateProcedure(int Id = 0)
        {
            int _userId = WebSecurity.CurrentUserId;
            var createProcedureModel = new AddProcedureModel();

            if (Id != 0)
            {
                createProcedureModel.campaign = _ctx.Campaigns.GetCampaign(Id);
                              
            }
            return View(createProcedureModel);
        }

        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult CreateProcedure(int Id, string submitProcedureButton, FormCollection collection)
        {
            if (submitProcedureButton.Equals("Create Procedures") && Id != 0)
            {
                string[] procTitles = null;
                string[] procMessages = null;
                // string[] startDates = null;
                //string[] endDates = null;
                var procList = new List<Procedure>();
                var startDates = new List<DateTime>();
                var endDates = new List<DateTime>();
                var sendTimes = new List<DateTime>();
                string[] compare = new string[10];

                //build a list of procedures
                foreach (var key in collection.AllKeys)
                {
                    if (key.Contains("procedureTitle[]"))
                    {
                        procTitles = collection.GetValues(key);
                    }

                    if (key.Contains("procedureMessage[]"))
                    {
                        procMessages = collection.GetValues(key);
                    }

                    if (key.StartsWith("startDates"))
                    {
                        var date = collection.GetValue(key).RawValue;
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        startDates.Add(z);


                        //   startDates.Add(Convert.ToDateTime(date));
                    }

                    if (key.StartsWith("endDates"))
                    {
                        var date = collection.GetValue(key);
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        endDates.Add(z);

                    }

                    if (key.StartsWith("sendTimes"))
                    {
                        var date = collection.GetValue(key);
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        sendTimes.Add(z);

                    }
                }



                if (procTitles.ToString() != string.Empty || procMessages.ToString() != string.Empty)
                {
                    for (int x = 0; x <= procTitles.Length - 1; x++)
                    {
                        //build procedres
                        var proc = new Procedure();
                        proc.ProcedureTitle = procTitles[x];
                        proc.ProcedureMessage = procMessages[x];
                        proc.CampaignID = Id;
                        proc.SendDateTime = Convert.ToDateTime(sendTimes[x]);
                        proc.StartDate = Convert.ToDateTime(startDates[x]);
                        proc.EndDate = Convert.ToDateTime(endDates[x]);
                        proc.CreatedDate = DateTime.Now;
                        proc.LastModifiedDate = DateTime.Now;


                        //save to List
                        procList.Add(proc);

                    }
                }

                //add procedures 
                foreach (var procedure in procList)
                {
                    procedure.CampaignID = Id;
                    _ctx.Procedures.Add(procedure);
                }


                return RedirectToAction("Manage", new { id = Id });
            }
            else
            {
                return RedirectToAction("Manage", new { id = Id });
            }
        }

        #endregion

       

        #region Procedure Ajax operations

     
        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxEditingProcedure(int id)
        {

            var procedure = _ctx.Procedures.Get(id);

            _ctx.Procedures.Delete(id);

            var model = new CampaignManageModel();

            model.Procedures = _ctx.Procedures.GetAllProceduresForCampaign(procedure.CampaignID);
            return View(new GridModel(model.Procedures));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingProcedure(int id, FormCollection collection)
        {

            var procedure = _ctx.Procedures.Get(id);

            procedure.ProcedureTitle = collection["ProcedureTitle"];
            procedure.ProcedureMessage = collection["ProcedureMessage"];
            procedure.StartDate = Convert.ToDateTime(collection["StartDate"]);
            procedure.EndDate = Convert.ToDateTime(collection["EndDate"]);
            procedure.SendDateTime = Convert.ToDateTime(collection["SendDateTime"]);

            _ctx.Procedures.UpdateProcedure(procedure);

            var model = new CampaignManageModel();
            model.Procedures = _ctx.Procedures.GetAllProceduresForCampaign(procedure.CampaignID);

            return View(new GridModel(model.Procedures));
        }
        #endregion

        #region Campaign Ajax Operation

       
        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _AjaxAllContacts(int id)
        {
            var modelScaffold = new CampaignModel();

            modelScaffold = modelScaffold.BuildCampaignData();
            return View(new GridModel(modelScaffold.AllContacts));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxEditingCampaign(int id)
        {

            var campaign = _ctx.Campaigns.GetCampaign(id);

            _ctx.Campaigns.Delete(id);

            var modelScaffold = new CampaignModel();

            modelScaffold = modelScaffold.BuildCampaignData();

            return View(new GridModel(modelScaffold.Campaigns));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxDeleteCampaignTemplate(int id)
        {

            var campaignTemplate = _ctx.Templates.GetCampaignTemplate(id);

            _ctx.Templates.DeleteCampaignTemplate(id);

            var modelScaffold = new CampaignModel();
            var templateDisplayModel = new List<CampaignTemplateListTable>();
            
            var templateList = _ctx.Templates.GetAllCampaignTemplateList();
            var procedureTemplates = _ctx.Templates.GetAllProcedureTemplateList();
            
            modelScaffold = modelScaffold.BuildCampaignData();

            foreach(var group in modelScaffold.Groups)
            {
                var campaignTemplates = templateList.FindAll(p => p.GroupID == campaignTemplate.GroupID);

                //Build templates display model
                foreach (var template in campaignTemplates)
                {
                    var temp = new CampaignTemplateListTable();
                    temp.CampaignTemplateId = template.CampaignTemplateId;
                    temp.CampaignTemplateMessage = template.CampaignTemplateMessage;
                    temp.CampaignTemplateTitle = template.CampaignTemplateTitle;
                    temp.CreatedDate = template.CreatedDate;
                    temp.GroupID = template.GroupID;
                    temp.GroupTitle = group.CompanyName;
                    temp.LastModifiedDate = template.LastModifiedDate;
                    temp.NumOfProcedures = procedureTemplates.FindAll(p => p.CampaignTemplateID == template.CampaignTemplateId).Count();
                    templateDisplayModel.Add(temp);

                }
               
            }

            return View(new GridModel(templateDisplayModel));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingCampaign(int id, FormCollection collection)
        {

            var campaign = _ctx.Campaigns.GetCampaign(id);

            campaign.CampaignTitle = collection["CampaignTitle"];
            campaign.CampaignMessage = collection["CampaignMessage"];
            campaign.SendDateTime = Convert.ToDateTime(collection["SendDateTime"]);

            _ctx.Campaigns.UpdateCampaign(campaign);

            var modelScaffold = new CampaignModel();

            modelScaffold = modelScaffold.BuildCampaignData();

            return View(new GridModel(modelScaffold.Campaigns));
        }

        #endregion

        #region Procedure Operations

        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult UpdateProcedureDetails(Procedure eProcedure, FormCollection collection)
        {
            List<string> procedureIds = collection.GetValues("ProcedureId").ToList<string>();
            List<int> procedureIdList = procedureIds.Select(int.Parse).Distinct().ToList();

            var procedureId = procedureIdList[0];
            procedureId = eProcedure.ProcedureId;

            var procedure = _ctx.Procedures.Get(eProcedure.ProcedureId);

            procedure.ProcedureTitle = collection["ProcedureTitle"];
            procedure.ProcedureMessage = collection["ProcedureMessage"];
            procedure.StartDate = Convert.ToDateTime(collection["StartDate"]);
            procedure.EndDate = Convert.ToDateTime(collection["EndDate"]);
            procedure.SendDateTime = Convert.ToDateTime(collection["SendDateTime"]);

            _ctx.Procedures.UpdateProcedure(procedure);

            return RedirectToAction("Manage", new { id = procedure.CampaignID });

        }

        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost]
        public ActionResult DeleteConfirmedProcedure(int id)
        {
            var procedure = _ctx.Procedures.Get(id);
            var campId = procedure.CampaignID;

            _ctx.Procedures.Delete(id);


            return RedirectToAction("Manage", new { id = campId });

        }

        #endregion

        #region Triage Operations

        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult UpdateTriageDetails(Triage triage, FormCollection collection)
        {
            _ctx.Triages.Update(triage);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost]
        public ActionResult DeleteConfirmedTriage(int id)
        {

            _ctx.Triages.Remove(id);


            return RedirectToAction("Index");
        }
        #endregion

        #region Campaign Operations

        //
        // POST: /Campaign/Create
        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Create(Campaign campaign, FormCollection collection)
        {
            int _userId = WebSecurity.CurrentUserId;
            campaign.CreatedDate = DateTime.Now;
            campaign.LastModifiedDate = DateTime.Now;

            int cId = _ctx.Campaigns.Add(campaign);

            var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);

            var patients = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);

            var contactList = new List<int>();

            foreach (var patient in patients)
            {
                contactList.Add(patient.ContactId);
            }

            contactList.Add(cId);

            _ctx.ManyToManyRelationShips.AddContactsToGroup(userGroups[0].GroupId, contactList);

            return RedirectToAction("Index", new { id = cId });
        }


        //
        // POST: /Campaign/Edit/5
        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult Edit(Campaign campaign, FormCollection collection)
        {
            campaign.LastModifiedDate = DateTime.Now;

            _ctx.Campaigns.UpdateCampaign(campaign);

            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Standard, Administrator")]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult EditCampaignIndex(int id, FormCollection form)
        {
            List<string> campaignIds = form.GetValues("CampaignId").ToList<string>();
            List<int> campaignIdList = campaignIds.Select(int.Parse).Distinct().ToList();

            var campId = campaignIdList[0];
            var campaign = _ctx.Campaigns.GetCampaign(campId);
            campaign.CampaignTitle = form["CampaignTitle"];
            campaign.CampaignMessage = form["CampaignMessage"];


            _ctx.Campaigns.UpdateCampaign(campaign);


            return RedirectToAction("Index", new { id = id });

        }

        //
        // POST: /Campaign/Delete/5
        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost]
        public ActionResult DeleteCampaignInGroup(int id)
        {
            var campaign = _ctx.Campaigns.GetCampaign(id);

            if (campaign != null)
            {
                _ctx.Campaigns.Delete(id);
            }

            return RedirectToAction("Index");
        }

        #endregion

        #region Create Campaign

        [HttpGet]
        public ActionResult CreateCampaign()
        {
            int _userId = WebSecurity.CurrentUserId;
            var model = new CampaignIndexModel();
            var list = new List<MyGroupListTable>();
                      


            if (Roles.GetRolesForUser().Contains("Administrator")) //Admin can get all contacts across application
            {
                var allContacts = _ctx.Contacts.GetAllList();
               model.Groups = _ctx.Groups.GetAllGroups(); 

                model.AllContacts = allContacts;

            }
            else//standard user can only see contacts from groups they are assigned to
            {
                //get list of groups user belongs to
                var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                var groupList = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);

                //Get Contacts for all those groups
                var contactsForUser = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);

                model.AllContacts = contactsForUser;
                model.Groups = groupList; 
            }

            //Get campaigns
            foreach (var group in model.Groups)
            {
                list.Add(new MyGroupListTable
                {
                    GroupId = group.GroupId,
                    CompanyName = group.CompanyName
                });
            }

            model.GroupDropDown = new SelectList(list, "GroupId", "CompanyName");

            return View(model);
        }

        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost]
        public ActionResult CreateCampaign(string submitCampaignButton, Campaign campaign, FormCollection collection, Triage triage = null)
        {

            int _userId = WebSecurity.CurrentUserId;
            string[] procTitles = null;
            string[] procMessages = null;
            // string[] startDates = null;
            //string[] endDates = null;
            var procList = new List<Procedure>();
            var contactIdList = new List<int>();
            var contactIds = new List<string>();
            var templateFlag = false; 
            var templateTitle = string.Empty; 

            if (submitCampaignButton.Equals("Create Campaign"))
            {
                #region Build Patients

                if (collection.GetValues("patient[]").Count() > 0)
                {
                    contactIds = collection.GetValues("patient[]").ToList<string>();
                    contactIdList = contactIds.Select(int.Parse).Distinct().ToList();
                }

                #endregion

                #region Build Campaign


                campaign.CampaignTitle = collection["NewCampaign.CampaignTitle"];
                campaign.CampaignMessage = collection["NewCampaign.CampaignTitle"];
                campaign.CampaignMessage = collection["NewCampaign.CampaignMessage"];
                string time = collection["SendDateTime"];

                campaign.SendDateTime = Convert.ToDateTime(time); 
                campaign.GroupID = Int32.Parse(collection["Groups"]);

                #endregion

                #region Build Procedures

                var startDates = new List<DateTime>();
                var endDates = new List<DateTime>();
                var sendTimes = new List<DateTime>(); 
                string[] compare = new string[10];

                //build a list of procedures
                foreach (var key in collection.AllKeys)
                {
                    if (key.Contains("procedureTitle[]"))
                    {
                        procTitles = collection.GetValues(key);
                    }

                    if (key.Contains("procedureMessage[]"))
                    {
                        procMessages = collection.GetValues(key);
                    }

                    if (key.StartsWith("startDates"))
                    {
                        var date = collection.GetValue(key).RawValue;
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        startDates.Add(z);


                        //   startDates.Add(Convert.ToDateTime(date));
                    }

                    if (key.StartsWith("endDates"))
                    {
                        var date = collection.GetValue(key);
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        endDates.Add(z);

                    }

                    if (key.StartsWith("sendTimes"))
                    {
                        var date = collection.GetValue(key);
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        sendTimes.Add(z);

                    }
                }



                if (procTitles.ToString() != string.Empty || procMessages.ToString() != string.Empty)
                {
                    for (int x = 0; x <= procTitles.Length - 1; x++)
                    {
                        //build procedres
                        var proc = new Procedure();
                        proc.ProcedureTitle = procTitles[x];
                        proc.ProcedureMessage = procMessages[x];
                        proc.CampaignID = campaign.CampaignId;
                        proc.SendDateTime = Convert.ToDateTime(sendTimes[x]);
                        proc.StartDate = Convert.ToDateTime(startDates[x]);
                        proc.EndDate = Convert.ToDateTime(endDates[x]);
                        proc.CreatedDate = DateTime.Now;
                        proc.LastModifiedDate = DateTime.Now;


                        //save to List
                        procList.Add(proc);

                    }
                }
                #endregion

                #region BuildTemplates
                templateFlag = collection["templateFlag"].Contains("true");
                templateTitle = collection["templateTitle"];
                

                if(templateFlag == true && (templateTitle != string.Empty || templateTitle != ""))
                CreateCampaign(campaign, _userId, contactIdList, procList, templateTitle);
                else
                    CreateCampaign(campaign, _userId, contactIdList, procList, "");

                #endregion

                return RedirectToAction("Index", "Campaign");
            }
            else
            {
                return RedirectToAction("Index", "Campaign");
            }
        }

        [HttpGet]
        public ActionResult CreateTemplateCampaign(int CampaignTemplateId = 0)
        {
            int _userId = WebSecurity.CurrentUserId;
            var model = new CreateTemplateCampaignModel();
            var list = new List<MyGroupListTable>();

            var campaignTemplate = _ctx.Templates.GetCampaignTemplate(CampaignTemplateId);


            if (Roles.GetRolesForUser().Contains("Administrator")) //Admin can get all contacts across application
            {
                
                model.Groups = _ctx.Groups.GetAllGroups();

                model.AllContacts = _ctx.Contacts.GetAllList();

            }
            else//standard user can only see contacts from groups they are assigned to
            {
                //get list of groups user belongs to
                var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);
                var groupList = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);

                //Get Contacts for all those groups
                var contactsForUser = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);

                model.AllContacts = contactsForUser;
                model.Groups = groupList;
            }

            //create campaign values
            model.CampaignTemplate.CampaignTemplateTitle = campaignTemplate.CampaignTemplateTitle;
            model.CampaignTemplate.CampaignTemplateMessage = campaignTemplate.CampaignTemplateMessage;
            model.CampaignTemplate.SendDateTime = campaignTemplate.SendDateTime;

            var procTemplates = _ctx.Templates.GetAllProcedureTemplatesForCampaignTemplate(CampaignTemplateId);

            //Get procedureTemplates
            foreach(var template in procTemplates)
            {
                var tempHolder = new ProcedureTemplateValueModel();
                tempHolder.ProcedureTemplateTitle = template.ProcedureTemplateTitle;
                tempHolder.ProcedureTemplateMessage = template.ProcedureTemplateMessage;
                tempHolder.StartDate = template.StartDate;
                tempHolder.EndDate = template.EndDate;
                tempHolder.SendDateTime = template.SendDateTime;
                model.ProcedureTemplates.Add(tempHolder);
            }

            //build group select list drop down
            foreach (var group in model.Groups)
            {
                list.Add(new MyGroupListTable
                {
                    GroupId = group.GroupId,
                    CompanyName = group.CompanyName
                });
            }

            model.GroupDropDown = new SelectList(list, "GroupId", "CompanyName");

            return View(model);
        }

        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost]
        public ActionResult CreateTemplateCampaign(string submitCampaignButton, Campaign campaign, FormCollection collection)
        {

            int _userId = WebSecurity.CurrentUserId;
            string[] procTitles = null;
            string[] procMessages = null;
            // string[] startDates = null;
            //string[] endDates = null;
            var procList = new List<Procedure>();
            var contactIdList = new List<int>();
            var contactIds = new List<string>();
           

            if (submitCampaignButton.Equals("Create Campaign"))
            {
                #region Build Patients

                if (collection.GetValues("patient[]").Count() > 0)
                {
                    contactIds = collection.GetValues("patient[]").ToList<string>();
                    contactIdList = contactIds.Select(int.Parse).Distinct().ToList();
                }

                #endregion

                #region Build Campaign


                campaign.CampaignTitle = collection["CampaignTemplate.CampaignTemplateTitle"];
                campaign.CampaignMessage = collection["CampaignTemplate.CampaignTemplateMessage"];
                string time = collection["CampaignTemplate.SendDateTime"];

                campaign.SendDateTime = Convert.ToDateTime(time);
                campaign.GroupID = Int32.Parse(collection["Groups"]);

                #endregion

                #region Build Procedures

                var startDates = new List<DateTime>();
                var endDates = new List<DateTime>();
                var sendTimes = new List<DateTime>();
                string[] compare = new string[10];

                //build a list of procedures
                foreach (var key in collection.AllKeys)
                {
                    if (key.Contains("procedureTitle"))
                    {
                        procTitles = collection.GetValues(key);
                    }

                    if (key.Contains("procedureMessage"))
                    {
                        procMessages = collection.GetValues(key);
                    }

                    if (key.StartsWith("startDates"))
                    {
                        var date = collection.GetValue(key).RawValue;
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        startDates.Add(z);


                        //   startDates.Add(Convert.ToDateTime(date));
                    }

                    if (key.StartsWith("endDates"))
                    {
                        var date = collection.GetValue(key);
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        endDates.Add(z);

                    }

                    if (key.StartsWith("sendTimes"))
                    {
                        var date = collection.GetValue(key);
                        var rawValue = collection.GetValues(key)[0];
                        var z = Convert.ToDateTime(rawValue);
                        sendTimes.Add(z);

                    }
                }



                if (procTitles.ToString() != string.Empty || procMessages.ToString() != string.Empty)
                {
                    for (int x = 0; x <= procTitles.Length - 1; x++)
                    {
                        //build procedres
                        var proc = new Procedure();
                        proc.ProcedureTitle = procTitles[x];
                        proc.ProcedureMessage = procMessages[x];
                        proc.CampaignID = campaign.CampaignId;
                        proc.SendDateTime = Convert.ToDateTime(sendTimes[x]);
                        proc.StartDate = Convert.ToDateTime(startDates[x]);
                        proc.EndDate = Convert.ToDateTime(endDates[x]);
                        proc.CreatedDate = DateTime.Now;
                        proc.LastModifiedDate = DateTime.Now;


                        //save to List
                        procList.Add(proc);

                    }
                }
                #endregion

                CreateCampaign(campaign, _userId, contactIdList, procList, "");

                return RedirectToAction("Index", "Campaign");
            }
            else
            {
                return RedirectToAction("Index", "Campaign");
            }
        }

        public void CreateCampaign(Campaign campaign, int userID, List<int> patientIds, List<Procedure> procList = null, string templateTitle = "")
        {

            try
            {
                //add campaign
                int campaignId = _ctx.Campaigns.Add(campaign);
                campaign.CampaignId = campaignId;

                //add contacts to campaign
                _ctx.ManyToManyRelationShips.AddContactstoCampaigns(campaignId, patientIds);

                // var contacts = _ctxFunctions.ManyToManyRelationShips.GetAllContactsForCampaign(campaignId);

                List<Contact> triagePatients = new List<Contact>();

                foreach (var contact in patientIds)
                {
                    var patient = _ctx.Contacts.Get(contact);
                    triagePatients.Add(patient);
                }

                //add procedures 
                foreach (var procedure in procList)
                {
                    procedure.CampaignID = campaignId;
                    _ctx.Procedures.Add(procedure);
                }

                //add templates
                if (templateTitle != "")
                {
                    //create campaign template
                    var tempCampTemplate = new CampaignTemplate();
                    tempCampTemplate.CampaignTemplateTitle = templateTitle;
                    tempCampTemplate.CampaignTemplateMessage = campaign.CampaignMessage;
                    tempCampTemplate.GroupID = campaign.GroupID;
                    tempCampTemplate.SendDateTime = campaign.SendDateTime;
                    _ctx.Templates.AddCampaignTemplate(tempCampTemplate);
                    
                    //make procedures into procedure Templates
                    foreach (var procedure in procList)
                    {
                        var tempProcTemplate = new ProcedureTemplate(); 
                        tempProcTemplate.CampaignTemplateID = tempCampTemplate.CampaignTemplateId;
                        tempProcTemplate.ProcedureTemplateTitle = procedure.ProcedureTitle;
                        tempProcTemplate.ProcedureTemplateMessage = procedure.ProcedureMessage;
                        tempProcTemplate.StartDate = procedure.StartDate;
                        tempProcTemplate.EndDate = procedure.EndDate;
                        tempProcTemplate.SendDateTime = procedure.SendDateTime;
                        
                        
                        _ctx.Templates.AddProcedureTemplate(tempProcTemplate);
                    }
                }
            }

            catch (Exception ex)
            {
                logger.Error("An Error occured creating campaign in index of campaign", ex);
            }

        }


    }
        #endregion

    public class MyGroupListTable
    {
        public int GroupId { get; set; }
        public string CompanyName { get; set; }
    }

   
    public class CampaignModel
    {
        private DataManager _ctx = DataManager.Instance;

        public Campaign Campaign { get; set; }
        public List<Group> Groups { get; set; }
        public List<Contact> AllContacts { get; set; }
        public List<Campaign> Campaigns { get; set; }
        public List<Procedure> Procedures { get; set; }
        public List<Triage> Triages { get; set; }


        public CampaignModel()
        {
            this.Campaign = new Campaign();
            this.Campaigns = new List<Campaign>();
            this.Groups = new List<Group>();
            this.AllContacts = new List<Contact>();
            this.Procedures = new List<Procedure>();
            this.Triages = new List<Triage>();
        }

        public CampaignModel BuildCampaignData()
        {

            int _userId = WebSecurity.CurrentUserId;
            var model = new CampaignModel();

            //get Groups
            var userGroups = _ctx.ManyToManyRelationShips.GetUserGroups(_userId);

            //Admin can get all contacts campaigns, and groups across application
            if (Roles.GetRolesForUser().Contains("Administrator"))
            {

                model.Groups = _ctx.Groups.GetAllGroups();
                model.AllContacts = _ctx.Contacts.GetAllList();
                model.Campaigns = _ctx.Campaigns.GetAllList();
                model.Triages = _ctx.Triages.GetAllTriage();
                model.Procedures = _ctx.Procedures.GetAllProcedureList();
            }

            else if (Roles.GetRolesForUser().Contains("Standard"))
            {

                model.Groups = _ctx.ManyToManyRelationShips.GetGroupsEntitiesForUser(userGroups);
                model.AllContacts = _ctx.ManyToManyRelationShips.GetContactEntitiesForUser(userGroups);


                var campaignsFG = _ctx.Campaigns.GetAllList();
                var triagesFG = _ctx.Triages.GetAllTriage();
                var proceduresFG = _ctx.Procedures.GetAllProcedureList();


                //Get campaigns
                foreach (var group in model.Groups)
                {
                    model.Triages.AddRange(triagesFG.FindAll(p => p.GroupID == group.GroupId));
                    model.Campaigns.AddRange(campaignsFG.FindAll(p => p.GroupID == group.GroupId));
                }

            }


            return model;
        }
    }
}