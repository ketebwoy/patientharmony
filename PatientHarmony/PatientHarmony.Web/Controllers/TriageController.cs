﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PatientHarmony.Data.Managers;
using WebMatrix.WebData;
using PatientHarmony.Data.Entities;
using PatientHarmony.Web.Filters;
using Telerik.Web.Mvc;
using PatientHarmony.Data.Models;

namespace PatientHarmony.Web.Controllers
{

    [HandleError]
    [InitializeSimpleMembership]
    public class TriageController : Controller
    {
        private DataManager _ctx = DataManager.Instance;
        private List<Triage> triageList = new List<Triage>();


        public ActionResult Index(int id = 0, int month = 0)
        {
            if (id == 0 && month == 0)
            {
                id = DateTime.Now.Year;
                month = DateTime.Now.Month;
            }

            return View(_ctx.Triages.GetCalendarData(id, month));
        }

        [HttpPost]
        [Authorize(Roles = "Standard, Administrator")]
        public ActionResult UpdateTriageDetails(Triage triage, FormCollection collection)
        {
           _ctx.Triages.Update(triage);
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Standard, Administrator")]
        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            
            _ctx.Triages.Remove(id);
            

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Ajax()
        {
            return Content("AJAX CONTENT RETURNED");
        }

        [HttpPost]
        public ActionResult GetEvents(string id)
        {
            return Content(_ctx.Triages.GetSimpleJsonDay(DateTime.Parse(id)));
        }

        public ActionResult GetEvents(int id, int m, int y)
        {
            return View("_DaySchedules", _ctx.Triages.Get(new DateTime(y, m, id)));
        }

        #region Triage Ajax operations

        public ActionResult AjaxTriageBinding()
        {
            return View();
        }

        [GridAction]
        public ActionResult _AjaxBindingTriage(DateTime date)
        {

            int _userId = WebSecurity.CurrentUserId;

            int y = date.Year;
            int m = date.Month;
            int d = date.Day;

            var triages = _ctx.Triages.Get(new DateTime(y, m, d));

            return View(new GridModel(triages.TriageList));
        }


        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _SaveAjaxEditingTriage(int id, FormCollection collection, DateTime date)
        {

            var temp = date.AddDays(1);
            var triage = _ctx.Triages.GetTriage(id);

            triage.Title = collection["Title"];
            triage.TriageMessage = collection["TriageMessage"];
            triage.GoodResponseMessage = collection["GoodResponseMessage"];
            triage.BadResponseMessage = collection["BadResponseMessage"];
            //   triage.SendDateTime = collection[""];

            triage.StartDate = Convert.ToDateTime(collection["StartDate"]);
            triage.EndDate = Convert.ToDateTime(collection["EndDate"]);
            triage.SendDateTime = Convert.ToDateTime(collection["SendDateTime"]);
            _ctx.Triages.Update(triage);

            int y = date.Year;
            int m = date.Month;
            int d = date.Day;

            var triages = _ctx.Triages.GetAllTriage().Where(
                    p =>
                    (p.StartDate.Day == date.Day && p.StartDate.Month == date.Month &&
                     p.StartDate.Year == date.Year
                     || p.EndDate.Day == date.Day && p.EndDate.Month == date.Month && p.EndDate.Year == date.Year) ||
                    (p.StartDate < date && p.EndDate > temp)).OrderByDescending(p => p.CreatedDate).ToList();

            var triageobject = new TriageObject();

            var triageObjectList = triageobject.TriageObjectList(triages, _ctx.Contacts.GetAllList()); 

            return View(new GridModel(triageObjectList));
        }

        

        [AcceptVerbs(HttpVerbs.Post)]
        [GridAction]
        public ActionResult _DeleteAjaxEditingTriage(int id, DateTime date)
        {
            var temp = date.AddDays(1);

           _ctx.Triages.Remove(id);

            var tempList = _ctx.Triages.GetAllTriage();

            var triages = tempList.FindAll(p => p.StartDate.Date == date.Date || p.EndDate.Date == date.Date || (date.Ticks >= p.StartDate.Ticks && date.Ticks <= p.EndDate.Ticks));

            return View(new GridModel(triages));
        }

        #endregion
    }
}
